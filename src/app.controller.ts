import { Controller, Get, Inject, Query, Res } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppService } from './app.service';
import { BaseController } from './common/controller/base.controller';
import { FileService } from './common/service/file.service';

@Controller()
export class AppController extends BaseController {
	constructor(
		private readonly appService: AppService,
		private readonly configService: ConfigService,
		@Inject(FileService) private readonly fileService: FileService,
	) {
		super();
	}

	@Get()
	getHello() {
		return this.response({ message: 'Echod' });
		// return this.appService.getHello();
	}

	@Get('business-types')
	getBusinessTypes() {
		return this.response(this.configService.get('businessTypes'));
	}

	@Get('get')
	async getFile(@Query('file') file: string, @Res() res: any) {
		const getFile = await this.fileService.getFile(res, file);
		return this.fileResponse(getFile);
	}
}
