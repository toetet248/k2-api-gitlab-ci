import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { useContainer, ValidationError } from 'class-validator';
import { json } from 'express';
import { pagination } from 'typeorm-pagination';
import { AppModule } from './app.module';
import { ApiResponseInterceptor } from './common/interceptor/api-response.interceptor';
import { ErrorHandlingInterceptor } from './common/interceptor/error-handling.interceptor';
import { tenancyMiddleware } from './modules/tenancy/tenancy.middleware';

const env = require('dotenv').config().parsed;

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	app.enableCors();

	app.use(tenancyMiddleware);

	app.use(json({ limit: '10mb' }));

	// aapp.useGlobalPipes(new ValidationPipe({ transform: true }));
	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
			exceptionFactory: (validationErrors: ValidationError[] = []) => {
				return new BadRequestException(validationErrors);
			},
			validationError: {
				target: false,
				value: false,
			},
			forbidUnknownValues: true,
		}),
	);

	app.useGlobalInterceptors(new ApiResponseInterceptor());

	app.useGlobalInterceptors(new ErrorHandlingInterceptor());

	app.use(pagination);

	useContainer(app.select(AppModule), {
		fallbackOnErrors: true,
	});
	await app.listen(env.APP_PORT);
}
bootstrap();
