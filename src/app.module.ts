import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/config';
import { UserModule } from './modules/central/users/user.module';
import { AuthModule } from './modules/central/auth/auth.module';
import { CommonModule } from './common/common.module';
import { TenantModule } from './modules/central/tenants/tenant.module';
import { TenancyModule } from './modules/tenancy/tenancy.module';
import { StaffModule } from './modules/tenancy/staffs/staff.module';
import { TenantAuthModule } from './modules/tenancy/auth/auth.module';
import businessTypesConfig from './config/business-types.config';
import { UniqueConstraint } from './common/validator/is-unique.validator';
import { ExistConstraint } from './common/validator/is-exist.validator';
import { MailerModule } from '@nestjs-modules/mailer';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { PlanModule } from './modules/central/plans/plan.module';
import { SubscriptionModule } from './modules/central/subscriptions/subscription.module';
import { InvoiceModule } from './modules/central/invoices/invoice.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TokenModule } from './modules/central/tokens/token.module';

@Module({
	imports: [
		ConfigModule.forRoot({
			cache: true,
			load: [configuration, businessTypesConfig],
			isGlobal: true,
		}),
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) =>
				configService.get('database.central'),
			inject: [ConfigService],
		}),
		MailerModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: async (config: ConfigService) => ({
				transport: {
					host: config.get('mail.host'),
					port: config.get('mail.port'),
					secure: true,
					auth: {
						user: config.get('mail.user'),
						pass: config.get('mail.password'),
					},
				},
				defaults: {
					from: config.get('mail.user'),
				},
				template: {
					dir: join(__dirname, './mail-templates'),
					adapter: new HandlebarsAdapter(),
					options: {
						strict: true,
					},
				},
			}),
			inject: [ConfigService],
		}),
		ScheduleModule.forRoot(),
		CommonModule,
		UserModule,
		AuthModule,
		TenantModule,
		TokenModule,
		PlanModule,
		SubscriptionModule,
		InvoiceModule,
		TenancyModule,
		// TenantAuthModule,
	],
	controllers: [AppController],
	providers: [AppService, UniqueConstraint, ExistConstraint],
})
export class AppModule {}
