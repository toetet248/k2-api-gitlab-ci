import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from 'src/common/common.module';
import { PlanModule } from '../plans/plan.module';
import { SubscriptionModule } from '../subscriptions/subscription.module';
import { UserModule } from '../users/user.module';
import { TenantController } from './tenant.controller';
import { TenantRepository } from './tenant.repository';
import { TenantService } from './tenant.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([TenantRepository]),
		ConfigModule,
		CommonModule,
		SubscriptionModule,
		UserModule,
		PlanModule,
	],
	controllers: [TenantController],
	providers: [TenantService],
	exports: [TenantService],
})
export class TenantModule {}
