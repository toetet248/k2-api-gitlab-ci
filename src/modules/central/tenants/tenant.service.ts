import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TenantRepository } from './tenant.repository';
import { CreateTenantDTO } from './dto/create-tenant.dto';
import {
	Brackets,
	getConnectionManager,
	getManager,
	IsNull,
	Not,
} from 'typeorm';
import { BadRequestException } from '@nestjs/common';
import { User } from '../users/user.entity';
import { Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';
import {
	generateId,
	generateNumber,
	todayDate,
} from 'src/common/service/helper.service';
import { getTenantConnection } from 'src/modules/tenancy/tenancy.utils';
import { Staff } from 'src/modules/tenancy/staffs/staff.entity';
import { tenantDatabaseSeeder } from 'src/database/seeds/tenant/tenant-db.seeder';
import { SubscriptionService } from '../subscriptions/subscription.service';
import { UserService } from '../users/user.service';
import { MailService } from 'src/common/service/mail.service';
import moment from 'moment';
import { ValidateTenantDTO } from './dto/validate-tenant.dto';
import { PlanService } from '../plans/plan.service';
import { Subscription } from '../subscriptions/subscription.entity';
import { Tenant } from './tenant.entity';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

@Injectable()
export class TenantService {
	constructor(
		@InjectRepository(TenantRepository)
		public readonly repo: TenantRepository,
		@Inject(PlanService) public planService: PlanService,
		@Inject(UserService) public userService: UserService,
		@Inject(SubscriptionService)
		private subscriptionService: SubscriptionService,
		private configService: ConfigService,
		private mailService: MailService,
	) {}

	public async dropTenantDatabase(tenantName: string) {
		const connectionManager = getConnectionManager();

		let centralConnection;
		const centralDBConfig = this.configService.get('database.central');
		if (connectionManager.has('default')) {
			centralConnection = connectionManager.get('default');
			centralConnection = await Promise.resolve(
				centralConnection.isConnected
					? centralConnection
					: centralConnection.connect(),
			);
		} else {
			centralConnection = await getConnectionManager()
				.create(centralDBConfig as MysqlConnectionOptions)
				.connect();
		}

		const centralQuery = centralConnection.createQueryRunner();

		const tenantDbPrefix = this.configService.get(
			'database.tenantDbPrefix',
		);
		const tenantDb = `${tenantDbPrefix + tenantName}`;

		if (await centralQuery.hasDatabase(tenantDb)) {
			const tenantConnection = await getTenantConnection(tenantDb);

			const tenantQuery = tenantConnection.createQueryRunner();

			await tenantQuery.dropDatabase(
				`${tenantDbPrefix + tenantName}`,
				true,
			);
		}
		console.log(`....drop ${tenantDbPrefix + tenantName} db`);
	}

	public async createTenantDatabase(
		tenantId: string,
		tenantName: string,
		user: User,
		password: string,
	) {
		try {
			// create tenant and run migration
			const dbPrefix = this.configService.get('database.tenantDbPrefix');

			await getManager().query(
				`CREATE SCHEMA ${
					dbPrefix + tenantName
				} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`,
			);

			const connection = await getTenantConnection(
				`${dbPrefix + tenantName}`,
			);

			await connection.runMigrations();

			// insert login in created tenant db
			const query = connection.createQueryBuilder();

			await tenantDatabaseSeeder(tenantId, connection);

			const salt = await bcrypt.genSalt();
			const hashPassword = await bcrypt.hash(password, salt);

			// insert authentication staff in created tenant
			await query
				.insert()
				.into(Staff)
				.values([
					{
						id: generateId(),
						userId: user.id,
						name: user.name,
						email: user.email,
						phone: user.phone,
						nameOnTab: user.name.slice(0, 8),
						password: hashPassword,
						status: 'in_work',
						isSystemAdmin: true,
						isOwner: true,
						loginType: 'email',
					},
				])
				.execute();

			await connection.close();
		} catch (error) {
			console.log(error, '....error');
			throw new BadRequestException('invalid data format');
		}
	}

	public generateTenantName(email: string): string {
		return (
			email
				.replace(/[&\/\\#,+()$~%.'"`^=:;*?<>{}!§±-]/g, '_')
				.substring(0, 3) +
			moment().format('YMMDDHHmmss') +
			generateNumber(3)
		);
	}

	public async createTenant(createTenantDTO: CreateTenantDTO) {
		const { name, email, companyName } = createTenantDTO;
		let user = await this.userService.repo.findOne({
			relations: ['tenant'],
			where: { email, role: 'owner' },
		});

		// create new user and tenant
		if (!user) {
			const code = generateNumber();

			try {
				await this.mailService.sendTenantVerificationCodeMail(
					email,
					code,
					companyName,
				);
			} catch (error) {
				throw new BadRequestException([
					'SMTP mail error. Please, try again.',
				]);
			}

			const tenantId = generateId();
			const userId = generateId();

			let tenantName = this.generateTenantName(email);

			// set unique tenant name
			while ((await this.repo.count({ name: tenantName })) > 0)
				tenantName = this.generateTenantName(email);

			let tenant: Tenant;

			tenant = await this.repo.save({
				id: tenantId,
				name: tenantName,
				customerId:
					moment().format('Y') +
					'-' +
					tenantName.substring(tenantName.length - 13),
				companyName,
			});

			user = await this.userService.repo.save({
				id: userId,
				name,
				email,
				tenantId,
				code,
				role: 'owner',
			});

			return {
				...user,
				tenant,
			};
		}

		// update user if user did not reset password when creating tenant
		if (!user.resetPasswordAt) {
			const code = user.code ?? generateNumber();

			try {
				await this.mailService.sendTenantVerificationCodeMail(
					email,
					code,
					companyName,
				);
			} catch (error) {
				throw new BadRequestException([
					'SMTP mail error. Please, try again.',
				]);
			}

			let tenant: Tenant;
			[tenant, user] = await Promise.all([
				this.repo.save({
					id: user.tenantId,
					name: user.tenant.name,
					companyName,
				}),
				this.userService.repo.save({
					id: user.id,
					name,
					email,
					code,
				}),
			]);

			return {
				...user,
				tenant,
			};
		}

		throw new BadRequestException([
			{
				property: 'email',
				constraints: {
					isDuplicated: 'user does not allow to create multi-tenant',
				},
			},
		]);
	}

	public async validateTenant(validateTenantDTO: ValidateTenantDTO) {
		const { name, email, companyName } = validateTenantDTO;
		let user = await this.userService.repo.findOne({
			relations: ['tenant'],
			where: { email, role: 'owner' },
		});
		if (user) {
			if (user.resetPasswordAt)
				throw new BadRequestException([
					{
						property: 'email',
						constraints: {
							isDuplicated:
								'user does not allow to create multi-tenant',
						},
					},
				]);
		}

		return { success: true };
	}

	async find(tenantName: string) {
		return await this.repo.findOne({
			where: {
				name: tenantName,
			},
		});
	}

	async getGmoId(tenantName: string) {
		const tenant = await this.repo.findOne({
			where: {
				name: tenantName,
			},
		});

		return tenant.gmoId;
	}

	async setGmoId(tenantName: string, gmoId: string) {
		const tenant = await this.repo.findOne({
			where: {
				name: tenantName,
			},
		});

		await this.repo.save({ ...tenant, gmoId });
	}

	async isGmoRegister(tenantName: string) {
		const tenant = await this.repo.findOne({
			where: {
				name: tenantName,
				gmoId: Not(IsNull()),
			},
		});

		return !tenant ? false : true;
	}

	async currentSubscription(tenantName: string) {
		const today = todayDate();
		return await this.subscriptionService.repo
			.createQueryBuilder('subscriptions')
			.leftJoinAndSelect('subscriptions.tenant', 'tenants')
			.where('tenants.name = :tenantName', {
				tenantName,
			})
			.andWhere(
				new Brackets((q) => {
					q.where('subscriptions.endedAt IS NULL').orWhere(
						'subscriptions.endedAt > :today',
						{ today },
					);
				}),
			)
			.orderBy('subscriptions.createdAt', 'ASC')
			.getOne();
	}

	async activeSubscription(tenantName: string) {
		const today = todayDate();

		return await this.subscriptionService.repo
			.createQueryBuilder('subscriptions')
			.leftJoinAndSelect('subscriptions.tenant', 'tenant')
			.leftJoinAndSelect('subscriptions.plan', 'plans')
			.where('tenant.name = :tenantName', {
				tenantName,
			})
			.andWhere(
				new Brackets((q) => {
					q.where('subscriptions.endedAt IS NULL').orWhere(
						'subscriptions.endedAt > :today',
						{ today },
					);
				}),
			)
			.orderBy('subscriptions.createdAt', 'DESC')
			.getOne();
	}

	async currentPlan(activeSubscription: Subscription) {
		if (activeSubscription) return activeSubscription.plan;
		else return await this.planService.getFreePlan();
	}
}
