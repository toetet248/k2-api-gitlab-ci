import { IsNotEmpty, IsEmail } from 'class-validator';

export class ValidateTenantDTO {
	@IsNotEmpty()
	name: string;

	@IsNotEmpty()
	@IsEmail()
	email: string;

	@IsNotEmpty()
	companyName: string;
}
