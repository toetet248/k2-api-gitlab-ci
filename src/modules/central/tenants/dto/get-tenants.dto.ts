import { IsNotEmpty } from 'class-validator';
import { IsEmailOrPhone } from 'src/common/validator/is-email-or-phone.validator';

export class GetTenantsDTO {
	@IsNotEmpty()
	@IsEmailOrPhone()
	emailOrPhone: string;
}
