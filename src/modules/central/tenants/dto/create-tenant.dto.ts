import { IsNotEmpty, IsEmail, MaxLength } from 'class-validator';

export class CreateTenantDTO {
	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	@IsNotEmpty()
	@IsEmail()
	email: string;

	@IsNotEmpty()
	@MaxLength(30)
	companyName: string;
}
