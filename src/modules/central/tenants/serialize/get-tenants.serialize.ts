import { Expose, Transform } from 'class-transformer';

export class GetTenantsSerialize {
	@Expose({ name: 'tenant_id' })
	id: string;

	@Expose({ name: 'tenant_name' })
	name: string;

	@Expose({ name: 'tenant_companyName' })
	companyName: string;

	@Expose({ name: 'users_id' })
	userId: string;

	@Expose({ name: 'users_name' })
	userName: string;

	@Expose({ name: 'users_role' })
	@Transform(({ value }) => value == 'owner')
	isOwner: boolean;

	@Expose()
	isResetPassword: boolean;

	@Expose()
	isOldUserVerify: boolean;
}
