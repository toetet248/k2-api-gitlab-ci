import { Expose, Type } from 'class-transformer';

class Tenant {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	companyName: string;
}

export class CreateTenantSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	email: string;

	@Expose()
	@Type(() => Tenant)
	tenant: Tenant;
}
