import {
	Controller,
	Post,
	Body,
	BadRequestException,
	Headers,
} from '@nestjs/common';
import { TenantService } from './tenant.service';
import { CreateTenantDTO } from './dto/create-tenant.dto';
import { CreateTenantSerialize } from './serialize/create-tenant.serialize';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { GetTenantsDTO } from './dto/get-tenants.dto';
import { GetTenantsSerialize } from './serialize/get-tenants.serialize';
import { ValidateTenantDTO } from './dto/validate-tenant.dto';
import { Brackets } from 'typeorm';

@Controller('tenants')
export class TenantController extends BaseController {
	constructor(private service: TenantService) {
		super();
	}

	@Post('create')
	@Serialize(CreateTenantSerialize)
	public async createTenant(@Body() createTenantDTO: CreateTenantDTO) {
		const tenant = await this.service.createTenant(createTenantDTO);
		return this.response(tenant);
	}

	@Post('validate')
	public async validateTenant(@Body() validateTenantDTO: ValidateTenantDTO) {
		const tenant = await this.service.validateTenant(validateTenantDTO);
		return this.response(tenant);
	}

	@Post('get')
	@Serialize(GetTenantsSerialize)
	public async getTenants(
		@Body() getTenantsDTO: GetTenantsDTO,
		@Headers() headers: any,
	) {
		console.log(8798989);
		let tenants = await this.service.repo
			.createQueryBuilder('tenant')
			.leftJoin('tenant.users', 'users')
			.select([
				'tenant.id',
				'tenant.name',
				'tenant.companyName',
				'users.id',
				'users.name',
				'users.role',
				'users.code',
				'users.resetPasswordAt',
			])
			.where(
				new Brackets((q) => {
					q.where('users.email = :email', {
						email: getTenantsDTO.emailOrPhone,
					}).orWhere('users.phone = :phone', {
						phone: getTenantsDTO.emailOrPhone,
					});
				}),
			)
			.andWhere('users.isActive = :isActive', { isActive: true })
			.orderBy('tenant.createdAt', 'ASC')
			.getRawMany();

		if (!tenants.length)
			throw new BadRequestException(['created tenant does not exist']);

		tenants = tenants.map((tenant) => ({
			...tenant,
			isResetPassword: tenant.users_resetPasswordAt ? true : false,
			isOldUserVerify:
				!tenant.users_resetPasswordAt && !tenant.users_code,
		}));

		return this.response(tenants, {
			title: 'get tenants success',
		});
	}
}
