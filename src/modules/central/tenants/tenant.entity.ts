import { MainEntity } from './../../../common/entity/main.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { User } from '../users/user.entity';
import { Subscription } from '../subscriptions/subscription.entity';
import { Invoice } from '../invoices/invoice.entity';

@Entity({ name: 'tenants' })
export class Tenant extends MainEntity {
	@Column()
	name: string;

	@Column()
	gmoId: string;

	@Column()
	customerId: string;

	@Column()
	isDatabaseExist: boolean;

	@Column()
	companyName: string;

	@Column()
	companyPhone: string;

	@Column()
	companyEmail: string;

	@Column()
	companyAddressPrefecture: string;

	@Column()
	companyAddressCity: string;

	@Column()
	companyAddressBuilding: string;

	@Column()
	companyImage: string;

	@Column()
	companyFax: string;

	@Column()
	companyLat: string;

	@Column()
	companyLng: string;

	@Column()
	companyBusinessTypes: string;

	@OneToMany(() => User, (user) => user.tenant)
	users: User[];

	@OneToMany(() => Subscription, (subscription) => subscription.tenant)
	subscriptions: Subscription[];

	@OneToMany(() => Invoice, (invoice) => invoice.tenant)
	invoices: Invoice[];
}
