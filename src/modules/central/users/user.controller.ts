import { Controller } from '@nestjs/common';
import { UserService } from './user.service';
import { BaseController } from 'src/common/controller/base.controller';

@Controller('users')
export class UserController extends BaseController {
	constructor(private userService: UserService) {
		super();
	}
}
