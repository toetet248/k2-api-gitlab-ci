import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { MainEntity } from './../../../common/entity/main.entity';
import { Tenant } from '../tenants/tenant.entity';
import { Token } from '../tokens/token.entity';

@Entity({ name: 'users' })
export class User extends MainEntity {
	@Column()
	name: string;

	@Column()
	email: string;

	@Column()
	phone: string;

	@Column()
	isActive: boolean;

	@Column()
	tenantId: string;

	@Column()
	role: 'owner' | 'staff';

	@Column()
	code: string;

	@Column()
	resetPasswordAt: Date;

	@ManyToOne(() => Tenant, (tenant) => tenant.users)
	tenant: Tenant;

	@OneToMany(() => Token, (token) => token.user)
	tokens: Token[];
}
