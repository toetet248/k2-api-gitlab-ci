import { getConnectionManager } from 'typeorm';
import * as configuration from './../../config/config';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { centralDatabaseSeeder } from 'src/database/seeds/central/central-db.seeder';

async function runCentralDatabaseSeeder() {
	const config = configuration.default();

	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const connection = await connectionManager
		.create(centralDBConfig as MysqlConnectionOptions)
		.connect();

	await centralDatabaseSeeder(connection);

	process.exit(1);
}

runCentralDatabaseSeeder();

// "central:db:seed": "ts-node dist/modules/central/central-db-seed.cli.js",
// "tenant:db:seed": "ts-node dist/modules/tenancy/tenancy-db-seed.cli.js
