import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TokenRepository } from './token.repository';
import { ConfigService } from '@nestjs/config';
import { User } from '../users/user.entity';
import { generateId } from 'src/common/service/helper.service';
import moment from 'moment-timezone';
import { nanoid } from 'nanoid';
import { JwtService } from '@nestjs/jwt';
import { DEVICE_TYPE_HEADER } from 'src/modules/tenancy/tenancy.middleware';
import { Token } from './token.entity';

@Injectable()
export class TokenService {
	constructor(
		@InjectRepository(TokenRepository)
		public readonly repo: TokenRepository,
		private jwtService: JwtService,
		private configService: ConfigService,
	) {}

	public async createToken(user: User, headers: any, ipAddress: string) {
		const { accessToken, refreshToken, accessTokenExpire } =
			this.generateToken(user);

		const deviceType = headers[DEVICE_TYPE_HEADER];

		const token = await this.repo.findOne({
			where: {
				userId: user.id,
				ipAddress,
				deviceType,
				userAgent: headers['user-agent'],
			},
			select: ['id', 'userId', 'ipAddress', 'deviceType', 'userAgent'],
		});

		const refreshExpire = this.generateRefreshTokenExpire(deviceType);
		console.log(accessTokenExpire, '...access expire subtract 5 minutes');

		console.log(refreshExpire, '...refresh expire');
		if (token) {
			await this.repo.save({
				...token,
				refreshToken,
				refreshExpire,
			});
		} else {
			await this.repo.save({
				id: generateId(),
				refreshToken,
				refreshExpire,
				userId: user.id,
				ipAddress,
				deviceType,
				userAgent: headers['user-agent'],
			});
		}

		return { accessToken, refreshToken, accessTokenExpire };
	}

	public generateToken(user: User) {
		const { timezone } = this.configService.get('app');
		let { accessTokenExpire } = this.configService.get('jwt');

		const [expireValue, expireUnit] = accessTokenExpire.split('-');

		const accessToken = this.jwtService.sign({
			id: user.id,
			email: user.email,
			phone: user.phone,
		});

		accessTokenExpire = moment()
			.tz(timezone)
			.add(parseInt(expireValue), expireUnit)
			// .subtract(5, 'minutes')
			.format('YYYY-MM-DD HH:mm:ss');

		const refreshToken = nanoid(50);

		return { accessToken, refreshToken, accessTokenExpire };
	}

	public generateRefreshTokenExpire(deviceType: string) {
		const { pcRefreshTokenExpire, mobileRefreshTokenExpire } =
			this.configService.get('jwt');
		const { timezone } = this.configService.get('app');

		const [expireValue, expireUnit] =
			deviceType == 'mobile'
				? mobileRefreshTokenExpire.split('-')
				: pcRefreshTokenExpire.split('-');

		return moment()
			.tz(timezone)
			.add(parseInt(expireValue), expireUnit)
			.format('YYYY-MM-DD HH:mm:ss');
	}

	public async saveToken(user: User, deviceType: string, token: Token) {
		const { accessToken, refreshToken, accessTokenExpire } =
			this.generateToken(user);
		console.log('....save token function');
		const refreshExpire = this.generateRefreshTokenExpire(deviceType);
		console.log(refreshExpire, '....refreshExpire save token function');

		const { timezone } = this.configService.get('app');

		await this.repo.save({
			...token,
			refreshToken,
			refreshExpire,
			oldRefreshToken: token.refreshToken,
			oldRefreshTokenExpire: moment()
				.tz(timezone)
				.add(1, 'minute')
				.format('YYYY-MM-DD HH:mm:ss'),
		});

		return { accessToken, refreshToken, accessTokenExpire };
	}
}
