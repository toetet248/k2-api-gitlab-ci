import { MainEntity } from '../../../common/entity/main.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from '../users/user.entity';

@Entity({ name: 'tokens' })
export class Token extends MainEntity {
	@Column()
	refreshToken: string;

	@Column()
	refreshExpire: Date;

	@Column()
	oldRefreshToken: string;

	@Column()
	oldRefreshTokenExpire: Date;

	@Column()
	userAgent: string;

	@Column()
	deviceType: string;

	@Column()
	ipAddress: string;

	@Column()
	userId: string;

	@ManyToOne(() => User, (user) => user.tokens)
	user: User;
}
