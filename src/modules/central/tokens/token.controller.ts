import { Controller } from '@nestjs/common';
import { BaseController } from 'src/common/controller/base.controller';
import { TokenService } from './token.service';

@Controller('tokens')
export class TokenController extends BaseController {
	constructor(private service: TokenService) {
		super();
	}
}
