import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenController } from './token.controller';
import { TokenRepository } from './token.repository';
import { TokenService } from './token.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([TokenRepository]),
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) =>
				configService.get('jwt.encode'),
			inject: [ConfigService],
		}),
	],
	controllers: [TokenController],
	providers: [TokenService],
	exports: [TokenService],
})
export class TokenModule {}
