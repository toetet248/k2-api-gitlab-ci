import { getConnectionManager } from 'typeorm';
import { map } from 'modern-async';
import * as configuration from '../../config/config';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

async function runCentralDatabaseFresh() {
	const config = configuration.default();

	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const connection = await connectionManager
		.create(centralDBConfig as MysqlConnectionOptions)
		.connect();

	// drop tables
	const query = connection.createQueryRunner();

	let table = await query.getTable('tenants');
	if (table) {
		const tenants = await connection.query('SELECT name FROM tenants');

		// run migration for all tenant db
		const tenantDBConfig = config.database.tenant;

		await map(tenants, async (tenant: any) => {
			const db = `${config.database.tenantDbPrefix + tenant.name}`;
			if (await query.hasDatabase(db)) {
				const tenantConnectionConfig = {
					...(tenantDBConfig as MysqlConnectionOptions),
					name: db,
					database: db,
				};

				const tenantConnection = await connectionManager
					.create(tenantConnectionConfig)
					.connect();

				const tenantQuery = tenantConnection.createQueryRunner();

				await tenantQuery.dropDatabase(
					`${config.database.tenantDbPrefix + tenant.name}`,
					true,
				);
			}
		});
	}

	table = await query.getTable('invoices');
	if (table) await query.dropTable('invoices');

	table = await query.getTable('subscriptions');
	if (table) await query.dropTable('subscriptions');

	table = await query.getTable('plans');
	if (table) await query.dropTable('plans');

	table = await query.getTable('tokens');
	if (table) await query.dropTable('tokens');

	table = await query.getTable('users');
	if (table) await query.dropTable('users');

	table = await query.getTable('tenants');
	if (table) await query.dropTable('tenants');

	table = await query.getTable('migrations');
	if (table) await query.dropTable('migrations');

	// run migrations
	await connection.runMigrations();

	process.exit(1);
}

runCentralDatabaseFresh();
