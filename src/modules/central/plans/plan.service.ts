import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanRepository } from './plan.repository';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PlanService {
	constructor(
		@InjectRepository(PlanRepository)
		public readonly repo: PlanRepository,
		private configService: ConfigService,
	) {}

	async getPlan(planId: string) {
		return await this.repo.findOne({
			id: planId,
		});
	}

	async getFreePlan() {
		return await this.repo.findOne({
			name: 'free',
		});
	}

	async getStandardPlan() {
		return await this.repo.findOne({
			name: 'standard',
		});
	}
}
