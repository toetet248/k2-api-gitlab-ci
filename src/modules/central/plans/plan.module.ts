import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanController } from './plan.controller';
import { PlanRepository } from './plan.repository';
import { PlanService } from './plan.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([PlanRepository]),
		PassportModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) =>
				configService.get('jwt.encode'),
			inject: [ConfigService],
		}),
		ConfigModule,
	],
	controllers: [PlanController],
	providers: [PlanService],
	exports: [PlanService],
})
export class PlanModule {}
