import { Controller } from '@nestjs/common';
import { BaseController } from 'src/common/controller/base.controller';
import { PlanService } from './plan.service';

@Controller('plans')
export class PlanController extends BaseController {
	constructor(private service: PlanService) {
		super();
	}
}
