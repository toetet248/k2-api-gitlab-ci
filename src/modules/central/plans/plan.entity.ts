import { MainEntity } from './../../../common/entity/main.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { Subscription } from '../subscriptions/subscription.entity';

@Entity({ name: 'plans' })
export class Plan extends MainEntity {
	@Column()
	name: string;

	@Column()
	nameFurigana: string;

	@Column()
	price: number;

	@Column({ type: 'json' })
	options: object;

	@OneToMany(() => Subscription, (subscription) => subscription.plan)
	subscriptions: Subscription[];
}
