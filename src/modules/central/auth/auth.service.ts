import {
	Injectable,
	UnauthorizedException,
	Inject,
	BadRequestException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserService } from '../users/user.service';
import { TenantService } from '../tenants/tenant.service';
import { Brackets, Connection } from 'typeorm';
import { LoginDTO } from './dto/login.dto';
import { Staff } from 'src/modules/tenancy/staffs/staff.entity';
import { getTenantConnection } from 'src/modules/tenancy/tenancy.utils';
import { ConfigService } from '@nestjs/config';
import { VerifyCodeDTO } from './dto/verify-code.dto';
import { ResetPasswordDTO } from './dto/reset-password.dto';
import {
	generateNumber,
	todayDatetime,
} from 'src/common/service/helper.service';
import { MailService } from 'src/common/service/mail.service';
import { User } from '../users/user.entity';
import { TokenService } from '../tokens/token.service';
import {
	DEVICE_TYPE_HEADER,
	REFRESH_TOKEN_HEADER,
} from 'src/modules/tenancy/tenancy.middleware';
import moment from 'moment';
import { UnauthorizedRetiredLoginException } from 'src/common/exception/unauthorized-retired-login.exception';
import { NewPasswordDTO } from './dto/new-password.dto';
import { SmsService } from 'src/common/service/sms.service';
import { UnauthorizedResetPasswordException } from 'src/common/exception/unauthorized-reset-password.exception';

@Injectable()
export class AuthService {
	constructor(
		@Inject(UserService) private userService: UserService,
		@Inject(TenantService) private tenantService: TenantService,
		@Inject(TokenService) private tokenService: TokenService,
		private configService: ConfigService,
		private mailService: MailService,
		private smsService: SmsService,
	) {}

	public async login(
		loginDTO: LoginDTO,
		tenantName: string,
		headers: any,
		ipAddress: string,
	) {
		const { emailOrPhone, password } = loginDTO;

		const user = await this.userService.repo
			.createQueryBuilder('user')
			.leftJoin('user.tenant', 'tenant')
			.select([
				'user.id',
				'user.email',
				'user.phone',
				'user.isActive',
				'user.resetPasswordAt',
			])
			.where('tenant.name = :tenantName', { tenantName })
			.andWhere(
				new Brackets((q) => {
					q.where('user.email = :email', {
						email: emailOrPhone,
					}).orWhere('user.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.getOne();

		if (!user) throw new UnauthorizedException('credentials incorrect');

		if (!user.isActive) throw new UnauthorizedRetiredLoginException();

		if (!user.resetPasswordAt)
			throw new UnauthorizedResetPasswordException();

		const dbPrefix = this.configService.get('database.tenantDbPrefix');

		const connection = await getTenantConnection(dbPrefix + tenantName);

		const staff = await connection
			.getRepository(Staff)
			.createQueryBuilder('staffs')
			.select([
				'staffs.id',
				'staffs.name',
				'staffs.nameOnTab',
				'staffs.nameFurigana',
				'staffs.email',
				'staffs.phone',
				'staffs.loginType',
				'staffs.password',
				'staffs.staffTeamId',
				'staffs.image',
				'staffs.status',
				'staffs.isSiteStaff',
				'staffs.isSiteAdmin',
				'staffs.isSystemAdmin',
				'staffs.isOwner',
				// staffTean
				'staffTeams.id',
				'staffTeams.name',
				'staffTeams.colorCode',
			])
			.where('staffs.userId = :userId', { userId: user.id })
			.andWhere(
				new Brackets((q) => {
					q.where('staffs.status != :status', {
						status: 'retired',
					}).orWhere('staffs.status IS NULL');
				}),
			)
			.leftJoin('staffs.staffTeam', 'staffTeams')
			.getOne();

		if (!staff) throw new UnauthorizedRetiredLoginException();

		if (staff.loginType === 'none') {
			throw new UnauthorizedRetiredLoginException();
		} else if (staff.loginType === 'email') {
			const regEx =
				/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/;

			if (!emailOrPhone.match(regEx)) {
				throw new BadRequestException('invalid-email-login-type');
			}
		} else if (staff.loginType === 'phone') {
			const regEx =
				/^[0][(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3,4}[-.]?[\s\.]?[0-9]{4,6}$/im;

			if (!emailOrPhone.match(regEx)) {
				throw new BadRequestException('invalid-phone-login-type');
			}
		}

		if (!(await bcrypt.compare(password, staff.password)))
			throw new BadRequestException('credentials incorrect');

		const { accessToken, refreshToken, accessTokenExpire } =
			await this.tokenService.createToken(user, headers, ipAddress);

		return {
			staff,
			accessToken,
			refreshToken,
			accessTokenExpire,
		};
	}

	public async getNewToken(headers: any, ipAddress: string) {
		const deviceType = headers[DEVICE_TYPE_HEADER];
		console.log(
			{
				refreshToken: headers[REFRESH_TOKEN_HEADER],
				userAgent: headers['user-agent'],
				deviceType,
				// ipAddress,
			},
			'......input data',
		);
		const token = await this.tokenService.repo.findOne({
			relations: ['user'],
			where: {
				refreshToken: headers[REFRESH_TOKEN_HEADER],
				userAgent: headers['user-agent'],
				deviceType,
				// ipAddress,
			},
			select: [
				'id',
				'userId',
				'ipAddress',
				'deviceType',
				'userAgent',
				'refreshExpire',
				'refreshToken',
			],
		});

		console.log(token, '...token');

		if (!token) {
			const oldToken = await this.tokenService.repo.findOne({
				relations: ['user'],
				where: {
					oldRefreshToken: headers[REFRESH_TOKEN_HEADER],
					userAgent: headers['user-agent'],
					deviceType,
					// ipAddress,
				},
				select: [
					'id',
					'userId',
					'ipAddress',
					'deviceType',
					'userAgent',
					'oldRefreshTokenExpire',
					'refreshToken',
				],
			});

			console.log(oldToken, '...oldToken');

			if (!oldToken) {
				console.log('...oldToken does not exists');

				if (deviceType === 'mobile') {
					throw new BadRequestException(['token not found']);
				} else {
					throw new UnauthorizedException(['token not found']);
				}
			}
			if (!oldToken.user.isActive) {
				console.log('...oldToken does not active');

				if (deviceType === 'mobile') {
					throw new BadRequestException(['token not found']);
				} else {
					throw new UnauthorizedException(['token not found']);
				}
			}
			console.log('...after oldToken does not active');

			if (
				moment().isAfter(
					moment(
						oldToken.oldRefreshTokenExpire,
						'YYYY-MM-DD HH:mm:ss',
					),
				)
			) {
				console.log('...oldToken expire');

				if (deviceType === 'mobile') {
					throw new BadRequestException(['token not found']);
				} else {
					throw new UnauthorizedException(['token not found']);
				}
			}

			return { refreshToken: oldToken.refreshToken };
		}

		console.log('.....token exists');

		// if (!token) throw new UnauthorizedException(['token not found']);
		if (!token.user.isActive) {
			console.log('.....token user not active');

			if (deviceType === 'mobile') {
				throw new BadRequestException(['token not found']);
			} else {
				throw new UnauthorizedException(['token not found']);
			}
		}
		const timezone = this.configService.get('app.timezone');

		if (
			moment()
				.tz(timezone)
				.isAfter(moment(token.refreshExpire, 'YYYY-MM-DD HH:mm:ss'))
		) {
			console.log('.....after refresh expire');

			await this.tokenService.repo.remove(token);

			if (deviceType === 'mobile') {
				throw new BadRequestException(['token expire']);
			} else {
				throw new UnauthorizedException(['token expire']);
			}
		}

		const user = await this.userService.repo.findOne(token.userId, {
			select: ['id', 'email', 'phone'],
		});

		console.log('.....find user');

		const { accessToken, refreshToken, accessTokenExpire } =
			await this.tokenService.saveToken(user, deviceType, token);
		console.log('.....save token');

		return { accessToken, refreshToken, accessTokenExpire };
	}

	public async verifyCode(verifyCodeDTO: VerifyCodeDTO, tenantName: string) {
		const { emailOrPhone, code } = verifyCodeDTO;

		const user = await this.userService.repo
			.createQueryBuilder('user')
			.leftJoinAndSelect('user.tenant', 'tenant')
			.select(['user.code'])
			.where('tenant.name = :tenantName', { tenantName })
			.andWhere('user.code = :code', { code })
			.andWhere(
				new Brackets((q) => {
					q.where('user.email = :email', {
						email: emailOrPhone,
					}).orWhere('user.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.andWhere('user.isActive = :isActive', { isActive: true })
			.getOne();

		return !user ? false : true;
	}

	public async oldUserVerifyCode(
		verifyCodeDTO: VerifyCodeDTO,
		tenantName: string,
	) {
		const { emailOrPhone, code } = verifyCodeDTO;

		const dbPrefix = this.configService.get('database.tenantDbPrefix');
		const connection = await getTenantConnection(dbPrefix + tenantName);

		const staff = await connection
			.getRepository(Staff)
			.createQueryBuilder('staff')
			.select(['staff.password', 'staff.status'])
			.where(
				new Brackets((q) => {
					q.where('staff.email = :email', {
						email: emailOrPhone,
					}).orWhere('staff.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.getOne();

		if (!staff) throw new UnauthorizedException('credentials incorrect');
		if (staff.status == 'retired')
			throw new UnauthorizedRetiredLoginException();

		if (!(await bcrypt.compare(code, staff.password))) return false;

		return true;
	}

	public async newPassword(
		newPasswordDTO: NewPasswordDTO,
		tenantName: string,
	) {
		const { emailOrPhone, code, password } = newPasswordDTO;

		const query = this.userService.repo.createQueryBuilder('user');

		const user = await query
			.leftJoinAndSelect('user.tenant', 'tenant')
			.select([
				'user.id',
				'user.name',
				'user.email',
				'user.phone',
				'user.code',
				'user.isActive',
				'user.tenantId',
				'tenant.companyName',
				'tenant.isDatabaseExist',
			])
			.where('tenant.name = :tenantName', { tenantName })
			.andWhere(
				new Brackets((q) => {
					q.where('user.email = :email', {
						email: emailOrPhone,
					}).orWhere('user.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.andWhere('user.code = :code', { code })
			.getOne();

		if (!user) throw new BadRequestException(['user does not exist']);
		if (!user.isActive) throw new UnauthorizedRetiredLoginException();

		const dbPrefix = this.configService.get('database.tenantDbPrefix');

		let connection: Connection;

		if (!user.tenant.isDatabaseExist) {
			await Promise.all([
				this.tenantService.createTenantDatabase(
					user.tenantId,
					tenantName,
					user,
					password,
				),
				this.tenantService.repo.update(user.tenantId, {
					isDatabaseExist: true,
				}),
				this.userService.repo.update(user.id, {
					code: null,
					resetPasswordAt: todayDatetime(),
				}),
			])
				.then(() => {
					const frontendUrl =
						this.configService.get('app.frontendUrl');
					this.mailService.sendTenantRegisteredSuccessMail(
						user.email,
						user.tenant.companyName,
						frontendUrl,
					);
				})
				.catch((err) => {
					Promise.all([
						this.tenantService.dropTenantDatabase(tenantName),
						this.tenantService.repo.update(user.tenantId, {
							isDatabaseExist: false,
						}),
						this.userService.repo.update(user.id, {
							code: user.code,
							resetPasswordAt: null,
						}),
					]);
					throw new UnauthorizedException(err);
				});
		}
		// reset password
		else {
			connection = await getTenantConnection(dbPrefix + tenantName);

			const salt = await bcrypt.genSalt();
			const hashPassword = await bcrypt.hash(password, salt);

			await Promise.all([
				connection
					.getRepository(Staff)
					.createQueryBuilder('staff')
					.update(Staff)
					.where('userId = :userId', { userId: user.id })
					.set({
						password: hashPassword,
					})
					.execute(),
				this.userService.repo.update(user.id, {
					code: null,
					resetPasswordAt: todayDatetime(),
				}),
			]).catch(() => {
				throw new UnauthorizedException('invalid data format');
			});
		}
	}

	public async oldUserNewPassword(
		newPasswordDTO: NewPasswordDTO,
		tenantName: string,
	) {
		const { emailOrPhone, code, password } = newPasswordDTO;

		const dbPrefix = this.configService.get('database.tenantDbPrefix');

		const connection = await getTenantConnection(dbPrefix + tenantName);

		const staff = await connection
			.getRepository(Staff)
			.createQueryBuilder('staff')
			.select([
				'staff.id',
				'staff.password',
				'staff.status',
				'staff.userId',
			])
			.where(
				new Brackets((q) => {
					q.where('staff.email = :email', {
						email: emailOrPhone,
					}).orWhere('staff.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.getOne();

		if (!staff) throw new UnauthorizedException('credentials incorrect');
		if (staff.status == 'retired')
			throw new UnauthorizedRetiredLoginException();

		if (!(await bcrypt.compare(code, staff.password)))
			throw new UnauthorizedException('credentials incorrect');

		const salt = await bcrypt.genSalt();
		const hashPassword = await bcrypt.hash(password, salt);

		await Promise.all([
			connection
				.getRepository(Staff)
				.createQueryBuilder('staff')
				.update(Staff)
				.where('id = :id', { id: staff.id })
				.set({
					password: hashPassword,
				})
				.execute(),
			this.userService.repo.update(staff.userId, {
				code: null,
				resetPasswordAt: todayDatetime(),
			}),
		]).catch(() => {
			throw new UnauthorizedException('invalid data format');
		});
	}

	public async resetPassword(
		resetPasswordDTO: ResetPasswordDTO,
		tenantName: string,
	) {
		const { emailOrPhone } = resetPasswordDTO;

		const query = this.userService.repo.createQueryBuilder('user');

		const user = await query
			.leftJoinAndSelect('user.tenant', 'tenant')
			.select([
				'user.id',
				'user.email',
				'user.phone',
				'user.isActive',
				'tenant.companyName',
			])
			.where('tenant.name = :tenantName', { tenantName })
			.andWhere(
				new Brackets((q) => {
					q.where('user.email = :email', {
						email: emailOrPhone,
					}).orWhere('user.phone = :phone', {
						phone: emailOrPhone,
					});
				}),
			)
			.getOne();

		if (!user) throw new BadRequestException(['user does not exist']);
		if (!user.isActive) throw new UnauthorizedRetiredLoginException();

		const code = generateNumber();

		// send code to email
		if (emailOrPhone.includes('@')) {
			if (user.email)
				try {
					const frontendUrl =
						this.configService.get('app.frontendUrl');
					this.mailService.sendStaffCodeMail(
						user.email,
						code,
						user.phone,
						user.tenant.companyName,
						frontendUrl,
					);
				} catch (error) {
					throw new BadRequestException([
						'SMTP mail error. Please, try again.',
					]);
				}
		}
		// send code to phone
		else {
			if (user.phone)
				try {
					const frontendUrl =
						this.configService.get('app.frontendUrl');

					this.smsService.sendStaffCodeSms(
						'+81' + user.phone.substring(1),
						user.email,
						code,
						user.tenant.companyName,
						frontendUrl,
					);
				} catch (error) {
					throw new BadRequestException([
						'SMS sending error. Please, try again.',
					]);
				}
		}

		await query
			.update(User)
			.where('id = :userId', { userId: user.id })
			.set({
				code,
			})
			.execute();
	}
}
