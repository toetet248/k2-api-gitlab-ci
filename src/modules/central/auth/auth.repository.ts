import { Repository, EntityRepository } from 'typeorm';
import { User } from '../users/user.entity';

@EntityRepository(User)
export class AuthRepository extends Repository<User> {}
