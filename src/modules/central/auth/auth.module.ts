import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { CentralJwtStrategy } from './strategy/central-jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from '../users/user.module';
import { TenantJwtStrategy } from 'src/modules/tenancy/auth/strategy/tenant-jwt.strategy';
import { CommonModule } from 'src/common/common.module';
import { TenantModule } from '../tenants/tenant.module';
import { TokenModule } from '../tokens/token.module';
import { HttpModule } from '@nestjs/axios';

@Module({
	imports: [
		HttpModule,
		PassportModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) =>
				configService.get('jwt.encode'),
			inject: [ConfigService],
		}),
		UserModule,
		CommonModule,
		TenantModule,
		TokenModule,
	],
	providers: [AuthService, CentralJwtStrategy, TenantJwtStrategy],
	controllers: [AuthController],
	exports: [AuthService],
})
export class AuthModule { }
