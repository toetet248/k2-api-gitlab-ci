import {
	Controller,
	Post,
	Body,
	Headers,
	Ip,
	Patch,
	Inject,
	BadRequestException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { TENANT_HEADER } from 'src/modules/tenancy/tenancy.middleware';
import { LoginDTO } from './dto/login.dto';
import { LoginSerialize } from './serialize/login.serialize';
import { VerifyCodeDTO } from './dto/verify-code.dto';
import { ResetPasswordDTO } from './dto/reset-password.dto';
import { NewPasswordDTO } from './dto/new-password.dto';
import { HttpService } from '@nestjs/axios';
import { Request } from 'express';
import { REQUEST } from '@nestjs/core';

@Controller('auth')
export class AuthController extends BaseController {
	constructor(
		private authService: AuthService,
		@Inject(REQUEST)
		private readonly request: Request,
		private readonly httpService: HttpService,
	) {
		super();
	}

	@Post('login')
	@Serialize(LoginSerialize)
	public async login(
		@Body() loginDTO: LoginDTO,
		@Headers(TENANT_HEADER) tenantName: string,
		@Headers() headers: any,
		@Ip() ipAddress: any,
	) {
		const { staff, accessToken, refreshToken, accessTokenExpire } =
			await this.authService.login(
				loginDTO,
				tenantName,
				headers,
				ipAddress,
			);

		return this.response(
			staff,
			{ title: 'Success!', body: 'tenant login successful.' },
			{ accessToken, refreshToken, accessTokenExpire },
		);
	}

	@Post('token/new')
	public async getNewToken(@Headers() headers: any, @Ip() ipAddress: any) {
		const token = await this.authService.getNewToken(headers, ipAddress);

		return this.response(
			null,
			{ title: 'Success!', body: 'get new token success' },
			token,
		);
	}

	@Post('verify/code')
	public async verifyCode(
		@Body() verifyCodeDTO: VerifyCodeDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		if (verifyCodeDTO.recapToken) {
			const isHuman = await this.validateRecapV3(
				verifyCodeDTO.recapToken,
			);
			if (isHuman === false) {
				throw new BadRequestException({
					error: 'recapToken',
					message: 'Captcha Validation Failed',
				});
			}
		}

		const isVerify = await this.authService.verifyCode(
			verifyCodeDTO,
			tenantName,
		);

		return this.response(
			{ success: isVerify },
			{
				title: isVerify ? 'Success!' : 'Failure!',
				body: isVerify ? 'get verify code success.' : 'wrong code!',
			},
		);
	}

	@Post('verify/code/old-user')
	public async oldUserVerifyCode(
		@Body() verifyCodeDTO: VerifyCodeDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		if (verifyCodeDTO.recapToken) {
			const isHuman = await this.validateRecapV3(
				verifyCodeDTO.recapToken,
			);
			if (isHuman === false) {
				throw new BadRequestException({
					error: 'recapToken',
					message: 'Captcha Validation Failed',
				});
			}
		}

		const isVerify = await this.authService.oldUserVerifyCode(
			verifyCodeDTO,
			tenantName,
		);

		return this.response(
			{ success: isVerify },
			{
				title: isVerify ? 'Success!' : 'Failure!',
				body: isVerify ? 'get verify code success.' : 'wrong code!',
			},
		);
	}

	@Patch('new/password')
	public async newPassword(
		@Body() newPasswordDTO: NewPasswordDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		await this.authService.newPassword(newPasswordDTO, tenantName);

		return this.response(null, {
			title: 'Success!',
			body: 'new password success.',
		});
	}

	@Patch('new/password/old-user')
	public async oldUserNewPassword(
		@Body() newPasswordDTO: NewPasswordDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		await this.authService.oldUserNewPassword(newPasswordDTO, tenantName);

		return this.response(null, {
			title: 'Success!',
			body: 'new password success.',
		});
	}

	@Post('reset/password')
	public async resetPassword(
		@Body() resetPasswordDTO: ResetPasswordDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		if (resetPasswordDTO.requiredToken) {
			const isHuman = await this.validateRecap(
				resetPasswordDTO.recapToken,
			);
			if (isHuman === false) {
				throw new BadRequestException({
					error: 'recapToken',
					message: 'Captcha Validation Failed',
				});
			}
		}
		await this.authService.resetPassword(resetPasswordDTO, tenantName);

		return this.response(null, {
			title: 'Success!',
			body: 'reset password success.',
		});
	}

	private async validateRecap(token: string): Promise<boolean> {
		const secretKey = process.env.GCAP_SECRETE_KEY;
		const url =
			'https://www.google.com/recaptcha/api/siteverify?secret=' +
			secretKey +
			'&response=' +
			token;
		try {
			const { data } = await this.httpService.axiosRef.post(url);
			return data.success;
		} catch (error) {
			console.log(error);
		}
	}

	private async validateRecapV3(token: string): Promise<boolean> {
		const secretKey = process.env.GCAP_SECRETE_KEY_V3;
		const url =
			'https://www.google.com/recaptcha/api/siteverify?secret=' +
			secretKey +
			'&response=' +
			token;
		try {
			const { data } = await this.httpService.axiosRef.post(url);
			console.log(data);
			return data.success;
		} catch (error) {
			console.log(error);
		}
	}
}
