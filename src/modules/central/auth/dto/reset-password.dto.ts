import { IsNotEmpty, IsOptional } from 'class-validator';
import { IsEmailOrPhone } from 'src/common/validator/is-email-or-phone.validator';

export class ResetPasswordDTO {
	@IsNotEmpty()
	@IsEmailOrPhone()
	emailOrPhone: string;

	@IsOptional()
	recapToken: string;

	@IsOptional()
	requiredToken: boolean;
}
