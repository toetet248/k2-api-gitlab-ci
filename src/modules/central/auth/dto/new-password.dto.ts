import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { IsEmailOrPhone } from 'src/common/validator/is-email-or-phone.validator';
import { IsMatch } from 'src/common/validator/is-match.validator';

export class NewPasswordDTO {
	@IsNotEmpty()
	@IsEmailOrPhone()
	emailOrPhone: string;

	@IsNotEmpty()
	@IsString()
	@MaxLength(6)
	@MinLength(6)
	code: string;

	@IsNotEmpty()
	@IsString()
	@MaxLength(200)
	@MinLength(6)
	password: string;

	@IsNotEmpty()
	@IsString()
	@MaxLength(200)
	@MinLength(6)
	@IsMatch('password')
	confirmPassword: string;
}
