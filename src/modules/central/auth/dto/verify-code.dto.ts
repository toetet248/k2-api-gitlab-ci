import { IsString, IsNotEmpty, MaxLength, MinLength, IsOptional } from 'class-validator';
import { IsEmailOrPhone } from 'src/common/validator/is-email-or-phone.validator';

export class VerifyCodeDTO {
	@IsNotEmpty()
	@IsEmailOrPhone()
	emailOrPhone: string;

	@IsNotEmpty()
	@IsString()
	@MaxLength(6)
	@MinLength(6)
	code: string;

	@IsOptional()
	recapToken: string;

	@IsOptional()
	requiredToken: boolean;

}
