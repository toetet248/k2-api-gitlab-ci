import { IsString, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { IsEmailOrPhone } from 'src/common/validator/is-email-or-phone.validator';

export class LoginDTO {
	@IsNotEmpty()
	@IsEmailOrPhone()
	emailOrPhone: string;

	@IsNotEmpty()
	@IsString()
	@MaxLength(200)
	@MinLength(6)
	password: string;
}
