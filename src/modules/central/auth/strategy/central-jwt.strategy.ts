import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import { UserService } from '../../users/user.service';

@Injectable()
export class CentralJwtStrategy extends PassportStrategy(
	Strategy,
	'central-jwt',
) {
	constructor(
		configService: ConfigService,
		@Inject(UserService) private userService: UserService,
	) {
		super(configService.get('jwt.decode'));
	}

	async validate(request: any, payload: any) {
		const user = await this.userService.repo.findOne({
			where: { email: payload.email },
		});

		if (user) {
			request.authUser = user;

			return payload;
		}

		throw new UnauthorizedException();
	}
}
