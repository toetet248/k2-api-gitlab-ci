import { Expose, Type } from 'class-transformer';

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

export class LoginSerialize {
	@Expose()
	id: string;

	@Expose()
	email: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	image: string;

	@Expose()
	nameFurigana: string;

	@Expose()
	status: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: number;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;
}
