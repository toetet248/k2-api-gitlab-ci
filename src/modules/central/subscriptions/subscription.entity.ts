import { MainEntity } from './../../../common/entity/main.entity';
import {
	Column,
	Entity,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
} from 'typeorm';
import { Tenant } from '../tenants/tenant.entity';
import { Plan } from '../plans/plan.entity';

@Entity({ name: 'subscriptions' })
export class Subscription extends MainEntity {
	@Column()
	tenantId: string;

	@Column()
	planId: string;

	@Column()
	recurringId: string;

	@Column()
	status: string;

	@Column()
	startedAt: Date;

	@Column()
	endedAt: Date;

	@ManyToOne(() => Tenant, (tenant) => tenant.subscriptions, {
		primary: true,
	})
	@JoinColumn({ name: 'tenantId' })
	tenant: Tenant;

	@ManyToOne(() => Plan, (plan) => plan.subscriptions, {
		primary: true,
	})
	@JoinColumn({ name: 'planId' })
	plan: Plan;
}
