import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubscriptionRepository } from './subscription.repository';
import { ConfigService } from '@nestjs/config';
import { generateId } from 'src/common/service/helper.service';

@Injectable()
export class SubscriptionService {
	constructor(
		@InjectRepository(SubscriptionRepository)
		public readonly repo: SubscriptionRepository,
		private configService: ConfigService,
	) {}

	async createSubscription(
		tenantId: string,
		recurringId: string,
		planId: string,
	) {
		const createSubscription = this.repo.create({
			id: generateId(),
			tenantId,
			recurringId,
			planId,
		});
		await this.repo.save(createSubscription);
	}
}
