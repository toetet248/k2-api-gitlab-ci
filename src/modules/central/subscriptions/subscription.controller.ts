import { Controller, Header, Headers, Post } from '@nestjs/common';
import { BaseController } from 'src/common/controller/base.controller';
import { TENANT_HEADER } from 'src/modules/tenancy/tenancy.middleware';
import { SubscriptionService } from './subscription.service';

@Controller('subscriptions')
export class SubscriptionController extends BaseController {
	constructor(private service: SubscriptionService) {
		super();
	}
}
