import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from 'src/common/common.module';
import { SubscriptionModule } from '../subscriptions/subscription.module';
import { UserModule } from '../users/user.module';
import { InvoiceController } from './invoice.controller';
import { InvoiceRepository } from './invoice.repository';
import { InvoiceService } from './invoice.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([InvoiceRepository]),
		CommonModule,
		ConfigModule,
		SubscriptionModule,
		UserModule,		
	],
	controllers: [InvoiceController],
	providers: [InvoiceService],
	exports: [InvoiceService],
})
export class InvoiceModule {}
