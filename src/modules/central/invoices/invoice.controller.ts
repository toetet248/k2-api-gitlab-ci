import { Controller, Get, Inject, Post} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import moment from 'moment';
import { BaseController } from 'src/common/controller/base.controller';
import { GmoService } from 'src/common/service/gmo.service';
import { generateId } from 'src/common/service/helper.service';
import { MailService } from 'src/common/service/mail.service';
import { SubscriptionService } from '../subscriptions/subscription.service';
import { UserService } from '../users/user.service';
import { InvoiceService } from './invoice.service';

@Controller('invoices')
export class InvoiceController extends BaseController {
	constructor(
		private service: InvoiceService,
		private gmoService: GmoService,
		private mailService: MailService,
		private configService: ConfigService,
		@Inject(SubscriptionService)
		private readonly subscriptionService: SubscriptionService,
		@Inject(UserService)
		private readonly userService: UserService,		
	) {
		super();
	}

	@Post('create')
	@Cron(CronExpression.EVERY_DAY_AT_10AM, {
		name: 'Create Invoice Cron Job',
	})
	async dispatchCreateInvoices() {
		const date = moment().subtract(1, 'day').format('YYYYMMDD')
		const recurrings = await this.gmoService.searchRecurringResultFile(
			date,
			// moment().subtract(3, 'day').format('YYYYMMDD'),
		);

		// numbers of payments
		let captureCount = 0;
		let failedCount = 0;
		let registCount = 0;
		let invalidCount = 0;

		/**
		 * $cells
		 *  0:ショップID, 1:自動売上ID, 2: オーダーID, 3: 課金日をyyyyMMdd書式で, 4: 取引状態,
		 *	5: 利用金額, 6: 税送料, 7: 次回課金予定日yyyyMMdd書式で,  8: アクセスID, 9: アクセスパスワード,
		 *  10: 仕向先コード, 11: 承認番号, 12: エラーコード, 13: エラー詳細コード, 14: 処理日時をyyyyMMddHHmmss
		 *
		 * 取引状態 >>
		 * INVALID：自動売上不可
		 * （該当会員の削除等により自動売上ができなかった場合など）
		 * CAPTURE：自動売上成功
		 * FAIL：自動売上エラー
		 * REGIST : 自動売上処理中
		 * （自動売上を行ったが、カード会社からエラーが返された場合など）
		 * */

		await Promise.all(recurrings
			.filter((r) => r[0])
			.map(async (recurring) => {
				const recurringId = recurring[1];
				console.log(recurring);
				const subscription =
					await this.subscriptionService.repo.findOne({
						relations: ['plan'],
						where: {
							recurringId,
						},
					});
				console.log(subscription, '....subscription');
				if (subscription) {
					let recentlyCreated = false;
					let invoice = await this.service.repo.findOne({
						where: {
							recurringId,
							processDate: moment(
								recurring[14],
								'YYYYMMDDHHmmss',
							).format('YYYY-MM-DD HH:mm:ss'),
						},
					});
					console.log(invoice, '....invoice');

					if (!invoice) {
						console.log(recurring[14], '....rec14');
						invoice = this.service.repo.create({
							id: generateId(),
							subscriptionId: subscription.id,
							recurringId,
							processDate: moment(
								recurring[14],
								'YYYYMMDDHHmmss',
							).format('YYYY-MM-DD HH:mm:ss'),
							planId: subscription.planId,
							tenantId: subscription.tenantId,
							orderId: recurring[2],
							amount: parseInt(recurring[5]),
							tax: parseInt(recurring[6]),
							status: recurring[4],
						});
						await this.service.repo.save(invoice);
						recentlyCreated = true;
						console.log('.....created');
					}
					console.log(invoice, '....create invoice');

					if (invoice.status == 'FAIL') {
						console.log('....deleteSubscription');

						await this.gmoService.deleteSubscription(recurringId);
						console.log('....success deleteSubscription');

						await this.subscriptionService.repo.update(
							{
								recurringId,
							},
							{
								status: 'suspended',
							},
						);

						console.log('.... suspend');
					}

					let status = '';
					switch (invoice.status) {
						case 'CAPTURE':
							captureCount++;
							status = '成功';
							break;

						case 'FAIL':
							failedCount++;
							status = 'エラー';
							break;

						case 'REGIST':
							registCount++;
							status = '処理中';
							break;

						case 'INVALID':
							invalidCount++;
							status = '不可';
							break;

						default:
							status = '';
							break;
					}

					console.log('.... status', status);
					if (recentlyCreated) {
						const user = await this.userService.repo.findOne({
							relations: ['tenant'],
							where: {
								tenantId: subscription.tenantId,
								role: 'owner',
							},
						});
						console.log('....user');
						if (user) {
							console.log(user, '....get user');

							const nextChargeDate = recurring[7]
								? moment(recurring[7], 'YYYYMMDD').format(
									'YYYY 年 MM 月 DD 日',
								)
								: '';

							const frontendUrl =
								this.configService.get('app.frontendUrl');

							const mailData = {
								planName: subscription.plan.name,
								amount: invoice.amount.toLocaleString('en-US'),
								chargeDate: moment(
									recurring[14],
									'YYYYMMDDHHmmss',
								).format('YYYY 年 MM 月 DD 日'),
								nextChargeDate,
								status,
								orderId: invoice.orderId,
								url: frontendUrl,
								// url: $tenant->id.'.'.env('CENTRAL_DOMAIN',''),
								customerId: user.tenant.customerId,
								fullName: user.name,
							};
							console.log(mailData, '....md');

							this.mailService.sendPaymentNoticeMail(
								user.email,
								mailData,
							);
						}
					}
				}
			}))
		console.log('------------cron job');
		return this.response(
			{
				capture: captureCount,
				failed: failedCount,
				regist: registCount,
				invalid: invalidCount,
			},
			{
				title: 'Success !',
				body: 'Payments are done.',
			},
		);
	}
}
