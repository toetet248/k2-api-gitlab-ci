import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InvoiceRepository } from './invoice.repository';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class InvoiceService {
	constructor(
		@InjectRepository(InvoiceRepository)
		public readonly repo: InvoiceRepository,
		private configService: ConfigService,
	) { }

	async getInvoice(invoiceId: string) {
		return await this.repo.findOne({
			id: invoiceId,
		});
	}

	async getInvoicesByTenantName(tenantName: string) {

		return await this.repo.createQueryBuilder('invoices')
			.innerJoin('invoices.tenant', 'tenant')
			.leftJoinAndMapOne(
				"invoices.plan",
				"plans",
				"plan",	
				"plan.id = invoices.planId",			
			)

			.where('tenant.name = :tenantName', { tenantName })
			.getMany()
		return await this.repo.find({
			relations: ['tenant'],
			where: {
				tenant: {
					name: tenantName,
				},
			},
		});
	}
}
