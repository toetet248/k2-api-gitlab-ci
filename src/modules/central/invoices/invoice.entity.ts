import { MainEntity } from './../../../common/entity/main.entity';
import {
	Column,
	Entity,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
} from 'typeorm';
import { Tenant } from '../tenants/tenant.entity';

@Entity({ name: 'invoices' })
export class Invoice extends MainEntity {
	@Column()
	tenantId: string;

	@Column()
	subscriptionId: string;

	@Column()
	recurringId: string;

	@Column()
	planId: string;

	@Column()
	orderId: string;

	@Column()
	amount: number;

	@Column()
	tax: number;

	@Column()
	processDate: Date;

	@Column()
	status: string;

	@ManyToOne(() => Tenant, (tenant) => tenant.invoices, {
		primary: true,
	})
	@JoinColumn({ name: 'tenantId' })
	tenant: Tenant;
}
