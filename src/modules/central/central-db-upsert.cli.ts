import { getConnectionManager } from 'typeorm';

import * as configuration from '../../config/config';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { Plan } from './plans/plan.entity';
import { generateId } from 'src/common/service/helper.service';
import { map } from 'modern-async';

async function runCentralDatabaseUpsert() {
	const config = configuration.default();

	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const connection = await connectionManager
		.create(centralDBConfig as MysqlConnectionOptions)
		.connect();

	const upsertConfigs = [
		// update
		// {
		// 	entity: Plan,
		// 	existCondition: {
		// 		name: 'standard',
		// 	},
		// 	upsert: {
		// 		name: 'standard',
		// 		price: 8980,
		// 	},
		// },
		// insert
		// {
		// 	entity: Plan,
		// 	existCondition: {
		// 		name: 'light',
		// 	},
		// 	upsert: {
		// 		name: 'light',
		// 		nameFurigana: 'ライト',
		// 		price: 4950,
		// 		options: {
		// 			safeFile: false,
		// 			staffLimit: null,
		// 			vehicleLimit: null,
		// 			siteImagesLimit: 30,
		// 			siteFilesLimit: 30,
		// 		},
		// 	},
		// },
	];

	await map(upsertConfigs, (upsertConfig) =>
		upsertData(connection, upsertConfig),
	);

	process.exit(1);
}

async function upsertData(connection, upsertConfig) {
	const findData = await connection
		.getRepository(upsertConfig.entity)
		.find(upsertConfig.existCondition);

	if (findData.length) {
		const updateData = findData.map((find) => {
			return {
				...find,
				...upsertConfig.upsert,
			};
		});

		await connection.getRepository(upsertConfig.entity).save(updateData);
	} else {
		await connection.getRepository(upsertConfig.entity).save({
			id: generateId(),
			...upsertConfig.upsert,
		});
	}
}

runCentralDatabaseUpsert();

// "central:db:seed": "ts-node dist/modules/central/central-db-seed.cli.js",
// "tenant:db:seed": "ts-node dist/modules/tenancy/tenancy-db-seed.cli.js
