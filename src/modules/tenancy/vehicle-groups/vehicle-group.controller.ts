import {
	BadRequestException,
	Get,
	Patch,
	Query,
	UseGuards,
} from '@nestjs/common';
import { Body, Controller } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { UpdateVehicleGroupDTO } from './dto/update-vehicle-group.dto';
import { GetVehicleGroupSerialize } from './serialize/get-vehicle-group.serialize';
import { UpdateVehicleGroupSerialize } from './serialize/update-vehicle-group.serialize';
import { VehicleGroupService } from './vehicle-group.service';

@Controller('tenants/vehicle-groups')
@UseGuards(AuthGuard('tenant-jwt'))
export class VehicleGroupController extends BaseController {
	constructor(private readonly service: VehicleGroupService) {
		super();
	}

	@Get('/')
	@Serialize(GetVehicleGroupSerialize)
	async getVehicleGroup(@Query('orderBy') orderBy: string) {
		const order = orderBy === 'asc' ? 'ASC' : 'DESC';
		const vehicleGroups = await this.service.repo
			.createQueryBuilder('vehicleGroups')
			.loadRelationCountAndMap(
				'vehicleGroups.memberCount',
				'vehicleGroups.vehicles',
			)
			.orderBy('vehicleGroups.orderNo', order)
			.getMany();
		return this.response(vehicleGroups);
	}

	@Patch('/update')
	@Serialize(UpdateVehicleGroupSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateVehicleGroup(
		@Body() updateVehicleGroupDTO: UpdateVehicleGroupDTO,
	) {
		// save and update vehicle groups
		const data = updateVehicleGroupDTO.vehicleGroups.map(
			(vehicleGroup, index) => {
				if (!vehicleGroup.id) vehicleGroup.id = generateId();

				let group = this.service.repo.create({
					...vehicleGroup,
					orderNo: index + 1,
				});

				return { ...group, memberCount: vehicleGroup.memberCount };
			},
		);
		await this.service.repo.save(data);

		// remove other vehicle groups
		const ids = data.map((d) => d.id);
		this.service.removeOtherVehicleGroupsByIds(ids);

		return this.response(data);
	}
}
