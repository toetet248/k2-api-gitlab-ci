import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { TenantJwtStrategy } from '../auth/strategy/tenant-jwt.strategy';
import { CONNECTION } from '../tenancy.symbols';
import { VehicleGroupController } from './vehicle-group.controller';
import { VehicleGroup } from './vehicle-group.entity';
// import { StaffRepository } from './staff.repository';
import { VehicleGroupService } from './vehicle-group.service';

@Module({
	imports: [TypeOrmModule.forFeature([VehicleGroup]), NestjsFormDataModule],
	controllers: [VehicleGroupController],
	providers: [VehicleGroupService],
})
export class VehicleGroupModule {}
