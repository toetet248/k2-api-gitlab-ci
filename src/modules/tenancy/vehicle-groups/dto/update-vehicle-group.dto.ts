import { Type } from 'class-transformer';
import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsNotEmpty,
	IsNumber,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsUniqueArray } from 'src/common/validator/is-unique-array.validator';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class UpdateVehicleGroupDTO {
	@IsArray()
	// @ArrayNotEmpty()
	// @ArrayUnique((vehicleGroup) => vehicleGroup.name, {
	// 	message: 'group name must be unique',
	// })
	@IsUniqueArray('name')
	@Type(() => VehicleGroup)
	@ValidateNested({ each: true })
	vehicleGroups: VehicleGroup[];
}

class VehicleGroup {
	@IsExist('vehicle_groups', 'id', 'new')
	@IsString()
	id: string;

	@IsNotEmpty()
	// @IsUnique('operations', 'name', 'id')
	name: string;

	@IsNotEmpty()
	colorCode: string;

	@IsNumber()
	memberCount: number = 0;
}
