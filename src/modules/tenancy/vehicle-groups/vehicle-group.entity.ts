import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { Vehicle } from '../vehicles/vehicle.entity';

@Entity({ name: 'vehicle_groups' })
export class VehicleGroup extends MainEntity {
	@Column()
	name: string;

	@Column()
	colorCode: string;

	@Column()
	orderNo: number;

	@OneToMany(() => Vehicle, (vehicle) => vehicle.vehicleGroup)
	vehicles: Vehicle[];
}
