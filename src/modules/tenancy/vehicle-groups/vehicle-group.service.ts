import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { Vehicle } from '../vehicles/vehicle.entity';
import { VehicleGroup } from './vehicle-group.entity';
// import { StaffRepository } from './staff.repository';

@Injectable()
export class VehicleGroupService {
	public readonly repo: Repository<VehicleGroup>;
	public readonly vehicleRepo: Repository<Vehicle>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(VehicleGroup);
		this.vehicleRepo = connection.getRepository(Vehicle);
	}

	async removeOtherVehicleGroupsByIds(ids: string[]) {
		const removeVehicleGroups = await this.repo.find({
			where: {
				id: Not(In(ids)),
			},
		});

		await this.repo.softRemove(removeVehicleGroups);

		await this.vehicleRepo.update(
			{
				vehicleGroupId: Not(In(ids)),
			},
			{
				vehicleGroupId: null,
			},
		);
	}
}
