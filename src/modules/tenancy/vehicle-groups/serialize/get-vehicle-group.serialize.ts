import { Expose } from 'class-transformer';

export class GetVehicleGroupSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	@Expose()
	memberCount: number;

	@Expose()
	orderNo: number;
}
