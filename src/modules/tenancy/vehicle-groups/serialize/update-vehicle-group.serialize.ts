import { Exclude, Expose, Transform } from 'class-transformer';
import Hashids from 'hashids/cjs';

export class UpdateVehicleGroupSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	@Expose()
	memberCount: number;

	@Expose()
	orderNo: number;
}
