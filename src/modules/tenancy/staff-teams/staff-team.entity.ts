import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'staff_teams' })
export class StaffTeam extends MainEntity {
	@Column()
	name: string;

	@Column()
	colorCode: string;

	@Column()
	orderNo: number;

	@OneToMany(() => Staff, (staff) => staff.staffTeam)
	staffs: Staff[];
}
