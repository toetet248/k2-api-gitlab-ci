import {
	BadRequestException,
	Get,
	Patch,
	Query,
	UseGuards,
} from '@nestjs/common';
import { Body, Controller } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { UpdateStaffTeamDTO } from './dto/update-staff-team.dto';
import { GetStaffTeamSerialize } from './serialize/get-staff-team.serialize';
import { UpdateStaffTeamSerialize } from './serialize/update-staff-team.serialize';
import { StaffTeamService } from './staff-team.service';

@Controller('tenants/staff-teams')
@UseGuards(AuthGuard('tenant-jwt'))
export class StaffTeamController extends BaseController {
	constructor(private readonly service: StaffTeamService) {
		super();
	}

	@Get('/')
	@Serialize(GetStaffTeamSerialize)
	async getStaffTeam(@Query('orderBy') orderBy: string) {
		const order = orderBy === 'asc' ? 'ASC' : 'DESC';
		const staffTeams = await this.service.repo
			.createQueryBuilder('staffTeams')
			.loadRelationCountAndMap(
				'staffTeams.memberCount',
				'staffTeams.staffs',
			)
			.orderBy('staffTeams.orderNo', order)
			.getMany();
		return this.response(staffTeams);
	}

	@Patch('/update')
	@Serialize(UpdateStaffTeamSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateStaffTeam(@Body() updateStaffTeamDTO: UpdateStaffTeamDTO) {
		// save and update staff teams
		const data = updateStaffTeamDTO.staffTeams.map((staffTeam, index) => {
			if (!staffTeam.id) staffTeam.id = generateId();

			let team = this.service.repo.create({
				...staffTeam,
				orderNo: index + 1,
			});

			return { ...team, memberCount: staffTeam.memberCount };
		});
		await this.service.repo.save(data);

		// remove other staff teams
		const ids = data.map((d) => d.id);
		this.service.removeStaffTeamsByIds(ids);

		return this.response(data);
	}
}
