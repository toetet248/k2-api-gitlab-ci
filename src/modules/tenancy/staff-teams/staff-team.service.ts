import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { Staff } from '../staffs/staff.entity';
import { CONNECTION } from '../tenancy.symbols';
import { StaffTeam } from './staff-team.entity';

@Injectable()
export class StaffTeamService {
	public readonly repo: Repository<StaffTeam>;
	public readonly staffRepo: Repository<Staff>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(StaffTeam);
		this.staffRepo = connection.getRepository(Staff);
	}

	async removeStaffTeamsByIds(ids: string[]) {
		const removeStaffTeams = await this.repo.find({
			where: {
				id: Not(In(ids)),
			},
		});

		await this.repo.softRemove(removeStaffTeams);

		await this.staffRepo.update(
			{
				staffTeamId: Not(In(ids)),
			},
			{
				staffTeamId: null,
			},
		);
	}
}
