import { Type } from 'class-transformer';
import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsNotEmpty,
	IsNumber,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsUniqueArray } from 'src/common/validator/is-unique-array.validator';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class UpdateStaffTeamDTO {
	@IsArray()
	// @ArrayNotEmpty()
	// @ArrayUnique((staffTeam) => staffTeam.name, {
	// 	message: 'team name must be unique',
	// })
	@IsUniqueArray('name')
	@Type(() => StaffTeam)
	@ValidateNested({ each: true })
	staffTeams: StaffTeam[];
}

class StaffTeam {
	@IsString()
	@IsExist('staff_teams', 'id', 'new')
	id: string;

	@IsNotEmpty()
	// @IsUnique('operations', 'name', 'id')
	name: string;

	@IsNotEmpty()
	colorCode: string;

	@IsNumber()
	memberCount: number = 0;
}
