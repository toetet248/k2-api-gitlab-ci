import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { TenantJwtStrategy } from '../auth/strategy/tenant-jwt.strategy';
import { CONNECTION } from '../tenancy.symbols';
import { StaffTeamController } from './staff-team.controller';
import { StaffTeam } from './staff-team.entity';
// import { StaffRepository } from './staff.repository';
import { StaffTeamService } from './staff-team.service';

@Module({
	imports: [TypeOrmModule.forFeature([StaffTeam]), NestjsFormDataModule],
	controllers: [StaffTeamController],
	providers: [StaffTeamService],
})
export class StaffTeamModule {}
