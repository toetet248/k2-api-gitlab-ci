import { BadRequestException, Global, Module, Scope } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { CONNECTION } from './tenancy.symbols';
import { APP_GUARD, REQUEST } from '@nestjs/core';
import { getTenantConnection } from './tenancy.utils';
import { StaffModule } from './staffs/staff.module';
import { StaffTeamModule } from './staff-teams/staff-team.module';
import { StaffLeaveModule } from './staff-leaves/staff-leave.module';
import { VehicleGroupModule } from './vehicle-groups/vehicle-group.module';
import { VehicleModule } from './vehicles/vehicle.module';
import { CustomerContactModule } from './customer-contacts/customer-contact.module';
import { CustomerModule } from './customers/customer.module';
import { SalesInchargeModule } from './sales-incharges/sales-incharge.module';
import { CompanyModule } from './my-company/company.module';
import { OperationModule } from './operations/operation.module';
import { SiteModule } from './sites/site.module';
import { SiteFileModule } from './site-files/site-file.module';
import { SiteImageModule } from './site-images/site-image.module';
import { SiteAlbumModule } from './site-albums/site-album.module';
import { SiteMemoModule } from './site-memos/site-memo.module';
import { DailyNoteModule } from './daily-notes/daily-note.module';
import { OperationInstructionModule } from './operation-instructions/operation-instruction.module';
import { ScheduleVehicleModule } from './schedule-vehicles/schedule-vehicle.module';
import { ScheduleStaffModule } from './schedule-staffs/schedule-staff.module';
import { SchedulerModule } from './schedulers/scheduler.module';
import { ScheduleSiteModule } from './schedule-sites/schedule-site.module';
import { TenantAuthModule } from './auth/auth.module';
import { CalendarModule } from './calenders/calendar.module';
import { PaymentModule } from './payments/payment.module';
import { ScheduleModule } from '@nestjs/schedule';

const connectionFactory = {
	provide: CONNECTION,
	scope: Scope.REQUEST,
	useFactory: (request: ExpressRequest) => {
		const { tenantName } = request;

		if (tenantName) {
			return getTenantConnection(
				process.env.TENANT_DB_PREFIX + tenantName,
			);
		}

		throw new BadRequestException(['tenant does noe exists']);
	},
	inject: [REQUEST],
};

@Global()
@Module({
	imports: [
		TenantAuthModule,
		StaffModule,
		StaffTeamModule,
		StaffLeaveModule,
		VehicleGroupModule,
		VehicleModule,
		CustomerModule,
		CustomerContactModule,
		SalesInchargeModule,
		CompanyModule,
		OperationModule,
		SiteModule,
		SiteMemoModule,
		SiteFileModule,
		SiteAlbumModule,
		SiteImageModule,
		DailyNoteModule,
		OperationInstructionModule,
		ScheduleStaffModule,
		ScheduleVehicleModule,
		ScheduleSiteModule,
		SchedulerModule,
		CalendarModule,
		PaymentModule,
		// for cron job from invoice.controller
		ScheduleModule.forRoot(),
	],
	providers: [
		connectionFactory,
		// {
		// 	provide: APP_GUARD,
		// 	useClass: AuthGuard('central-jwt'),
		// },
	],
	exports: [CONNECTION],
})
export class TenancyModule {}
