import * as configuration from '../../config/config';
import { getConnectionManager } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

async function runTenantMigration() {
	const config = configuration.default();

	// get tenant names from central
	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const centralConnection = connectionManager.create({
		...(centralDBConfig as MysqlConnectionOptions),
		name: centralDBConfig.database,
		database: centralDBConfig.database,
	});

	const tenants = await (
		await centralConnection.connect()
	).query('SELECT name,isDatabaseExist FROM tenants');

	// run migration for all tenant db
	const tenantDBConfig = config.database.tenant;

	const tenantMigratePromises = tenants.map((tenant: any) =>
		migrateTenant(
			connectionManager,
			config.database.tenantDbPrefix,
			tenantDBConfig,
			tenant,
		),
	);

	await Promise.all(tenantMigratePromises);

	process.exit(1);
}

async function migrateTenant(
	connectionManager: any,
	tenantDbPrefix: any,
	tenantDBConfig: any,
	tenant: any,
) {
	if (tenant.isDatabaseExist) {
		const tenantConnectionConfig = {
			...(tenantDBConfig as MysqlConnectionOptions),
			name: `${tenantDbPrefix + tenant.name}`,
			database: `${tenantDbPrefix + tenant.name}`,
		};

		const tenantConnection = await connectionManager
			.create(tenantConnectionConfig)
			.connect();

		console.log('migrating => ' + tenant.name);

		await tenantConnection.runMigrations();

		await tenantConnection.close();
	}
}

runTenantMigration();
