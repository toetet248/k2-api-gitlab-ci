import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdateSiteImageDescriptionDTO {
	// @IsNotEmpty()
	@IsOptional()
	@IsString()
	description: string;
}
