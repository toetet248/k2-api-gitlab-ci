import { IsNotEmpty, IsOptional, Max, MaxLength } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class CreateSiteDTO {
	@IsNotEmpty()
	@IsExist('customers', 'id')
	customerId: string;

	@IsOptional()
	@IsExist('customer_contacts', 'id', 'new')
	customerContactId: string;

	@IsNotEmpty()
	@IsExist('staffs', 'id')
	staffId: string;

	@IsNotEmpty()
	name: string;

	@IsOptional()
	addressPrefecture: string;

	@IsOptional()
	addressCity: string;

	@IsOptional()
	addressBuilding: string;

	@IsOptional()
	lat: string;

	@IsOptional()
	lng: string;

	@IsOptional()
	@MaxLength(10) // 0 - 9
	price: number;
}
