import { IsOptional, IsString } from 'class-validator';

export class UpdateSiteFileNameDTO {
	@IsOptional()
	@IsString()
	name: string;
}
