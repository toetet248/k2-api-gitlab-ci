import { Type } from 'class-transformer';
import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsInt,
	IsNotEmpty,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsUniqueArray } from 'src/common/validator/is-unique-array.validator';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class UpdateSiteOperationDTO {
	@IsArray()
	@ArrayNotEmpty()
	// @ArrayUnique((Operation) => Operation.name, {
	// 	message: 'operation name must be unique',
	// })
	@IsUniqueArray('name')
	@Type(() => Operation)
	@ValidateNested({ each: true })
	operations: Operation[];
}

class Operation {
	@IsExist('operations', 'id', 'new')
	@IsString()
	id: string;

	@IsNotEmpty()
	// @IsUnique('operations', 'name', 'id')
	name: string;

	@IsNotEmpty()
	colorCode: string;

	// @IsInt()
	// checkOrderNo: number;
}
