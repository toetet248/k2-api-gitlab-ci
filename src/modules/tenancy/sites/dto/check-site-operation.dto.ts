import { ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class CheckSiteOperationDTO {
	@IsExist('operations', 'id', 'new')
	@IsArray()
	@ArrayUnique({
		message: 'operationIds must be unique',
	})
	@IsString({ each: true })
	operationIds: string[];
}
