import { IsNotEmpty, MaxLength } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class CreateSiteMemoDTO {
	// @IsExist('staffs', 'id')
	// @IsNotEmpty()
	// staffId: string;

	@IsNotEmpty()
	@MaxLength(255)
	memo: string;
}
