import { IsDateString, IsOptional } from 'class-validator';

export class GetTodaySitesDTO {
	@IsOptional()
	@IsDateString()
	date: string;
}
