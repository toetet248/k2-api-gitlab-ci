import { IsNotEmpty } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateSiteMemoDTO {
	// @IsExist('staffs', 'id')
	// @IsNotEmpty()
	// staffId: string;

	@IsNotEmpty()
	memo: string;
}
