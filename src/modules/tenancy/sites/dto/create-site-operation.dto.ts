import { Type } from 'class-transformer';
import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsNotEmpty,
	ValidateNested,
} from 'class-validator';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class CreateSiteOperationDTO {
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique((Operation) => Operation.name, {
		message: 'operation name must be unique',
	})
	@Type(() => Operation)
	@ValidateNested({ each: true })
	operations: Operation[];
}

class Operation {
	@IsNotEmpty()
	@IsUnique('operations', 'name')
	name: string;

	@IsNotEmpty()
	colorCode: string;
}
