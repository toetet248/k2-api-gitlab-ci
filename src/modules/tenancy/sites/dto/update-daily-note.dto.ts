import {
	IsMilitaryTime,
	IsNotEmpty,
	IsOptional,
	ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateDailyNoteDTO {
	@IsNotEmpty()
	@Type(() => DailyNote)
	@ValidateNested({ each: true })
	dailyNotes: DailyNote[];
}

class DailyNote {
	@IsNotEmpty()
	@IsExist('daily_notes', 'id')
	id: string;

	@IsNotEmpty()
	actualMan: number;

	@IsNotEmpty()
	actualHour: number;

	@IsNotEmpty()
	actualManPower: number;

	@IsOptional()
	@IsMilitaryTime()
	actualStartTime: string;

	@IsOptional()
	@IsMilitaryTime()
	actualEndTime: string;

	@IsOptional()
	memo: string;
}
