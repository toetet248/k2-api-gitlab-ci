import { ArrayNotEmpty, ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class DeleteSiteFileDTO {
	@IsExist('site_files', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique()
	@IsString({ each: true })
	fileIds: string[];
}
