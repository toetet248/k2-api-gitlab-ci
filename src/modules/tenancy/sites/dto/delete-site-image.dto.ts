import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsNotEmpty,
	IsString,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class DeleteSiteImageDTO {
	@IsExist('site_images', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique()
	@IsString({ each: true })
	imageIds: string[];
}
