import {
	IsArray,
	IsBoolean,
	IsBooleanString,
	IsOptional,
	IsString,
} from 'class-validator';

export class PaginateSiteDTO {
	@IsOptional()
	page: string;

	@IsOptional()
	search: string;

	// @IsOptional()
	// staffIds: string | Array<string>;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	staffIds: string[];

	@IsOptional()
	isUnplanned: any; //未予定 boolean value

	@IsOptional()
	isBeforeConstruction: any; //施工前

	@IsOptional()
	isUnderConstruction: any; //施工中

	@IsOptional()
	isCompleted: any; //施工済

	@IsOptional()
	startDate: string; //施工中

	@IsOptional()
	endDate: string; //施工済
}
