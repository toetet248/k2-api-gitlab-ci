import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class GetSiteDTO {
	@IsString()
	id: string;

	@IsNotEmpty()
	customerId: string;

	@IsNotEmpty()
	customerContactId: string;

	@IsNotEmpty()
	staffId: string;

	@IsNotEmpty()
	name: string;
}
