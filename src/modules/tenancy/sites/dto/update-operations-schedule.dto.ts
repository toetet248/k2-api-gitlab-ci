import { Type } from 'class-transformer';
import {
	IsArray,
	IsDateString,
	IsNotEmpty,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateOperationsScheduleDTO {
	@IsExist('operation_instructions', 'id')
	@IsNotEmpty()
	operationInstructionId: string;

	@IsArray()
	@Type(() => DailyNotes)
	@ValidateNested({ each: true })
	dailyNotes: DailyNotes[];
}

class DailyNotes {
	@IsExist('daily_notes', 'id', 'new')
	@IsString()
	id: string;

	@IsNotEmpty()
	@IsDateString()
	scheduleDate: string;
}
