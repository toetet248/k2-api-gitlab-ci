import {
	Column,
	Entity,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
} from 'typeorm';
import { SiteFile } from '../site-files/site-file.entity';
import { SiteMemo } from '../site-memos/site-memo.entity';
import { SiteAlbum } from '../site-albums/site-album.entity';
import { SiteImage } from '../site-images/site-image.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { Customer } from '../customers/customer.entity';
import { Staff } from '../staffs/staff.entity';
import { OperationInstruction } from '../operation-instructions/operation-instruction.entity';
import { MainEntity } from 'src/common/entity/main.entity';
import { CustomerContact } from '../customer-contacts/customer-contact.entity';
import { todayDate } from 'src/common/service/helper.service';
import { ScheduleSite } from '../schedule-sites/schedule-site.entity';
import { Operation } from '../operations/operation.entity';

export const siteStatusCase = () => {
	const today = todayDate();
	return `CASE
					WHEN (sites.startDate IS NULL AND sites.endDate IS NULL) THEN 1
					WHEN (sites.startDate > '${today}') THEN 2
					WHEN (sites.startDate <= '${today}' AND sites.endDate >= '${today}') THEN 3
					WHEN (sites.endDate < '${today}') THEN 4
					ELSE 1
				END`;
};

@Entity({ name: 'sites' })
export class Site extends MainEntity {
	@Column()
	customerId: string;

	@Column()
	customerContactId: string;

	@Column()
	staffId: string;

	@Column()
	name: string;

	@Column()
	addressPrefecture: string;

	@Column()
	addressCity: string;

	@Column()
	addressBuilding: string;

	@Column()
	lat: string;

	@Column()
	lng: string;

	@Column()
	startDate: Date;

	@Column()
	endDate: Date;

	@Column()
	startTime: string;

	@Column()
	endTime: string;

	@Column()
	price: number;

	@OneToMany(() => SiteFile, (siteFile) => siteFile.site)
	siteFiles: SiteFile[];

	@OneToMany(() => SiteMemo, (siteMemo) => siteMemo.site)
	siteMemos: SiteMemo[];

	@OneToMany(
		() => OperationInstruction,
		(operationInstruction) => operationInstruction.site,
	)
	operationInstructions: OperationInstruction[];

	@OneToMany(() => SiteAlbum, (siteAlbum) => siteAlbum.site)
	siteAlbums: SiteAlbum[];

	@OneToMany(() => SiteImage, (siteImage) => siteImage.site)
	siteImages: SiteImage[];

	@OneToMany(() => DailyNote, (dailyNote) => dailyNote.site)
	dailyNotes: DailyNote[];

	@ManyToOne(() => Staff, (staff) => staff.staffs, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;

	@ManyToOne(() => Customer, (customer) => customer.customers, {
		primary: true,
	})
	@JoinColumn({ name: 'customerId' })
	customer: Customer;

	@ManyToOne(
		() => CustomerContact,
		(customerContact) => customerContact.sites,
		{
			primary: true,
		},
	)
	@JoinColumn({ name: 'customerContactId' })
	customerContact: CustomerContact;

	@OneToMany(() => ScheduleSite, (scheduleSite) => scheduleSite.site)
	scheduleSites: ScheduleSite[];

	@ManyToMany(() => Operation, (operation) => operation.sites)
	@JoinTable({
		name: 'operation_instructions',
		joinColumn: {
			name: 'siteId',
			referencedColumnName: 'id',
		},
		inverseJoinColumn: {
			name: 'operationId',
			referencedColumnName: 'id',
		},
	})
	operations: Operation[];
}
