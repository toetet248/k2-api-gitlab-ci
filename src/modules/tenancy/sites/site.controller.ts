import {
	Controller,
	Post,
	UseGuards,
	Body,
	Patch,
	Param,
	Get,
	Delete,
	BadRequestException,
	Query,
	Inject,
	UploadedFile,
	Headers,
	ForbiddenException,
	NotAcceptableException,
} from '@nestjs/common';
import { BaseController } from 'src/common/controller/base.controller';
import { AuthGuard } from '@nestjs/passport';
import { SiteService } from './site.service';
import { SiteImageService } from '../site-images/site-image.service';
import { SiteFileService } from '../site-files/site-file.service';
import { CreateSiteSerialize } from './serialize/create-site.serialize';
import { UpdateSiteSerialize } from './serialize/update-site.serialize';
import { CreateSiteDTO } from './dto/create-site.dto';
import { UpdateSiteDTO } from './dto/update-site.dto';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import {
	formatDate,
	generateId,
	todayDate,
	validateExists,
	filterImageFile,
	filterFile,
} from 'src/common/service/helper.service';
import { UpdateOperationsScheduleSerialize } from './serialize/update-operations-schedule.serialize';
import { UpdateOperationsScheduleDTO } from './dto/update-operations-schedule.dto';
import { GetOperationsScheduleSerialize } from './serialize/get-operations-schedule.serialize';
import { DailyNoteService } from '../daily-notes/daily-note.service';
import { CreateSiteOperationSerialize } from './serialize/create-site-operation.serialize';
import { CreateSiteOperationDTO } from './dto/create-site-operation.dto';
import { OperationService } from '../operations/operation.service';
import { UpdateSiteOperationDTO } from './dto/update-site-operation.dto';
import { GetSiteOperationSerialize } from './serialize/get-site-operation.serialize';
import { GetSiteSerialize } from './serialize/get-site.serialize';
import { SiteMemoService } from '../site-memos/site-memo.service';
import { CreateSiteMemoSerialize } from './serialize/create-site-memo.serialize';
import { GetSiteMemoSerialize } from './serialize/get-site-memo.serialize';
import { OperationInstructionService } from '../operation-instructions/operation-instruction.service';
import { GetDailyNoteSerialize } from './serialize/get-daily-note.serialize';
import { PaginateSiteSerialize } from './serialize/paginate-site.serialize';
import { CheckSiteOperationSerialize } from './serialize/check-site-operation.serialize';
import { In, Not } from 'typeorm';
import { CreateSiteMemoDTO } from './dto/create-site-memo.dto';
import { UpdateSiteMemoDTO } from './dto/update-site-memo.dto';
import { UpdateDailyNoteSerialize } from './serialize/update-daily-note.serialize';
import { UpdateDailyNoteDTO } from './dto/update-daily-note.dto';
import { PaginateSiteDTO } from './dto/paginate-site.dto';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Storage } from 'src/common/interceptor/storage.interceptor';
import { DeleteSiteFileDTO } from './dto/delete-site-file.dto';
import { DeleteSiteImageDTO } from './dto/delete-site-image.dto';
import { FileService } from 'src/common/service/file.service';
import { ConfigService } from '@nestjs/config';
import { GetSiteImageSerialize } from './serialize/get-site-images.serialize';
import { GetSiteFileSerialize } from './serialize/get-site-files.serialize';
import { UpdateSiteImageDescriptionDTO } from './dto/update-site-image-description.dto';
import { UploadSiteImageSerialize } from './serialize/upload-site-image.serialize';
import { UploadSiteFileSerialize } from './serialize/upload-site-file.serialize';
import { UpdateSiteFileNameDTO } from './dto/update-site-file-name.dto';
import { GetTodaySitesDTO } from './dto/get-today-sites.dto';
import { GetAllSitesTodaySerialize } from './serialize/get-today-sites.serialize';
import { TENANT_HEADER } from '../tenancy.middleware';
import { siteStatusCase } from './site.entity';
import { CheckSiteOperationDTO } from './dto/check-site-operation.dto';
import { AuthStaff } from '../auth/decorator/auth-staff.decorator';
import { Staff } from '../staffs/staff.entity';
import { TenantService } from 'src/modules/central/tenants/tenant.service';
import { UpdateSiteOperationSerialize } from './serialize/update-site-operation.serialize';

@Controller('tenants/sites')
@UseGuards(AuthGuard('tenant-jwt'))
export class SiteController extends BaseController {
	constructor(
		private readonly service: SiteService,
		private readonly dailyNoteService: DailyNoteService,
		private readonly operationService: OperationService,
		private readonly siteMemoService: SiteMemoService,
		private readonly operationInstructionService: OperationInstructionService,
		private readonly siteImageService: SiteImageService,
		private readonly siteFileService: SiteFileService,
		private readonly configService: ConfigService,
		@Inject(FileService) private readonly fileService: FileService,
		@Inject(TenantService) private readonly tenantService: TenantService,
	) {
		super();
	}

	// site paginate
	@Get('/paginate')
	@Serialize(PaginateSiteSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async paginateSite(@Query() paginateSiteDTO: PaginateSiteDTO) {
		const paginate = await this.service.paginateSite(paginateSiteDTO);

		return this.paginateResponse(paginate.items, paginate.meta);
	}

	// get all site memos of today
	@Get('/today')
	@Serialize(GetAllSitesTodaySerialize)
	async getAllSiteMemo(@Query() getTodaySitesDTO: GetTodaySitesDTO) {
		let scheduleDate;
		if (getTodaySitesDTO.date) {
			scheduleDate = getTodaySitesDTO.date;
		} else {
			const today = new Date();
			scheduleDate =
				today.getFullYear() +
				'-' +
				(today.getMonth() + 1) +
				'-' +
				today.getDate();
		}

		const todaySite = await this.service.getTodaySites(scheduleDate);
		return this.response(todaySite, {
			title: 'get all sites of today success',
		});
	}

	//get site and site_memos by schedule_date
	@Get('/:id')
	@Serialize(GetSiteSerialize)
	async findSite(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site not found']);

		const [site, siteStatus, dailyNote] = await Promise.all([
			// site
			this.service.repo
				.createQueryBuilder('sites')
				.where({ id })
				.select([
					'sites',
					'customer.id',
					'customer.name',
					'customerContact.id',
					'customerContact.name',
					'customerContact.phone',
					'customerContact.email',
					'staff.id',
					'staff.name',
					'staff.nameOnTab',
					'staff.phone',
					'staff.email',
				])
				.leftJoin('sites.customer', 'customer')
				.leftJoin('sites.customerContact', 'customerContact')
				.leftJoin('sites.staff', 'staff')
				.getOne(),
			this.service.repo
				.createQueryBuilder('sites')
				.addSelect(siteStatusCase(), 'siteStatusIndex')
				.where({ id })
				.getRawOne(),
			// dailyNote
			this.dailyNoteService.repo
				.createQueryBuilder('daily_notes')
				.select([
					'SUM(daily_notes.reserveMan) AS reserveMan',
					'SUM(daily_notes.reserveHour) AS reserveHour',
					'ROUND(SUM(daily_notes.reserveManPower),"2")AS reserveManPower',
					'SUM(daily_notes.actualMan) AS totalActualMan',
					'SUM(daily_notes.actualHour) AS totalActualHour',
					'ROUND(SUM(daily_notes.actualManPower),"2")AS totalActualManPower',
				])
				.where('daily_notes.siteId = :id', { id })
				.getRawMany(),
			// // site memos
			// this.siteMemoService.repo
			// 	.createQueryBuilder('site_memos')
			// 	.select([
			// 		'site_memos',
			// 		'DATE_FORMAT(site_memos.scheduleDate, "%Y-%m-%d") AS scheduleDate',
			// 	])
			// 	.where('site_memos.siteId = :siteId', { siteId: id })
			// 	.getRawMany(),
		]);

		// const siteMemos = [];

		// for (var i = 0; i < siteMemosData.length; i++) {
		// 	var added = false;
		// 	for (var j = 0; j < siteMemos.length; j++) {
		// 		if (
		// 			siteMemos[j]['scheduleDate'] ==
		// 			siteMemosData[i]['scheduleDate']
		// 		) {
		// 			siteMemos[j].memos.push(siteMemosData[i]);
		// 			added = true;
		// 			break;
		// 		}
		// 	}
		// 	if (!added) {
		// 		var entry = { memos: [] };
		// 		entry['scheduleDate'] = siteMemosData[i]['scheduleDate'];
		// 		entry.memos.push(siteMemosData[i]);
		// 		siteMemos.push(entry);
		// 	}
		// }
		const res = {
			...site,
			siteStatusIndex: siteStatus.siteStatusIndex,
			...dailyNote[0],
			// siteMemos,
		};
		return this.response(res);
	}

	//site create
	@Post('/create')
	@Serialize(CreateSiteSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async createSite(@Body() createSiteDTO: CreateSiteDTO) {
		const site = await this.service.createSite(createSiteDTO);

		return this.response(site, { title: 'site create success' });
	}

	//site update
	@Patch('/:id/update')
	@Serialize(UpdateSiteSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateSite(
		@Body() updateSiteDTO: UpdateSiteDTO,
		@Param('id') id: string,
	) {
		const site = await this.service.updateSite(id, updateSiteDTO);
		return this.response(site, { title: 'site update success' });
	}

	//create site_memo
	@Post('/:id/site-memos/create')
	@Serialize(CreateSiteMemoSerialize)
	@InjectRequestToBody()
	async createSiteMemo(
		@Body() createSiteMemoDTO: CreateSiteMemoDTO,
		@Param('id') siteId: string,
		@AuthStaff() authStaff: Staff,
	) {
		const siteMemo = await this.siteMemoService.createSiteMemo(
			siteId,
			createSiteMemoDTO,
			authStaff.id,
		);

		return this.response(siteMemo, {
			title: 'site memo create success',
		});
	}

	@Get('/:id/site-memos')
	@Serialize(GetSiteMemoSerialize)
	async getSiteMemo(@Param('id') siteId: string) {
		const siteMemo = await this.siteMemoService.getSiteMemobySiteId(siteId);

		return this.response(siteMemo, {
			title: 'get site memo success',
		});
	}

	// update site_memos
	@Patch('/:id/site-memos/:siteMemoId/update')
	@InjectRequestToBody()
	async updateSiteMemo(
		@Body() updateSiteMemoDTO: UpdateSiteMemoDTO,
		@Param('siteMemoId') siteMemoId: string,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteMemoService.updateSiteMemo(
			siteMemoId,
			updateSiteMemoDTO,
			authStaff,
		);
		return this.response(null, {
			title: 'site memo update success',
		});
	}

	// remove site_memos
	@Delete('/:id/site-memos/:siteMemoId/delete')
	async deleteSiteMemo(
		@Param('siteMemoId') siteMemoId: string,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteMemoService.deleteSiteMemo(siteMemoId, authStaff);
		return this.response(null, {
			title: 'site memo delete success',
		});
	}

	//  get daily_notes
	@Get('/:id/daily-notes')
	@Serialize(GetDailyNoteSerialize)
	async getDailyNotes(@Param('id') siteId: string) {
		if (!(await validateExists(this.service.repo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		const dailyNotes = await this.dailyNoteService.repo
			.createQueryBuilder('dailyNotes')
			.select([
				// dailyNotes
				'dailyNotes.id',
				'dailyNotes.siteId',
				'dailyNotes.operationId',
				'dailyNotes.operationInstructionId',
				'dailyNotes.scheduleDate',
				'dailyNotes.reserveMan',
				'dailyNotes.reserveHour',
				'dailyNotes.reserveManPower',
				'dailyNotes.reserveStartTime',
				'dailyNotes.reserveEndTime',
				'dailyNotes.actualMan',
				'dailyNotes.actualHour',
				'dailyNotes.actualManPower',
				'dailyNotes.actualStartTime',
				'dailyNotes.actualEndTime',
				'dailyNotes.memo',
				// author
				'author.id',
				'author.name',
				'author.image',
				// scheduleStaffs
				'scheduleStaffs.id',
				'scheduleStaffs.staffId',
				'scheduleStaffs.dayNightSupport',
				'scheduleStaffs.nightSupportCount',
				'scheduleStaffs.orderNo',
				// staff
				'staff.id',
				'staff.name',
				'staff.nameOnTab',
				'staff.image',
				'staff.status',
				// staffTeam
				'staffTeam.id',
				'staffTeam.name',
				'staffTeam.colorCode',
				// staffLeaves
				'staffLeaves.id',
				'staffLeaves.date',
				// scheduleVehicles
				'scheduleVehicles.id',
				'scheduleVehicles.vehicleId',
				'scheduleVehicles.dayNightSupport',
				'scheduleVehicles.nightSupportCount',
				'scheduleVehicles.orderNo',
				// vehicle
				'vehicle.id',
				'vehicle.nameOnTab',
				'vehicle.image',
				'vehicle.status',
				'vehicle.licenseEndDate',
				'vehicle.insuranceEndDate',
				// vehicleGroup
				'vehicleGroup.id',
				'vehicleGroup.name',
				'vehicleGroup.colorCode',
				// operation
				'operation.id',
				'operation.name',
				'operation.colorCode',
			])
			.leftJoin('dailyNotes.staff', 'author')
			.leftJoin(
				'dailyNotes.scheduleStaffs',
				'scheduleStaffs',
				'scheduleStaffs.deletedAt IS NULL',
			)
			.leftJoin(
				'scheduleStaffs.staff',
				'staff',
				'staff.deletedAt IS NULL',
			)
			.withDeleted()
			.leftJoin('staff.staffTeam', 'staffTeam')
			.leftJoin('staff.staffLeaves', 'staffLeaves')
			.leftJoin(
				'dailyNotes.scheduleVehicles',
				'scheduleVehicles',
				'scheduleVehicles.deletedAt IS NULL',
			)
			.leftJoin(
				'scheduleVehicles.vehicle',
				'vehicle',
				'vehicle.deletedAt IS NULL',
			)
			.leftJoin('vehicle.vehicleGroup', 'vehicleGroup')
			.leftJoin(
				'dailyNotes.operation',
				'operation',
				'operation.deletedAt IS NULL',
			)
			.where('dailyNotes.siteId = :siteId', { siteId })
			.andWhere('dailyNotes.deletedAt IS NULL')
			.andWhere('dailyNotes.scheduleDate <= :start_at', {
				start_at: new Date(),
			})
			.orderBy('dailyNotes.scheduleDate', 'DESC')
			.getMany();

		// console.log(dailyNotes[0]);
		const data = [];
		dailyNotes.forEach((dailyNote) => {
			const scheduleDate = formatDate(dailyNote.scheduleDate);

			const dataIndex = data.findIndex(
				(d) => d.scheduleDate == scheduleDate,
			);

			if (dataIndex >= 0) {
				data[dataIndex].dailyNotes.push(dailyNote);
			} else {
				data.push({
					scheduleDate,
					dailyNotes: [dailyNote],
				});
			}
		});

		return this.response(data, {
			title: 'get daily notes success',
		});
	}

	//update daily note
	@Patch('/update/daily-notes')
	@Serialize(UpdateDailyNoteSerialize)
	@InjectRequestToBody()
	async updateDailyNotes(
		@Body() updateDailyNoteDTO: UpdateDailyNoteDTO,
		@AuthStaff() authStaff: Staff,
	) {
		await this.service.updateDailyNotes(updateDailyNoteDTO, authStaff);

		return this.response(null, {
			title: 'daily note update success',
		});
	}

	//create and update operations schedule
	@Patch('/:id/operations-schedule/update')
	@Serialize(UpdateOperationsScheduleSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOperationsSchedule(
		@Param('id') id: string,
		@Body() updateOperationsScheduleDTO: UpdateOperationsScheduleDTO,
	) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site not found']);

		const data = await this.service.updateOperationsSchedule(
			id,
			updateOperationsScheduleDTO,
		);

		return this.response(data, {
			title: 'Operations Schedule update success',
		});
	}

	//get operation schedule data
	@Get('/:id/operations-schedule/daily-notes')
	@Serialize(GetOperationsScheduleSerialize)
	async getOperationsSchedule(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site not found']);

		const data = await this.operationInstructionService.repo
			.createQueryBuilder('operation_instructions')
			.leftJoinAndSelect('operation_instructions.operation', 'operation')
			.leftJoinAndSelect(
				'operation_instructions.dailyNotes',
				'dailyNotes',
			)
			.select([
				'operation_instructions.id',
				'operation_instructions.orderNo',
				'operation.id',
				'operation.name',
				'operation.colorCode',
				'operation_instructions',
				'dailyNotes.id',
				'dailyNotes.operationId',
				'dailyNotes.siteId',
				'dailyNotes.operationInstructionId',
				'dailyNotes.scheduleDate',
			])
			.where('operation_instructions.siteId = :siteId', { siteId: id })
			// .orWhere('dailyNotes.siteId = :siteId', { sitedId: id })
			.orderBy('operation_instructions.orderNo', 'ASC')
			.getMany();

		return this.response(data, {
			title: 'get Operation Schedule info success ',
		});
	}

	//create site operation
	@Post('/:id/operations/create')
	@InjectRequestToBody()
	@Serialize(CreateSiteOperationSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async createSiteOperations(
		@Param('id') id: string,
		@Body() createSiteOperationDTO: CreateSiteOperationDTO,
	) {
		const orderNo = await (
			await this.operationService.repo
				.createQueryBuilder('operations')
				.select('MAX(operations.orderNo)', 'max')
				.getRawOne()
		).max;

		// save site operation data
		const siteOperationData = createSiteOperationDTO.operations.map(
			(op, index) => {
				const requestData = {
					name: op.name,
					colorCode: op.colorCode,
				};

				return this.operationService.repo.create({
					...requestData,
					id: generateId(),
					orderNo: orderNo + (index + 1),
				});
			},
		);

		await this.operationService.repo.save(siteOperationData);

		return this.response(siteOperationData, {
			title: 'create operations success',
		});
	}

	//update site operation
	// @Patch('/:id/operations/update')
	// @InjectRequestToBody()
	// @Serialize(CheckSiteOperationSerialize)
	// async createSiteOperations(
	// 	@Param('id') id: string,
	// 	@Body() updateSiteOperationDTO: UpdateSiteOperationDTO,
	// ): Promise<any> {
	// 	// show error if site id does not exists
	// 	if (!(await validateExists(this.service.repo, ['id', id])))
	// 		throw new BadRequestException(['site not found']);

	// 	let checkIds = [];

	// 	const operationIds = updateSiteOperationDTO.operations.map(
	// 		(operation, index) => {
	// 			if (!operation.id) {
	// 				operation.id = generateId();
	// 			}

	// 			if (operation.checkOrderNo > 0) {
	// 				checkIds.push(operation.id);
	// 			}

	// 			return this.operationService.repo.create(operation);
	// 		},
	// 	);

	// 	await this.operationService.repo.save(operationIds);
	// 	// show error if operation ids do not exists

	// 	// remove other operations
	// 	const ids = operationIds.map((d) => d.id);

	// 	// check deleted operation is used by other sites
	// 	const otherOperationInstructions =
	// 		await this.operationInstructionService.repo.count({
	// 			where: {
	// 				siteId: Not(id),
	// 				operationId: Not(In(ids)),
	// 			},
	// 		});
	// 	// show error if deleted operation ids are used by other sites
	// 	if (otherOperationInstructions)
	// 		throw new BadRequestException([
	// 			'deleted operationId is used by other sites',
	// 		]);

	// 	// update site operation data
	// 	const updatedOperationInstructions =
	// 		await this.operationInstructionService.updateOperationInstruction(
	// 			id,
	// 			checkIds,
	// 			updateSiteOperationDTO,
	// 		);

	// 	this.operationService.removeOtherOperationsByIds(ids);

	// 	return this.response(updatedOperationInstructions, {
	// 		title: 'update site operation success',
	// 	});
	// }

	//check site operation
	@Patch('/:id/operations/check')
	@InjectRequestToBody()
	@Serialize(CheckSiteOperationSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async checkeSiteOperations(
		@Param('id') id: string,
		@Body() checkSiteOperationDTO: CheckSiteOperationDTO,
	): Promise<any> {
		// show error if site id does not exists
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site not found']);

		const { operationIds } = checkSiteOperationDTO;

		// update site operation data
		const updatedOperationInstructions =
			await this.operationInstructionService.updateOperationInstruction(
				id,
				operationIds,
			);

		return this.response(updatedOperationInstructions, {
			title: 'update site operation success',
		});
	}

	//update site operation
	@Patch('/:id/operations/update')
	@InjectRequestToBody()
	@Serialize(UpdateSiteOperationSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async updateSiteOperations(
		@Param('id') id: string,
		@Body() updateSiteOperationDTO: UpdateSiteOperationDTO,
	): Promise<any> {
		// show error if site id does not exists
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site not found']);

		const operationIds = updateSiteOperationDTO.operations
			.map((operation) => operation.id)
			.filter((op) => op);

		// check deleted operations are used
		const otherOperationInstructions =
			await this.operationInstructionService.repo.count({
				where: {
					operationId: Not(In(operationIds)),
				},
			});

		// show error if deleted operation ids are used
		if (otherOperationInstructions)
			throw new BadRequestException(['deleted operationId is used']);

		// delete operations that are not used
		await this.operationService.repo.softDelete({
			id: Not(In(operationIds)),
		});

		const updateSiteOperations = updateSiteOperationDTO.operations.map(
			(operation, index) => {
				if (!operation.id) operation.id = generateId();
				return this.operationService.repo.create({
					...operation,
					orderNo: index + 1,
				});
			},
		);
		await this.operationService.repo.save(updateSiteOperations);

		const siteOperations = await this.service.getSiteOperations(id);

		return this.response(siteOperations, {
			title: 'update site operations success',
		});
	}

	//get site operation
	@Get('/:id/operations')
	@Serialize(GetSiteOperationSerialize)
	// @UseGuards(AuthGuard('site-admin'))
	async getSiteOperations(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site does not exists']);

		const siteOperations = await this.service.getSiteOperations(id);

		console.log(siteOperations, '...site operations');

		return this.response(siteOperations);
	}

	/**
	 * Check if there are schedules and uploaded files
	 * @param id site id
	 * @returns
	 */
	@Post('/:id/check-site')
	@UseGuards(AuthGuard('site-admin'))
	async checkSite(
		@Param('id') id: string,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site does not exists']);

		const site = await this.service.repo
			.createQueryBuilder('sites')
			.innerJoinAndSelect('sites.dailyNotes', 'dn')
			.select('COUNT(DISTINCT(dn.id)) as noteCount')
			.where('sites.id = :id', { id })
			.getRawOne();

		const zip = await this.service.getZipFile(id, tenantName);
		return {
			data: {
				hasScheduled: site.noteCount > 0,
				hasFiles: zip !== null,
				filePath: zip,
			},
		};
	}

	//delete site
	@Delete('/:id')
	@UseGuards(AuthGuard('site-admin'))
	async deleteSite(@Param('id') id: string) {
		const today = todayDate();
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['site does not exists']);

		const site = await this.service.repo.findOne({
			where: { id: id },
		});

		// if (formatDate(site.endDate) < today) {
		// 	throw new BadRequestException(['site in use cannot be deleted']);
		// }
		this.service.repo.softDelete(site);

		return this.response(null, { title: 'site deleted success' });
	}

	@Get('/:id/zip')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async getZipFile(
		@Param('id') id: string,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const zip = await this.service.getZipFile(id, tenantName);

		return this.response(
			{ zip },
			{
				title: 'get zip file success',
			},
		);
	}

	//upload site images
	@Post('/:id/images/upload')
	@Storage({
		fieldName: 'image',
		path: '/sites/images',
		filter: filterImageFile,
		sku: 'STI',
		limits: { fileSize: 10000000 },
	})
	@Serialize(UploadSiteImageSerialize)
	async uploadSiteImage(
		@Param('id') siteId: string,
		@UploadedFile() siteImage: Express.Multer.File,
		@AuthStaff() authStaff: Staff,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		// start - check plan and unauthorized
		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);
		const currentPlan = await this.tenantService.currentPlan(
			activeSubscription,
		);

		if (currentPlan.name == 'free')
			throw new NotAcceptableException(['not-accepted-for-free-plan']);
		// end - check plan and unauthorized

		const size = siteImage.size;

		if (!(await validateExists(this.service.repo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		if (!siteImage)
			throw new BadRequestException(['image must not be empty']);

		let resizeImage = await this.fileService.resizeImage(siteImage);

		let data = await this.siteImageService.uploadSiteImage(
			siteId,
			size,
			resizeImage,
			authStaff.id,
		);

		return this.response(data, { title: 'upload site image success' });
	}

	// delete site images
	@Delete('images/delete')
	@InjectRequestToBody()
	async deleteSiteImage(
		@Body() deleteSiteImageDTO: DeleteSiteImageDTO,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteImageService.deleteSiteImages(
			deleteSiteImageDTO,
			authStaff,
		);

		return this.response(null, {
			title: 'delete site images success',
		});
	}

	@Get('/:id/images')
	@Serialize(GetSiteImageSerialize)
	async getSiteImages(@Param('id') siteId: string) {
		if (!(await validateExists(this.service.repo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		const array = await this.siteImageService.repo
			.createQueryBuilder('site_images')
			.leftJoinAndSelect('site_images.site', 'sites')
			.leftJoinAndSelect('site_images.staff', 'staffs')
			.leftJoin('staffs.staffTeam', 'staffTeam')
			.select([
				'site_images',
				'DATE_FORMAT(site_images.createdAt, "%Y-%m-%d") AS createdDate',
				'sites.name',
				'staffs.id',
				'staffs.name',
				'staffs.image',
				'staffTeam.colorCode',
			])
			.where('site_images.siteId = :siteId', { siteId })
			.orderBy('createdDate', 'DESC')
			.getRawMany();

		const data = [];

		for (let i = 0; i < array.length; i++) {
			let added = false;
			for (let j = 0; j < data.length; j++) {
				if (data[j]['createdDate'] == array[i]['createdDate']) {
					data[j].images.push(array[i]);
					added = true;
					break;
				}
			}
			if (!added) {
				const entry = { images: [] };
				entry['createdDate'] = array[i]['createdDate'];
				entry.images.push(array[i]);
				data.push(entry);
			}
		}

		return this.response(data, {
			title: 'get site images success',
		});
	}

	//update site image description
	@Patch('/images/:id')
	async updateSiteImageDescription(
		@Body() updateSiteImageDescriptionDTO: UpdateSiteImageDescriptionDTO,
		@Param('id') imageId: string,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteImageService.updateSiteImageDescription(
			imageId,
			updateSiteImageDescriptionDTO,
			authStaff,
		);
		return this.response(null, { title: 'site image update success' });
	}

	//update site file name
	@Patch('/files/:id')
	async updateSiteFileName(
		@Body() updateSiteFileNameDTO: UpdateSiteFileNameDTO,
		@Param('id') fileId: string,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteFileService.updateSiteFileName(
			fileId,
			updateSiteFileNameDTO,
			authStaff,
		);
		return this.response(null, { title: 'file name success' });
	}

	//upload site files
	@Post('/:id/files/upload')
	@Storage({
		fieldName: 'file',
		path: '/sites/files',
		filter: filterFile,
		sku: 'STF',
		limits: { fileSize: 10000000 },
	})
	@Serialize(UploadSiteFileSerialize)
	async uploadSiteFile(
		@Param('id') siteId: string,
		@UploadedFile() siteFile: any,
		@AuthStaff() authStaff: Staff,
		@Headers(TENANT_HEADER) tenantName: string,
		@Headers() headers: any,
	) {
		siteFile.originalname = headers['content-disposition']
			? decodeURI(headers['content-disposition'])
			: siteFile.originalname;

		// start - check plan and unauthorized
		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);
		const currentPlan = await this.tenantService.currentPlan(
			activeSubscription,
		);

		if (currentPlan.name == 'free')
			throw new NotAcceptableException(['not-accepted-for-free-plan']);
		// end - check plan and unauthorized

		const name = siteFile.originalname;
		const size = siteFile.size;
		if (!(await validateExists(this.service.repo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		if (!siteFile)
			throw new BadRequestException(['file must not be empty']);

		const data = await this.siteFileService.uploadSiteFile(
			siteId,
			name,
			size,
			this.fileService.uploadedFile(siteFile),
			authStaff.id,
		);

		return this.response(data, { title: 'upload site file success' });
	}

	// delete site files
	@Delete('/files/delete')
	@InjectRequestToBody()
	async deleteSiteFile(
		@Body() deleteSiteFileDTO: DeleteSiteFileDTO,
		@AuthStaff() authStaff: Staff,
	) {
		await this.siteFileService.deleteSiteFiles(
			deleteSiteFileDTO,
			authStaff,
		);

		return this.response(null, {
			title: 'delete site files success',
		});
	}

	@Get('/:id/files')
	@Serialize(GetSiteFileSerialize)
	async getSiteFiles(@Param('id') siteId: string) {
		if (!(await validateExists(this.service.repo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		const array = await this.siteFileService.repo
			.createQueryBuilder('site_files')
			.leftJoinAndSelect('site_files.site', 'sites')
			.leftJoinAndSelect('site_files.staff', 'staffs')
			.leftJoin('staffs.staffTeam', 'staffTeam')
			.select([
				'site_files',
				'DATE_FORMAT(site_files.createdAt, "%Y-%m-%d") AS createdDate',
				'sites.name',
				'staffs.id',
				'staffs.name',
				'staffs.image',
				'staffTeam.colorCode',
			])
			.where('site_files.siteId = :siteId', { siteId })
			.getRawMany();

		const data = [];

		for (let i = 0; i < array.length; i++) {
			let added = false;
			for (let j = 0; j < data.length; j++) {
				if (data[j]['createdDate'] == array[i]['createdDate']) {
					data[j].files.push(array[i]);
					added = true;
					break;
				}
			}
			if (!added) {
				const entry = { files: [] };
				entry['createdDate'] = array[i]['createdDate'];
				entry.files.push(array[i]);
				data.push(entry);
			}
		}

		return this.response(data, {
			title: 'get site files success',
		});
	}
}
