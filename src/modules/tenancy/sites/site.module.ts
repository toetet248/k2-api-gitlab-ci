import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';
import { CustomerModule } from '../customers/customer.module';
import { DailyNoteModule } from '../daily-notes/daily-note.module';
import { OperationInstructionModule } from '../operation-instructions/operation-instruction.module';
import { OperationModule } from '../operations/operation.module';
import { SiteFileModule } from '../site-files/site-file.module';
import { SiteImageModule } from '../site-images/site-image.module';
import { SiteMemoModule } from '../site-memos/site-memo.module';
import { StaffModule } from '../staffs/staff.module';
import { SiteController } from './site.controller';
import { Site } from './site.entity';
import { SiteService } from './site.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([Site]),
		NestjsFormDataModule,
		OperationModule,
		OperationInstructionModule,
		DailyNoteModule,
		SiteMemoModule,
		SiteImageModule,
		SiteFileModule,
		CommonModule,
		TenantModule,
	],
	controllers: [SiteController],
	providers: [SiteService],
	exports: [SiteService],
})
export class SiteModule {}
