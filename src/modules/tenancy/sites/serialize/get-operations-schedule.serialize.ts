import { Expose, Transform, Type } from 'class-transformer';
import { formatDate } from 'src/common/service/helper.service';

export class GetOperationsScheduleSerialize {
	@Expose({ name: 'id' })
	operationInstructionId: string;

	@Expose({ name: 'operation' })
	@Type(() => Operation)
	// @Transform(({ value }) => value[0].id)
	operation: string[];

	@Expose()
	@Type(() => DailyNotes)
	dailyNotes: DailyNotes[];
}

class Operation {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class DailyNotes {
	@Expose()
	id: string;

	@Expose()
	operationId: string;

	@Expose()
	siteId: string;

	@Expose()
	operationInstructionId: string;

	@Expose()
	@Transform(({ value }) => formatDate(value))
	scheduleDate: string;
}
