import { Expose, Transform } from 'class-transformer';
import moment from 'moment';

export class UploadSiteFileSerialize {
	@Expose()
	id: string;

	@Expose()
	siteId: string;

	@Expose()
	name: string;

	@Expose()
	file: string;

	@Expose()
	orderNo: string;

	@Expose()
	size: string;

	@Expose({ name: 'createdAt' })
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD HH:MM') : value,
	)
	createdDate: string;
}
