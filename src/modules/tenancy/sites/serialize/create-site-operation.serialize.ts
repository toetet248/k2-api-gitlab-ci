import { Expose, Transform } from 'class-transformer';

export class CreateSiteOperationSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	// @Expose({ name: 'orderNo' })
	// @Transform(({ value }) => 0)
	// checkOrderNo: number;
}
