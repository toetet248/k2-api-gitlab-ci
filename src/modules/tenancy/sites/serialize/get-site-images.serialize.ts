import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class GetSiteImageSerialize {
	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	createdDate: string;

	@Expose()
	@Type(() => Image)
	images: Image[];
}
class Image {
	@Expose({ name: 'site_images_id' })
	id: string;

	@Expose({ name: 'site_images_siteId' })
	siteId: string;

	@Expose({ name: 'sites_name' })
	siteName: string;

	@Expose({ name: 'site_images_image' })
	image: string;

	@Expose({ name: 'site_images_albumId' })
	albumId: string;

	@Expose({ name: 'site_images_orderNo' })
	orderNo: string;

	@Expose({ name: 'site_images_description' })
	description: string;

	@Expose({ name: 'site_images_size' })
	size: string;

	@Expose({ name: 'staffs_id' })
	staffId: string;

	@Expose({ name: 'staffs_name' })
	staffName: string;

	@Expose({ name: 'staffs_image' })
	staffImage: string;

	@Expose({ name: 'staffTeam_colorCode' })
	staffTeamColor: string;

	@Expose({ name: 'site_images_createdAt' })
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD HH:MM') : value,
	)
	createdDate: Date;
}
