import { Expose, Transform } from 'class-transformer';

export class UpdateSiteOperationSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	@Expose({ name: 'operationInstructions' })
	@Transform(({ value }) => (value.length !== 0 ? true : false))
	isCheck: number;

	@Expose({ name: 'otherOperationInstructions' })
	@Transform(({ value }) => (value.length !== 0 ? true : false))
	isUsedByOtherSites: number;
}
