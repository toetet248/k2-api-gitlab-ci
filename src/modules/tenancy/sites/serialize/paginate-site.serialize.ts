import { Expose, Transform } from 'class-transformer';
import { formatDate } from 'src/common/service/helper.service';

export class PaginateSiteSerialize {
	@Expose({ name: 'sites_id' })
	id: string;

	@Expose({ name: 'sites_name' })
	name: string;

	@Expose({ name: 'sites_startDate' })
	@Transform(({ value }) => (value ? formatDate(value) : value))
	startDate: string;

	@Expose({ name: 'sites_endDate' })
	@Transform(({ value }) => (value ? formatDate(value) : value))
	endDate: string;

	@Expose({ name: 'customers_name' })
	customerName: string;

	@Expose({ name: 'staffs_name' })
	staffName: string;

	@Expose({ name: 'staffs_nameOnTab' })
	staffNameOnTab: string;

	@Expose()
	siteStatusIndex: number;
}
