import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class UploadSiteImageSerialize {
	@Expose()
	id: string;

	@Expose()
	siteId: string;

	@Expose()
	image: string;

	@Expose()
	albumId: string;

	@Expose()
	orderNo: string;

	@Expose()
	description: string;

	@Expose()
	size: string;

	@Expose({ name: 'createdAt' })
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD HH:MM') : value,
	)
	createdDate: string;
}
