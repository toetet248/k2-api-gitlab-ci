import { Expose } from 'class-transformer';

export class CheckSiteOperationSerialize {
	@Expose({ name: 'id' })
	operationInstructionId: string;

	@Expose()
	operationId: string;
}
