import { Exclude, Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class GetSiteSerialize {
	@Expose()
	id: string;

	@Expose({ name: 'customer' })
	@Type(() => Customer)
	customer: string;

	@Expose({ name: 'customerContact' })
	@Type(() => CustomerContact)
	customerContact: string;

	@Expose({ name: 'staff' })
	@Type(() => Staff)
	staff: string;

	@Expose()
	name: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;

	@Expose()
	lat: string;

	@Expose()
	lng: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	startDate: Date;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	endDate: Date;

	@Expose()
	price: string;

	@Expose()
	reserveMan: string;

	@Expose()
	reserveHour: string;

	@Expose()
	reserveManPower: number;

	@Expose()
	totalActualMan: number;

	@Expose()
	totalActualHour: number;

	@Expose()
	totalActualManPower: number;

	@Expose()
	siteStatusIndex: number;

	@Expose()
	@Type(() => SiteMemo)
	siteMemos: SiteMemo[];
}
class SiteMemo {
	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	scheduleDate: string;

	// @Expose()
	// @Type(() => Memo)
	// memos: Memo[];
}

// class Memo {
// 	@Expose({ name: 'site_memos_id' })
// 	id: string;

// 	@Expose({ name: 'site_memos_siteId' })
// 	siteId: string;

// 	@Expose({ name: 'site_memos_memo' })
// 	memo: string;

// 	@Expose({ name: 'site_memos_scheduleDate' })
// 	@Transform(({ value }) =>
// 		value ? moment(value).format('YYYY-MM-DD') : value,
// 	)
// 	scheduleDate: Date;

// 	@Expose({ name: 'site_memos_orderNo' })
// 	orderNo: string;

// 	@Expose({ name: 'site_memos_staffId' })
// 	staffId: string;

// 	@Expose({ name: 'site_memos_createdAt' })
// 	createdAt: Date;
// }

class Customer {
	@Expose()
	id: string;

	@Expose()
	name: string;
}

class CustomerContact {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	phone: string;

	@Expose()
	email: string;
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	phone: string;

	@Expose()
	email: string;
}
