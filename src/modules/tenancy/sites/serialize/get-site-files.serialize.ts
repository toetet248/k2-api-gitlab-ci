import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class GetSiteFileSerialize {
	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	createdDate: string;

	@Expose()
	@Type(() => File)
	files: File[];
}

class File {
	@Expose({ name: 'site_files_id' })
	id: string;

	@Expose({ name: 'site_files_siteId' })
	siteId: string;

	@Expose({ name: 'sites_name' })
	siteName: string;

	@Expose({ name: 'site_files_name' })
	name: string;

	@Expose({ name: 'site_files_file' })
	file: string;

	@Expose({ name: 'site_files_orderNo' })
	orderNo: string;

	@Expose({ name: 'site_files_description' })
	description: string;

	@Expose({ name: 'site_files_size' })
	size: string;

	@Expose({ name: 'staffs_id' })
	staffId: string;

	@Expose({ name: 'staffs_name' })
	staffName: string;

	@Expose({ name: 'staffs_image' })
	staffImage: string;

	@Expose({ name: 'staffTeam_colorCode' })
	staffTeamColor: string;

	@Expose({ name: 'site_files_createdAt' })
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD HH:MM') : value,
	)
	createdDate: Date;
}
