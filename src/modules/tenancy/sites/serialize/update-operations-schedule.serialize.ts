import { Expose } from 'class-transformer';

export class UpdateOperationsScheduleSerialize {
	@Expose()
	id: string;

	@Expose()
	operationId: string;

	@Expose()
	siteId: string;

	@Expose()
	operationInstructionId: string;

	@Expose()
	scheduleDate: string;
}
