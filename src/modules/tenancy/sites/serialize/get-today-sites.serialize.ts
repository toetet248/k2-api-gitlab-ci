import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class GetAllSitesTodaySerialize {
	@Expose({ name: 'sites_id' })
	id: string;

	@Expose({ name: 'sites_name' })
	name: string;

	@Expose({ name: 'sites_lat' })
	lat: string;

	@Expose({ name: 'sites_lng' })
	lng: string;

	@Expose({ name: 'sites_addressPrefecture' })
	addressPrefecture: string;

	@Expose({ name: 'sites_addressCity' })
	addressCity: string;
}
