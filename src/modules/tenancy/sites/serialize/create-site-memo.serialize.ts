import { Expose } from 'class-transformer';

export class CreateSiteMemoSerialize {
	@Expose()
	id: string;

	@Expose()
	siteId: string;

	@Expose()
	memo: string;

	@Expose()
	scheduleDate: string;

	@Expose()
	staffId: number;
}
