import { Expose } from 'class-transformer';

export class UpdateDailyNoteSerialize {
	@Expose({ name: 'daily_notes_id' })
	id: string;

	@Expose({ name: 'daily_notes_operationId' })
	operationId: string;

	@Expose({ name: 'daily_notes_operationInstructionId' })
	operationInstructionId: string;

	@Expose({ name: 'daily_notes_scheduleDate' })
	scheduleDate: string;

	@Expose({ name: 'daily_notes_actualMan' })
	actualMan: string;

	@Expose({ name: 'daily_notes_actualHour' })
	actualHour: string;

	@Expose({ name: 'daily_notes_actualManPower' })
	actualManPower: string;

	@Expose({ name: 'daily_notes_startTime' })
	startTime: string;

	@Expose({ name: 'daily_notes_endTime' })
	endTime: string;

	@Expose({ name: 'daily_notes_author' })
	author: string;

	@Expose({ name: 'daily_notes_memo' })
	memo: string;
}
