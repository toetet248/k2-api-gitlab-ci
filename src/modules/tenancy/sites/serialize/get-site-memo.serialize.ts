import { Expose, Transform } from 'class-transformer';
import { formatDate } from 'src/common/service/helper.service';

export class GetSiteMemoSerialize {
	// @Expose({ name: 'daily_notes_id' })
	// id: string;

	@Expose({ name: 'site_memos_id' })
	id: string;

	@Expose({ name: 'site_memos_siteId' })
	siteId: string;

	@Expose({ name: 'site_memos_memo' })
	memo: string;

	@Expose({ name: 'site_memos_scheduleDate' })
	@Transform(({ value }) => formatDate(value))
	scheduleDate: string;

	@Expose({ name: 'site_memos_staffId' })
	staffId: number;

	@Expose({ name: 'site_memos_orderNo' })
	orderNo: number;

	@Expose({ name: 'staffs_name' })
	name: string;

	@Expose({ name: 'staffs_nameOnTab' })
	nameOnTab: string;

	@Expose({ name: 'staffs_image' })
	image: string;

	@Expose({ name: 'staff_teams_colorCode' })
	teamColor: string;
}
