import { Expose, Type } from 'class-transformer';

class Customer {
	@Expose()
	name: string;
}

class Staff {
	@Expose()
	name: string;
}

export class UpdateSiteSerialize {
	@Expose()
	id: string;

	@Expose()
	customerId: string;

	@Expose()
	customerContactId: string;

	@Expose()
	staffId: string;

	@Expose()
	name: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;

	@Expose()
	lat: string;

	@Expose()
	lng: string;

	@Expose()
	startDate: Date;

	@Expose()
	endDate: Date;

	@Expose()
	price: number;

	@Expose()
	@Type(() => Customer)
	customer: Customer;

	@Expose()
	@Type(() => Staff)
	staff: Staff;
}
