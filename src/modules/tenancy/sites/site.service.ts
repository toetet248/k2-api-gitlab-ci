import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import {
	Brackets,
	Connection,
	LessThanOrEqual,
	MoreThanOrEqual,
	Repository,
} from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { Site, siteStatusCase } from './site.entity';
import { CreateSiteDTO } from './dto/create-site.dto';
import { generateId, todayDate } from 'src/common/service/helper.service';
import { CustomerContact } from '../customer-contacts/customer-contact.entity';
import { Customer } from '../customers/customer.entity';
import { OperationInstruction } from '../operation-instructions/operation-instruction.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { SiteMemo } from '../site-memos/site-memo.entity';
import { UpdateOperationsScheduleDTO } from './dto/update-operations-schedule.dto';
import { Operation } from '../operations/operation.entity';
import { Staff } from '../staffs/staff.entity';
import { UpdateSiteDTO } from './dto/update-site.dto';
import { paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import moment from 'moment';
import { DailyNoteService } from '../daily-notes/daily-note.service';
import { UpdateDailyNoteDTO } from './dto/update-daily-note.dto';
import { PaginateSiteDTO } from './dto/paginate-site.dto';
import { isBoolean } from 'node-boolify';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import JSZip from 'jszip';
import { FileService } from 'src/common/service/file.service';
import { map } from 'modern-async';
import { isEmpty } from 'lodash';

@Injectable()
export class SiteService {
	public readonly repo: Repository<Site>;
	public readonly customerContactRepo: Repository<CustomerContact>;
	public readonly customerRepo: Repository<Customer>;
	public readonly staffRepo: Repository<Staff>;
	public readonly operationInstructionRepo: Repository<OperationInstruction>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly operationRepo: Repository<Operation>;
	public readonly siteMemoRepo: Repository<SiteMemo>;
	public readonly scheduleStaffRepo: Repository<ScheduleStaff>;
	public readonly scheduleVehicleRepo: Repository<ScheduleVehicle>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(DailyNoteService) private dailyNoteService: DailyNoteService,
		@Inject(FileService) private readonly fileService: FileService,
	) {
		this.repo = connection.getRepository(Site);
		this.customerRepo = connection.getRepository(Customer);
		this.customerContactRepo = connection.getRepository(CustomerContact);
		this.staffRepo = connection.getRepository(Staff);
		this.operationInstructionRepo =
			connection.getRepository(OperationInstruction);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.operationRepo = connection.getRepository(Operation);
		this.siteMemoRepo = connection.getRepository(SiteMemo);
		this.scheduleStaffRepo = connection.getRepository(ScheduleStaff);
		this.scheduleVehicleRepo = connection.getRepository(ScheduleVehicle);
	}

	async paginateSite(
		paginateSiteDTO: PaginateSiteDTO,
	): Promise<Pagination<Site>> {
		let {
			page = 1,
			search,
			staffIds,
			isUnplanned, //未予定 boolean value
			isBeforeConstruction, //施工前
			isUnderConstruction, //施工中
			isCompleted, //施工済
			startDate,
			endDate,
		} = paginateSiteDTO;

		isUnplanned = isBoolean(isUnplanned);
		isBeforeConstruction = isBoolean(isBeforeConstruction);
		isUnderConstruction = isBoolean(isUnderConstruction);
		isCompleted = isBoolean(isCompleted);

		const today = todayDate();

		const query = this.repo
			.createQueryBuilder('sites')
			.leftJoinAndSelect('sites.staff', 'staffs')
			.leftJoinAndSelect('sites.customer', 'customers')
			.select([
				'sites.id',
				'sites.name',
				'sites.startDate',
				'sites.endDate',
				'customers.name',
				'staffs.name',
				'staffs.nameOnTab',
			])
			.addSelect(siteStatusCase(), 'siteStatusIndex')
			.orderBy('siteStatusIndex', 'ASC')
			.addOrderBy('sites.startDate', 'DESC');

		if (search)
			query.andWhere(
				new Brackets((q) => {
					return q
						.where('sites.name like :name', {
							name: `%${search}%`,
						})
						.orWhere('sites.price like :price', {
							price: `%${search}%`,
						})
						.orWhere(
							'sites.addressPrefecture like :addressPrefecture',
							{
								addressPrefecture: `%${search}%`,
							},
						)
						.orWhere('sites.addressCity like :addressCity', {
							addressCity: `%${search}%`,
						})
						.orWhere('staffs.name like :name', {
							name: `%${search}%`,
						})
						.orWhere('staffs.nameOnTab like :nameOnTab', {
							nameOnTab: `%${search}%`,
						})
						.orWhere('customers.name like :name', {
							name: `%${search}%`,
						});
				}),
			);

		if (staffIds)
			query.andWhere('staffs.id IN (:staffIds)', {
				staffIds,
			});

		if (
			isUnplanned ||
			isBeforeConstruction ||
			isUnderConstruction ||
			isCompleted
		) {
			query.andWhere(
				new Brackets((q) => {
					//未予定 - red
					if (isUnplanned)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('sites.startDate IS NULL').andWhere(
									'sites.endDate IS NULL',
								);
							}),
						);

					//施工前 - green
					if (isBeforeConstruction)
						q.orWhere('sites.startDate > :today', { today });

					//施工中 - blue
					if (isUnderConstruction)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('sites.startDate <= :today', {
									today,
								}).andWhere('sites.endDate >= :today', {
									today,
								});
							}),
						);

					//施工済 - grey
					if (isCompleted)
						q.orWhere('sites.endDate < :today', { today });
				}),
			);
		}

		if (startDate || endDate) {
			query.andWhere(
				new Brackets((q) => {
					if (startDate)
						q.andWhere(
							new Brackets((sq) => {
								sq.where(
									'sites.startDate IS NOT NULL',
								).andWhere('sites.startDate >= :startDate', {
									startDate,
								});
							}),
						);

					if (endDate)
						q.andWhere(
							new Brackets((sq) => {
								sq.where('sites.endDate IS NOT NULL').andWhere(
									'sites.endDate <= :endDate',
									{
										endDate,
									},
								);
							}),
						);
				}),
			);
		}

		// const data = await query.paginate(30);

		// return {
		// 	items: data.data,
		// 	meta: {
		// 		totalItems: data.total,
		// 		itemCount: data.data.length,
		// 		itemsPerPage: data.per_page,
		// 		totalPages: data.last_page,
		// 		currentPage: data.current_page,
		// 	},
		// };

		const data = await paginateRaw<Site>(query, { page, limit: 50 });

		return data;
	}

	async createSite(createSiteDTO: CreateSiteDTO) {
		// if (
		// 	!(await validateExists(this.customerRepo, [
		// 		'id',
		// 		createSiteDTO.customerId,
		// 	]))
		// )
		// 	throw new BadRequestException(['customerId does not exists']);

		// if (createSiteDTO.customerContactId)
		// 	if (
		// 		!(await validateExists(this.customerContactRepo, [
		// 			'id',
		// 			createSiteDTO.customerContactId,
		// 		]))
		// 	)
		// 		throw new BadRequestException([
		// 			'customerContactId does not exists',
		// 		]);

		// if (
		// 	!(await validateExists(this.staffRepo, [
		// 		'id',
		// 		createSiteDTO.staffId,
		// 	]))
		// )
		// 	throw new BadRequestException(['staffId does not exists']);
		if (!createSiteDTO.customerContactId)
			delete createSiteDTO['customerContactId'];

		if (!createSiteDTO.customerContactId)
			delete createSiteDTO['customerContactId'];

		const requestData = {
			customerId: createSiteDTO.customerId,
			customerContactId: createSiteDTO.customerContactId,
			staffId: createSiteDTO.staffId,
			name: createSiteDTO.name,
			addressPrefecture: createSiteDTO.addressPrefecture,
			addressCity: createSiteDTO.addressCity,
			addressBuilding: createSiteDTO.addressBuilding,
			lat: createSiteDTO.lat,
			lng: createSiteDTO.lng,
			price: createSiteDTO.price,
		};

		const data = this.repo.create({
			id: generateId(),
			...requestData,
			// ...createSiteDTO,
		});
		await this.repo.save(data);

		return await this.repo.findOne(data.id, {
			relations: ['customer', 'staff'],
		});
	}

	async updateSite(id: string, updateSiteDTO: UpdateSiteDTO) {
		const site = await this.repo.findOne(id);

		if (site) {
			const requestData = {
				customerId: updateSiteDTO.customerId,
				// customerContactId: updateSiteDTO.customerContactId,
				staffId: updateSiteDTO.staffId,
				name: updateSiteDTO.name,
				addressPrefecture: updateSiteDTO.addressPrefecture,
				addressCity: updateSiteDTO.addressCity,
				addressBuilding: updateSiteDTO.addressBuilding,
				lat: updateSiteDTO.lat,
				lng: updateSiteDTO.lng,
				price: updateSiteDTO.price,
			};

			await this.repo.save({
				...site,
				...requestData,
				customerContactId: updateSiteDTO.customerContactId
					? updateSiteDTO.customerContactId
					: null,
			});

			return await this.repo.findOne(id, {
				relations: ['customer', 'staff'],
			});
		}

		throw new BadRequestException(['site not found']);
	}

	async updateOperationsSchedule(
		id: string,
		updateOperationsScheduleDTO: UpdateOperationsScheduleDTO,
	) {
		const { operationInstructionId, dailyNotes } =
			updateOperationsScheduleDTO;

		let opId = await this.operationInstructionRepo.findOne({
			where: { id: operationInstructionId },
			select: ['operationId'],
		});

		// remove
		// if (
		// 	!(await validateExists(this.operationRepo, [
		// 		'id',
		// 		opId.operationId,
		// 	]))
		// )
		// 	throw new BadRequestException(['operationId does not exists']);

		// if (dailyNotes.length) {
		// 	if (
		// 		!(await validateExists(this.dailyNoteRepo, [
		// 			'id',
		// 			dailyNotes
		// 				.map((dailyNote) => dailyNote.id)
		// 				.filter((dailyNoteId) => dailyNoteId),
		// 		]))
		// 	)
		// 		throw new BadRequestException(['daily note does not exists']);
		// }

		const createDailyNotes = [];
		const dailyNoteIds = [];

		const dailyNotesData = dailyNotes.map((dailyNote) => {
			let create = false;
			if (!dailyNote.id) {
				dailyNote.id = generateId();
				create = true;
			}

			const resquestData = {
				id: dailyNote.id,
				scheduleDate: dailyNote.scheduleDate,
			};

			const dn = this.dailyNoteRepo.create({
				...resquestData,
				operationInstructionId,
				siteId: id,
				operationId: opId.operationId,
			});

			dailyNoteIds.push(dn.id);

			if (create) createDailyNotes.push(dn);

			return dn;
		});
		await this.dailyNoteRepo.save(createDailyNotes);

		//remove schedule staff if remove other daily notes ids have
		const delScheduleStaffsQuery = this.scheduleStaffRepo
			.createQueryBuilder('scheduleStaffs')
			.leftJoinAndSelect('scheduleStaffs.dailyNote', 'dailyNotes')
			.where('dailyNotes.siteId = :id', { id })
			.andWhere(
				'dailyNotes.operationInstructionId = :operationInstructionId',
				{
					operationInstructionId,
				},
			);

		if (dailyNoteIds.length > 0) {
			delScheduleStaffsQuery.andWhere(
				'dailyNotes.id NOT IN(:dailyNoteIds)',
				{ dailyNoteIds },
			);
		}

		const delScheduleStaffs = await delScheduleStaffsQuery.getMany();

		const delScheduleVehiclesQuery = this.scheduleVehicleRepo
			.createQueryBuilder('scheduleVehicles')
			.leftJoinAndSelect('scheduleVehicles.dailyNote', 'dailyNotes')
			.andWhere('dailyNotes.siteId = :id', { id })
			.andWhere(
				'dailyNotes.operationInstructionId = :operationInstructionId',
				{
					operationInstructionId,
				},
			);

		if (dailyNoteIds.length > 0) {
			delScheduleVehiclesQuery.andWhere(
				'dailyNotes.id NOT IN(:dailyNoteIds)',
				{ dailyNoteIds },
			);
		}

		const delScheduleVehicles = await delScheduleVehiclesQuery.getMany();

		await Promise.all([
			this.scheduleStaffRepo.remove(delScheduleStaffs),
			this.scheduleVehicleRepo.remove(delScheduleVehicles),
			this.dailyNoteService.removeOtherDailyNotesByIds(
				dailyNoteIds,
				operationInstructionId,
			),
		]);

		const scheduleDates = await this.dailyNoteService.repo
			.find({
				where: { siteId: id },
				select: ['scheduleDate'],
			})
			.then((dailyNotes) => {
				return dailyNotes.map((dailyNote) =>
					moment(dailyNote.scheduleDate, 'YYYY-MM-DD'),
				);
			});

		// update start date & end date in site table
		let startDate = null;
		let endDate = null;
		if (!isEmpty(scheduleDates)) {
			startDate = moment.min(scheduleDates).format('YYYY-MM-DD');
			endDate = moment.max(scheduleDates).format('YYYY-MM-DD');
		}

		await this.repo.save({
			id,
			startDate,
			endDate,
		});

		return dailyNotesData;
	}

	async updateDailyNotes(
		updateDailyNoteDTO: UpdateDailyNoteDTO,
		authStaff: Staff,
	) {
		const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
			authStaff;

		let dailyNotes: DailyNote[];

		// throw error if site staff update site memo of other staffs
		if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner) {
			const staffDailyNoteIds = await this.dailyNoteRepo
				.createQueryBuilder('dailyNotes')
				.select(['dailyNotes.id'])
				.leftJoin('dailyNotes.scheduleStaffs', 'scheduleStaffs')
				.where('scheduleStaffs.staffId = :staffId', { staffId: id })
				.getMany()
				.then((dns) => {
					return dns.map((dn) => dn.id);
				});

			dailyNotes = updateDailyNoteDTO.dailyNotes
				.map((dailyNote) => {
					if (staffDailyNoteIds.includes(dailyNote.id)) {
						const requestData = {
							id: dailyNote.id,
							actualMan: dailyNote.actualMan,
							actualHour: dailyNote.actualHour,
							actualManPower: dailyNote.actualManPower,
							actualStartTime: dailyNote.actualStartTime,
							actualEndTime: dailyNote.actualEndTime,
							memo: dailyNote.memo,
						};
						// console.log(requestData, '...requestData');
						return this.dailyNoteRepo.create({
							...requestData,
							staffId: id,
						});
					}
				})
				.filter((dn) => dn);

			if (dailyNotes.length !== updateDailyNoteDTO.dailyNotes.length)
				throw new BadRequestException([
					'site staff does not have permission to update',
				]);
		} else {
			dailyNotes = updateDailyNoteDTO.dailyNotes.map((dailyNote) => {
				const requestData = {
					id: dailyNote.id,
					actualMan: dailyNote.actualMan,
					actualHour: dailyNote.actualHour,
					actualManPower: dailyNote.actualManPower,
					actualStartTime: dailyNote.actualStartTime,
					actualEndTime: dailyNote.actualEndTime,
					memo: dailyNote.memo,
				};
				// console.log(requestData, '...requestData');
				return this.dailyNoteRepo.create({
					...requestData,
					staffId: id,
				});
			});
		}

		await this.dailyNoteRepo.save(dailyNotes);
	}

	async getSiteIdsByDate(date: string, staffId: string) {
		const sites = await this.repo
			.createQueryBuilder('sites')
			.select('sites.id')
			.leftJoinAndSelect('sites.dailyNotes', 'dailyNotes')
			.leftJoinAndSelect('dailyNotes.scheduleStaffs', 'scheduleStaffs')
			.where('sites.startDate <= :date', { date })
			.andWhere('sites.endDate >= :date', { date })
			.andWhere('dailyNotes.scheduleDate = :date', { date })
			.andWhere(
				new Brackets((q) => {
					if (staffId)
						q.where('scheduleStaffs.staffId = :staffId', {
							staffId,
						});
				}),
			)
			.getMany();

		return sites.map((site) => site.id);
	}

	// get all sites of today
	async getTodaySites(scheduleDate: string) {
		const query = await this.repo
			.createQueryBuilder('sites')
			.leftJoinAndSelect('sites.dailyNotes', 'dailyNotes')
			.where('dailyNotes.scheduleDate = :scheduleDate', {
				scheduleDate,
			})
			.andWhere('sites.lat is not null')
			.andWhere('sites.lat!=""')
			.andWhere('sites.lng is not null')
			.andWhere('sites.lng !=""')
			.groupBy('sites.id')

			.getRawMany();
		return query;
	}

	// get all sites of today
	async getZipFile(id: string, tenantName: string) {
		const site = await this.repo
			.createQueryBuilder('sites')
			.leftJoinAndSelect('sites.siteImages', 'siteImages')
			.leftJoinAndSelect('sites.siteFiles', 'siteFiles')
			.where('sites.id = :id', {
				id,
			})
			.getOne();
		const { siteImages, siteFiles } = site;

		// check site images and site files exist
		// generate zip file and return download link
		if (siteImages.length + siteFiles.length > 0) {
			const jszip = new JSZip();

			const images = jszip.folder('画像');
			const files = jszip.folder('添付書類');

			// put images to site/images folder
			const imagesPromises = [];
			await map(siteImages, async (siteImage) => {
				const split = siteImage.image.split('/');
				const name = split[split.length - 1];
				imagesPromises.push(
					this.fileService
						.getBuffer(siteImage.image)
						.then((buffer) => {
							if (buffer)
								images.file(name, buffer, { binary: true });
						}),
				);
			});

			// put images to site/files folder
			const filesPromises = [];
			await map(siteFiles, async (siteFile) => {
				const split = siteFile.file.split('/');
				const name = split[split.length - 1];
				filesPromises.push(
					this.fileService.getBuffer(siteFile.file).then((buffer) => {
						if (buffer) files.file(name, buffer, { binary: true });
					}),
				);
			});

			// call images and files promises
			await Promise.all([...imagesPromises, ...filesPromises]);

			// generate zip
			const zipFilepath = `/tenant/${tenantName}/sites/zips/`;
			const zipFileName = `${moment().format('YYYY-MM-DD')}-${site.name
				.toLowerCase()
				.replace(' ', '-')}_site-files.zip`;
			const zip = await this.fileService.uploadZip(
				jszip,
				zipFilepath,
				zipFileName,
			);

			return zip;
		}

		return null;
	}

	async getSiteOperations(id: string) {
		return await this.operationRepo
			.createQueryBuilder('operations')
			.select([
				// operations
				'operations.id',
				'operations.name',
				'operations.colorCode',
				// operationInstructions
				'operationInstructions.id',
				'operationInstructions.siteId',
				// sites
				'sites.id',
			])
			.leftJoinAndSelect(
				'operations.operationInstructions',
				'operationInstructions',
			)
			.leftJoinAndSelect('operationInstructions.site', 'sites')
			.orderBy('operations.orderNo', 'ASC')
			.getMany()
			.then((operations) => {
				return operations.map((operation) => {
					const otherOperationInstructions =
						operation.operationInstructions.filter((oi) => {
							return oi.siteId !== id && oi.site;
						});
					operation.operationInstructions =
						operation.operationInstructions.filter(
							(oi) => oi.siteId === id,
						);

					return {
						...operation,
						otherOperationInstructions,
					};
				});
			});
	}
}
