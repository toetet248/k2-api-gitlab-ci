import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { Site } from '../sites/site.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'schedule_staffs' })
export class ScheduleStaff extends MainEntity {
	@Column()
	staffId: string;

	@Column()
	dailyNoteId: string;

	@Column()
	dayNightSupport: string;

	@Column()
	nightSupportCount: number;

	@Column()
	orderNo: number;

	@ManyToOne(() => DailyNote, (dailyNote) => dailyNote.scheduleStaffs, {
		primary: true,
	})
	@JoinColumn({ name: 'dailyNoteId' })
	dailyNote: DailyNote;

	@ManyToOne(() => Staff, (staff) => staff.scheduleStaffs, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
