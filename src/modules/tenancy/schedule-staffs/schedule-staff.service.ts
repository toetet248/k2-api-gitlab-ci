import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import {
	generateId,
	validateExists,
	validateTwoArrayUnique,
} from 'src/common/service/helper.service';
import { Connection, In, Repository } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { CreateScheduleStaffDTO } from '../schedulers/dto/create-schedule-staff.dto';
import { Staff } from '../staffs/staff.entity';
import { CONNECTION } from '../tenancy.symbols';
import { ScheduleStaff } from './schedule-staff.entity';

@Injectable()
export class ScheduleStaffService {
	public readonly repo: Repository<ScheduleStaff>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly staffRepo: Repository<Staff>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(ScheduleStaff);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.staffRepo = connection.getRepository(Staff);
	}

	async createScheduleStaff(createScheduleStaffDTO: CreateScheduleStaffDTO) {
		const {
			dailyNoteId,
			staffIds,
			// dayStaffIds,
			// supportStaffIds,
			isUpdateToday,
			isUpdateTomorrowToEndDate,
		} = createScheduleStaffDTO;

		if (isUpdateToday) {
			// if isUpdateToday is true
			// find scheduleStaffs of given dailyNoteId
			const findScheduleStaffData = await this.repo.find({
				where: { dailyNoteId },
			});

			// delete scheduleStaffs of given dailyNoteId
			await this.repo.remove(findScheduleStaffData);

			const scheduleStaffData = staffIds.map(
				({ staffId, type }, index) => {
					return this.repo.create({
						id: generateId(),
						dailyNoteId,
						dayNightSupport: type,
						orderNo: index + 1,
						staffId,
					});
				},
			);

			await this.repo.save(scheduleStaffData);

			// const dayScheduleStaffData = dayStaffIds.map((staffId, index) => {
			// 	return this.repo.create({
			// 		id: generateId(),
			// 		dailyNoteId,
			// 		dayNightSupport: 'day',
			// 		orderNo: index + 1,
			// 		staffId,
			// 	});
			// });

			// const supportScheduleStaffData = supportStaffIds.map(
			// 	(staffId, index) => {
			// 		return this.repo.create({
			// 			id: generateId(),
			// 			dailyNoteId,
			// 			dayNightSupport: 'support',
			// 			orderNo: index + 1,
			// 			staffId,
			// 		});
			// 	},
			// );

			// await this.repo.save(
			// 	dayScheduleStaffData.concat(supportScheduleStaffData),
			// );
		}

		// else {
		// 	throw new BadRequestException(['isUpdateToday must be true']);
		// }

		if (isUpdateTomorrowToEndDate) {
			// find siteId, operationId from given dailyNoteId
			const { siteId, operationId, scheduleDate } =
				await this.dailyNoteRepo.findOne({
					select: ['siteId', 'operationId', 'scheduleDate'],
					where: { id: dailyNoteId },
				});

			const getTomorrowToEndDateDailyNoteIds = await this.dailyNoteRepo
				.createQueryBuilder('daily_notes')
				.select(['daily_notes.id'])
				.where('daily_notes.scheduleDate > :scheduleDate', {
					scheduleDate,
				})
				.andWhere('daily_notes.siteId = :siteId', {
					siteId,
				})
				.andWhere('daily_notes.operationId = :operationId', {
					operationId,
				})
				.getMany();

			const tomorrowToEndDateDailyNoteIds =
				getTomorrowToEndDateDailyNoteIds.map((did) => {
					return did.id;
				});

			// delete scheduleStaffs  (where dayNightSupport is day) ( whereIn tomorrowToEndDateDailyNoteIds  )
			// const findDelDayStaff = await this.repo.find({
			// 	where: {
			// 		dailyNoteId: In(tomorrowToEndDateDailyNoteIds), //whereIn for array
			// 		dayNightSupport: 'day',
			// 	},
			// });

			// const findDelSupporStaff = await this.repo.find({
			// 	where: {
			// 		dailyNoteId: In(tomorrowToEndDateDailyNoteIds),
			// 		staffId: In(dayStaffIds),
			// 		dayNightSupport: 'support',
			// 	},
			// });

			// await this.repo.remove(findDelDayStaff.concat(findDelSupporStaff));
			const delStaffs = await this.repo.find({
				where: [
					{
						dailyNoteId: In(tomorrowToEndDateDailyNoteIds), //whereIn for array
						dayNightSupport: 'day',
					},
					{
						dailyNoteId: In(tomorrowToEndDateDailyNoteIds),
						staffId: In(
							staffIds
								.filter((sf) => sf.type === 'day')
								.map((sf) => sf.staffId),
						),
						dayNightSupport: 'support',
					},
				],
			});
			await this.repo.remove(delStaffs);

			const createtomorrowToEndDateScheduleStaffs = [];
			// loop tomorrowToEndDateDailyNoteIds and push create scheduleStaffsDay with orderNo to new variable
			tomorrowToEndDateDailyNoteIds.map((dnId) => {
				const scheduleStaffData = staffIds
					.filter((sf) => sf.type === 'day')
					.map(({ staffId }, index) => {
						return this.repo.create({
							id: generateId(),
							dailyNoteId: dnId,
							dayNightSupport: 'day',
							orderNo: index + 1,
							staffId,
						});
					});

				createtomorrowToEndDateScheduleStaffs.push(
					...scheduleStaffData,
				);
			});

			// save scheduleStaffsDay
			await this.repo.save(createtomorrowToEndDateScheduleStaffs);
		}

		// response schedueStaffs (where given dailyNoteId) (orderBy orderNo) (with staff) (with staffTeam)
		const resScheduleStaff = await this.repo
			.createQueryBuilder('schedule_staffs')
			.leftJoinAndSelect('schedule_staffs.dailyNote', 'dailyNotes')
			.leftJoinAndSelect('schedule_staffs.staff', 'staffs')
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeams')
			.where('schedule_staffs.dailyNoteId = :dailyNoteId', {
				dailyNoteId,
			})
			.orderBy('schedule_staffs.orderNo', 'ASC')
			.getMany();

		return resScheduleStaff;
	}
}
