import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { ScheduleStaff } from './schedule-staff.entity';
import { ScheduleStaffService } from './schedule-staff.service';

@Module({
	imports: [TypeOrmModule.forFeature([ScheduleStaff]), NestjsFormDataModule],
	providers: [ScheduleStaffService],
	exports: [ScheduleStaffService],
})
export class ScheduleStaffModule {}
