import * as configuration from '../../config/config';
import { getConnectionManager } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { map } from 'modern-async';
import { StaffTeam } from './staff-teams/staff-team.entity';
import { Staff } from './staffs/staff.entity';

async function runTenantDatabaseFresh() {
	const config = configuration.default();

	// get tenant names from central
	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const centralConnection = connectionManager.create(
		centralDBConfig as MysqlConnectionOptions,
	);

	const tenants = await (
		await centralConnection.connect()
	).query('SELECT name FROM tenants');

	// run migration for all tenant db
	const tenantDBConfig = config.database.tenant;

	await map(tenants, async (tenant: any) => {
		const tenantConnectionConfig = {
			...(tenantDBConfig as MysqlConnectionOptions),
			name: `${config.database.tenantDbPrefix + tenant.name}`,
			database: `${config.database.tenantDbPrefix + tenant.name}`,
		};

		const tenantConnection = await connectionManager
			.create(tenantConnectionConfig)
			.connect();

		// drop tables
		const query = tenantConnection.createQueryRunner();

		let table = await query.getTable('sales_incharges');
		if (table) await query.dropTable('sales_incharges');

		await tenantConnection
			.createQueryBuilder()
			.delete()
			.from(Staff)
			.where('isOwner = :isOwner', { isOwner: false })
			.execute();

		await tenantConnection
			.createQueryBuilder()
			.delete()
			.from(StaffTeam)
			.execute();

		table = await query.getTable('schedule_staffs');
		if (table) await query.dropTable('schedule_staffs');

		table = await query.getTable('staff_leaves');
		if (table) await query.dropTable('staff_leaves');

		table = await query.getTable('schedule_vehicles');
		if (table) await query.dropTable('schedule_vehicles');

		table = await query.getTable('vehicles');
		if (table) await query.dropTable('vehicles');

		table = await query.getTable('vehicle_groups');
		if (table) await query.dropTable('vehicle_groups');

		table = await query.getTable('daily_notes');
		if (table) await query.dropTable('daily_notes');

		table = await query.getTable('operation_instructions');
		if (table) await query.dropTable('operation_instructions');

		table = await query.getTable('operations');
		if (table) await query.dropTable('operations');

		table = await query.getTable('site_images');
		if (table) await query.dropTable('site_images');

		table = await query.getTable('site_albums');
		if (table) await query.dropTable('site_albums');

		table = await query.getTable('site_files');
		if (table) await query.dropTable('site_files');

		table = await query.getTable('site_memos');
		if (table) await query.dropTable('site_memos');

		table = await query.getTable('schedule_sites');
		if (table) await query.dropTable('schedule_sites');

		table = await query.getTable('sites');
		if (table) await query.dropTable('sites');

		table = await query.getTable('customer_contacts');
		if (table) await query.dropTable('customer_contacts');

		table = await query.getTable('customers');
		if (table) await query.dropTable('customers');

		await tenantConnection.query('DELETE FROM migrations WHERE id > 3');

		await tenantConnection.runMigrations();

		await tenantConnection.close();
	});

	process.exit(1);
}

runTenantDatabaseFresh();
