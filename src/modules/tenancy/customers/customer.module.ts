import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CustomerContactModule } from '../customer-contacts/customer-contact.module';
import { SalesInchargeModule } from '../sales-incharges/sales-incharge.module';
import { Customer } from './customer.entity';
import { CustomerService } from './customer.service';
import { CustomersController } from './customer.controller';
import { StaffModule } from '../staffs/staff.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Customer]),
		NestjsFormDataModule,
		CustomerContactModule,
		SalesInchargeModule,
		StaffModule,
	],
	controllers: [CustomersController],
	providers: [CustomerService],
})
export class CustomerModule {}
