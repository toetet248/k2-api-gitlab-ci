import { Type } from 'class-transformer';
import {
	IsArray,
	IsNotEmpty,
	IsOptional,
	ValidateNested,
} from 'class-validator';

export class CreateCustomerContactDTO {
	@IsArray()
	// @ArrayNotEmpty()
	@Type(() => CustomerContact)
	@ValidateNested({ each: true })
	contacts: CustomerContact[];
}

class CustomerContact {
	@IsOptional()
	id: string;

	@IsNotEmpty()
	name: string;

	@IsOptional()
	email: string;

	@IsOptional()
	phone: string;
}
