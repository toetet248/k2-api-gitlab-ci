import { Type } from 'class-transformer';
import {
	ArrayNotEmpty,
	ArrayUnique,
	IsArray,
	IsNotEmpty,
	IsOptional,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsPhoneNo } from 'src/common/validator/is-phone-no.validators';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class UpdateCustomerDTO {
	@IsNotEmpty()
	@IsUnique('customers', 'name', 'id')
	name: string;

	@IsOptional()
	nameFurigana: string;

	@IsOptional()
	businessTypes: string;

	@IsOptional()
	postalCode: string;

	@IsOptional()
	addressPrefecture: string;

	@IsOptional()
	addressCity: string;

	@IsOptional()
	addressBuilding: string;

	@IsOptional()
	@IsPhoneNo()
	phone: string;

	@IsOptional()
	fax: string;

	@IsOptional()
	@IsArray()
	@Type(() => CustomerContacts)
	@ValidateNested({ each: true })
	customerContacts: CustomerContacts[];

	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique((salesIncharge) => salesIncharge.staffId, {
		message: 'staffId name must be unique',
	})
	@Type(() => SalesIncharge)
	@ValidateNested({ each: true })
	salesIncharges: SalesIncharge[];
}

class CustomerContacts {
	@IsOptional()
	@IsExist('customer_contacts', 'id', 'new')
	@IsString()
	id: string;

	@IsNotEmpty()
	name: string;

	@IsOptional()
	@IsPhoneNo()
	phone: string;

	@IsOptional()
	email: string;
}

class SalesIncharge {
	@IsOptional()
	@IsExist('sales_incharges', 'id', 'new')
	@IsString()
	id: string;

	@IsExist('staffs', 'id')
	@IsNotEmpty()
	staffId: string;
}
