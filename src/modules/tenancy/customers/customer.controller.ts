import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
	Query,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CustomerService } from './customer.service';
import { CreateCustomerDTO } from './dto/create-customer.dto';
import { CreateCustomerSerialize } from './serialize/create-customer.serialize';
import { PaginateCustomerSerialize } from './serialize/paginate-customer.serialize';
import { UpdateCustomerDTO } from './dto/update-customer.dto';
import { UpdateCustomerSerialize } from './serialize/update-customer.serialize';
import { FindCustomerSerialize } from './serialize/find-customer.serialize';
import { UpdateOrderCustomerDTO } from './dto/update-order-customer.dto';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { GetCustomerSerialize } from './serialize/get-customer.serialize';
import { CreateCustomerContactDTO } from './dto/create-customer-contact.dto';
import { CustomerContactService } from '../customer-contacts/customer-contact.service';

@Controller('tenants/customers')
@UseGuards(AuthGuard('tenant-jwt'))
export class CustomersController extends BaseController {
	constructor(
		private readonly service: CustomerService,
		private readonly customerContactService: CustomerContactService,
	) {
		super();
	}

	@Get('/')
	@Serialize(GetCustomerSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async getCustomer() {
		const customer = await this.service.repo.find({
			relations: ['customerContacts', 'salesIncharges'],
			order: {
				orderNo: 'ASC',
			},
		});

		return this.response(customer);
	}

	@Post('/:id/contacts/create')
	// @Serialize(GetCustomerSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async createCustomerContact(
		@Param('id') id: string,
		@Body() createCustomerContactDTO: CreateCustomerContactDTO,
	) {
		let oldIds = [];
		const siteOperationData = createCustomerContactDTO.contacts.map(
			(contact, index) => {
				contact.id && oldIds.push(contact.id);
				let contactId = contact.id || generateId();
				return this.customerContactService.repo.create({
					...contact,
					id: contactId,
					customerId: id,
				});
			},
		);

		await this.customerContactService.removeOtherCustomerContactsByIds(
			oldIds,
			id,
		);

		await this.customerContactService.repo.save(siteOperationData);

		return this.response(siteOperationData, {
			title: 'create customer contacts success',
		});
	}

	@Get('/paginate')
	@Serialize(PaginateCustomerSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async paginateCustomer(@Query() query: any) {
		const paginate = await this.service.paginateCustomer(query);

		return this.paginateResponse(paginate.items, paginate.meta);
	}

	//find id
	@Get('/:id')
	@Serialize(FindCustomerSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async findCustomer(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['customer does not exists']);

		const data = await this.service.repo
			.createQueryBuilder('customers')
			.leftJoinAndSelect('customers.salesIncharges', 'salesIncharges')
			.leftJoinAndSelect('salesIncharges.staff', 'staff')
			.leftJoinAndSelect('customers.customerContacts', 'customerContacts')
			.loadRelationCountAndMap('customers.sitesCount', 'customers.sites')
			.where('customers.id = :id', { id })
			.getOne();

		return this.response(data);
	}

	//update order
	@Patch('/update/order')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOrderCustomer(
		@Body() updateOrderCustomerDTO: UpdateOrderCustomerDTO,
	) {
		const data = updateOrderCustomerDTO.ids.map((id, index) => {
			return this.service.repo.create({
				id,
				orderNo: index + 1,
			});
		});

		this.service.repo.save(data);

		return this.response(null, { title: 'customer order update success' });
	}

	@Post('/create')
	@Serialize(CreateCustomerSerialize)
	@InjectRequestToBody()
	async createCustomer(@Body() createCustomerDTO: CreateCustomerDTO) {
		const data = await this.service.createCustomer(createCustomerDTO);

		return this.response(data, { title: 'customer create success' });
	}

	@Patch('/:id/update')
	@InjectRequestToBody()
	@Serialize(UpdateCustomerSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async updateCustomer(
		@Param('id') id: string,
		@Body() updateCustomerDTO: UpdateCustomerDTO,
	) {
		const data = await this.service.updateCustomer(id, updateCustomerDTO);
		return this.response(data, { title: 'customer update success' });
	}

	@Delete('/:id')
	@UseGuards(AuthGuard('site-admin'))
	async deleteCustomer(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['customer does not exists']);

		Promise.all([
			this.service.repo.softDelete(id),
			this.service.salesInchargeRepo.softDelete({
				customerId: id,
			}),
		]).catch((err) => {
			Promise.all([
				this.service.repo.restore(id),
				this.service.salesInchargeRepo.restore({
					customerId: id,
				}),
			]);

			throw new BadRequestException(err);
		});

		return this.response(null, { title: 'customer deleted success' });
	}

	@Patch('/:id/restore')
	@UseGuards(AuthGuard('site-admin'))
	async restoreCustomer(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['customer does not exists']);

		this.service.repo.restore(id);

		return this.response(null, { title: 'customer restore success' });
	}
}
