import { Exclude, Expose, Type } from 'class-transformer';

export class CreateCustomerSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	// @Expose()
	// nameFurigana: string;

	// @Expose()
	// businessTypes: string;

	// @Expose()
	// postalCode: string;

	// @Expose()
	// addressPrefecture: string;

	// @Expose()
	// addressCity: string;

	// @Expose()
	// addressBuilding: string;

	// @Expose()
	// phone: string;

	// @Expose()
	// fax: string;

	// @Expose()
	// orderNo: number;
}
