import { Exclude, Expose, Type } from 'class-transformer';

export class UpdateCustomerSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;

	@Expose()
	phone: string;

	@Expose()
	orderNo: number;

	@Expose()
	@Type(() => SalesIncharge)
	salesIncharges: SalesIncharge[]; // postman's result
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;
}

class SalesIncharge {
	@Expose()
	id: string;

	@Expose()
	@Type(() => Staff)
	staff: Staff; // postman's result

	// @Expose()
	// phone: string;

	// @Expose()
	// email: string;
}
