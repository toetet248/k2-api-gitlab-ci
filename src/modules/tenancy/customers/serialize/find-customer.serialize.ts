import { Exclude, Expose, Type } from 'class-transformer';

export class FindCustomerSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameFurigana: string;

	@Expose()
	businessTypes: string;

	@Expose()
	postalCode: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;

	@Expose()
	phone: string;

	@Expose()
	fax: string;

	@Expose()
	@Type(() => SalesIncharge)
	salesIncharges: SalesIncharge[]; // postman's result

	@Expose()
	@Type(() => CustomerContacts)
	customerContacts: CustomerContacts[];

	@Expose()
	sitesCount: number;
}

class CustomerContacts {
	@Expose()
	id: string;

	@Expose()
	customerId: string;

	@Expose()
	name: string;

	@Expose()
	phone: string;

	@Expose()
	email: string;
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	email: string;

	@Expose()
	phone: string;
}

class SalesIncharge {
	@Expose()
	id: string;

	@Expose()
	@Type(() => Staff)
	staff: Staff; // postman's result

	// @Expose()
	// phone: string;

	// @Expose()
	// email: string;
}
