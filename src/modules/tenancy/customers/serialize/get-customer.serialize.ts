import { Expose } from 'class-transformer';

export class GetCustomerSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;
}
