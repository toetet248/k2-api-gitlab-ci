import { Expose, Type } from 'class-transformer';

export class PaginateCustomerSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;

	@Expose()
	phone: string;

	@Expose()
	orderNo: number;

	@Expose()
	sitesCount: number;

	// @Expose({ name: 'staffs' }) // staffs => response from controller
	// @Type(() => Staff)
	// salesIncharges: Staff[]; // postman's result

	@Expose({ name: 'salesIncharges' }) // staffs => response from controller
	@Type(() => SalesIncharge)
	salesIncharges: SalesIncharge[]; // postman's result
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;
}

class SalesIncharge {
	@Expose()
	id: string;

	@Expose()
	@Type(() => Staff)
	staff: Staff[]; // postman's result
}
