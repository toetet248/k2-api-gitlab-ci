import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { CustomerContact } from '../customer-contacts/customer-contact.entity';
import { SalesIncharge } from '../sales-incharges/sales-incharge.entity';
import { Staff } from '../staffs/staff.entity';
import { Site } from '../sites/site.entity';

@Entity({ name: 'customers' })
export class Customer extends MainEntity {
	@Column()
	name: string;

	@Column()
	nameFurigana: string;

	@Column()
	businessTypes: string;

	@Column()
	postalCode: string;

	@Column()
	addressPrefecture: string;

	@Column()
	addressCity: string;

	@Column()
	addressBuilding: string;

	@Column()
	phone: string;

	@Column()
	fax: string;

	@Column()
	orderNo: number;

	@OneToMany(
		() => CustomerContact,
		(customerContact) => customerContact.customer,
	)
	customerContacts: CustomerContact[];

	@OneToMany(() => SalesIncharge, (saleIncharge) => saleIncharge.customer)
	salesIncharges: SalesIncharge[];

	@OneToMany(() => Site, (site) => site.customer)
	customers: Staff[];

	@ManyToMany(() => Staff, (staff) => staff.customers)
	@JoinTable({
		name: 'sales_incharges',
		joinColumn: {
			name: 'customerId',
			referencedColumnName: 'id',
		},
		inverseJoinColumn: {
			name: 'staffId',
			referencedColumnName: 'id',
		},
	})
	staffs: Staff[];

	@OneToMany(() => Site, (site) => site.customer)
	sites: Site[];
}
