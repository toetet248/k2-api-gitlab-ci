import { BadRequestException, Inject, Injectable } from '@nestjs/common';

import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { Brackets, Connection, Repository } from 'typeorm';
import { CustomerContact } from '../customer-contacts/customer-contact.entity';
import { CustomerContactService } from '../customer-contacts/customer-contact.service';
import { SalesIncharge } from '../sales-incharges/sales-incharge.entity';
import { SalesInchargeService } from '../sales-incharges/sales-incharge.service';
import { Staff } from '../staffs/staff.entity';
import { StaffService } from '../staffs/staff.service';
import { CONNECTION } from '../tenancy.symbols';
import { Customer } from './customer.entity';
import { CreateCustomerDTO } from './dto/create-customer.dto';
import { UpdateCustomerDTO } from './dto/update-customer.dto';

@Injectable()
export class CustomerService {
	public readonly repo: Repository<Customer>;
	public readonly customerContactRepo: Repository<CustomerContact>;
	public readonly salesInchargeRepo: Repository<SalesIncharge>;
	public readonly staffRepo: Repository<Staff>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(CustomerContactService)
		private customerContactService: CustomerContactService,
		@Inject(SalesInchargeService)
		private salesInchargeService: SalesInchargeService,
	) {
		this.repo = connection.getRepository(Customer);
		this.customerContactRepo = connection.getRepository(CustomerContact);
		this.salesInchargeRepo = connection.getRepository(SalesIncharge);
		this.staffRepo = connection.getRepository(Staff);
	}

	//list and keyword
	async paginateCustomer({ page, search, contactName, staffId }) {
		if (!page) page = 1;
		const query = this.repo
			.createQueryBuilder('customers')
			// .leftJoinAndSelect('customers.staffs', 'staffs')
			.leftJoinAndSelect('customers.salesIncharges', 'salesIncharge')
			.loadRelationCountAndMap('customers.sitesCount', 'customers.sites')
			.leftJoinAndSelect('salesIncharge.staff', 'staffs')
			.orderBy('customers.orderNo', 'ASC');

		if (search)
			query.andWhere(
				new Brackets((q) => {
					return (
						q
							.where('customers.name like :name', {
								name: `%${search}%`,
							})
							.orWhere(
								'customers.addressPrefecture like :addressPrefecture',
								{
									addressPrefecture: `%${search}%`,
								},
							)
							.orWhere(
								'customers.addressCity like :addressCity',
								{
									addressCity: `%${search}%`,
								},
							)
							// .orWhere('staffs.name like :staffname', {
							// 	staffname: `%${search}%`,
							// })
							.orWhere('staffs.nameOnTab like :staffname', {
								staffname: `%${search}%`,
							})
					);
				}),
			);

		if (staffId)
			query.andWhere('salesIncharge.staffId IN (:staffId)', {
				staffId,
			});

		// const data = await paginate<Customer>(query, { page, limit: 10 });
		const data = await query.paginate(50);

		return {
			items: data.data,
			meta: {
				totalItems: data.total,
				itemCount: data.data.length,
				itemsPerPage: data.per_page,
				totalPages: data.last_page,
				currentPage: data.current_page,
			},
		};
	}

	//create customer
	async createCustomer(createCustomerDTO: CreateCustomerDTO) {
		// if (
		// 	!(await validateExists(this.staffRepo, [
		// 		'id',
		// 		createCustomerDTO.salesIncharges.map((si) => si.staffId),
		// 	]))
		// )
		// 	throw new BadRequestException(['staffId does not exists']);

		// set order no to create data
		const orderNo =
			(await (
				await this.repo
					.createQueryBuilder('customers')
					.select('MAX(customers.orderNo)', 'max')
					.getRawOne()
			).max) + 1;

		const requestData = {
			name: createCustomerDTO.name,
			nameFurigana: createCustomerDTO.nameFurigana,
			businessTypes: createCustomerDTO.businessTypes,
			postalCode: createCustomerDTO.postalCode,
			addressPrefecture: createCustomerDTO.addressPrefecture,
			addressCity: createCustomerDTO.addressCity,
			addressBuilding: createCustomerDTO.addressBuilding,
			phone: createCustomerDTO.phone,
			fax: createCustomerDTO.fax,
			customerContacts: createCustomerDTO.customerContacts,
			salesIncharges: createCustomerDTO.salesIncharges,
		};

		//create customer
		const data = this.repo.create({
			...requestData,
			id: generateId(),
			orderNo,
		});
		await this.repo.save(data);

		//save and update customer contact
		const customerContactData = createCustomerDTO.customerContacts.map(
			(customerContact, index) => {
				const contactReqData = {
					name: customerContact.name,
					email: customerContact.email,
					phone: customerContact.phone,
				};

				return this.customerContactRepo.create({
					...contactReqData,
					id: generateId(),
					customerId: data.id,
				});
			},
		);
		await this.customerContactRepo.save(customerContactData);

		//save and update salesIncharge
		const salesInchargeData = createCustomerDTO.salesIncharges.map(
			(salesIncharge) => {
				const inchargeReqData = {
					staffId: salesIncharge.staffId,
				};

				return this.salesInchargeRepo.create({
					...inchargeReqData,
					id: generateId(),
					customerId: data.id,
				});
			},
		);
		// await this.salesInchargeRepo.save(salesInchargeData);
		await this.salesInchargeRepo.upsert(salesInchargeData, [
			'staffId',
			'customerId',
		]);

		return data;
	}

	//update
	async updateCustomer(id: string, updateCustomerDTO: UpdateCustomerDTO) {
		const customerData = await this.repo.findOne({
			where: { id },
		});

		if (customerData) {
			// if (
			// 	!(await validateExists(this.salesInchargeRepo, [
			// 		'id',
			// 		updateCustomerDTO.salesIncharges
			// 			.map((salesIncharge) => salesIncharge.id)
			// 			.filter((salesInchargeId) => salesInchargeId),
			// 	]))
			// )
			// 	throw new BadRequestException([
			// 		'salesInchargeId does not exists',
			// 	]);

			// if (
			// 	!(await validateExists(this.staffRepo, [
			// 		'id',
			// 		updateCustomerDTO.salesIncharges
			// 			.map((salesIncharge) => salesIncharge.staffId)
			// 			.filter((staffId) => staffId),
			// 	]))
			// )
			// 	throw new BadRequestException(['staffId does not exists']);

			// no need to use filter
			// if (
			// 	!(await validateExists(this.customerContactRepo, [
			// 		'id',
			// 		updateCustomerDTO.customerContacts
			// 			.map((customerContact) => customerContact.id)
			// 			.filter((customerContactId) => customerContactId),
			// 	]))
			// )
			// 	throw new BadRequestException([
			// 		'customerContactId does not exists',
			// 	]);

			//destructing with relation data
			let { customerContacts, salesIncharges, ...customer } =
				updateCustomerDTO;

			const requestData = {
				name: customer.name,
				nameFurigana: customer.nameFurigana,
				businessTypes: customer.businessTypes,
				postalCode: customer.postalCode,
				addressPrefecture: customer.addressPrefecture,
				addressCity: customer.addressCity,
				addressBuilding: customer.addressBuilding,
				phone: customer.phone,
				fax: customer.fax,
			};

			await this.repo.save({
				...customerData,
				...requestData,
			});

			//save and update customer contact
			const customerContactData = customerContacts.map(
				(customerContact, index) => {
					if (!customerContact.id) customerContact.id = generateId();
					const contactReqData = {
						id: customerContact.id ?? generateId(),
						customerId: id,
						name: customerContact.name,
						email: customerContact.email,
						phone: customerContact.phone,
					};

					return this.customerContactRepo.create({
						...contactReqData,
						customerId: id,
					});
				},
			);

			await this.customerContactRepo.save(customerContactData);

			// remove customer contacts
			const customerContactIds = customerContactData.map((d) => d.id);
			this.customerContactService.removeOtherCustomerContactsByIds(
				customerContactIds,
				id,
			);

			//save and update salesIncharge
			const salesInchargeData = salesIncharges.map((salesIncharge) => {
				if (!salesIncharge.id) salesIncharge.id = generateId();
				const inchargeReqData = {
					id: salesIncharge.id ?? generateId(),
					staffId: salesIncharge.staffId,
				};

				return this.salesInchargeRepo.create({
					...inchargeReqData,
					customerId: id,
				});
			});
			// await this.salesInchargeRepo.save(salesInchargeData);
			await this.salesInchargeRepo.upsert(salesInchargeData, [
				'staffId',
				'customerId',
			]);

			// remove sales Incharges
			const salesInchargeIds = salesInchargeData.map((d) => d.id);
			await this.salesInchargeService.removeOtherSalesInchargesByIds(
				salesInchargeIds,
				id,
			);

			const updatedCustomer = this.repo.findOne({
				where: { id },
				relations: [
					'salesIncharges',
					'salesIncharges.staff',
					'customerContacts',
				],
			});

			return updatedCustomer;
		}

		throw new BadRequestException(['customer not found']);
	}

	//delete customer
	// async deleteCustomer(id: string) {
	// 	if (!(await validateExists(this.repo, ['id', id])))
	// 		throw new BadRequestException(['customer does not exists']);

	// 	const deleteCustomerContact = await this.customerContactRepo.find({
	// 		where: {
	// 			customerId: id,
	// 		},
	// 	});
	// 	await this.customerContactRepo.softRemove(deleteCustomerContact);

	// 	const deleteSalesIncharge = await this.salesInchargeRepo.find({
	// 		where: {
	// 			customerId: id,
	// 		},
	// 	});
	// 	await this.salesInchargeRepo.softRemove(deleteSalesIncharge);

	// 	await this.repo.softDelete(id);
	// }
}
