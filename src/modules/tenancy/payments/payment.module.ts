import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { InvoiceModule } from 'src/modules/central/invoices/invoice.module';
import { PlanModule } from 'src/modules/central/plans/plan.module';
import { SubscriptionModule } from 'src/modules/central/subscriptions/subscription.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';
import { PdfService } from 'src/common/service/pdf.service';
import { PaymentController } from './payment.controller';

@Module({
	imports: [
		TenantModule,
		CommonModule,
		PlanModule,
		SubscriptionModule,
		InvoiceModule,
	],
	controllers: [PaymentController],
	providers: [PdfService],
})
export class PaymentModule {}
