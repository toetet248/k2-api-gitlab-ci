import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Headers,
	Inject,
	Param,
	Patch,
	Post,
	Res,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import moment from 'moment';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { GmoService } from 'src/common/service/gmo.service';
import { todayDate } from 'src/common/service/helper.service';
import { InvoiceService } from 'src/modules/central/invoices/invoice.service';
import { Plan } from 'src/modules/central/plans/plan.entity';
import { PlanService } from 'src/modules/central/plans/plan.service';
import { SubscriptionService } from 'src/modules/central/subscriptions/subscription.service';
import { Tenant } from 'src/modules/central/tenants/tenant.entity';
import { TenantService } from 'src/modules/central/tenants/tenant.service';
import { PdfService } from 'src/common/service/pdf.service';
import { AuthStaff } from '../auth/decorator/auth-staff.decorator';
import { Staff } from '../staffs/staff.entity';
import { TENANT_HEADER } from '../tenancy.middleware';
import { CreatePaymentMethodDTO } from './dto/create-payment-method.dto';
import { DeletePaymentMethodDTO } from './dto/delete-payment-method.dto';
import { SubscribePlanDTO } from './dto/subscribe-plan.dto';
import { UpdateDefaultMethodDTO } from './dto/update-default-method.dto';
import { UpgradePlanDTO } from './dto/upgrade-plan.dto';
import { GetPlansSerialize } from './serialize/get-plans.serialize';

@Controller('tenants/payments')
@UseGuards(AuthGuard('tenant-jwt'))
export class PaymentController extends BaseController {
	constructor(
		private gmoService: GmoService,
		@Inject(TenantService)
		private tenantService: TenantService,
		@Inject(PlanService)
		private readonly planService: PlanService,
		@Inject(SubscriptionService)
		private readonly subscriptionService: SubscriptionService,
		@Inject(InvoiceService)
		private readonly invoiceService: InvoiceService,
		@Inject(PdfService)
		private readonly pdfService: PdfService,
	) {
		super();
	}

	@Get('index')
	// @UseGuards(AuthGuard('system-admin'))
	async getIndex(@Headers(TENANT_HEADER) tenantName: string) {
		// get active subscription
		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);

		// get current plan - start
		const currentPlan = await this.tenantService.currentPlan(
			activeSubscription,
		);
		// get current plan - end

		// get default payment - start
		const isGmoRegister = await this.tenantService.isGmoRegister(
			tenantName,
		);
		const gmoId = await this.tenantService.getGmoId(tenantName);

		const defaultPayment = await this.gmoService.getDefaultPayment(
			gmoId,
			isGmoRegister,
		);
		// get default payment - end

		// get upcoming Invoice
		const upcomingInvoice = await this.gmoService.upcomingInvoice(
			activeSubscription,
		);

		// get invoices
		const invoices = await this.invoiceService.getInvoicesByTenantName(
			tenantName,
		);

		return this.response(
			{
				currentPlan,
				activeSubscription: activeSubscription ?? null,
				defaultPayment: defaultPayment ?? null,
				upcomingInvoice: upcomingInvoice ?? null,
				invoices,
			},
			{
				title: 'get payment index data success',
			},
		);
	}

	@Get('methods')
	@UseGuards(AuthGuard('system-admin'))
	async getPaymentMethods(@Headers(TENANT_HEADER) tenantName: string) {
		const isGmoRegister = await this.tenantService.isGmoRegister(
			tenantName,
		);
		const gmoId = await this.tenantService.getGmoId(tenantName);

		let paymentMethods = await this.gmoService.getPaymentMethods(
			gmoId,
			isGmoRegister,
		);

		if (!Array.isArray(paymentMethods)) paymentMethods = [paymentMethods];

		return this.response(paymentMethods, {
			title: 'get payment methods success',
		});
	}

	async createPaymentMethod(
		tenantName: string,
		createPaymentMethodDTO: CreatePaymentMethodDTO | SubscribePlanDTO,
		authStaff: Staff,
	) {
		let card: Object;

		const isGmoRegister = await this.tenantService.isGmoRegister(
			tenantName,
		);
		const tenant = await this.tenantService.find(tenantName);

		if (!isGmoRegister) {
			const createGmoMember = await this.gmoService.createGmoMember(
				tenantName,
				tenant.companyName,
			);

			this.tenantService.setGmoId(
				tenantName,
				createGmoMember['MemberID'],
			);

			card = await this.gmoService.addPaymentMethod(
				createPaymentMethodDTO,
				createGmoMember['MemberID'],
				true,
			);

			this.sendPaymentMethodMail(tenant, authStaff);
		} else {
			const gmoId = await this.tenantService.getGmoId(tenantName);

			const defaultPayment = await this.gmoService.getDefaultPayment(
				gmoId,
				isGmoRegister,
			);

			card = await this.gmoService.addPaymentMethod(
				createPaymentMethodDTO,
				gmoId,
				!defaultPayment ? true : false,
			);

			if (!defaultPayment) this.sendPaymentMethodMail(tenant, authStaff);
		}

		return card;
	}

	async sendPaymentMethodMail(tenant: Tenant, authStaff: Staff) {
		const activeSubscription = await this.tenantService.activeSubscription(
			tenant.name,
		);

		this.gmoService.sendPaymentMethodMail(
			authStaff,
			tenant.customerId,
			activeSubscription,
		);
	}

	@Post('methods/create')
	@UseGuards(AuthGuard('system-admin'))
	async createPaymentMethods(
		@Headers(TENANT_HEADER) tenantName: string,
		@Body() createPaymentMethodDTO: CreatePaymentMethodDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const card = await this.createPaymentMethod(
			tenantName,
			createPaymentMethodDTO,
			authStaff,
		);

		return this.response(card, { title: 'create payment method success' });
	}

	@Patch('methods/default')
	@UseGuards(AuthGuard('system-admin'))
	async updaetDefaultMethods(
		@Headers(TENANT_HEADER) tenantName: string,
		@Body() updateDefaultMethodDTO: UpdateDefaultMethodDTO,
	) {
		const gmoId = await this.tenantService.getGmoId(tenantName);
		const card = await this.gmoService.updateDefaultMethod(
			updateDefaultMethodDTO,
			gmoId,
		);
		return this.response(
			{ card, success: true },
			{ title: 'update default payment method success' },
		);
	}

	@Delete('methods/delete')
	@UseGuards(AuthGuard('system-admin'))
	async deletePaymentMethod(
		@Headers(TENANT_HEADER) tenantName: string,
		@Body() deletePaymentMethodDTO: DeletePaymentMethodDTO,
	) {
		const gmoId = await this.tenantService.getGmoId(tenantName);
		await this.gmoService.deletePaymentMethod(
			gmoId,
			deletePaymentMethodDTO.cardSequence,
		);

		return this.response(null, { title: 'delete payment method success' });
	}

	async subscribe(tenant: Tenant, plan: Plan, authStaff: Staff) {
		let recurringId = this.gmoService.generateRecurringId();

		while (
			(await this.subscriptionService.repo.count({ recurringId })) > 0
		) {
			recurringId = this.gmoService.generateRecurringId();
		}

		await this.gmoService.createNewSubscription(tenant, plan, recurringId);

		await this.subscriptionService.createSubscription(
			tenant.id,
			recurringId,
			plan.id,
		);

		this.gmoService.sendPlanChangeMail(authStaff, tenant.customerId, plan);
	}

	@Get('plans/get')
	@Serialize(GetPlansSerialize)
	@UseGuards(AuthGuard('system-admin'))
	async getPlans() {
		const plans = await this.planService.repo.find();

		return this.response(plans, { title: 'get plans success' });
	}

	@Post('plans/upgrade')
	@UseGuards(AuthGuard('system-admin'))
	async upgradePlan(
		@Headers(TENANT_HEADER) tenantName: string,
		@Body() upgradePlanDTO: UpgradePlanDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const isGmoRegister = await this.tenantService.isGmoRegister(
			tenantName,
		);

		if (!isGmoRegister) throw new BadRequestException(['invalid card']);

		const tenant = await this.tenantService.find(tenantName);

		const defaultPayment = await this.gmoService.getDefaultPayment(
			tenant.gmoId,
			isGmoRegister,
		);

		if (!defaultPayment) throw new BadRequestException(['invalid card']);

		const [plan, currentSubscription] = await Promise.all([
			this.planService.getPlan(upgradePlanDTO.planId),
			this.tenantService.currentSubscription(tenantName),
		]);

		if (!currentSubscription) {
			await this.subscribe(tenant, plan, authStaff);

			return this.response(null, { title: 'upgrade plan success' });
		}

		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);
		await this.gmoService.upcomingInvoice(activeSubscription);

		await this.gmoService.deleteSubscription(
			activeSubscription.recurringId,
		);
		delete activeSubscription.tenant;
		await this.subscriptionService.repo.save({
			...activeSubscription,
			endedAt: todayDate(),
			status: 'cancelled',
		});

		await this.subscribe(tenant, plan, authStaff);

		return this.response(null, { title: 'upgrade plan success' });
	}

	@Post('plans/subscribe')
	@UseGuards(AuthGuard('system-admin'))
	async subscribePlan(
		@Headers(TENANT_HEADER) tenantName: string,
		@Body() subscribePlanDTO: SubscribePlanDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const [card, plan] = await Promise.all([
			this.createPaymentMethod(tenantName, subscribePlanDTO, authStaff),
			this.planService.getPlan(subscribePlanDTO.planId),
		]);

		const tenant = await this.tenantService.find(tenantName);

		await this.subscribe(tenant, plan, authStaff);

		return this.response(null, { title: 'subscribe plan success' });
	}

	@Get('invoices/:id/download')
	async downloadInvoice(@Param('id') id: string, @Res() res: any) {
		let invoice = await this.invoiceService.repo.findOne(id, {
			relations: ['tenant'],
		});

		if (invoice) {
			const buffer = await this.pdfService.generatePDF(invoice);
			res.set({
				'Content-Type': 'application/pdf',
				'Content-Disposition': `attachment; filename=invoice${moment(
					invoice.processDate,
				).format('YYYYMMDD')}.pdf`,
				'Content-Length': buffer.length,
			});
			// return this.response({ data: invoice });

			res.end(buffer);
		}
	}

	@Get('invoices/:id/download/base64')
	async downloadInvoiceBase64(@Param('id') id: string, @Res() res: any) {
		let invoice = await this.invoiceService.repo.findOne(id, {
			relations: ['tenant'],
		});

		if (invoice) {
			const buffer = await this.pdfService.generatePDF(invoice);

			res.set({
				'Content-Type': 'application/octet-stream',
				// 'Content-Disposition': `attachment; filename=invoice${moment(invoice.processDate).format('YYYYMMDD')}.pdf`,
				// 'Content-Length': buffer.length,
			});
			// return this.response({ data: invoice });

			res.end(buffer.toString('base64'));

			// return this.response({ data: buffer });
		}
	}
}
