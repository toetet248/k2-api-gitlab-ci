import { Expose } from 'class-transformer';

export class GetPlansSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameFurigana: string;

	@Expose()
	price: number;

	@Expose()
	options: any;
}
