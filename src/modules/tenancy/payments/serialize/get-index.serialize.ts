import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class GetIndexSerialize {
	@Expose()
	@Type(() => CurrentPlan)
	currentPlan: CurrentPlan;

	@Expose()
	@Type(() => ActiveSubscription)
	activeSubscription: ActiveSubscription;

	@Expose()
	defaultPayment: any;

	@Expose()
	upcomingInvoice: any;

	@Expose()
	@Type(() => Invoice)
	invoices: Invoice[];
}

class CurrentPlan {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameFurigana: string;

	@Expose()
	price: number;

	@Expose()
	options: any;
}

class ActiveSubscription {
	@Expose()
	id: string;

	@Expose()
	recurringId: string;

	@Expose()
	status: string;

	@Expose()
	startedAt: string;

	@Expose()
	endedAt: string;
}

class Invoice {
	@Expose()
	id: string;

	@Expose()
	tenantId: string;

	@Expose()
	subscriptionId: string;

	@Expose()
	recurringId: string;

	@Expose()
	planId: string;

	@Expose()
	orderId: string;

	@Expose()
	amount: number;

	@Expose()
	tax: number;

	@Expose()
	@Transform(({ value }) => moment(value).format('YYYY/MM/DD'))
	processDate: Date;

	@Expose()
	status: string;
}
