import { IsNotEmpty } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpgradePlanDTO {
	@IsNotEmpty()
	@IsExist('plans', 'id')
	planId: string;
}
