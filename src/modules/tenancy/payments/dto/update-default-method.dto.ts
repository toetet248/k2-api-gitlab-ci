import { IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateDefaultMethodDTO {
	@IsNotEmpty()
	CardSeq: string;

	@IsNotEmpty()
	Expire: string;

	@IsOptional()
	HolderName: string;

	@IsOptional()
	CardName: string;
}
