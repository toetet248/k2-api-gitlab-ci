import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreatePaymentMethodDTO {
	@IsOptional()
	cardName: string;

	@IsNotEmpty()
	token: string;
}
