import { IsNotEmpty } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class SubscribePlanDTO {
	@IsNotEmpty()
	cardName: string;

	@IsNotEmpty()
	token: string;

	@IsNotEmpty()
	@IsExist('plans', 'id')
	planId: string;
}
