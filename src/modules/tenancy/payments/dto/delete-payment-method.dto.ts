import { IsNotEmpty } from 'class-validator';

export class DeletePaymentMethodDTO {
	@IsNotEmpty()
	cardSequence: string;
}
