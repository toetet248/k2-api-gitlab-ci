import { Inject, Injectable } from '@nestjs/common';
import { generateId } from 'src/common/service/helper.service';
import { Connection, In, Repository } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { CONNECTION } from '../tenancy.symbols';
import { Vehicle } from '../vehicles/vehicle.entity';
import { CreateScheduleVehicleDTO } from '../schedulers/dto/create-schedule-vehicle.dto';
import { ScheduleVehicle } from './schedule-vehicle.entity';

@Injectable()
export class ScheduleVehicleService {
	public readonly repo: Repository<ScheduleVehicle>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly vehicleRepo: Repository<Vehicle>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(ScheduleVehicle);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.vehicleRepo = connection.getRepository(Vehicle);
	}

	async createScheduleVehicle(
		createScheduleVehicleDTO: CreateScheduleVehicleDTO,
	) {
		const {
			dailyNoteId,
			vehicleIds,
			// dayVehicleIds,
			// supportVehicleIds,
			isUpdateToday,
			isUpdateTomorrowToEndDate,
		} = createScheduleVehicleDTO;

		// if (!(await validateExists(this.dailyNoteRepo, ['id', dailyNoteId])))
		// 	throw new BadRequestException(['DailyNoteId does not exists']);

		// if (
		// 	!(await validateExists(this.vehicleRepo, [
		// 		'id',
		// 		dayVehicleIds
		// 			.map((vId) => vId)
		// 			.filter((vehicleId) => vehicleId),
		// 	]))
		// )
		// 	throw new BadRequestException(['dayVehicleIds does not exists']);

		// if (
		// 	!(await validateExists(this.vehicleRepo, [
		// 		'id',
		// 		supportVehicleIds
		// 			.map((vId) => vId)
		// 			.filter((vehicleId) => vehicleId),
		// 	]))
		// )
		// 	throw new BadRequestException([
		// 		'supportVehicleIds does not exists',
		// 	]);

		if (isUpdateToday) {
			// if isUpdateToday is true
			// find scheduleVehicles to given dailyNoteId
			const findScheduleVehicleData = await this.repo.find({
				where: { dailyNoteId },
			});

			// delete scheduleVehicles to given dailyNoteId
			await this.repo.remove(findScheduleVehicleData);

			const scheduleVehicleData = vehicleIds.map(
				({ vehicleId, type }, index) => {
					return this.repo.create({
						id: generateId(),
						dailyNoteId,
						dayNightSupport: type,
						orderNo: index + 1,
						vehicleId,
					});
				},
			);

			await this.repo.save(scheduleVehicleData);

			// const dayScheduleVehicleData = dayVehicleIds.map(
			// 	(vehicleId, index) => {
			// 		return this.repo.create({
			// 			id: generateId(),
			// 			dailyNoteId,
			// 			dayNightSupport: 'day',
			// 			orderNo: index + 1,
			// 			vehicleId,
			// 		});
			// 	},
			// );

			// const supportScheduleVehicleData = supportVehicleIds.map(
			// 	(vehicleId, index) => {
			// 		return this.repo.create({
			// 			id: generateId(),
			// 			dailyNoteId,
			// 			dayNightSupport: 'support',
			// 			orderNo: index + 1,
			// 			vehicleId,
			// 		});
			// 	},
			// );

			// await this.repo.save(
			// 	dayScheduleVehicleData.concat(supportScheduleVehicleData),
			// );
		}
		// else {
		// 	throw new BadRequestException(['isUpdateToday must be true']);
		// }

		if (isUpdateTomorrowToEndDate) {
			// find siteId, operationId from given dailyNoteId
			const { siteId, operationId, scheduleDate } =
				await this.dailyNoteRepo.findOne({
					select: ['siteId', 'operationId', 'scheduleDate'],
					where: { id: dailyNoteId },
				});

			const getTomorrowToEndDateDailyNoteIds = await this.dailyNoteRepo
				.createQueryBuilder('daily_notes')
				.select(['daily_notes.id'])
				.where('daily_notes.scheduleDate > :scheduleDate', {
					scheduleDate,
				})
				.andWhere('daily_notes.siteId = :siteId', {
					siteId,
				})
				.andWhere('daily_notes.operationId = :operationId', {
					operationId,
				})
				.getMany();

			const tomorrowToEndDateDailyNoteIds =
				getTomorrowToEndDateDailyNoteIds.map((did) => {
					return did.id;
				});

			// delete scheduleVehicles  (where dayNightSupport is day) ( whereIn dailyNoteIds  )
			// const findDelDayVehicle = await this.repo.find({
			// 	where: {
			// 		dailyNoteId: In(tomorrowToEndDateDailyNoteIds), //whereIn for array
			// 		dayNightSupport: 'day',
			// 	},
			// });
			// // await this.repo.remove(findDelDayVehicle);

			// const findDelSupporVehicle = await this.repo.find({
			// 	where: {
			// 		dailyNoteId: In(tomorrowToEndDateDailyNoteIds),
			// 		vehicleId: In(dayVehicleIds),
			// 		dayNightSupport: 'support',
			// 	},
			// });
			// await this.repo.remove(
			// 	findDelDayVehicle.concat(findDelSupporVehicle),
			// );

			const delVehicles = await this.repo.find({
				where: [
					{
						dailyNoteId: In(tomorrowToEndDateDailyNoteIds), //whereIn for array
						dayNightSupport: 'day',
					},
					{
						dailyNoteId: In(tomorrowToEndDateDailyNoteIds),
						vehicleId: In(
							vehicleIds
								.filter((vh) => vh.type === 'day')
								.map((vh) => vh.vehicleId),
						),
						dayNightSupport: 'support',
					},
				],
			});
			await this.repo.remove(delVehicles);

			const createtomorrowToEndDateScheduleVehicles = [];
			// loop dailyNoteIds and push create scheduleVehiclesDay with orderNo to new variable
			tomorrowToEndDateDailyNoteIds.map((dnId) => {
				const scheduleVehicleData = vehicleIds
					.filter((vh) => vh.type === 'day')
					.map(({ vehicleId }, index) => {
						return this.repo.create({
							id: generateId(),
							dailyNoteId: dnId,
							dayNightSupport: 'day',
							orderNo: index + 1,
							vehicleId,
						});
					});
				createtomorrowToEndDateScheduleVehicles.push(
					...scheduleVehicleData,
				);
			});

			// save scheduleVehiclesDay
			await this.repo.save(createtomorrowToEndDateScheduleVehicles);
		}

		// response schedueStaffs (where given dailyNoteId) (orderBy orderNo) (with staff) (with vehicleGroup)
		const resScheduleVehicle = await this.repo
			.createQueryBuilder('schedule_vehicles')
			.leftJoinAndSelect('schedule_vehicles.dailyNote', 'dailyNotes')
			.leftJoinAndSelect('schedule_vehicles.vehicle', 'vehicles')
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroups')
			.where('schedule_vehicles.dailyNoteId = :dailyNoteId', {
				dailyNoteId,
			})
			.orderBy('schedule_vehicles.orderNo', 'ASC')
			.getMany();

		return resScheduleVehicle;
	}
}
