import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { ScheduleVehicle } from './schedule-vehicle.entity';
import { ScheduleVehicleService } from './schedule-vehicle.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([ScheduleVehicle]),
		NestjsFormDataModule,
	],
	providers: [ScheduleVehicleService],
	exports: [ScheduleVehicleService],
})
export class ScheduleVehicleModule {}
