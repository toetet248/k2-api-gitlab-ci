import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { Vehicle } from '../vehicles/vehicle.entity';

@Entity({ name: 'schedule_vehicles' })
export class ScheduleVehicle extends MainEntity {
	@Column()
	vehicleId: string;

	@Column()
	dailyNoteId: string;

	@Column()
	dayNightSupport: string;

	@Column()
	nightSupportCount: number;

	@Column()
	orderNo: number;

	@ManyToOne(() => DailyNote, (dailyNote) => dailyNote.scheduleVehicles, {
		primary: true,
	})
	@JoinColumn({ name: 'dailyNoteId' })
	dailyNote: DailyNote;

	@ManyToOne(() => Vehicle, (vehicle) => vehicle.scheduleVehicles, {
		primary: true,
	})
	@JoinColumn({ name: 'vehicleId' })
	vehicle: Vehicle;
}
