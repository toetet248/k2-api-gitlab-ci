import * as configuration from '../../config/config';
import { Connection, getConnectionManager } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { map } from 'modern-async';
import { customTenantDatabaseSeeder } from 'src/database/seeds/tenant/tenant-db.seeder';

async function runTenantDatabaseSeeder() {
	const config = configuration.default();

	// get tenant names from central
	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const centralConnection = connectionManager.create(
		centralDBConfig as MysqlConnectionOptions,
	);

	const tenants = await (
		await centralConnection.connect()
	).query('SELECT id,name FROM tenants');

	// run migration for all tenant db
	const tenantDBConfig = config.database.tenant;

	await map(tenants, async (tenant: any) => {
		const tenantConnectionConfig = {
			...(tenantDBConfig as MysqlConnectionOptions),
			name: `${config.database.tenantDbPrefix + tenant.name}`,
			database: `${config.database.tenantDbPrefix + tenant.name}`,
		};

		const tenantConnection = await connectionManager
			.create(tenantConnectionConfig)
			.connect();

		await customTenantDatabaseSeeder(tenant.id, tenantConnection);

		await tenantConnection.close();
	});

	process.exit(1);
}

runTenantDatabaseSeeder();
