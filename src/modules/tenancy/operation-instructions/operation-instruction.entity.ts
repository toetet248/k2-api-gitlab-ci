import {
	BeforeInsert,
	Column,
	Entity,
	Generated,
	JoinColumn,
	ManyToOne,
	OneToMany,
} from 'typeorm';
import { Site } from '../sites/site.entity';
import { Operation } from '../operations/operation.entity';
import { MainEntity } from 'src/common/entity/main.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';

@Entity({ name: 'operation_instructions' })
export class OperationInstruction extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	operationId: string;

	@Column()
	orderNo: Number;

	@ManyToOne(() => Site, (site) => site.operationInstructions, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;

	@ManyToOne(
		() => Operation,
		(operation) => operation.operationInstructions,
		{
			primary: true,
		},
	)
	@JoinColumn({ name: 'operationId' })
	operation: Operation;

	@OneToMany(() => DailyNote, (dailyNote) => dailyNote.operationInstruction)
	dailyNotes: DailyNote[];
}
