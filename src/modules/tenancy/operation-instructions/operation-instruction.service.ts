import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { OperationInstruction } from './operation-instruction.entity';
import { generateId } from 'src/common/service/helper.service';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { Operation } from '../operations/operation.entity';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import moment from 'moment';
import { Site } from '../sites/site.entity';

@Injectable()
export class OperationInstructionService {
	public readonly repo: Repository<OperationInstruction>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly operationRepo: Repository<Operation>;
	public readonly scheduleStaffRepo: Repository<ScheduleStaff>;
	public readonly scheduleVehicleRepo: Repository<ScheduleVehicle>;
	public readonly siteRepo: Repository<Site>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(OperationInstruction);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.operationRepo = connection.getRepository(Operation);
		this.scheduleStaffRepo = connection.getRepository(ScheduleStaff);
		this.scheduleVehicleRepo = connection.getRepository(ScheduleVehicle);
		this.siteRepo = connection.getRepository(Site);
	}

	// async updateOperationInstruction(
	// 	siteId: string,
	// 	opIds: string[],
	// 	updateSiteOperationDTO: UpdateSiteOperationDTO,
	// ) {
	// 	const delOpIns = await this.repo.find({
	// 		where: {
	// 			siteId,
	// 			operationId: Not(In(opIds)),
	// 		},
	// 		select: ['id'],
	// 	});
	// 	const delOpInsIds = delOpIns.map((delOpIn) => delOpIn.id);

	// 	const delDailyNotesData = await this.dailyNoteRepo.find({
	// 		select: ['id'],
	// 		where: {
	// 			siteId,
	// 			operationInstructionId: In(delOpInsIds),
	// 		},
	// 	});
	// 	const delDailyNotesIds = delDailyNotesData.map(
	// 		(delDailyNote) => delDailyNote.id,
	// 	);

	// 	//delete scheduleStaff
	// 	this.scheduleStaffRepo.delete({
	// 		dailyNoteId: In(delDailyNotesIds),
	// 	});
	// 	//delete scheduleVehicle
	// 	this.scheduleVehicleRepo.delete({
	// 		dailyNoteId: In(delDailyNotesIds),
	// 	});
	// 	//delete dailyNotes
	// 	await this.dailyNoteRepo.remove(delDailyNotesData);

	// 	// get scheduleDate from DailyNotes
	// 	const getDailyNotes = await this.dailyNoteRepo.find({
	// 		where: {
	// 			siteId,
	// 			operationId: In(opIds),
	// 		},
	// 		select: ['id', 'scheduleDate'],
	// 	});

	// 	const dates = getDailyNotes.map((date) => date.scheduleDate);
	// 	const scheduleDates = dates.map((d) => moment(d, 'YYYY-MM-DD'));

	// 	// if dailyNotes are delete, update start date & end date in site table
	// 	let startDate = '';
	// 	let endDate = '';
	// 	if (scheduleDates.length != 0) {
	// 		startDate = moment.min(scheduleDates).format('YYYY-MM-DD');
	// 		endDate = moment.max(scheduleDates).format('YYYY-MM-DD');
	// 	} else {
	// 		startDate = '';
	// 		endDate = '';
	// 	}

	// 	await this.siteRepo.save({
	// 		id: siteId,
	// 		startDate,
	// 		endDate,
	// 	});

	// 	//delete operation instruction
	// 	await this.repo.remove(delOpIns);

	// 	return await map(updateSiteOperationDTO.operations, async (op) => {
	// 		const operationInstructionCondition = {
	// 			siteId,
	// 			operationId: op.id,
	// 		};

	// 		//orderNo is greater than 0
	// 		if (op.checkOrderNo) {
	// 			const operationInstruction = await this.repo.findOne({
	// 				where: operationInstructionCondition,
	// 			});

	// 			//creat data, if operationInstruction does not exist
	// 			if (!operationInstruction) {
	// 				const operationInstructionData = this.repo.create({
	// 					id: generateId(),
	// 					siteId,
	// 					operationId: op.id,
	// 					// orderNo: op.checkOrderNo,
	// 				});
	// 				await this.repo.save(operationInstructionData);
	// 			} else {
	// 				await this.repo.update(operationInstruction.id, {
	// 					// orderNo: op.checkOrderNo,
	// 				});
	// 			}
	// 		}
	// 	}).then(
	// 		async () =>
	// 			await this.repo.find({
	// 				where: {
	// 					siteId,
	// 				},
	// 			}),
	// 	);
	// }

	async updateOperationInstruction(siteId: string, opIds: string[]) {
		const delOpIns = await this.repo.find({
			where: {
				siteId,
				operationId: Not(In(opIds)),
			},
			select: ['id'],
		});
		const delOpInsIds = delOpIns.map((delOpIn) => delOpIn.id);

		const delDailyNotesData = await this.dailyNoteRepo.find({
			select: ['id'],
			where: {
				siteId,
				operationInstructionId: In(delOpInsIds),
			},
		});
		const delDailyNotesIds = delDailyNotesData.map(
			(delDailyNote) => delDailyNote.id,
		);

		//delete scheduleStaff
		this.scheduleStaffRepo.delete({
			dailyNoteId: In(delDailyNotesIds),
		});
		//delete scheduleVehicle
		this.scheduleVehicleRepo.delete({
			dailyNoteId: In(delDailyNotesIds),
		});
		//delete dailyNotes
		await this.dailyNoteRepo.remove(delDailyNotesData);

		// get scheduleDate from DailyNotes
		const getDailyNotes = await this.dailyNoteRepo.find({
			where: {
				siteId,
				operationId: In(opIds),
			},
			select: ['id', 'scheduleDate'],
		});

		const dates = getDailyNotes.map((date) => date.scheduleDate);
		const scheduleDates = dates.map((d) => moment(d, 'YYYY-MM-DD'));

		// if dailyNotes are delete, update start date & end date in site table
		let startDate = '';
		let endDate = '';
		if (scheduleDates.length != 0) {
			startDate = moment.min(scheduleDates).format('YYYY-MM-DD');
			endDate = moment.max(scheduleDates).format('YYYY-MM-DD');
		} else {
			startDate = '';
			endDate = '';
		}
		await Promise.all([
			this.siteRepo.save({
				id: siteId,
				startDate,
				endDate,
			}),
			//delete operation instruction
			this.repo.remove(delOpIns),
		]);

		await Promise.all(
			opIds.map((opId, index) => this.createOperationInstruction(siteId, opId, index + 1)),
		);

		return await this.repo.find({
			select: ['id', 'operationId'],
			where: {
				siteId,
			},
		});
	}

	async createOperationInstruction(siteId: string, opId: string, orderNo?: Number) {
		const operationInstructionCondition = {
			siteId,
			operationId: opId,
		};

		const operationInstruction = await this.repo.findOne({
			where: operationInstructionCondition,
		});

		//creat data, if operationInstruction does not exist
		if (!operationInstruction) {
			const operationInstructionData = this.repo.create({
				id: generateId(),
				siteId,
				operationId: opId,
				orderNo
			});
			await this.repo.save(operationInstructionData);
		} else {

			if (operationInstruction.orderNo !== orderNo) {
				operationInstruction.orderNo = orderNo;
				await this.repo.save(operationInstruction);
			}

		}
	}
}

// 	async updateOperationInstruction(
// 		siteId: string,
// 		updateSiteOperationDTO: UpdateSiteOperationDTO,
// 	) {
// 		return await map(updateSiteOperationDTO.operations, async (op) => {
// 			const operationInstructionCondition = {
// 				siteId,
// 				operationId: op.id,
// 			};
// 			//orderNo is greater than 0
// 			if (op.checkOrderNo) {
// 				const operationInstruction = await this.repo.findOne({
// 					where: operationInstructionCondition,
// 				});

// 				//creat data, if operationInstruction does not exist
// 				if (!operationInstruction) {
// 					const operationInstructionData = this.repo.create({
// 						id: generateId(),
// 						siteId,
// 						operationId: op.id,
// 						orderNo: op.checkOrderNo,
// 					});
// 					await this.repo.save(operationInstructionData);
// 				} else {
// 					await this.repo.update(operationInstruction.id, {
// 						orderNo: op.checkOrderNo,
// 					});
// 				}
// 			} else {
// 				// delete daily notes data
// 				const findDailyNotesData = await this.dailyNoteRepo.find({
// 					where: operationInstructionCondition,
// 				});
// 				if (findDailyNotesData)
// 					await this.dailyNoteRepo.remove(findDailyNotesData);

// 				// delete operation instruction data
// 				const findOperationInstructionData = await this.repo.findOne({
// 					where: operationInstructionCondition,
// 				});
// 				if (findOperationInstructionData)
// 					await this.repo.remove(findOperationInstructionData);
// 			}
// 		}).then(
// 			async () =>
// 				await this.repo.find({
// 					where: {
// 						siteId,
// 					},
// 					order: {
// 						orderNo: 'ASC',
// 					},
// 				}),
// 		);
// 	}
