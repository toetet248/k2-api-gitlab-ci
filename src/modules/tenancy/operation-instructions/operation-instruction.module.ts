import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { OperationInstruction } from './operation-instruction.entity';
import { OperationInstructionService } from './operation-instruction.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([OperationInstruction]),
		NestjsFormDataModule,
	],
	providers: [OperationInstructionService],
	exports: [OperationInstructionService],
})
export class OperationInstructionModule {}
