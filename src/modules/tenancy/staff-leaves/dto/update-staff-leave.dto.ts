import { Type } from 'class-transformer';
import {
	IsArray,
	IsDateString,
	IsNotEmpty,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateStaffLeaveDTO {
	@IsExist('staffs', 'id')
	@IsNotEmpty()
	staffId: string;

	@IsArray()
	@Type(() => StaffLeaves)
	@ValidateNested({ each: true })
	staffLeaves: StaffLeaves[];
}

class StaffLeaves {
	@IsExist('staff_leaves', 'id', 'new')
	@IsString()
	id: string;

	@IsNotEmpty()
	@IsDateString()
	date: string;
}
