import { Expose } from 'class-transformer';

export class UpdateStaffLeaveSerialize {
	@Expose()
	id: string;

	@Expose()
	staffId: string;

	@Expose()
	date: Date;
}
