import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'staff_leaves' })
export class StaffLeave extends MainEntity {
	@Column()
	staffId: string;

	@Column()
	date: Date;

	@ManyToOne(() => Staff, (staff) => staff.staffLeaves, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
