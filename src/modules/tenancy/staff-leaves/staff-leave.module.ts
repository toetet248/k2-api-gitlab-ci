import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StaffLeaveController } from './staff-leave.controller';
import { StaffLeave } from './staff-leave.entity';
import { StaffLeaveService } from './staff-leave.service';

@Module({
	imports: [TypeOrmModule.forFeature([StaffLeave])],
	controllers: [StaffLeaveController],
	providers: [StaffLeaveService],
})
export class StaffLeaveModule {}
