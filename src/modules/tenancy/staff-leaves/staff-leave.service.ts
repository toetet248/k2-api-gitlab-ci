import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { StaffLeave } from './staff-leave.entity';

@Injectable()
export class StaffLeaveService {
	public readonly repo: Repository<StaffLeave>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(StaffLeave);
	}

	async removeStaffLeavesByIds(ids: string[], staffId: string) {
		await this.repo.delete({
			id: Not(In(ids)),
			staffId,
		});
	}
}
