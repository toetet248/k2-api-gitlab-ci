import { Patch, UseGuards } from '@nestjs/common';
import { Body, Controller } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { generateId } from 'src/common/service/helper.service';
import { StaffLeaveService } from './staff-leave.service';
import { UpdateStaffLeaveSerialize } from './serialize/update-staff-leave.serialize';
import { UpdateStaffLeaveDTO } from './dto/update-staff-leave.dto';

@Controller('tenants/staff-leaves')
@UseGuards(AuthGuard('tenant-jwt'))
export class StaffLeaveController extends BaseController {
	constructor(private readonly service: StaffLeaveService) {
		super();
	}

	@Patch('/update')
	@Serialize(UpdateStaffLeaveSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateStaffTeam(@Body() updateStaffLeaveDTO: UpdateStaffLeaveDTO) {
		// save and update staff leaves
		const data = updateStaffLeaveDTO.staffLeaves.map((staffLeave) => {
			if (!staffLeave.id) staffLeave.id = generateId();

			const requestData = {
				id: staffLeave.id ?? generateId(),
				date: staffLeave.date,
			};

			return this.service.repo.create({
				staffId: updateStaffLeaveDTO.staffId,
				...requestData,
			});
		});
		await this.service.repo.save(data);

		// remove other staff leaves
		const ids = data.map((d) => d.id);
		this.service.removeStaffLeavesByIds(ids, updateStaffLeaveDTO.staffId);

		return this.response(data);
	}
}
