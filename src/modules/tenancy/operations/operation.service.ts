import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { Operation } from './operation.entity';

@Injectable()
export class OperationService {
	public readonly repo: Repository<Operation>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(Operation);
	}

	async removeOtherOperationsByIds(ids: string[]) {
		const removeOperations = await this.repo.find({
			where: {
				id: Not(In(ids)),
			},
		});

		await this.repo.softRemove(removeOperations);
	}
}
