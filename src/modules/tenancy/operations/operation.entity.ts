import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { OperationInstruction } from '../operation-instructions/operation-instruction.entity';
import { Site } from '../sites/site.entity';

@Entity({ name: 'operations' })
export class Operation extends MainEntity {
	@Column()
	name: string;

	@Column()
	colorCode: string;

	@Column()
	orderNo: number;

	@OneToMany(
		() => OperationInstruction,
		(operationInstruction) => operationInstruction.operation,
	)
	operationInstructions: OperationInstruction[];

	@OneToMany(() => DailyNote, (dailyNote) => dailyNote.operation)
	dailyNotes: DailyNote[];

	checkOrderNo: number;

	@ManyToMany(() => Site, (site) => site.operations)
	@JoinTable({
		name: 'operation_instructions',
		joinColumn: {
			name: 'operationId',
			referencedColumnName: 'id',
		},
		inverseJoinColumn: {
			name: 'siteId',
			referencedColumnName: 'id',
		},
	})
	sites: Site[];
}
