import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { Operation } from './operation.entity';
import { OperationService } from './operation.service';

@Module({
	imports: [TypeOrmModule.forFeature([Operation]), NestjsFormDataModule],
	providers: [OperationService],
	exports: [OperationService],
})
export class OperationModule {}
