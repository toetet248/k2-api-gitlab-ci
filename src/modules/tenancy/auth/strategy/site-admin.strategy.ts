import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SiteAdminStrategy extends PassportStrategy(
	Strategy,
	'site-admin',
) {
	constructor(configService: ConfigService) {
		super(configService.get('jwt.decode'));
	}

	async validate(request: any, payload: any) {
		const { isSiteAdmin, isSystemAdmin, isOwner } = request.authStaff;

		if (isSiteAdmin || isSystemAdmin || isOwner) return payload;

		throw new UnauthorizedException([
			'staff does not have permission to access',
		]);
	}
}
