import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import { getTenantConnection } from '../../tenancy.utils';
import { TENANT_HEADER } from '../../tenancy.middleware';
import { UserService } from 'src/modules/central/users/user.service';

@Injectable()
export class TenantJwtStrategy extends PassportStrategy(
	Strategy,
	'tenant-jwt',
) {
	constructor(
		private configService: ConfigService,
		@Inject(UserService) private userService: UserService,
	) {
		super(configService.get('jwt.decode'));
	}

	async validate(request: any, payload: any) {
		const authTenant = request.headers[TENANT_HEADER];

		if (authTenant) {
			const { email, phone } = payload;

			const user = await this.userService.repo.findOne(
				email
					? {
							email,
					  }
					: {
							phone,
					  },
			);

			if (user) {
				const connection = await getTenantConnection(
					`${this.configService.get(
						'database.tenantDbPrefix',
					)}${authTenant}`,
				);

				let staffRaw = await connection.query(
					'SELECT `staffs`.`id` AS `staffs_id`, `staffs`.`createdAt` AS `staffs_createdAt`, `staffs`.`updatedAt` AS `staffs_updatedAt`, `staffs`.`deletedAt` AS `staffs_deletedAt`, `staffs`.`userId` AS `staffs_userId`, `staffs`.`staffTeamId` AS `staffs_staffTeamId`, `staffs`.`name` AS `staffs_name`, `staffs`.`email` AS `staffs_email`, `staffs`.`password` AS `staffs_password`, `staffs`.`nameFurigana` AS `staffs_nameFurigana`, `staffs`.`nameOnTab` AS `staffs_nameOnTab`, `staffs`.`phone` AS `staffs_phone`, `staffs`.`image` AS `staffs_image`, `staffs`.`status` AS `staffs_status`, `staffs`.`isSiteStaff` AS `staffs_isSiteStaff`, `staffs`.`isSiteAdmin` AS `staffs_isSiteAdmin`, `staffs`.`isSystemAdmin` AS `staffs_isSystemAdmin`, `staffs`.`orderNo` AS `staffs_orderNo`, `staffs`.`isOwner` AS `staffs_isOwner`, `staffs`.`loginType` AS `staffs_loginType`, `staffTeams`.`id` AS `staffTeams_id`, `staffTeams`.`createdAt` AS `staffTeams_createdAt`, `staffTeams`.`updatedAt` AS `staffTeams_updatedAt`, `staffTeams`.`deletedAt` AS `staffTeams_deletedAt`, `staffTeams`.`name` AS `staffTeams_name`, `staffTeams`.`colorCode` AS `staffTeams_colorCode`, `staffTeams`.`orderNo` AS `staffTeams_orderNo` FROM `staffs` `staffs` LEFT JOIN `staff_teams` `staffTeams` ON `staffTeams`.`id`=`staffs`.`staffTeamId` AND (`staffTeams`.`deletedAt` IS NULL) WHERE ( `staffs`.`userId` = ? ) AND ( `staffs`.`deletedAt` IS NULL )',
					[payload.id],
				);

				// const staff = await tenantStaffRepo.findOne({
				// 	where: { userId: payload.id, deletedAt: null },
				// 	relations: ['staffTeam'],
				// 	withDeleted: true,
				// });

				if (staffRaw) {
					if (staffRaw.length) {
						staffRaw = staffRaw[0];
						const staffTeam = staffRaw.staffTeamId
							? {
									id: staffRaw.staffTeams_id,
									name: staffRaw.staffTeams_name,
									colorCode: staffRaw.staffTeams_colorCode,
									orderNo: staffRaw.staffTeams_orderNo,
									createdAt: staffRaw.staffTeams_createdAt,
									updatedAt: staffRaw.staffTeams_updatedAt,
									deletedAt: staffRaw.staffTeams_deletedAt,
							  }
							: null;

						const staff = {
							id: staffRaw.staffs_id,
							userId: staffRaw.staffs_userId,
							staffTeamId: staffRaw.staffs_staffTeamId,
							name: staffRaw.staffs_name,
							email: staffRaw.staffs_email,
							password: staffRaw.staffs_password,
							nameFurigana: staffRaw.staffs_nameFurigana,
							nameOnTab: staffRaw.staffs_nameOnTab,
							phone: staffRaw.staffs_phone,
							image: staffRaw.staffs_image,
							status: staffRaw.staffs_status,
							isSiteStaff: staffRaw.staffs_isSiteStaff,
							isSiteAdmin: staffRaw.staffs_isSiteAdmin,
							isSystemAdmin: staffRaw.staffs_isSystemAdmin,
							orderNo: staffRaw.staffs_orderNo,
							isOwner: staffRaw.staffs_isOwner,
							loginType: staffRaw.staffs_loginType,
							createdAt: staffRaw.staffs_createdAt,
							updatedAt: staffRaw.staffs_updatedAt,
							deletedAt: staffRaw.staffs_deletedAt,
							staffTeam,
						};

						request.authStaff = staff;

						return payload;
					}
				}
			}
		}

		throw new UnauthorizedException();
	}
}
