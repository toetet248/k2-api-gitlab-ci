import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SystemAdminStrategy extends PassportStrategy(
	Strategy,
	'system-admin',
) {
	constructor(configService: ConfigService) {
		super(configService.get('jwt.decode'));
	}

	async validate(request: any, payload: any) {
		const { isSystemAdmin, isOwner } = request.authStaff;
		if (isSystemAdmin || isOwner) return payload;

		throw new UnauthorizedException([
			'staff does not have permission to access',
		]);
	}
}
