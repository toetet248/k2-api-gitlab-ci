import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SiteStaffStrategy extends PassportStrategy(
	Strategy,
	'site-staff',
) {
	constructor(configService: ConfigService) {
		super(configService.get('jwt.decode'));
	}

	async validate(request: any, payload: any) {
		const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner } =
			request.authStaff;

		if (isSiteStaff || isSiteAdmin || isSystemAdmin || isOwner)
			return payload;

		throw new UnauthorizedException([
			'staff does not have permission to access',
		]);
	}
}
