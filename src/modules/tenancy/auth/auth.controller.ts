import {
	Controller,
	Body,
	Get,
	UseGuards,
	Patch,
	Headers,
	Post,
} from '@nestjs/common';
import { TenantAuthService } from './auth.service';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { AuthGuard } from '@nestjs/passport';
import { AuthStaff } from './decorator/auth-staff.decorator';
import { Staff } from '../staffs/staff.entity';
import { TENANT_HEADER } from '../tenancy.middleware';
import { UpdateProfileDTO } from './dto/update-profile.dto';
import { UpdateProfileSerialize } from './serialize/update-profile.serialize';
import { FindProfileSerialize } from './serialize/find-profile.serialize';
import { ValidatePasswordDTO } from './dto/validate-password.dto';
import * as bcrypt from 'bcrypt';
import { ValidateIsOwnerDTO } from './dto/validate-isowner.dto';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { ValidateTokenSerialize } from './serialize/validate-token.serialize';

@Controller('tenants/auth')
@UseGuards(AuthGuard('tenant-jwt'))
export class AuthController extends BaseController {
	constructor(private authService: TenantAuthService) {
		super();
	}

	@Get('token/validate')
	@Serialize(ValidateTokenSerialize)
	public async validateToken(
		@AuthStaff() staff: Staff,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const company = await this.authService.getCompany(tenantName);

		return this.response({ ...staff, company });
	}

	@Get('profile')
	@Serialize(FindProfileSerialize)
	public async findProfile(
		@AuthStaff() staff: Staff,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const company = await this.authService.getCompany(tenantName);

		return this.response({ ...staff, company });
	}

	@Post('/password/validate')
	async validatePassword(
		@Body() validatePasswordDTO: ValidatePasswordDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const isValidate = await bcrypt.compare(
			validatePasswordDTO.password,
			authStaff.password,
		);

		return this.response(isValidate, {
			title: 'validate password ' + (isValidate ? 'success' : 'fail'),
		});
	}

	@Post('/verify/isowner')
	async verifyIsOwner(
		@Body() validatePasswordDTO: ValidateIsOwnerDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const isOwnerValidate = validatePasswordDTO.id === authStaff.id;
		const isPassValidate = await bcrypt.compare(
			validatePasswordDTO.password,
			authStaff.password,
		);

		const isValidate = isOwnerValidate && isPassValidate;

		return this.response(isValidate, {
			title: 'validate password ' + (isValidate ? 'success' : 'fail'),
		});
	}

	@Patch('/update/profile')
	@Serialize(UpdateProfileSerialize)
	@InjectRequestToBody()
	async updateProfile(
		@Body() updateProfileDTO: UpdateProfileDTO,
		@AuthStaff() authStaff: Staff,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const staff = await this.authService.updateProfile(
			authStaff,
			updateProfileDTO,
			tenantName,
		);
		return this.response(staff, { title: 'profile update success' });
	}
}
