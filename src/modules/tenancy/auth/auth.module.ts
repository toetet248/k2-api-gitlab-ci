import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TenantAuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CommonModule } from 'src/common/common.module';
import { StaffModule } from '../staffs/staff.module';
import { TenantJwtStrategy } from './strategy/tenant-jwt.strategy';
import { SiteStaffStrategy } from './strategy/site-staff.strategy';
import { SiteAdminStrategy } from './strategy/site-admin.strategy';
import { SystemAdminStrategy } from './strategy/system-admin.strategy';
import { UserModule } from 'src/modules/central/users/user.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';

@Module({
	imports: [
		PassportModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) =>
				configService.get('jwt.encode'),
			inject: [ConfigService],
		}),
		StaffModule,
		CommonModule,
		UserModule,
		TenantModule,
	],
	providers: [
		TenantJwtStrategy,
		SiteStaffStrategy,
		SiteAdminStrategy,
		SystemAdminStrategy,
		TenantAuthService,
	],
	controllers: [AuthController],
	exports: [TenantAuthService],
})
export class TenantAuthModule {}
