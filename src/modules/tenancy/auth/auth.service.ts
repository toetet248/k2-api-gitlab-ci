import { Injectable, Inject, ConsoleLogger } from '@nestjs/common';
import { StaffService } from '../staffs/staff.service';
import { UpdateProfileDTO } from './dto/update-profile.dto';
import { Staff } from '../staffs/staff.entity';
import { Repository } from 'typeorm';
import { FileService } from 'src/common/service/file.service';
import { TenantService } from 'src/modules/central/tenants/tenant.service';
import { UserService } from 'src/modules/central/users/user.service';

@Injectable()
export class TenantAuthService {
	public readonly repo: Repository<Staff>;

	constructor(
		@Inject(StaffService)
		private service: StaffService,
		@Inject(UserService)
		private readonly userService: UserService,
		@Inject(TenantService)
		private tenantService: TenantService,
		@Inject(FileService) private readonly fileService: FileService,
	) {
		this.repo = this.service.repo;
	}

	public async getCompany(tenantName: string) {
		const company = await this.tenantService.repo.findOne({
			where: {
				name: tenantName,
			},
			select: [
				'companyName',
				'companyEmail',
				'companyPhone',
				'companyAddressPrefecture',
				'companyAddressCity',
				'companyAddressBuilding',
				'companyFax',
				'companyLat',
				'companyLng',
				'companyImage',
				'companyBusinessTypes',
			],
		});

		return company;
	}

	//Profile
	async updateProfile(
		authStaff: Staff,
		updateProfileDTO: UpdateProfileDTO,
		tenantName: string,
	) {
		const {
			staffTeamId,
			name,
			nameFurigana,
			nameOnTab,
			phone,
			email,
			image,
			isSiteStaff,
			isSiteAdmin,
			isOwner,
		} = updateProfileDTO;

		if (image) this.fileService.delete(authStaff.image);

		await this.repo.save({
			id: authStaff.id,
			userId: authStaff.userId,
			staffTeamId,
			name,
			nameFurigana,
			nameOnTab,
			phone,
			email,
			image:
				image === 'delete'
					? null
					: this.fileService.uploadFile(
							image,
							'/staffs/images',
							'SI',
							tenantName,
					  ) ?? authStaff.image,
			isSiteAdmin,
			isSiteStaff,
			isOwner,
		});

		// update user from central
		await this.userService.repo.update(authStaff.userId, {
			email,
			phone,
		});

		const profile = await this.repo.findOne({
			where: { id: authStaff.id },
			relations: ['staffTeam'],
			withDeleted: true,
		});
		return profile;
	}
}
