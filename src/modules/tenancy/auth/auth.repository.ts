import { Inject } from '@nestjs/common';
import { Repository, EntityRepository, Connection } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { Staff } from '../staffs/staff.entity';

@EntityRepository(Staff)
export class AuthRepository extends Repository<Staff> {
	public readonly authRepo: Repository<Staff>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		super();
		this.authRepo = connection.getRepository(Staff);
	}
}
