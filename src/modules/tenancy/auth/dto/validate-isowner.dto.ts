import { IsNotEmpty } from 'class-validator';

export class ValidateIsOwnerDTO {
	@IsNotEmpty()
	id: string;

	@IsNotEmpty()
	password: string;
}
