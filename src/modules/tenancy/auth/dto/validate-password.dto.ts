import { IsNotEmpty } from 'class-validator';

export class ValidatePasswordDTO {
	@IsNotEmpty()
	password: string;
}
