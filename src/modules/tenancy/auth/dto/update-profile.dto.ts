import { IsEmail, IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';
import { IsOwnerExist } from 'src/common/validator/is-owner-exist.validator';
import { IsPhoneNo } from 'src/common/validator/is-phone-no.validators';

export class UpdateProfileDTO {
	@IsOptional()
	staffTeamId: string;

	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	@IsOptional()
	nameFurigana: string;

	@IsOptional()
	@MaxLength(8)
	nameOnTab: string;

	@IsOptional()
	@IsPhoneNo()
	phone: string;

	@IsNotEmpty()
	@IsEmail()
	@IsOwnerExist()
	email: string;

	@IsOptional()
	@IsBase64Image(true)
	image: any;

	@IsOptional()
	isSiteStaff: boolean;

	@IsOptional()
	isSiteAdmin: boolean;

	@IsOptional()
	isOwner: boolean;
}
