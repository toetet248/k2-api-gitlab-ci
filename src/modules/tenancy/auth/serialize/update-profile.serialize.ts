import { Expose, Type } from 'class-transformer';
class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}
export class UpdateProfileSerialize {
	@Expose()
	id: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	email: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	image: string;

	@Expose()
	orderNo: number;

	@Expose()
	nameFurigana: string;

	@Expose()
	status: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	loginType: string;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;
}
