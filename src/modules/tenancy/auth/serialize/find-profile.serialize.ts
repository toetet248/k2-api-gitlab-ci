import { Expose, Type } from 'class-transformer';

class Company {
	@Expose({ name: 'companyName' })
	name: string;

	@Expose({ name: 'companyPhone' })
	phone: string;

	@Expose({ name: 'companyEmail' })
	email: string;

	@Expose({ name: 'companyAddressPrefecture' })
	addressPrefecture: string;

	@Expose({ name: 'companyAddressCity' })
	addressCity: string;

	@Expose({ name: 'companyAddressBuilding' })
	addressBuilding: string;

	@Expose({ name: 'companyImage' })
	image: string;

	@Expose({ name: 'companyFax' })
	fax: string;

	@Expose({ name: 'companyLat' })
	lat: string;

	@Expose({ name: 'companyLng' })
	lng: string;

	@Expose({ name: 'companyBusinessTypes' })
	businessTypes: string;
}

export class FindProfileSerialize {
	@Expose()
	id: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	email: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	image: string;

	@Expose()
	nameFurigana: string;

	@Expose()
	status: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: boolean;

	@Expose()
	createdAt: Date;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam[];

	@Expose()
	@Type(() => Company)
	company: Company;
}

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	colorCode: string;
}
