import { MainEntity } from 'src/common/entity/main.entity';
import {
	BeforeInsert,
	Column,
	Entity,
	Generated,
	JoinColumn,
	ManyToOne,
} from 'typeorm';
import { Site } from '../sites/site.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'site_files' })
export class SiteFile extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	name: string;

	@Column()
	file: string;

	@Column()
	size: string;

	@Column()
	staffId: string;

	@ManyToOne(() => Site, (site) => site.siteFiles, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;

	@ManyToOne(() => Staff, (staff) => staff.siteImages, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
