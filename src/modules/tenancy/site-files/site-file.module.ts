import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { TenantJwtStrategy } from '../auth/strategy/tenant-jwt.strategy';
import { CONNECTION } from '../tenancy.symbols';
import { SiteFileController } from './site-file.controller';
import { SiteFile } from './site-file.entity';
import { SiteFileService } from './site-file.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([SiteFile]),
		NestjsFormDataModule,
		CommonModule,
	],
	controllers: [SiteFileController],
	providers: [SiteFileService],
	exports: [SiteFileService],
})
export class SiteFileModule {}
