import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { Connection, In, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { SiteFile } from './site-file.entity';
import { FileService } from 'src/common/service/file.service';
import { DeleteSiteFileDTO } from '../sites/dto/delete-site-file.dto';
import { UpdateSiteFileNameDTO } from '../sites/dto/update-site-file-name.dto';
import { Staff } from '../staffs/staff.entity';

@Injectable()
export class SiteFileService {
	public readonly repo: Repository<SiteFile>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(FileService) private readonly fileService: FileService,
	) {
		this.repo = connection.getRepository(SiteFile);
	}

	async uploadSiteFile(
		siteId: string,
		name: string,
		size: string,
		file: string,
		staffId: string,
	) {
		const data = await this.repo.save({
			id: generateId(),
			siteId,
			name,
			file,
			size,
			staffId,
		});

		return data;
	}

	// delete site files
	async deleteSiteFiles(
		deleteSiteFileDTO: DeleteSiteFileDTO,
		authStaff: Staff,
	) {
		let removeSiteFiles: SiteFile[];

		const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
			authStaff;

		// throw error if site staff delete site files of other staffs
		if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner) {
			removeSiteFiles = await this.repo.find({
				where: {
					id: In(deleteSiteFileDTO.fileIds),
					staffId: id,
				},
			});

			if (removeSiteFiles.length !== deleteSiteFileDTO.fileIds.length) {
				throw new BadRequestException([
					'cannot delete site file of other staffs',
				]);
			}
		} else {
			removeSiteFiles = await this.repo.find({
				where: {
					id: In(deleteSiteFileDTO.fileIds),
				},
			});
		}

		// delete file from folder
		removeSiteFiles.map((removeSiteFile: SiteFile) => {
			this.fileService.delete(removeSiteFile.file);
		});

		// delete file data from db
		return await this.repo.remove(removeSiteFiles);
	}

	//update site image description
	async updateSiteFileName(
		siteFileId: string,
		updateSiteFileNameDTO: UpdateSiteFileNameDTO,
		authStaff: Staff,
	) {
		const siteFile = await this.repo.findOne({
			where: {
				id: siteFileId,
			},
		});

		if (siteFile) {
			const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
				authStaff;

			// throw error if site staff update site files of other staffs
			if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner) {
				if (siteFile.staffId !== id)
					throw new BadRequestException([
						'cannot update site file of other staffs',
					]);
			}

			await this.repo.update(siteFileId, {
				name: updateSiteFileNameDTO.name,
			});

			return this.repo.findOne(siteFileId);
		}

		throw new BadRequestException(['site file not found']);
	}
}
