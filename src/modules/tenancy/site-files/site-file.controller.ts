import { Controller } from '@nestjs/common';
import { SiteFileService } from './site-file.service';
import { BaseController } from 'src/common/controller/base.controller';

@Controller('site_files')
export class SiteFileController extends BaseController {
	constructor(private readonly service: SiteFileService) {
		super();
	}
}
