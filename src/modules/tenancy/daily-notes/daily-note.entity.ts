import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { OperationInstruction } from '../operation-instructions/operation-instruction.entity';
import { Operation } from '../operations/operation.entity';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import { Site } from '../sites/site.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'daily_notes' })
export class DailyNote extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	operationId: string;

	@Column()
	operationInstructionId: string;

	@Column()
	scheduleDate: Date;

	@Column()
	reserveMan: number;

	@Column()
	reserveHour: number;

	@Column()
	reserveManPower: number;

	@Column()
	reserveStartTime: number;

	@Column()
	reserveEndTime: number;

	@Column()
	actualMan: number;

	@Column()
	actualHour: number;

	@Column()
	actualManPower: number;

	@Column()
	actualStartTime: string;

	@Column()
	actualEndTime: string;

	@Column()
	staffId: string;

	@Column()
	memo: string;

	@ManyToOne(() => Site, (site) => site.dailyNotes, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;

	@ManyToOne(() => Operation, (operation) => operation.dailyNotes, {
		primary: true,
	})
	@JoinColumn({ name: 'operationId' })
	operation: Operation;

	@ManyToOne(
		() => OperationInstruction,
		(operationInstruction) => operationInstruction.dailyNotes,
		{
			primary: true,
		},
	)
	@JoinColumn({ name: 'operationInstructionId' })
	operationInstruction: OperationInstruction;

	@OneToMany(() => ScheduleStaff, (scheduleStaff) => scheduleStaff.dailyNote)
	scheduleStaffs: ScheduleStaff[];

	@OneToMany(
		() => ScheduleVehicle,
		(scheduleVehicle) => scheduleVehicle.dailyNote,
	)
	scheduleVehicles: ScheduleVehicle[];

	@ManyToOne(() => Staff, (staff) => staff.dailyNotes, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
