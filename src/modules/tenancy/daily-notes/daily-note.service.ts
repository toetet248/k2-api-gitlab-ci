import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { DailyNote } from './daily-note.entity';

@Injectable()
export class DailyNoteService {
	public readonly repo: Repository<DailyNote>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(DailyNote);
	}

	async removeOtherDailyNotesByIds(
		ids: string[],
		operationInstructionId: string,
	) {
		const removeDailyNotes = await this.repo.find({
			where: {
				id: Not(In(ids)),
				operationInstructionId,
			},
		});

		await this.repo.remove(removeDailyNotes);
	}
}
