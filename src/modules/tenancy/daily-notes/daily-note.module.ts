import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { SchedulerModule } from '../schedulers/scheduler.module';
import { DailyNote } from './daily-note.entity';
import { DailyNoteService } from './daily-note.service';

@Module({
	imports: [TypeOrmModule.forFeature([DailyNote]), NestjsFormDataModule],
	providers: [DailyNoteService],
	exports: [DailyNoteService],
})
export class DailyNoteModule {}
