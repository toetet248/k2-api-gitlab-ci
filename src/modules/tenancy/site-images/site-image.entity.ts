import { MainEntity } from 'src/common/entity/main.entity';
import {
	BeforeInsert,
	Column,
	Entity,
	Generated,
	JoinColumn,
	ManyToOne,
} from 'typeorm';
import { Site } from '../sites/site.entity';
import { SiteAlbum } from '../site-albums/site-album.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'site_images' })
export class SiteImage extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	albumId: string;

	// @Column()
	// name: string;

	@Column()
	image: string;

	@Column()
	description: string;

	@Column()
	size: string;

	@Column()
	staffId: string;

	@ManyToOne(() => Site, (site) => site.siteImages, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;

	@ManyToOne(() => SiteAlbum, (siteAlbum) => siteAlbum.siteImages, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	siteAlbum: SiteAlbum;

	@ManyToOne(() => Staff, (staff) => staff.siteImages, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
