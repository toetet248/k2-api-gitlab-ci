import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { generateId, validateExists } from 'src/common/service/helper.service';
import { Connection, In, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { SiteImage } from './site-image.entity';
import { FileService } from 'src/common/service/file.service';
import { DeleteSiteImageDTO } from '../sites/dto/delete-site-image.dto';
import { UpdateSiteImageDescriptionDTO } from '../sites/dto/update-site-image-description.dto';
import { Staff } from '../staffs/staff.entity';

@Injectable()
export class SiteImageService {
	public readonly repo: Repository<SiteImage>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(FileService) private readonly fileService: FileService,
	) {
		this.repo = connection.getRepository(SiteImage);
		this.fileService = fileService;
	}

	// async UploadImage(
	// 	id: string,
	// 	updateSiteImageDTO: UpdateSiteImageDTO,
	// 	images: Express.Multer.File,
	// ) {
	// 	// set order no to create data
	// 	console.log(images, '....imagesssss');
	// 	const orderNo =
	// 		(await (
	// 			await this.repo
	// 				.createQueryBuilder('site_images')
	// 				.select('MAX(site_images.orderNo)', 'max')
	// 				.getRawOne()
	// 		).max) + 1;

	// 	console.log(orderNo, '.....orderNo');
	// 	// if (images)
	// 	// 	images.forEach((img) => {
	// 	// 		if (existsSync(img.path))
	// 	// 			unlink(img.path, (err) => {
	// 	// 				if (err) throw err;
	// 	// 			});
	// 	// 	});

	// 	console.log(images, '..........Image');

	// 	const data = updateSiteImageDTO.siteImages.map((images) => {
	// 		if (!images.id) images.id = generateId();
	// 		return this.repo.create({
	// 			...images,
	// 			...updateSiteImageDTO,
	// 			// image: images.path,
	// 			orderNo,
	// 		});
	// 	});

	// 	console.log(data, '.........createImage');

	// 	return await this.repo.save(data);
	// }

	// upload site image
	async uploadSiteImage(
		siteId: string,
		size: any,
		image: string,
		staffId: string,
	) {
		const data = await this.repo.save({
			id: generateId(),
			siteId,
			image,
			description: null,
			size,
			staffId,
		});
		return data;
	}

	// delete site images
	async deleteSiteImages(
		deleteSiteImageDTO: DeleteSiteImageDTO,
		authStaff: Staff,
	) {
		let removeSiteImages: SiteImage[];

		const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
			authStaff;

		// throw error if site staff delete site images of other staffs
		if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner) {
			removeSiteImages = await this.repo.find({
				where: {
					id: In(deleteSiteImageDTO.imageIds),
					staffId: id,
				},
			});

			if (
				removeSiteImages.length !== deleteSiteImageDTO.imageIds.length
			) {
				throw new BadRequestException([
					'cannot delete site image of other staffs',
				]);
			}
		} else {
			removeSiteImages = await this.repo.find({
				where: {
					id: In(deleteSiteImageDTO.imageIds),
				},
			});
		}

		// delete image file from folder
		removeSiteImages.map((removeSiteImage: SiteImage) => {
			this.fileService.delete(removeSiteImage.image);
		});

		// delete image data from db
		return await this.repo.remove(removeSiteImages);
	}

	//update site image description
	async updateSiteImageDescription(
		siteImageId: string,
		updateSiteImageDescriptionDTO: UpdateSiteImageDescriptionDTO,
		authStaff: Staff,
	) {
		const siteImage = await this.repo.findOne({
			where: {
				id: siteImageId,
			},
		});

		if (siteImage) {
			const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
				authStaff;

			// throw error if site staff update site images of other staffs
			if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner) {
				if (siteImage.staffId !== id)
					throw new BadRequestException([
						'cannot update site image of other staffs',
					]);
			}

			await this.repo.update(siteImageId, {
				description: updateSiteImageDescriptionDTO.description,
			});

			return this.repo.findOne(siteImageId);
		}

		throw new BadRequestException(['site image not found']);
	}
}
