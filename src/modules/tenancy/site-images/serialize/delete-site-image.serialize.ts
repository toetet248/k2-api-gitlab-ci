import { Expose } from 'class-transformer';

export class DeleteSiteImageSerialize {
	@Expose()
	id: string;

	@Expose()
	siteId: string;

	@Expose()
	albumId: string;

	// @Expose()
	// name: string;

	@Expose()
	image: string;

	@Expose()
	description: string;
}
