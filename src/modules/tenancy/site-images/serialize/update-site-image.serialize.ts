import { Expose } from 'class-transformer';

export class UpdateSiteImageSerialize {
	@Expose()
	id: string;

	@Expose()
	siteId: string;

	@Expose()
	albumId: string;

	// @Expose()
	// name: string;

	@Expose()
	image: string;

	@Expose()
	description: string;
}
