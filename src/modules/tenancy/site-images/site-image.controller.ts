import { UseGuards } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SiteImageService } from './site-image.service';
import { BaseController } from 'src/common/controller/base.controller';

@Controller('tenants/site-images')
@UseGuards(AuthGuard('tenant-jwt'))
export class SiteImageController extends BaseController {
	constructor(private readonly service: SiteImageService) {
		super();
	}

	// @Get('/:id')
	// @Serialize(GetSiteImageSerialize)
	// async getImages(@Param('id') id: string) {
	// 	const data = await this.service.repo.findOne({
	// 		where: { id },
	// 		relations: ['siteAlbum'],
	// 	});

	// 	return this.response(data);
	// }

	// @Patch('/update')
	// @Storage({
	// 	fieldName: 'siteImages',
	// 	path: '/sites/images',
	// 	filter: filterImageFile,
	// 	sku: 'SI',
	// })
	// // @UseInterceptors(FileInterceptor('siteImages'))
	// @Serialize(UpdateSiteImageSerialize)
	// async uploadImages(
	// 	@Body() updateSiteImageDTO: any,
	// 	@Param('id') id: string,
	// 	@UploadedFile() image: Express.Multer.File,
	// ) {
	// 	console.log(image, '.......imagecontroller');
	// 	// console.log(siteImages.buffer);
	// 	return this.response();
	// 	// const data = await this.service.UploadImage(
	// 	// 	id,
	// 	// 	updateSiteImageDTO,
	// 	// 	image,
	// 	// );

	// 	// // const response = [];
	// 	// // await images.forEach((image) => {
	// 	// // 	const imageResponse = this.service.UploadImage(
	// 	// // 		id,
	// 	// // 		updateSiteImageDTO,
	// 	// // 		images,
	// 	// // 	);
	// 	// // 	response.push(imageResponse);
	// 	// // });
	// 	// // // return response;

	// 	// console.log(data, '........ImageDate');

	// 	// return this.response(data, { title: 'upload image success' });
	// }
}
