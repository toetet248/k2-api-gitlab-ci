import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { TenantJwtStrategy } from '../auth/strategy/tenant-jwt.strategy';
import { CONNECTION } from '../tenancy.symbols';
import { SiteImageController } from './site-image.controller';
import { SiteImage } from './site-image.entity';
import { SiteImageService } from './site-image.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([SiteImage]),
		NestjsFormDataModule,
		CommonModule,
	],
	controllers: [SiteImageController],
	providers: [SiteImageService],
	exports: [SiteImageService],
})
export class SiteImageModule {}
