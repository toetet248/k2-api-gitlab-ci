import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

class VechilceGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class DailyNote {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	scheduleDate: string;
}

class ScheduleVehicle {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	@Type(() => DailyNote)
	dailyNote: DailyNote;
}

export class PaginateCalendarVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	licenseEndDate: string;

	@Expose()
	insuranceEndDate: string;

	@Expose()
	image: string;

	@Expose()
	memo: string;

	@Expose()
	@Type(() => VechilceGroup)
	vehicleGroup: VechilceGroup[];

	@Expose()
	@Type(() => ScheduleVehicle)
	scheduleVehicles: ScheduleVehicle[];
}
