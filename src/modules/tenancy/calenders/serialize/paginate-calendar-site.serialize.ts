import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

export class PaginateCalendarSiteSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	@Type(() => dailyNotes)
	dailyNotes: dailyNotes[];
}

class dailyNotes {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	scheduleDate: string;

	@Expose()
	@Type(() => Operations)
	operation: Operations[];
}

class Operations {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}
