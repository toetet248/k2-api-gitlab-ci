import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';
export class PaginateCalendarStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	image: string;

	@Expose()
	email: string;

	@Expose()
	phone: string;


	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: boolean;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam[];

	@Expose()
	@Type(() => ScheduleStaff)
	scheduleStaffs: ScheduleStaff[];

	@Expose()
	@Type(() => StaffLeave)
	staffLeaves: StaffLeave[];
}

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class DailyNote {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	scheduleDate: string;
}

class ScheduleStaff {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	@Type(() => DailyNote)
	dailyNote: DailyNote;
}

class StaffLeave {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	date: string;
}
