import { IsDateString, IsNotEmpty, IsOptional } from 'class-validator';

export class PaginateCalendarVehicleDTO {
	@IsNotEmpty()
	@IsDateString()
	from: string;

	@IsNotEmpty()
	@IsDateString()
	to: string;

	@IsOptional()
	page: number;

	@IsOptional()
	search: string;
}
