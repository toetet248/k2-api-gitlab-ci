import { Module } from '@nestjs/common';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { ScheduleSiteModule } from '../schedule-sites/schedule-site.module';
import { ScheduleStaffModule } from '../schedule-staffs/schedule-staff.module';
import { SiteModule } from '../sites/site.module';
import { ScheduleVehicleModule } from '../schedule-vehicles/schedule-vehicle.module';
import { CalendarController } from './calendar.controller';
import { CalendarService } from './calendar.service';
import { ConfigModule } from '@nestjs/config';
import { CommonModule } from 'src/common/common.module';

@Module({
	imports: [
		NestjsFormDataModule,
		ScheduleStaffModule,
		ScheduleVehicleModule,
		SiteModule,
		ScheduleStaffModule,
		ScheduleVehicleModule,
		ScheduleSiteModule,
		ConfigModule,
		CommonModule,
		ScheduleStaffModule,
		ScheduleVehicleModule,
	],
	controllers: [CalendarController],
	providers: [CalendarService],
	exports: [CalendarService],
})
export class CalendarModule {}
