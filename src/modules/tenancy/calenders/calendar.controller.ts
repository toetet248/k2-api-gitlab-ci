import { Controller, UseGuards, Get, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { CalendarService } from './calendar.service';
import { PaginateCalendarStaffDTO } from './dto/paginate-calendar-staff.dto';
import { PaginateCalendarVehicleDTO } from './dto/paginate-calendar-vehicle.dto';
import { PaginateCalendarSiteDTO } from './dto/paginate-calendar-site.dto';
import { PaginateCalendarStaffSerialize } from './serialize/paginate-calendar-staff.serialize';
import { PaginateCalendarVehicleSerialize } from './serialize/paginate-calendar-vehicle.serialize';
import { PaginateCalendarSiteSerialize } from './serialize/paginate-calendar-site.serialize';
import { AuthStaff } from '../auth/decorator/auth-staff.decorator';
import { Staff } from '../staffs/staff.entity';

@Controller('tenants/calendar')
@UseGuards(AuthGuard('tenant-jwt'))
export class CalendarController extends BaseController {
	constructor(private calendarService: CalendarService) {
		super();
	}
	//get calendar site vertical(drag down)
	@Get('/sites/vertical')
	@Serialize(PaginateCalendarSiteSerialize)
	async getVerticalCalendarSite(
		@Query() paginateCalendarSiteDTO: PaginateCalendarSiteDTO,
	) {
		const { data, ...metaData } =
			await this.calendarService.getCalendarSite(
				paginateCalendarSiteDTO,
				'vertical',
			);

		const meta = await this.calendarService.setMeta(metaData);

		return this.paginateResponse(data, meta);
	}

	//get calendar site horizontal(drag right)
	@Get('/sites/horizontal')
	@Serialize(PaginateCalendarSiteSerialize)
	async getHorizontalCalendarSite(
		@Query() paginateCalendarSiteDTO: PaginateCalendarSiteDTO,
	) {
		const calendarSite = await this.calendarService.getCalendarSite(
			paginateCalendarSiteDTO,
			'horizontal',
		);
		return this.response(calendarSite, {
			title: 'get calendar vertical site success',
		});
	}

	//calendar staff vertical(scroll down)
	@Get('/staffs/vertical')
	@Serialize(PaginateCalendarStaffSerialize)
	async getVerticalCalendarStaff(
		@Query() paginateCalendarStaffDTO: PaginateCalendarStaffDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const { data, ...metaData } =
			await this.calendarService.paginateCalendarStaff(
				paginateCalendarStaffDTO,
				'vertical',
				authStaff.id,
			);

		const meta = await this.calendarService.setMeta(metaData);

		return this.paginateResponse(data, meta);
	}

	//calendar staff horizontal(scroll right)
	@Get('/staffs/horizontal')
	@Serialize(PaginateCalendarStaffSerialize)
	async getHorizontalCalendarStaff(
		@Query() paginateCalendarStaffDTO: PaginateCalendarStaffDTO,
		@AuthStaff() authStaff: Staff,
	) {
		const paginate = await this.calendarService.paginateCalendarStaff(
			paginateCalendarStaffDTO,
			'horizontal',
			authStaff.id,
		);

		return this.paginateResponse(paginate);
	}

	//calendar vehicle vertical(scroll down)
	@Get('/vehicles/vertical')
	@Serialize(PaginateCalendarVehicleSerialize)
	async getVerticalCalendarVehicle(
		@Query() paginateCalendarVehicleDTO: PaginateCalendarVehicleDTO,
	) {
		const { data, ...metaData } =
			await this.calendarService.paginateCalendarVehicle(
				paginateCalendarVehicleDTO,
				'vertical',
			);

		console.log(data);
		const meta = await this.calendarService.setMeta(metaData);

		return this.paginateResponse(data, meta);
	}

	//calendar vehicle horizontal(scroll right)
	@Get('/vehicles/horizontal')
	@Serialize(PaginateCalendarVehicleSerialize)
	async getHorizontalCalendarVehicle(
		@Query() paginateCalendarVehicleDTO: PaginateCalendarVehicleDTO,
	) {
		const paginate = await this.calendarService.paginateCalendarVehicle(
			paginateCalendarVehicleDTO,
			'horizontal',
		);

		return this.paginateResponse(paginate);
	}
}
