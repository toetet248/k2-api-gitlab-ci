import { Inject, Injectable } from '@nestjs/common';
import { Brackets, Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { Site, siteStatusCase } from '../sites/site.entity';
import { authStaffOnTopCase, Staff } from '../staffs/staff.entity';
import { Vehicle } from '../vehicles/vehicle.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { ScheduleSite } from '../schedule-sites/schedule-site.entity';
import { PaginateCalendarStaffDTO } from './dto/paginate-calendar-staff.dto';
import { PaginateCalendarVehicleDTO } from './dto/paginate-calendar-vehicle.dto';
import { PaginateCalendarSiteDTO } from './dto/paginate-calendar-site.dto';

@Injectable()
export class CalendarService {
	public readonly scheduleStaffRepo: Repository<ScheduleStaff>;
	public readonly scheduleSiteRepo: Repository<ScheduleSite>;
	public readonly staffRepo: Repository<Staff>;
	public readonly vehicleRepo: Repository<Vehicle>;
	public readonly scheduleVehicleRepo: Repository<ScheduleVehicle>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly siteRepo: Repository<Site>;
	private readonly perPage = 50;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.staffRepo = connection.getRepository(Staff);
		this.vehicleRepo = connection.getRepository(Vehicle);
		this.vehicleRepo = connection.getRepository(Vehicle);
		this.scheduleVehicleRepo = connection.getRepository(ScheduleVehicle);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.siteRepo = connection.getRepository(Site);
		this.scheduleSiteRepo = connection.getRepository(ScheduleSite);
		this.scheduleStaffRepo = connection.getRepository(ScheduleStaff);
	}

	// get Calendar Site
	async getCalendarSite(
		paginateCalendarSiteDTO: PaginateCalendarSiteDTO,
		type: 'vertical' | 'horizontal',
	): Promise<any> {
		const { search, from, to, page = 1 } = paginateCalendarSiteDTO;

		const query = this.siteRepo
			.createQueryBuilder('sites')
			.leftJoinAndSelect('sites.dailyNotes', 'dailyNotes')
			.leftJoinAndSelect('dailyNotes.operation', 'operation')
			.leftJoin('dailyNotes.operationInstruction', 'instruction')
			.where('dailyNotes.scheduleDate BETWEEN :from AND :to', {
				from,
				to,
			})
			.select([
				// sites
				'sites.id',
				'sites.name',
				// dailyNotes
				'dailyNotes.id',
				'dailyNotes.scheduleDate',
				// operation
				'operation.id',
				'operation.name',
				'operation.colorCode',
				'instruction.orderNo as orderNo',
			])
			.addSelect(siteStatusCase(), 'siteStatusIndex')
			.orderBy('siteStatusIndex', 'ASC')
			.orderBy('dailyNotes.scheduleDate', 'ASC')
			.orderBy('orderNo', 'ASC');

		if (search) {
			query.andWhere(
				new Brackets((q) => {
					return q.where('sites.name like :name', {
						name: `%${search}%`,
					});
				}),
			);
		}

		return type == 'horizontal'
			? await query.take(page * this.perPage).getMany()
			: await query.paginate(this.perPage);
	}

	async paginateCalendarStaff(
		paginateCalendarStaffDTO: PaginateCalendarStaffDTO,
		type: 'vertical' | 'horizontal',
		staffId: string,
	): Promise<any> {
		const { search, from, to, page = 1 } = paginateCalendarStaffDTO;
		console.log('...paginateCalendarStaff 1');
		const query = this.staffRepo
			.createQueryBuilder('staffs')
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeam')
			.leftJoinAndSelect('staffs.scheduleStaffs', 'scheduleStaffs')
			.leftJoinAndSelect('staffs.staffLeaves', 'staffLeaves')
			.leftJoinAndSelect(
				'scheduleStaffs.dailyNote',
				'dailyNote',
				`dailyNote.scheduleDate BETWEEN :from AND :to`,
				{
					from,
					to,
				},
			)
			.select([
				// staffs
				'staffs.id',
				'staffs.name',
				'staffs.nameOnTab',
				'staffs.image',
				'staffs.orderNo',
				'staffs.email',
				'staffs.phone',
				'staffs.isSiteStaff',
				'staffs.isSiteAdmin',
				'staffs.isSystemAdmin',
				'staffs.isOwner',
				// staffTeam
				'staffTeam.id',
				'staffTeam.name',
				'staffTeam.colorCode',
				// staffLeaves
				'staffLeaves.id',
				'staffLeaves.date',
				// scheduleStaffs
				'scheduleStaffs.id',
				'scheduleStaffs.dayNightSupport',
				// dailyNote
				'dailyNote.id',
				'dailyNote.scheduleDate',
			])
			// .where('staffs.isOwner = :isOwner', { isOwner: false })
			.where('staffs.status != :status', { status: 'retired' })
			.andWhere('staffs.isSiteStaff = :isSiteStaff', {
				isSiteStaff: true,
			})
			.addSelect(authStaffOnTopCase(staffId), 'authStaffIndex')
			.orderBy('authStaffIndex', 'DESC')
			.addOrderBy('staffs.orderNo', 'ASC');
		console.log('...paginateCalendarStaff 2');

		if (search) {
			query.andWhere(
				new Brackets((q) => {
					return q.where('staffs.name like :name', {
						name: `%${search}%`,
					});
				}),
			);
		}
		console.log('...paginateCalendarStaff 3');

		return type == 'horizontal'
			? await query.take(page * this.perPage).getMany()
			: await query.paginate(this.perPage);
	}

	async paginateCalendarVehicle(
		paginateCalendarVehicleDTO: PaginateCalendarVehicleDTO,
		type: 'vertical' | 'horizontal',
	): Promise<any> {
		let { search, from, to, page = 1 } = paginateCalendarVehicleDTO;

		const query = this.vehicleRepo
			.createQueryBuilder('vehicles')
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroup')
			.leftJoinAndSelect('vehicles.scheduleVehicles', 'scheduleVehicles')
			.leftJoinAndSelect(
				'scheduleVehicles.dailyNote',
				'dailyNote',
				`dailyNote.scheduleDate BETWEEN :from AND :to`,
				{
					from,
					to,
				},
			)
			.select([
				// vehicles
				'vehicles.id',
				'vehicles.nameOnTab',
				'vehicles.image',
				'vehicles.licenseEndDate',
				'vehicles.insuranceEndDate',
				'vehicles.orderNo',
				'vehicles.memo',
				// vehicleGroup
				'vehicleGroup.id',
				'vehicleGroup.name',
				'vehicleGroup.colorCode',
				// scheduleVehicles
				'scheduleVehicles.id',
				'scheduleVehicles.dayNightSupport',
				// dailyNote
				'dailyNote.id',
				'dailyNote.scheduleDate',
			])
			.orderBy('vehicles.orderNo', 'ASC');

		if (search) {
			query.andWhere(
				new Brackets((q) => {
					return q.where('vehicles.nameOnTab like :name', {
						name: `%${search}%`,
					});
				}),
			);
		}

		return type == 'horizontal'
			? await query.take(page * this.perPage).getMany()
			: await query.paginate(this.perPage);
	}

	async setMeta(metaData: any) {
		const {
			from,
			to,
			total,
			per_page,
			current_page,
			prev_page,
			next_page,
			last_page,
		} = metaData;

		return {
			from: from,
			to: to,
			totalItems: total,
			itemsPerPage: per_page,
			currentPage: current_page,
			prevPage: prev_page,
			nextPage: next_page,
			lastPage: last_page,
			totalPages: last_page,
		};
	}
}
