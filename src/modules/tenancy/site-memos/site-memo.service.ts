import { Inject, Injectable, BadRequestException } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { SiteMemo } from './site-memo.entity';
import {
	todayDate,
	generateId,
	validateExists,
} from 'src/common/service/helper.service';
import { Site } from '../sites/site.entity';
import { Staff } from '../staffs/staff.entity';
import { CreateSiteMemoDTO } from '../sites/dto/create-site-memo.dto';
import { UpdateSiteMemoDTO } from '../sites/dto/update-site-memo.dto';

@Injectable()
export class SiteMemoService {
	public readonly repo: Repository<SiteMemo>;
	public readonly siteRepo: Repository<Site>;
	public readonly staffRepo: Repository<Staff>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(SiteMemo);
		this.siteRepo = connection.getRepository(Site);
		this.staffRepo = connection.getRepository(Staff);
	}

	async getSiteMemo() {
		const query = await this.repo
			.createQueryBuilder('site_memos')
			.groupBy('site_memos.scheduleDate')
			.getRawMany();
		return query;
	}

	async getSiteMemobySiteId(siteId: string) {
		if (!(await validateExists(this.siteRepo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		const query = await this.repo
			.createQueryBuilder('site_memos')
			// .leftJoinAndSelect('site_memos.staffs', 'staffs')
			.leftJoin('site_memos.staffs', 'staffs')
			.addSelect([
				'staffs.id',
				'staffs.name',
				'staffs.nameOnTab',
				'staffs.image',
			])
			.leftJoin('staffs.staffTeam', 'staff_teams')
			.addSelect(['staff_teams.colorCode'])
			.orderBy('site_memos.id', 'DESC')
			.where('siteId = :siteId', { siteId: siteId })
			.getRawMany();
		return query;
	}

	async getMemo() {
		const query = this.repo
			.createQueryBuilder('customers')
			.leftJoinAndSelect('customers.staffs', 'staffs')
			.leftJoinAndSelect('customers.salesIncharges', 'salesIncharge')
			.orderBy('customers.orderNo', 'ASC');
		return query;
	}

	async createSiteMemo(
		siteId: string,
		createSiteMemoDTO: CreateSiteMemoDTO,
		staffId: string,
	) {
		if (!(await validateExists(this.siteRepo, ['id', siteId])))
			throw new BadRequestException(['site not found']);

		//create memo
		const data = this.repo.create({
			memo: createSiteMemoDTO.memo,
			id: generateId(),
			siteId,
			scheduleDate: todayDate(),
			staffId,
		});
		return await this.repo.save(data);
	}

	//update memo
	async updateSiteMemo(
		siteMemoId: string,
		updateSiteMemoDTO: UpdateSiteMemoDTO,
		authStaff: Staff,
	) {
		const siteMemo = await this.repo.findOne({
			where: { id: siteMemoId },
		});

		if (siteMemo) {
			const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
				authStaff;

			// throw error if site staff update site memo of other staffs
			if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner)
				if (siteMemo.staffId !== id)
					throw new BadRequestException([
						'cannot update site memo of other staffs',
					]);

			await this.repo.update(siteMemoId, {
				memo: updateSiteMemoDTO.memo,
				staffId: id,
			});

			return await this.repo.findOne(siteMemoId);
		}

		throw new BadRequestException(['site memo not found']);
	}

	//remove memo
	async deleteSiteMemo(siteMemoId: string, authStaff: Staff) {
		const siteMemo = await this.repo.findOne({
			where: { id: siteMemoId },
		});

		if (siteMemo) {
			const { isSiteStaff, isSiteAdmin, isSystemAdmin, isOwner, id } =
				authStaff;

			// throw error if site staff delete site memo of other staffs
			if (isSiteStaff && !isSiteAdmin && !isSystemAdmin && !isOwner)
				if (siteMemo.staffId !== id)
					throw new BadRequestException([
						'cannot delete site memo of other staffs',
					]);

			return this.repo.delete(siteMemoId);
		}

		throw new BadRequestException(['site memo not found']);
	}
}
