import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { SiteMemo } from './site-memo.entity';
import { SiteMemoService } from './site-memo.service';

@Module({
	imports: [TypeOrmModule.forFeature([SiteMemo]), NestjsFormDataModule],
	providers: [SiteMemoService],
	exports: [SiteMemoService],
})
export class SiteMemoModule {}
