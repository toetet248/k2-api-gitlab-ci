import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Site } from '../sites/site.entity';
import { MainEntity } from 'src/common/entity/main.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'site_memos' })
export class SiteMemo extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	memo: string;

	@Column()
	scheduleDate: string;

	@Column()
	staffId: string;

	@ManyToOne(() => Staff, (staff) => staff.siteMemos, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staffs: Staff;

	@ManyToOne(() => Site, (site) => site.siteMemos, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;
}
