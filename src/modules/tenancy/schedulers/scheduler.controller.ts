import {
	BadRequestException,
	Body,
	Controller,
	Param,
	Patch,
	Post,
	UseGuards,
	Get,
	Query,
	Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { validateDateString } from 'src/common/service/helper.service';
import { ScheduleSiteService } from '../schedule-sites/schedule-site.service';
import { CreateScheduleStaffDTO } from './dto/create-schedule-staff.dto';
import { UpdateOrerScheduleStaffDTO } from './dto/update-order-schedule-staff.dto';
import { ScheduleStaffService } from '../schedule-staffs/schedule-staff.service';
import { CreateScheduleStaffSerialize } from './serialize/create-schedule-staff.serialize';
import { CreateScheduleVehicleDTO } from './dto/create-schedule-vehicle.dto';
import { CreateScheduleVehicleSerialize } from './serialize/create-schedule-vehicle.serialize';
import { ScheduleVehicleService } from '../schedule-vehicles/schedule-vehicle.service';
import { SiteService } from '../sites/site.service';
import { GetSchedulerSerialize } from './serialize/get-scheduler.serialize';
import { SchedulerService } from './scheduler.service';
import { GetSchedulerStaffSerialize } from './serialize/get-scheduler-staff.serialize';
import { GetSchedulerVehicleSerialize } from './serialize/get-scheduler-vehicle.serialize';
import { UpdateDailyNoteDTO } from './dto/update-daily-note.dto';
import { UpdateSitesOrderDTO } from './dto/update-sites-order.dto';
import { UpdateOrderScheduleVehicleDTO } from './dto/update-order-schedule-vehicle.dto';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { AuthStaff } from '../auth/decorator/auth-staff.decorator';
import { Staff } from '../staffs/staff.entity';

@Controller('tenants/scheduler')
@UseGuards(AuthGuard('tenant-jwt'))
export class SchedulerController extends BaseController {
	constructor(
		private readonly service: SchedulerService,
		private readonly scheduleStaffService: ScheduleStaffService,
		private readonly scheduleVehicleService: ScheduleVehicleService,
		private readonly scheduleSiteService: ScheduleSiteService,
		private siteService: SiteService,
	) {
		super();
	}

	//schedle-staff order
	@Patch('/schedule-staffs/order')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOrderScheduleStaff(
		@Body() updateOrderScheduleStaffDTO: UpdateOrerScheduleStaffDTO,
	) {
		const data = updateOrderScheduleStaffDTO.scheduleStaffIds.map(
			(id, index) => {
				return this.scheduleStaffService.repo.create({
					id,
					orderNo: index + 1,
				});
			},
		);

		this.scheduleStaffService.repo.save(data);

		return this.response(null, {
			title: 'schedule staff order success',
		});
	}

	//schedle-vehicle order
	@Patch('/schedule-vehicles/order')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOrderScheduleVehicle(
		@Body() updateOrderScheduleVehicleDTO: UpdateOrderScheduleVehicleDTO,
	) {
		const data = updateOrderScheduleVehicleDTO.scheduleVehicleIds.map(
			(id, index) => {
				return this.scheduleVehicleService.repo.create({
					id,
					orderNo: index + 1,
				});
			},
		);

		this.scheduleVehicleService.repo.save(data);

		return this.response(null, {
			title: 'schedule vehicle order success',
		});
	}

	//add schedule staffs
	@Post('/schedule-staffs')
	@Serialize(CreateScheduleStaffSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async createScheduleStaff(
		@Body() createScheduleStaffDTO: CreateScheduleStaffDTO,
	) {
		const scheduleStaff =
			await this.scheduleStaffService.createScheduleStaff(
				createScheduleStaffDTO,
			);

		return this.response(scheduleStaff, {
			title: 'schedule staff create success',
		});
	}

	//add schedule vehicle
	@Post('/schedule-vehicles')
	@Serialize(CreateScheduleVehicleSerialize)
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async createScheduleVehicle(
		@Body() createScheduleVehicleDTO: CreateScheduleVehicleDTO,
	) {
		const scheduleVehicle =
			await this.scheduleVehicleService.createScheduleVehicle(
				createScheduleVehicleDTO,
			);

		return this.response(scheduleVehicle, {
			title: 'schedule vehicle create success',
		});
	}

	@Get('/')
	@Serialize(GetSchedulerSerialize)
	async getScheduler(
		@Query('date') date: string,
		@Query('staffOnly') staffOnly: '0' | '1',
		@AuthStaff() authStaff: Staff,
	) {
		if (!validateDateString(date))
			throw new BadRequestException(['invalid date format']);

		// get siteIds of current date
		const siteIds = await this.siteService.getSiteIdsByDate(
			date,
			staffOnly == '1' ? authStaff.id : null,
		);
		if (!siteIds.length) return this.response({ info: {}, sites: [] });

		// get unique scheduleSites by siteIds order with current date
		const orderScheduleSites =
			await this.scheduleSiteService.getOrderScheduleSitesByDate(
				date,
				siteIds,
			);

		// create schedule sites by date (from recent) or (of site)
		await this.scheduleSiteService.createScheduleSitesByDate(
			date,
			siteIds,
			orderScheduleSites,
		);

		// get sites of current date with schedule sites order no
		const sites = await this.service.getSchedulerData(date, siteIds);

		return this.response({ info: {}, sites });
	}

	// get scheduler staffs with staffTeam info
	@Get('/staffs')
	@Serialize(GetSchedulerStaffSerialize)
	public async getSchedulerStaff(@Query('date') date: string) {
		if (!validateDateString(date))
			throw new BadRequestException(['invalid date format']);

		const scheduleStaffs = await this.service.getSchedulerStaff(date);
		return this.response(scheduleStaffs, {
			title: 'get schedule staff success',
		});
	}

	// get vehicle with vehicle group info
	@Get('/vehicles')
	@Serialize(GetSchedulerVehicleSerialize)
	public async getSchedulerVehicle(@Query('date') date: string) {
		if (!validateDateString(date))
			throw new BadRequestException(['invalid date format']);

		const scheduleVehicles = await this.service.getSchedulerVehicle(date);
		return this.response(scheduleVehicles, {
			title: 'get schedule vehicle success',
		});
	}

	// update daily note info
	@Patch('/daily-notes/:id/update')
	@UseGuards(AuthGuard('site-admin'))
	public async updateDailyNote(
		@Param('id') id: string,
		@Body() updateDailyNoteDTO: UpdateDailyNoteDTO,
	) {
		await this.service.updateDailyNote(id, updateDailyNoteDTO);

		return this.response(null, {
			title: 'update daily note success',
		});
	}

	// update site order
	@Patch('/sites/order')
	@UseGuards(AuthGuard('site-admin'))
	@InjectRequestToBody()
	public async updateSitesOrder(
		@Query('date') date: string,
		@Body() updateSitesOrderDTO: UpdateSitesOrderDTO,
	) {
		if (!validateDateString(date))
			throw new BadRequestException(['invalid date format']);

		await this.service.updateSitesOrder(date, updateSitesOrderDTO);

		return this.response(null, {
			title: 'update site order No success',
		});
	}

	@Delete('/schedule-staffs/:scheduleStaffId')
	@UseGuards(AuthGuard('site-admin'))
	public async deleteScheduleStaff(
		@Param('scheduleStaffId') scheduleStaffId: string,
	) {
		const scheduleStaff = await this.scheduleStaffService.repo.findOne({
			where: { id: scheduleStaffId },
		});

		if (scheduleStaff) {
			await this.scheduleStaffService.repo.remove(scheduleStaff);

			return this.response(null, {
				title: 'remove schedule staff success',
			});
		}

		throw new BadRequestException(['scheduleStaffId does not exists']);
	}

	@Delete('/schedule-vehicles/:scheduleVehicleId')
	@UseGuards(AuthGuard('site-admin'))
	public async deleteScheduleVehicle(
		@Param('scheduleVehicleId') scheduleVehicleId: string,
	) {
		const scheduleVehicle = await this.scheduleVehicleService.repo.findOne({
			where: { id: scheduleVehicleId },
		});

		if (scheduleVehicle) {
			await this.scheduleVehicleService.repo.remove(scheduleVehicle);

			return this.response(null, {
				title: 'remove schedule vehicle success',
			});
		}

		throw new BadRequestException(['scheduleVehicleId does not exists']);
	}
}
