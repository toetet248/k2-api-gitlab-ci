import { Type } from 'class-transformer';
import {
	ArrayUnique,
	IsArray,
	IsBoolean,
	IsNotEmpty,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsTwoArrayUnique } from 'src/common/validator/is-two-array-unique.validator';

export class CreateScheduleStaffDTO {
	@IsExist('daily_notes', 'id')
	@IsNotEmpty()
	@IsString()
	dailyNoteId: string;

	@IsBoolean()
	isUpdateToday: boolean;

	@IsBoolean()
	isUpdateTomorrowToEndDate: boolean;


	@IsArray()
	@Type(() => ScheduledStaff)
	@ValidateNested({ each: true, message: 'staff id does not exist' })
	staffIds: ScheduledStaff[];


	// @IsExist('staffs', 'id', 'new')
	// @IsArray()
	// @ArrayUnique({
	// 	message: 'dayStaffIds must be unique',
	// })
	// @IsString({ each: true })
	// @IsTwoArrayUnique('supportStaffIds')
	// dayStaffIds: string[];

	// @IsExist('staffs', 'id', 'new')
	// @IsArray()
	// @ArrayUnique({
	// 	message: 'supportStaffIds must be unique',
	// })
	// @IsString({ each: true })
	// supportStaffIds: string[];

}

class ScheduledStaff {
	@IsExist('staffs', 'id')
	@IsNotEmpty()	
	staffId: string;

	@IsNotEmpty()
	type: string;
}
