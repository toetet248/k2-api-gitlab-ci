import { ArrayNotEmpty, ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateSitesOrderDTO {
	@IsExist('sites', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique()
	@IsString({ each: true })
	siteIds: string[];
}
