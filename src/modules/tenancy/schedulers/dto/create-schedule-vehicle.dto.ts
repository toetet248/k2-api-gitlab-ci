import { Type } from 'class-transformer';
import {
	ArrayUnique,
	IsArray,
	IsBoolean,
	IsNotEmpty,
	IsString,
	ValidateNested,
} from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsTwoArrayUnique } from 'src/common/validator/is-two-array-unique.validator';

export class CreateScheduleVehicleDTO {
	@IsExist('daily_notes', 'id', 'new')
	@IsNotEmpty()
	@IsString()
	dailyNoteId: string;

	@IsBoolean()
	isUpdateToday: boolean;

	@IsBoolean()
	isUpdateTomorrowToEndDate: boolean;

	@IsArray()
	@Type(() => ScheduledVehicle)
	@ValidateNested({ each: true, message: 'vehicle id does not exist' })
	vehicleIds: ScheduledVehicle[];

	// @IsExist('vehicles', 'id', 'new')
	// @IsArray()
	// @ArrayUnique({
	// 	message: 'daySheduleVehicleIds must be unique',
	// })
	// @IsString({ each: true })
	// @IsTwoArrayUnique('supportVehicleIds')
	// dayVehicleIds: string[];

	// @IsExist('vehicles', 'id')
	// @IsArray()
	// @ArrayUnique({
	// 	message: 'supportScheduleVehicleIds must be unique',
	// })
	// @IsString({ each: true })
	// supportVehicleIds: string[];
}


class ScheduledVehicle {
	@IsExist('vehicles', 'id')
	@IsNotEmpty()
	vehicleId: string;

	@IsNotEmpty()
	type: string;
}
