import { ArrayNotEmpty, ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateOrderScheduleVehicleDTO {
	@IsExist('schedule_vehicles', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique({
		message: 'scheduleVehicleId must be unique',
	})
	@IsString({ each: true })
	scheduleVehicleIds: string[];
}
