import {
	IsBoolean,
	IsEmpty,
	IsMilitaryTime,
	IsNotEmpty,
	IsOptional,
} from 'class-validator';

export class UpdateDailyNoteDTO {
	@IsNotEmpty()
	reserveMan: number;

	@IsNotEmpty()
	reserveHour: number;

	@IsNotEmpty()
	reserveManPower: number;

	@IsOptional()
	@IsMilitaryTime()
	reserveStartTime: number;

	@IsOptional()
	@IsMilitaryTime()
	reserveEndTime: number;

	@IsNotEmpty()
	@IsBoolean()
	isUpdateToday: boolean;

	@IsNotEmpty()
	@IsBoolean()
	isUpdateTomorrowToEndDate: boolean;
}
