import { ArrayNotEmpty, ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateOrerScheduleStaffDTO {
	@IsExist('schedule_staffs', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique({
		message: 'scheduleStaffId must be unique',
	})
	@IsString({ each: true })
	scheduleStaffIds: string[];
}
