import { Expose, Type } from 'class-transformer';

export class GetSchedulerStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => ScheduleStaffs)
	scheduleStaffs: ScheduleStaffs[];

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam[];
}

class StaffTeam {
	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class ScheduleStaffs {
	@Expose()
	id: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	nightSupportCount: string;
}
