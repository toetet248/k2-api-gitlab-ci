import { Expose, Type } from 'class-transformer';

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	phone: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;
}

export class CreateScheduleStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	@Type(() => Staff)
	staff: Staff;
}
