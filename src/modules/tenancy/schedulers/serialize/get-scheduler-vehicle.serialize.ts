import { Expose, Type } from 'class-transformer';

export class GetSchedulerVehicleSerialize {
	@Expose()
	id: string;

	@Expose({ name: 'nameOnTab' })
	name: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => ScheduleVehicles)
	scheduleVehicles: ScheduleVehicles[];

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup[];
}

class VehicleGroup {
	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class ScheduleVehicles {
	@Expose()
	id: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	nightSupportCount: string;
}
