import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';
import { formatDate } from 'src/common/service/helper.service';

export class GetSchedulerSerialize {
	@Expose()
	info: string;

	@Expose()
	@Type(() => Site)
	sites: Site[];
}

class Site {
	@Expose({ name: 'sites_id' })
	id: string;

	@Expose({ name: 'sites_name' })
	name: string;

	@Expose({ name: 'customer_name' })
	customerName: string;

	@Expose({ name: 'sites_addressPrefecture' })
	addressPrefecture: string;

	@Expose({ name: 'sites_addressCity' })
	addressCity: string;

	@Expose({ name: 'sites_addressBuilding' })
	addressBuilding: string;

	@Expose()
	@Type(() => DailyNote)
	dailyNotes: DailyNote[];
}

class Operation {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class DailyNote {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) => formatDate(value))
	scheduleDate: string;

	@Expose()
	operationInstructionId: string;

	@Expose()
	reserveMan: string;

	@Expose()
	reserveHour: string;

	@Expose()
	reserveManPower: string;

	@Expose()
	reserveStartTime: string;

	@Expose()
	reserveEndTime: string;

	@Expose()
	actualMan: string;

	@Expose()
	actualHour: string;

	@Expose()
	actualManPower: string;

	@Expose()
	actualStartTime: string;

	@Expose()
	actualEndTime: string;

	@Expose()
	memo: string;

	@Expose()
	noteAuthor: string;

	@Expose()
	operation: Operation;

	@Expose()
	@Type(() => ScheduleStaff)
	scheduleStaffs: ScheduleStaff[];

	@Expose()
	@Type(() => ScheduleVehicle)
	scheduleVehicles: ScheduleVehicle[];
}

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class Vehicle {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	@Transform(({ value }) => {
		return value ? moment(value).format('YYYY-MM-DD') : value;
	})
	licenseEndDate: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	insuranceEndDate: string;

	@Expose()
	image: string;

	@Expose()
	memo: string;

	@Expose()
	status: string;

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup;
}

class ScheduleVehicle {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	@Type(() => Vehicle)
	vehicle: Vehicle;
}

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class StaffLeave {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) => formatDate(value))
	date: string;
}

class Staff {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	phone: string;

	@Expose()
	email: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: boolean;

	@Expose()
	status: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;

	@Expose()
	@Type(() => StaffLeave)
	staffLeaves: StaffLeave[];
}

class ScheduleStaff {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	@Type(() => Staff)
	staff: Staff;
}
