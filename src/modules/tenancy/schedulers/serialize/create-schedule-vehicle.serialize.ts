import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

class Vehicle {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	image: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	licenseEndDate: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	insuranceEndDate: string;

	@Expose()
	status: string;

	@Expose()
	memo: string;

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup;
}

export class CreateScheduleVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	dayNightSupport: string;

	@Expose()
	dailyNoteId: string;

	@Expose()
	@Type(() => Vehicle)
	vehicle: Vehicle;
}
