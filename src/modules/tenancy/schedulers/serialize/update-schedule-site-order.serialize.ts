import { Expose, Type } from 'class-transformer';

export class UpdateScheduleSiteSerialize {
	@Expose()
	id: string;

	@Expose()
	customerId: string;

	@Expose()
	customerContactId: string;

	@Expose()
	staffId: string;

	@Expose()
	name: string;

	@Expose()
	addressPrefecture: string;

	@Expose()
	addressCity: string;

	@Expose()
	addressBuilding: string;
}
