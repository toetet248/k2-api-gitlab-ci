import { Module } from '@nestjs/common';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { ScheduleSiteModule } from '../schedule-sites/schedule-site.module';
import { ScheduleStaffModule } from '../schedule-staffs/schedule-staff.module';
import { SiteModule } from '../sites/site.module';
import { ScheduleVehicleModule } from '../schedule-vehicles/schedule-vehicle.module';
import { SchedulerController } from './scheduler.controller';
import { SchedulerService } from './scheduler.service';
import { ConfigModule } from '@nestjs/config';
import { CommonModule } from 'src/common/common.module';
import { StaffModule } from '../staffs/staff.module';
import { VehicleModule } from '../vehicles/vehicle.module';

@Module({
	imports: [
		NestjsFormDataModule,
		ScheduleStaffModule,
		ScheduleVehicleModule,
		SiteModule,
		ScheduleSiteModule,
		ConfigModule,
		CommonModule,
        StaffModule,
        VehicleModule
	],
	controllers: [SchedulerController],
	providers: [SchedulerService],
	exports: [SchedulerService],
})
export class SchedulerModule {}
