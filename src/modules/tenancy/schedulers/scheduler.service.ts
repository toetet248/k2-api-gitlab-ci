import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { Brackets, Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { Site } from '../sites/site.entity';
import { Staff } from '../staffs/staff.entity';
import { Vehicle } from '../vehicles/vehicle.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { UpdateDailyNoteDTO } from './dto/update-daily-note.dto';
import { UpdateSitesOrderDTO } from './dto/update-sites-order.dto';
import { ScheduleSite } from '../schedule-sites/schedule-site.entity';
import { validateExists } from 'src/common/service/helper.service';
import moment from 'moment';
import { StaffService } from '../staffs/staff.service';

@Injectable()
export class SchedulerService {
	public readonly scheduleStaffRepo: Repository<ScheduleStaff>;
	public readonly scheduleSiteRepo: Repository<ScheduleSite>;
	public readonly staffRepo: Repository<Staff>;
	public readonly vehicleRepo: Repository<Vehicle>;
	public readonly scheduleVehicleRepo: Repository<ScheduleVehicle>;
	public readonly dailyNoteRepo: Repository<DailyNote>;
	public readonly siteRepo: Repository<Site>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(StaffService) private readonly staffService: StaffService,
	) {
		this.staffRepo = this.staffService.repo;
		this.vehicleRepo = connection.getRepository(Vehicle);
		this.scheduleVehicleRepo = connection.getRepository(ScheduleVehicle);
		this.dailyNoteRepo = connection.getRepository(DailyNote);
		this.siteRepo = connection.getRepository(Site);
		this.scheduleSiteRepo = connection.getRepository(ScheduleSite);
		this.scheduleStaffRepo = connection.getRepository(ScheduleStaff);
	}

	async getSchedulerData(date: string, siteIds: string[]) {
		const scheduleStaffsQuery: any = this.scheduleStaffRepo
			.createQueryBuilder('scheduleStaffs')
			.leftJoinAndSelect('scheduleStaffs.staff', 'staff')
			.withDeleted()
			.leftJoinAndSelect('staff.staffTeam', 'staffTeam')
			.leftJoinAndSelect('staff.staffLeaves', 'staffLeaves')
			.where('staff.deletedAt IS NULL')
			.andWhere('staff.status != "retired" OR staff.status IS NULL') // null is checked for previously created tenant owner 
			.select([
				// schedule staffs
				'scheduleStaffs.id',
				'scheduleStaffs.staffId',
				'scheduleStaffs.dailyNoteId',
				'scheduleStaffs.dayNightSupport',
				'scheduleStaffs.orderNo',
				// staffs
				'staff.id',
				'staff.name',
				'staff.nameOnTab',
				'staff.phone',
				'staff.email',
				'staff.isSiteStaff',
				'staff.isSiteAdmin',
				'staff.isSystemAdmin',
				'staff.isOwner',
				'staff.status',
				'staff.image',
				// staff teams
				'staffTeam.id',
				'staffTeam.name',
				'staffTeam.colorCode',
				// staff leaves
				'staffLeaves',
			]);

		const scheduleVehiclesQuery: any = this.scheduleVehicleRepo
			.createQueryBuilder('scheduleVehicles')
			.leftJoinAndSelect('scheduleVehicles.vehicle', 'vehicle')
			.withDeleted()
			.leftJoinAndSelect('vehicle.vehicleGroup', 'vehicleGroup')
			.where('vehicle.deletedAt IS NULL')
			.andWhere('vehicle.status != "out_from_list"')
			.select([
				// scheduleVehicles
				'scheduleVehicles.id',
				'scheduleVehicles.vehicleId',
				'scheduleVehicles.dailyNoteId',
				'scheduleVehicles.dayNightSupport',
				'scheduleVehicles.orderNo',
				// vehicle
				'vehicle.id',
				'vehicle.nameOnTab',
				'vehicle.licenseEndDate',
				'vehicle.insuranceEndDate',
				'vehicle.image',
				'vehicle.memo',
				'vehicle.status',
				// vehicleGroup
				'vehicleGroup.id',
				'vehicleGroup.name',
				'vehicleGroup.colorCode',
			]);

		const dailyNotesQuery: any = this.dailyNoteRepo
			.createQueryBuilder('dailyNotes')
			.leftJoinAndSelect(
				'dailyNotes.operationInstruction',
				'operationInstruction',
			)
			.leftJoinAndSelect('dailyNotes.staff', 'author')
			.leftJoinAndSelect('dailyNotes.operation', 'operation')
			.leftJoinAndSelect(
				'(' + scheduleStaffsQuery.getQuery() + ')',
				'scheduleStaffs',
				'dailyNotes.id = scheduleStaffs_dailyNoteId',
			)
			.leftJoinAndSelect(
				'(' + scheduleVehiclesQuery.getQuery() + ')',
				'scheduleVehicles',
				'dailyNotes.id = scheduleVehicles_dailyNoteId',
			)
			.select([
				// dailyNotes
				'dailyNotes.id',
				'dailyNotes.siteId',
				'dailyNotes.scheduleDate',
				'dailyNotes.operationInstructionId',
				'dailyNotes.reserveMan',
				'dailyNotes.reserveHour',
				'dailyNotes.reserveManPower',
				'dailyNotes.reserveStartTime',
				'dailyNotes.reserveEndTime',
				'dailyNotes.actualMan',
				'dailyNotes.actualHour',
				'dailyNotes.actualManPower',
				'dailyNotes.actualStartTime',
				'dailyNotes.actualEndTime',
				'dailyNotes.memo',
				// operationInstruction
				'operationInstruction.id',
				// author
				'author.name',
				'author.image',
				// operation
				'operation.id',
				'operation.name',
				'operation.colorCode',
				// scheduleStaffs
				'scheduleStaffs',
				// scheduleVehicles
				'scheduleVehicles',
			])
			.where('dailyNotes.scheduleDate = :date', { date });

		const orderSitesData = await this.siteRepo
			.createQueryBuilder('sites')
			.select([
				// sites
				'sites.id',
				'sites.name',
				'sites.addressPrefecture',
				'sites.addressCity',
				'sites.addressBuilding',
				// customer
				'customer.name',
			])
			.leftJoinAndSelect('sites.scheduleSites', 'scheduleSites')
			.leftJoinAndSelect('sites.customer', 'customer')
			.leftJoinAndSelect(
				'(' + dailyNotesQuery.getQuery() + ')',
				'dailyNotes',
				'sites.id = dailyNotes_siteId',
			)
			.where('sites.id IN(:...siteIds)', { siteIds })
			.andWhere('scheduleSites.scheduleDate = :date', { date })
			.orderBy('scheduleSites_orderNo', 'ASC')
			.addOrderBy('operationInstruction_id', 'ASC')
			.addOrderBy('scheduleStaffs_orderNo', 'ASC')
			.addOrderBy('scheduleVehicles_orderNo', 'ASC')
			.getRawMany();

		let orderSites = [];

		orderSitesData.map((orderSite) => {
			const dailyNote = {
				id: orderSite.dailyNotes_id,
				scheduleDate: orderSite.dailyNotes_scheduleDate,
				operationInstructionId:
					orderSite.dailyNotes_operationInstructionId,
				orderNo: orderSite.operationInstruction_orderNo,
				reserveMan: orderSite.dailyNotes_reserveMan,
				reserveHour: orderSite.dailyNotes_reserveHour,
				reserveManPower: orderSite.dailyNotes_reserveManPower,
				reserveStartTime: orderSite.dailyNotes_reserveStartTime,
				reserveEndTime: orderSite.dailyNotes_reserveEndTime,
				actualMan: orderSite.dailyNotes_actualMan,
				actualHour: orderSite.dailyNotes_actualHour,
				actualManPower: orderSite.dailyNotes_actualManPower,
				actualStartTime: orderSite.dailyNotes_actualStartTime,
				actualEndTime: orderSite.dailyNotes_actualEndTime,
				noteAuthor: {
					name: orderSite.author_name,
					image: orderSite.author_image,
				},
				memo: orderSite.dailyNotes_memo,
				operation: {
					id: orderSite.operation_id,
					name: orderSite.operation_name,
					colorCode: orderSite.operation_colorCode,
				},
				scheduleStaffs: [],
				scheduleVehicles: [],
			};

			const scheduleStaff = {
				id: orderSite.scheduleStaffs_id,
				dayNightSupport: orderSite.scheduleStaffs_dayNightSupport,
				dailyNoteId: orderSite.scheduleStaffs_dailyNoteId,
				staff: {
					id: orderSite.staff_id,
					name: orderSite.staff_name,
					nameOnTab: orderSite.staff_nameOnTab,
					phone: orderSite.staff_phone,
					email: orderSite.staff_email,
					image: orderSite.staff_image,
					isSiteStaff: orderSite.staff_isSiteStaff,
					isSiteAdmin: orderSite.staff_isSiteAdmin,
					isSystemAdmin: orderSite.staff_isSystemAdmin,
					isOwner: orderSite.staff_isOwner,
					status: orderSite.staff_status,
					staffTeam: {
						id: orderSite.staffTeam_id,
						name: orderSite.staffTeam_name,
						colorCode: orderSite.staffTeam_colorCode,
					},
					staffLeaves: [],
				},
			};

			const staffLeave = {
				id: orderSite.staffLeaves_id,
				date: orderSite.staffLeaves_date,
			};

			const scheduleVehicle = {
				id: orderSite.scheduleVehicles_id,
				dayNightSupport: orderSite.scheduleVehicles_dayNightSupport,
				dailyNoteId: orderSite.scheduleVehicles_dailyNoteId,
				vehicle: {
					id: orderSite.vehicle_id,
					nameOnTab: orderSite.vehicle_nameOnTab,
					licenseEndDate: orderSite.vehicle_licenseEndDate,
					insuranceEndDate: orderSite.vehicle_insuranceEndDate,
					image: orderSite.vehicle_image,
					memo: orderSite.vehicle_memo,
					status: orderSite.vehicle_status,
					vehicleGroup: {
						id: orderSite.vehicleGroup_id,
						name: orderSite.vehicleGroup_name,
						colorCode: orderSite.vehicleGroup_colorCode,
					},
				},
			};

			if (
				orderSites.find((site) => site.sites_id == orderSite.sites_id)
			) {
				// console.log(orderSites, '...orderSites');
				const index = orderSites.findIndex(
					(site) => site.sites_id == orderSite.sites_id,
				);

				if (dailyNote.id) {
					if (
						orderSites[index]['dailyNotes'].find(
							(dn) => dn.id == orderSite.dailyNotes_id,
						)
					) {
						const dailyNoteIndex = orderSites[index][
							'dailyNotes'
						].findIndex((dn) => dn.id == orderSite.dailyNotes_id);

						if (orderSite.scheduleStaffs_id) {
							const orderSitesScheduleStaffs =
								orderSites[index]['dailyNotes'][dailyNoteIndex]
									.scheduleStaffs;

							const scheduleStaffIndex =
								orderSitesScheduleStaffs.findIndex(
									(ss) =>
										ss.id == orderSite.scheduleStaffs_id,
								);

							if (scheduleStaffIndex < 0) {
								if (staffLeave.id) {
									// console.log('aaaa');
									scheduleStaff['staff']['staffLeaves'].push(
										staffLeave,
									);
								}
								orderSites[index]['dailyNotes'][
									dailyNoteIndex
								].scheduleStaffs.push(scheduleStaff);
							} else {
								if (staffLeave.id) {
									orderSites[index]['dailyNotes'][
										dailyNoteIndex
									]['scheduleStaffs'][scheduleStaffIndex][
										'staff'
									]['staffLeaves'].push(staffLeave);
								}
							}
						}

						if (orderSite.scheduleVehicles_id) {
							const orderSitesScheduleVehicles =
								orderSites[index]['dailyNotes'][dailyNoteIndex]
									.scheduleVehicles;
							if (
								!orderSitesScheduleVehicles.find(
									(sv) =>
										sv.id == orderSite.scheduleVehicles_id,
								)
							)
								orderSites[index]['dailyNotes'][
									dailyNoteIndex
								].scheduleVehicles.push(scheduleVehicle);
						}
					} else {
						if (orderSite.scheduleStaffs_id) {
							if (staffLeave.id) {
								// console.log('bbb');
								scheduleStaff['staff']['staffLeaves'].push(
									staffLeave,
								);
								// console.log(scheduleStaff);
							}
							dailyNote.scheduleStaffs.push(scheduleStaff);
						}

						if (orderSite.scheduleVehicles_id)
							dailyNote.scheduleVehicles.push(scheduleVehicle);

						orderSites[index]['dailyNotes'].push(dailyNote);
					}
				}
			} else {
				if (dailyNote.id) {
					if (orderSite.scheduleStaffs_id) {
						if (staffLeave.id) {
							// console.log('ccc');
							scheduleStaff['staff']['staffLeaves'].push(
								staffLeave,
							);
						}
						dailyNote.scheduleStaffs.push(scheduleStaff);
					}

					if (orderSite.scheduleVehicles_id)
						dailyNote.scheduleVehicles.push(scheduleVehicle);

					orderSites.push({
						...orderSite,
						dailyNotes: [dailyNote],
					});
				}
			}
		});

		return orderSites;
	}

	// // get Scheduler Staff with staffTeam  and staff info
	// async getSchedulerStaff(date: string) {
	// 	const query = this.staffRepo
	// 		.createQueryBuilder('staffs')
	// 		.withDeleted()
	// 		.leftJoinAndSelect(
	// 			'staffs.scheduleStaffs',
	// 			'scheduleStaffs',
	// 			'scheduleStaffs.deletedAt IS NULL',
	// 		)
	// 		.leftJoinAndSelect('staffs.staffTeam', 'staffTeam')
	// 		.leftJoinAndSelect(
	// 			'scheduleStaffs.dailyNote',
	// 			'dailyNote',
	// 			'dailyNote.deletedAt IS NULL',
	// 		)
	// 		.where('dailyNote.scheduleDate = :date', {
	// 			date,
	// 		})
	// 		// .where(
	// 		// 	new Brackets((q) => {
	// 		// 		q.where('dailyNote.scheduleDate = :date', {
	// 		// 			date,
	// 		// 		}).orWhere('scheduleStaffs.dailyNote IS NULL');
	// 		// 	}),
	// 		// )
	// 		// .andWhere('staffs.isOwner = :isOwner', {
	// 		// 	isOwner: 0,
	// 		// })
	// 		.select([
	// 			'staffs.id',
	// 			'staffs.name',
	// 			'staffs.image',
	// 			'staffTeam',
	// 			'scheduleStaffs',
	// 		])
	// 		.orderBy('staffs.orderNo', 'ASC')
	// 		.getMany();

	// 	return query;
	// }

	// get Scheduler Staff with staffTeam  and staff info
	async getSchedulerStaff(date: string) {
		let staffs = await this.staffRepo
			.createQueryBuilder('staffs')
			.withDeleted()
			.leftJoinAndSelect(
				'staffs.scheduleStaffs',
				'scheduleStaffs',
				'scheduleStaffs.deletedAt IS NULL',
			)
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeam')
			.leftJoinAndSelect('scheduleStaffs.dailyNote', 'dailyNote')
			// .where('staffs.isOwner = :isOwner', { isOwner: false })
			.andWhere('staffs.isSiteStaff = :isSiteStaff', {
				isSiteStaff: true,
			})
			.andWhere('staffs.deletedAt IS NULL')
			.select([
				'staffs.id',
				'staffs.name',
				'staffs.image',
				'staffs.nameOnTab',
				'staffTeam',
				'scheduleStaffs',
				'dailyNote',
			])
			.orderBy('staffs.orderNo', 'ASC')
			.getMany();

		staffs = staffs.map((staff) => {
			if (staff.scheduleStaffs) {
				staff.scheduleStaffs = staff.scheduleStaffs.filter(
					(scheduleStaff) => {
						return (
							moment(scheduleStaff.dailyNote.scheduleDate).format(
								'YYYY-MM-DD',
							) == date && !scheduleStaff.dailyNote.deletedAt
						);
					},
				);
			}
			return staff;
		});

		return staffs;
	}

	async getSchedulerVehicle(date: string) {
		let vehicles = await this.vehicleRepo
			.createQueryBuilder('vehicles')
			.withDeleted()
			.leftJoinAndSelect(
				'vehicles.scheduleVehicles',
				'scheduleVehicles',
				'scheduleVehicles.deletedAt IS NULL',
			)
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroup')
			.leftJoinAndSelect(
				'scheduleVehicles.dailyNote',
				'dailyNote',
				// `(dailyNote.scheduleDate = ${date}) AND (dailyNote.deletedAt IS NULL)`,
			)
			.where('vehicles.deletedAt IS NULL')
			.select([
				'vehicles.id',
				'vehicles.nameOnTab',
				'vehicles.image',
				'vehicleGroup',
				'scheduleVehicles',
				'dailyNote',
			])
			.orderBy('vehicles.orderNo', 'ASC')
			.getMany();

		vehicles = vehicles.map((vehicle) => {
			if (vehicle.scheduleVehicles) {
				vehicle.scheduleVehicles = vehicle.scheduleVehicles.filter(
					(scheduleVehicle) => {
						return (
							moment(
								scheduleVehicle.dailyNote.scheduleDate,
							).format('YYYY-MM-DD') == date &&
							!scheduleVehicle.dailyNote.deletedAt
						);
					},
				);
			}
			return vehicle;
		});

		return vehicles;
	}

	//get Scheduler Vehicle with VehicleGroup and vehicle info
	async updateDailyNote(id: string, updateDailyNoteDTO: UpdateDailyNoteDTO) {
		const {
			reserveMan,
			reserveHour,
			reserveManPower,
			reserveStartTime,
			reserveEndTime,
			isUpdateToday,
			isUpdateTomorrowToEndDate,
		} = updateDailyNoteDTO;

		const dailyNote = await this.dailyNoteRepo.findOne({
			where: { id },
		});

		if (!dailyNote)
			throw new BadRequestException(['dailyNote does not exists']);

		if (isUpdateToday) {
			await this.dailyNoteRepo.update(id, {
				reserveMan,
				reserveHour,
				reserveManPower,
				reserveStartTime,
				reserveEndTime,
			});
		}
		// else {
		// 	throw new BadRequestException(['isUpdateToday must be true']);
		// }

		if (isUpdateTomorrowToEndDate) {
			const { siteId, operationId, scheduleDate } = dailyNote;

			await this.dailyNoteRepo
				.createQueryBuilder()
				.update('daily_notes')
				.set({
					reserveMan,
					reserveHour,
					reserveManPower,
					reserveStartTime,
					reserveEndTime,
				})
				.where('scheduleDate > :scheduleDate', {
					scheduleDate,
				})
				.andWhere('siteId = :siteId', {
					siteId,
				})
				.andWhere('operationId = :operationId', {
					operationId,
				})
				.execute();
		}
	}

	// update Site Order No
	async updateSitesOrder(
		date: string,
		updateSitesOrderDTO: UpdateSitesOrderDTO,
	) {
		const updateSitesOrderPromises = [];
		updateSitesOrderDTO.siteIds.map((siteId, index) => {
			updateSitesOrderPromises.push(
				this.scheduleSiteRepo
					.createQueryBuilder()
					.update('schedule_sites')
					.set({
						orderNo: index + 1,
					})
					.where('scheduleDate >= :date', {
						date,
					})
					.andWhere('siteId = :siteId', {
						siteId,
					})
					.execute(),
			);
		});

		await Promise.all(updateSitesOrderPromises);
	}
}
