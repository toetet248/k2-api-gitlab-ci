import * as configuration from '../../config/config';
import { getConnectionManager } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { map } from 'modern-async';

async function revertTenantMigration() {
	const config = configuration.default();

	// get tenant names from central
	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const centralConnection = connectionManager.create({
		...(centralDBConfig as MysqlConnectionOptions),
		name: 'k2_central',
		database: 'k2_central',
	});

	const tenants = await (
		await centralConnection.connect()
	).query('SELECT name FROM tenants');

	// run migration for all tenant db
	const tenantDBConfig = config.database.tenant;

	await map(tenants, async (tenant: any) => {
		const tenantConnectionConfig = {
			...(tenantDBConfig as MysqlConnectionOptions),
			name: `${config.database.tenantDbPrefix + tenant.name}`,
			database: `${config.database.tenantDbPrefix + tenant.name}`,
		};

		const tenantConnection = await connectionManager
			.create(tenantConnectionConfig)
			.connect();

		await tenantConnection.undoLastMigration();

		await tenantConnection.close();
	});

	process.exit(1);
}

revertTenantMigration();
