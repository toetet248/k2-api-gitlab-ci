import { Controller } from '@nestjs/common';
import { BaseController } from 'src/common/controller/base.controller';

@Controller('tenants')
export class TenancyController extends BaseController {}
