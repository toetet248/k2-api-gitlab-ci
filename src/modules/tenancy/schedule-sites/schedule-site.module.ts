import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { ScheduleSite } from './schedule-site.entity';
import { ScheduleSiteService } from './schedule-site.service';

@Module({
	imports: [TypeOrmModule.forFeature([ScheduleSite]), NestjsFormDataModule],
	providers: [ScheduleSiteService],
	exports: [ScheduleSiteService],
})
export class ScheduleSiteModule {}
