import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { DailyNote } from '../daily-notes/daily-note.entity';
import { Site } from '../sites/site.entity';

@Entity({ name: 'schedule_sites' })
export class ScheduleSite extends MainEntity {
	@Column()
	siteId: string;

	@Column()
	scheduleDate: Date;

	@Column()
	orderNo: number;

	@ManyToOne(() => Site, (site) => site.scheduleSites, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;
}
