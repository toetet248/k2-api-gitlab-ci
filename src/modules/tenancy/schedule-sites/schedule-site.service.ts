import { Inject, Injectable } from '@nestjs/common';
import { formatDate, generateId } from 'src/common/service/helper.service';
import { Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { ScheduleSite } from './schedule-site.entity';

@Injectable()
export class ScheduleSiteService {
	public readonly repo: Repository<ScheduleSite>;
	public readonly connection: Connection;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(ScheduleSite);
        this.connection = connection;
	}

	async getOrderScheduleSitesByDate(date: string, siteIds: string[]) {
        const [query,parameters] = this.connection.driver.escapeQueryWithParameters(
            'SELECT * FROM ( SELECT DISTINCT (siteId), id, scheduleDate, orderNo FROM schedule_sites WHERE (scheduleDate <= :date) AND (siteId IN (:siteIds)) ORDER BY scheduleDate DESC ) sub GROUP BY siteId',
            {
                date,
                siteIds
            },
            {}
         );

		return await this.repo.query(query, parameters);
	}

	async createScheduleSitesByDate(
		date: string,
		siteIds: string[],
		orderScheduleSites: ScheduleSite[],
	) {
		const newScheduleSitesIds = [];

		const newScheduleSitesFromRecent = [];

		// set new scheduleSite to create ( from recent dates ) or  ( of new site )
		siteIds.map((siteId) => {
			const orderScheduleSite = orderScheduleSites.find(
				(oss) => oss.siteId == siteId,
			);

			if (orderScheduleSite) {
				if (formatDate(orderScheduleSite.scheduleDate) !== date) {
					const newScheduleSite = this.repo.create({
						id: generateId(),
						siteId,
						scheduleDate: date,
						orderNo: orderScheduleSite.orderNo,
					});
					newScheduleSitesFromRecent.push(newScheduleSite);
				}
			} else {
				newScheduleSitesIds.push(siteId);
			}
		});

		// create new scheduleSite of new sites
		const newScheduleSites = [];
		if (newScheduleSitesIds.length) {
			let orderNo =
				(await (
					await this.repo
						.createQueryBuilder('scheduleSites')
						.select('MAX(scheduleSites.orderNo)', 'max')
						.where('scheduleSites.scheduleDate = :date', { date })
						.getRawOne()
				).max) + 1;

			newScheduleSitesIds.map((newScheduleSiteId) => {
				const newScheduleSite = this.repo.create({
					id: generateId(),
					siteId: newScheduleSiteId,
					scheduleDate: date,
					orderNo,
				});
				orderNo++;
				newScheduleSites.push(newScheduleSite);
			});
		}

		// create new scheduleSite ( from recent dates ) and ( new site )
		await this.repo.save([
			...newScheduleSitesFromRecent,
			...newScheduleSites,
		]);
	}
}
