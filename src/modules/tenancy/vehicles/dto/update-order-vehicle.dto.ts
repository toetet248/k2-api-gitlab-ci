import { ArrayNotEmpty, ArrayUnique, IsArray, IsString } from 'class-validator';
import { IsExist } from 'src/common/validator/is-exist.validator';

export class UpdateOrderVehicleDTO {
	@IsExist('vehicles', 'id')
	@IsArray()
	@ArrayNotEmpty()
	@ArrayUnique({
		message: 'id must be unique',
	})
	@IsString({ each: true })
	ids: string[];
}
