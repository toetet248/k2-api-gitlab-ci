import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsUnique } from 'src/common/validator/is-unique.validator';

export class UpdateVehicleDTO {
	@IsNotEmpty()
	@IsUnique('vehicles', 'nameOnTab', 'id')
	@MaxLength(8)
	nameOnTab: string;

	@IsOptional()
	licenseEndDate: string;

	@IsOptional()
	insuranceEndDate: string;

	@IsNotEmpty()
	status:
		| 'in_operation'
		| 'no_operation'
		| 'inspection'
		| 'repairing'
		| 'out_from_list';

	@IsExist('vehicle_groups', 'id', 'new')
	@IsOptional()
	vehicleGroupId: string;

	@IsOptional()
	@IsBase64Image(true)
	image: any;

	@IsOptional()
	memo: string;
}
