import { Transform } from 'class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';

export class PaginateVehicleDTO {
	@IsOptional()
	page: string;

	@IsOptional()
	search: string;

	@IsOptional()
	status: string;

	@IsOptional()
	isInsuranceAlert: any; //保管期日一か月前

	@IsOptional()
	isInsuranceExpired: any; //保管期日超過

	@IsOptional()
	isLicenseAlert: any; // 免許期日一か月前

	@IsOptional()
	isLicenseExpired: any; //免許期日超過

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	vehicleGroupIds: string[];

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => (value === 'true' || value === '1'))
	noTeam: boolean;
}
