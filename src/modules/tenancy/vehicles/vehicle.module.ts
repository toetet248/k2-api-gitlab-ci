import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { Vehicle } from './vehicle.entity';
import { VehicleService } from './vehicle.service';
import { VehicleController } from './vehicle.controller';
import { ConfigModule } from '@nestjs/config';
import { CommonModule } from 'src/common/common.module';
import { ScheduleVehicleModule } from '../schedule-vehicles/schedule-vehicle.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';

@Module({
	imports: [
		TypeOrmModule.forFeature([Vehicle]),
		NestjsFormDataModule,
		ConfigModule,
		CommonModule,
		ScheduleVehicleModule,
		TenantModule,
	],
	controllers: [VehicleController],
	providers: [VehicleService],
})
export class VehicleModule {}
