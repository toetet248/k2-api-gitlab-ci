import {
	BadRequestException,
	ForbiddenException,
	Inject,
	Injectable,
	NotAcceptableException,
} from '@nestjs/common';
import { Brackets, Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { CreateVehicleDTO } from './dto/create-vehicle.dto';
import { Vehicle } from './vehicle.entity';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UpdateVehicleDTO } from './dto/update-vehicle.dto';
import {
	afterOneMonthDate,
	formatDate,
	generateId,
	todayDate,
} from 'src/common/service/helper.service';
import { VehicleGroup } from '../vehicle-groups/vehicle-group.entity';
import { FileService } from 'src/common/service/file.service';
import { PaginateVehicleDTO } from './dto/paginate-vehicle.dto';
import { isBoolean } from 'node-boolify';
import { TenantService } from 'src/modules/central/tenants/tenant.service';

@Injectable()
export class VehicleService {
	public readonly repo: Repository<Vehicle>;
	public readonly vehicleGroupRepo: Repository<VehicleGroup>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(FileService) private readonly fileService: FileService,
		@Inject(TenantService) private readonly tenantService: TenantService,
	) {
		this.repo = connection.getRepository(Vehicle);
		this.vehicleGroupRepo = connection.getRepository(VehicleGroup);
	}

	async paginateVehicle(
		paginateVehicleDTO: PaginateVehicleDTO,
	): Promise<Pagination<Vehicle>> {
		let {
			search,
			status,
			isInsuranceAlert,
			isInsuranceExpired,
			isLicenseAlert,
			isLicenseExpired,
			vehicleGroupIds,
			noTeam,
			page = 1,
		} = paginateVehicleDTO;

		isInsuranceAlert = isBoolean(isInsuranceAlert);
		isInsuranceExpired = isBoolean(isInsuranceExpired);
		isLicenseAlert = isBoolean(isLicenseAlert);
		isLicenseExpired = isBoolean(isLicenseExpired);

		const today = todayDate();
		const dateSoon = afterOneMonthDate(31).format('YYYY-MM-DD');

		// let page;
		// if (!page) page = 1;

		const query = this.repo
			.createQueryBuilder('vehicles')
			.withDeleted()
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroups')
			.where('vehicles.deletedAt IS NULL')
			.orderBy('vehicles.orderNo', 'ASC');

		if (search)
			query.andWhere(
				new Brackets((q) => {
					return q
						.where('vehicles.nameOnTab like :nameOnTab', {
							nameOnTab: `%${search}%`,
						})
						.orWhere('vehicleGroups.name like :name', {
							name: `%${search}%`,
						})
						.orWhere(
							'vehicles.licenseEndDate like :licenseEndDate',
							{
								licenseEndDate: `%${search}%`,
							},
						)
						.orWhere(
							'vehicles.insuranceEndDate like :insuranceEndDate',
							{
								insuranceEndDate: `%${search}%`,
							},
						);
				}),
			);

		query.andWhere(
			new Brackets((q) => {
				if (noTeam) {
					return q
						.where('vehicles.vehicleGroupId IN(:vehicleGroupIds)', {
							vehicleGroupIds: vehicleGroupIds || [''],
						})
						.orWhere('vehicles.vehicleGroupId IS NULL');
				} else if (vehicleGroupIds) {
					return q.where(
						'vehicles.vehicleGroupId IN(:vehicleGroupIds)',
						{
							vehicleGroupIds,
						},
					);
				}
			}),
		);

		// if (vehicleGroupIds)
		// 	query.andWhere('vehicles.vehicleGroupId IN(:vehicleGroupIds)', {
		// 		vehicleGroupIds,
		// 	});

		if (status) {
			// normal ==>  exclude junk vehicles
			if (status === 'normal') {
				query.andWhere('vehicles.status != :status', {
					status: 'out_from_list',
				});
			} else {
				query.andWhere('vehicles.status = :status', {
					status: status,
				});
			}
		}

		if (
			isInsuranceAlert ||
			isInsuranceExpired ||
			isLicenseAlert ||
			isLicenseExpired
		) {
			query.andWhere(
				new Brackets((q) => {
					//保管期日一か月前
					if (isInsuranceAlert)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('vehicles.insuranceEndDate >=:today', {
									today,
								}).andWhere(
									'vehicles.insuranceEndDate <= :dateSoon',
									{ dateSoon },
								);
							}),
						);

					//保管期日超過
					if (isInsuranceExpired)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('vehicles.insuranceEndDate < :today', {
									today,
								});
							}),
						);

					// 免許期日一か月前
					if (isLicenseAlert)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('vehicles.licenseEndDate >=:today', {
									today,
								}).andWhere(
									'vehicles.licenseEndDate <= :dateSoon',
									{ dateSoon },
								);
							}),
						);

					//免許期日超過
					if (isLicenseExpired)
						q.orWhere(
							new Brackets((sq) => {
								sq.where('vehicles.licenseEndDate < :today', {
									today,
								});
							}),
						);
				}),
			);
		}

		// const data = await paginate<Vehicle>(query, { page, limit: 30 });

		// return data;

		const data = await query.paginate(50);

		return {
			items: data.data,
			meta: {
				totalItems: data.total,
				itemCount: data.data.length,
				itemsPerPage: data.per_page,
				totalPages: data.last_page,
				currentPage: data.current_page,
			},
		};
	}

	async createVehicle(
		createVehicleDTO: CreateVehicleDTO,
		tenantName: string,
	) {
		// start - check plan and unauthorized
		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);
		const currentPlan = await this.tenantService.currentPlan(
			activeSubscription,
		);

		if (currentPlan.name == 'free') {
			const activeVehiclesCount = await this.repo.count({
				where: [
					{
						status: 'in_operation',
					},
					{
						status: 'no_operation',
					},
					{
						status: 'inspection',
					},
					{
						status: 'repairing',
					},
				],
			});

			if (activeVehiclesCount >= currentPlan.options['vehicleLimit'])
				throw new NotAcceptableException([
					'not-accepted-for-free-plan',
				]);
		}
		// end - check plan and unauthorized

		// set order no to create data
		const orderNo =
			(await (
				await this.repo
					.createQueryBuilder('vehicles')
					.select('MAX(vehicles.orderNo)', 'max')
					.getRawOne()
			).max) + 1;

		const requestData = {
			nameOnTab: createVehicleDTO.nameOnTab,
			vehicleGroupId: createVehicleDTO.vehicleGroupId,
			licenseEndDate: formatDate(createVehicleDTO.licenseEndDate),
			insuranceEndDate: formatDate(createVehicleDTO.insuranceEndDate),
			status: createVehicleDTO.status,
			memo: createVehicleDTO.memo,
		};

		// also for vehicle create from scheduler
		const createVehicle = await this.repo.save({
			...requestData,
			id: generateId(),
			image: this.fileService.uploadFile(
				createVehicleDTO.image,
				'/vehicles/images',
				'VI',
				tenantName,
			),
			orderNo,
		});

		const result = await this.repo
			.createQueryBuilder('vehicles')
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroup')
			.leftJoin('vehicles.scheduleVehicles', 'schedule_vehicles')
			.addSelect([
				'schedule_vehicles.dayNightSupport',
				'schedule_vehicles.nightSupportCount',
			])
			.orderBy('vehicles.orderNo', 'ASC')
			.select([
				'vehicles.id',
				'vehicles.nameOnTab',
				'vehicles.image',
				'vehicles.insuranceEndDate',
				'vehicles.licenseEndDate',
				'vehicles.status',
				'vehicles.vehicleGroupId',
				'schedule_vehicles',
				'vehicleGroup',
			])
			.where('vehicles.id = :id', { id: createVehicle.id })
			.getOne();

		return result;
	}

	async updateVehicle(
		id: string,
		updateVehicleDTO: UpdateVehicleDTO,
		tenantName: string,
	) {
		const vehicle = await this.repo.findOne({
			where: { id },
			withDeleted: true,
		});

		if (vehicle) {
			// start - check plan and unauthorized
			const activeSubscription =
				await this.tenantService.activeSubscription(tenantName);
			const currentPlan = await this.tenantService.currentPlan(
				activeSubscription,
			);

			if (currentPlan.name == 'free') {
				const activeVehiclesCount = await this.repo.count({
					where: [
						{
							status: 'in_operation',
						},
						{
							status: 'no_operation',
						},
						{
							status: 'inspection',
						},
						{
							status: 'repairing',
						},
					],
				});

				if (vehicle.status == 'out_from_list') {
					if (updateVehicleDTO.status !== 'out_from_list') {
						if (
							activeVehiclesCount >=
							currentPlan.options['vehicleLimit']
						)
							throw new ForbiddenException([
								'forbidden-for-free-plan',
							]);
					}
				}
			}
			// end - check plan and unauthorized

			if (updateVehicleDTO.image) this.fileService.delete(vehicle.image);

			const requestData = {
				nameOnTab: updateVehicleDTO.nameOnTab,
				vehicleGroupId: updateVehicleDTO.vehicleGroupId,
				licenseEndDate: formatDate(updateVehicleDTO.licenseEndDate),
				insuranceEndDate: formatDate(updateVehicleDTO.insuranceEndDate),
				status: updateVehicleDTO.status,
				memo: updateVehicleDTO.memo,
			};

			await this.repo.save({
				...vehicle,
				...requestData,
				image:
					updateVehicleDTO.image === 'delete'
						? null
						: this.fileService.uploadFile(
								updateVehicleDTO.image,
								'/vehicles/images',
								'VI',
								tenantName,
						  ) ?? vehicle.image,
			});

			return await this.repo.findOne({
				where: { id },
				relations: ['vehicleGroup'],
				withDeleted: true,
			});
		}

		throw new BadRequestException(['vehicle not found']);
	}
}
