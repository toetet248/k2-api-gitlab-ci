// import { Exclude, Expose, Transform } from 'class-transformer';
// import Hashids from 'hashids/cjs';

// export class CreateVehicleSerialize {
// 	@Expose()
// 	id: string;

// 	@Expose()
// 	nameOnTab: string;
// }

import { Expose, Type } from 'class-transformer';
export class CreateVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	image: string;

	@Expose()
	insuranceEndDate: string;

	@Expose()
	licenseEndDate: string;

	@Expose()
	orderNo: string;

	@Expose()
	status: string;

	@Expose()
	vehicleGroupId: string;

	@Expose({name:'schedule_vehicles_dayNightSupport'})
	isDay: string;

	@Expose({name:'schedule_vehicles_nightSupportCount'})
	isSupport: string;

	// @Expose()
	// @Type(() => ScheduleStaffs)
	// scheduleStaffs: ScheduleStaffs[];

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup[];
}

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}