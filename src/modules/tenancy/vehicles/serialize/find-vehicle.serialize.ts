import { Exclude, Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

export class FindVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	licenseEndDate: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	insuranceEndDate: string;

	@Expose()
	status: string;

	@Expose()
	vehicleGroupId: string;

	@Expose()
	image: string;

	@Expose()
	memo: string;

	@Expose()
	createdAt: Date;

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup;

	@Expose({ name: 'schCount' })
	@Transform(({ value }) => value > 0)
	hasScheduled: boolean;
}
