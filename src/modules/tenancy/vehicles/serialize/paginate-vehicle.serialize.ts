import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	@Expose()
	deletedAt: string;
}

export class PaginateVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	licenseEndDate: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	insuranceEndDate: string;

	@Expose()
	status: string;

	@Expose()
	vehicleGroupId: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup;
}
