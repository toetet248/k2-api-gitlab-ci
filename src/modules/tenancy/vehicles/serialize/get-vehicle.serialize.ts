import { Expose, Type } from 'class-transformer';

class VehicleGroup {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

export class GetVehicleSerialize {
	@Expose()
	id: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => VehicleGroup)
	vehicleGroup: VehicleGroup;
}
