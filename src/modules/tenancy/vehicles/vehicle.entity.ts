import { MainEntity } from 'src/common/entity/main.entity';
import {
	Column,
	Entity,
	ManyToOne,
	JoinColumn,
	Generated,
	OneToMany,
} from 'typeorm';
import { ScheduleVehicle } from '../schedule-vehicles/schedule-vehicle.entity';
import { VehicleGroup } from '../vehicle-groups/vehicle-group.entity';

@Entity({ name: 'vehicles' })
export class Vehicle extends MainEntity {
	@Column()
	nameOnTab: string;

	@Column()
	licenseEndDate: Date;

	@Column()
	insuranceEndDate: Date;

	@Column()
	status:
		| 'in_operation'
		| 'no_operation'
		| 'inspection'
		| 'repairing'
		| 'out_from_list';

	@Column()
	vehicleGroupId: string;

	@Column()
	image: string;

	@Column()
	memo: string;

	@Column()
	@Generated('increment')
	orderNo: number;

	@ManyToOne(() => VehicleGroup, (vehicleGroup) => vehicleGroup.vehicles)
	@JoinColumn({ name: 'vehicleGroupId' })
	vehicleGroup: VehicleGroup;

	@OneToMany(
		() => ScheduleVehicle,
		(scheduleVehicle) => scheduleVehicle.vehicle,
	)
	scheduleVehicles: ScheduleVehicle[];
}
