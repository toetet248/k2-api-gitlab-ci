import {
	BadRequestException,
	Delete,
	Get,
	Headers,
	Param,
	Patch,
	Query,
	UseGuards,
} from '@nestjs/common';
import { Body, Controller, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/common/controller/base.controller';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { validateExists } from 'src/common/service/helper.service';
import { ScheduleVehicleService } from '../schedule-vehicles/schedule-vehicle.service';
import { TENANT_HEADER } from '../tenancy.middleware';
import { CreateVehicleDTO } from './dto/create-vehicle.dto';
import { PaginateVehicleDTO } from './dto/paginate-vehicle.dto';
import { UpdateOrderVehicleDTO } from './dto/update-order-vehicle.dto';
import { UpdateVehicleDTO } from './dto/update-vehicle.dto';
import { CreateVehicleSerialize } from './serialize/create-vehicle.serialize';
import { FindVehicleSerialize } from './serialize/find-vehicle.serialize';
import { GetVehicleSerialize } from './serialize/get-vehicle.serialize';
import { PaginateVehicleSerialize } from './serialize/paginate-vehicle.serialize';
import { UpdateVehicleSerialize } from './serialize/update-vehicle.serialize';
import { VehicleService } from './vehicle.service';

@Controller('tenants/vehicles')
@UseGuards(AuthGuard('tenant-jwt'))
export class VehicleController extends BaseController {
	constructor(
		private readonly service: VehicleService,
		private readonly scheduleVehicleService: ScheduleVehicleService,
	) {
		super();
	}

	@Get('/')
	@Serialize(GetVehicleSerialize)
	async getVehicle() {
		const vehicle = await this.service.repo.find({
			relations: ['vehicleGroup'],
			order: {
				orderNo: 'ASC',
			},
		});

		return this.response(vehicle);
	}

	@Get('/paginate')
	@Serialize(PaginateVehicleSerialize)
	async paginateVehicle(@Query() paginateVehicleDTO: PaginateVehicleDTO) {
		const paginate = await this.service.paginateVehicle(paginateVehicleDTO);

		return this.paginateResponse(paginate.items, paginate.meta);
	}

	@Get('/:id')
	@Serialize(FindVehicleSerialize)
	async findVehicle(@Param('id') id: string) {
		const vehicle = await this.service.repo
			.createQueryBuilder('vehicles')
			.withDeleted()
			.leftJoinAndSelect('vehicles.vehicleGroup', 'vehicleGroups')
			.loadRelationCountAndMap(
				'vehicles.schCount',
				'vehicles.scheduleVehicles',
			)
			.where('vehicles.id = :id', { id })
			.andWhere('vehicles.deletedAt IS NULL')
			.getOne();

		if (vehicle) return this.response(vehicle);

		throw new BadRequestException(['vehicle not found']);
	}

	@Post('/create')
	@InjectRequestToBody()
	@Serialize(CreateVehicleSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async createVehicle(
		@Body() createVehicleDTO: CreateVehicleDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const vehicle = await this.service.createVehicle(
			createVehicleDTO,
			tenantName,
		);
		return this.response(vehicle, { title: 'vehicle create success' });
	}

	@Patch('/:id/update')
	@InjectRequestToBody()
	@Serialize(UpdateVehicleSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async updateVehicle(
		@Body() updateVehicleDTO: UpdateVehicleDTO,
		@Param('id') id: string,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const vehicle = await this.service.updateVehicle(
			id,
			updateVehicleDTO,
			tenantName,
		);
		return this.response(vehicle, { title: 'vehicle update success' });
	}

	@Patch('/update/order')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOrderVehicle(
		@Body() updateOrderVehicleDTO: UpdateOrderVehicleDTO,
	) {
		const data = updateOrderVehicleDTO.ids.map((id, index) => {
			return this.service.repo.create({
				id,
				orderNo: index + 1,
			});
		});

		this.service.repo.save(data);

		return this.response(null, { title: 'vehicle order update success' });
	}

	@Delete('/:id')
	@UseGuards(AuthGuard('site-admin'))
	async deleteVehicle(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['vehicle does not exists']);

		const scheduleStaff = await this.scheduleVehicleService.repo.findOne({
			where: { vehicleId: id },
		});

		if (scheduleStaff)
			throw new BadRequestException([
				'vehicle cannot be deleted!That vehicle is positioned in schedule vehicle',
			]);

		this.service.repo.softDelete(id);

		return this.response(null, { title: 'vehicle delete success' });
	}

	@Patch('/:id/restore')
	@UseGuards(AuthGuard('site-admin'))
	async restoreVehicle(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['vehicle does not exists']);

		this.service.repo.restore(id);

		return this.response(null, { title: 'vehicle restore success' });
	}
}
