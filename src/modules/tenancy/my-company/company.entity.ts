import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'my_company' })
export class Company extends MainEntity {
	@Column()
	name: string;

	@Column()
	businessTypes: string;

	@Column()
	postalCode: string;

	@Column()
	addressPrefecture: string;

	@Column()
	addressCity: string;

	@Column()
	addressBuilding: string;

	@Column()
	phone: string;

	@Column()
	email: string;

	@Column()
	fax: string;

	@Column()
	image: string;

	@Column()
	lat: string;

	@Column()
	lng: string;
}
