import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { Company } from './company.entity';

@Injectable()
export class CompanyService {
	public readonly repo: Repository<Company>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(Company);
	}
}
