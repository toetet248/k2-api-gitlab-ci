import {
	Get,
	UseGuards,
	BadRequestException,
	Inject,
	Headers,
} from '@nestjs/common';
import { Body, Controller, Patch } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UpdateCompanyDTO } from './dto/update-company.dto';
import { UpdateCompanySerialize } from './serialize/update-company.serialize';
import { GetCompanySerialize } from './serialize/get-company.serialize';
import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { FileService } from 'src/common/service/file.service';
import { TENANT_HEADER } from '../tenancy.middleware';
import { TenantService } from 'src/modules/central/tenants/tenant.service';

@Controller('tenants/my-company')
@UseGuards(AuthGuard('tenant-jwt'))
export class CompanyController extends BaseController {
	constructor(
		@Inject(FileService) private readonly fileService: FileService,
		@Inject(TenantService) private readonly tenantService: TenantService,
	) {
		super();
	}

	@Get('/')
	@Serialize(GetCompanySerialize)
	async getCompany(@Headers(TENANT_HEADER) tenantName: string) {
		const company = await this.tenantService.repo
			.createQueryBuilder('tenant')
			.select([
				'tenant.companyName',
				'tenant.companyPhone',
				'tenant.companyEmail',
				'tenant.companyAddressPrefecture',
				'tenant.companyAddressCity',
				'tenant.companyAddressBuilding',
				'tenant.companyImage',
				'tenant.companyFax',
				'tenant.companyLat',
				'tenant.companyLng',
				'tenant.companyBusinessTypes',
			])
			.where('name = :tenantName', { tenantName })
			.getOne();

		return this.response(company);
	}

	@Patch('/update')
	@Serialize(UpdateCompanySerialize)
	@UseGuards(AuthGuard('site-admin'))
	async updateCompany(
		@Body() updateCompanyDTO: UpdateCompanyDTO,
		@Headers(TENANT_HEADER) tenantName: string,
	) {
		const tenant = await this.tenantService.repo
			.createQueryBuilder('tenant')
			.select([
				'tenant.id',
				'tenant.companyName',
				'tenant.companyPhone',
				'tenant.companyEmail',
				'tenant.companyAddressPrefecture',
				'tenant.companyAddressCity',
				'tenant.companyAddressBuilding',
				'tenant.companyImage',
				'tenant.companyFax',
				'tenant.companyLat',
				'tenant.companyLng',
				'tenant.companyBusinessTypes',
			])
			.where('name = :tenantName', { tenantName })
			.getOne();

		if (tenant) {
			if (updateCompanyDTO.image)
				this.fileService.delete(tenant.companyImage);

			const update = {
				companyName: updateCompanyDTO.name,
				companyEmail: updateCompanyDTO.email,
				companyPhone: updateCompanyDTO.phone,
				companyAddressPrefecture: updateCompanyDTO.addressPrefecture,
				companyAddressCity: updateCompanyDTO.addressCity,
				companyAddressBuilding: updateCompanyDTO.addressBuilding,
				companyFax: updateCompanyDTO.fax,
				companyLat: updateCompanyDTO.lat,
				companyLng: updateCompanyDTO.lng,
				companyImage:
					updateCompanyDTO.image == 'delete'
						? null
						: this.fileService.uploadFile(
								updateCompanyDTO.image,
								'/my-company/images',
								'CI',
								tenantName,
						  ) ?? tenant.companyImage,
				companyBusinessTypes: updateCompanyDTO.businessTypes,
			};

			await this.tenantService.repo.update(tenant.id, update);

			return this.response(
				{ id: tenant.id, ...update },
				{ title: 'company update success' },
			);
		} else {
			throw new BadRequestException(['Company Not Found!']);
		}
	}
}
