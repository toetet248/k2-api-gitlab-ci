import { IsEmail, IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';

export class UpdateCompanyDTO {
	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	@IsOptional()
	@IsEmail()
	email: string;

	@IsOptional()
	phone: string;

	@IsOptional()
	addressPrefecture: string;

	@IsOptional()
	addressCity: string;

	@IsOptional()
	addressBuilding: string;

	@IsOptional()
	businessTypes: string;

	@IsOptional()
	lat: string;

	@IsOptional()
	lng: string;

	@IsOptional()
	fax: string;

	@IsOptional()
	@IsBase64Image(true)
	image: string;
}
