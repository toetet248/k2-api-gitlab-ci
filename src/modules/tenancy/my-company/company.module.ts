import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { CommonModule } from 'src/common/common.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';
import { CompanyController } from './company.controller';

@Module({
	imports: [NestjsFormDataModule, ConfigModule, CommonModule, TenantModule],
	controllers: [CompanyController],
})
export class CompanyModule {}
