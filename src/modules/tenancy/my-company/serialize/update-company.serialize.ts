import { Exclude, Expose, Transform } from 'class-transformer';

export class UpdateCompanySerialize {
	@Expose()
	id: string;

	@Expose({ name: 'companyName' })
	name: string;

	// @Expose()
	// businessTypes: string;

	// @Expose()
	// postalCode: string;

	// @Expose()
	// addressPrefecture: string;

	// @Expose()
	// addressCity: string;

	// @Expose()
	// addressBuilding: string;

	// @Expose()
	// phone: string;

	// @Expose()
	// email: string;

	// @Expose()
	// fax: string;

	@Expose({ name: 'companyImage' })
	image: string;

	// @Expose()
	// lat: string;

	// @Expose()
	// lng: string;
}
