import { Exclude, Expose, Transform } from 'class-transformer';

export class GetCompanySerialize {
	@Expose()
	id: string;

	@Expose({ name: 'companyName' })
	name: string;

	@Expose({ name: 'companyPhone' })
	phone: string;

	@Expose({ name: 'companyEmail' })
	email: string;

	@Expose({ name: 'companyAddressPrefecture' })
	addressPrefecture: string;

	@Expose({ name: 'companyAddressCity' })
	addressCity: string;

	@Expose({ name: 'companyAddressBuilding' })
	addressBuilding: string;

	@Expose({ name: 'companyImage' })
	image: string;

	@Expose({ name: 'companyFax' })
	fax: string;

	@Expose({ name: 'companyLat' })
	lat: string;

	@Expose({ name: 'companyLng' })
	lng: string;

	@Expose({ name: 'companyBusinessTypes' })
	businessTypes: string;
}
