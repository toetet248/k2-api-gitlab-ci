import {
	Column,
	Entity,
	JoinColumn,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
} from 'typeorm';
import { StaffTeam } from '../staff-teams/staff-team.entity';
import { SalesIncharge } from '../sales-incharges/sales-incharge.entity';
import { Customer } from '../customers/customer.entity';
import { MainEntity } from 'src/common/entity/main.entity';
import { Site } from '../sites/site.entity';
import { ScheduleStaff } from '../schedule-staffs/schedule-staff.entity';
import { SiteMemo } from '../site-memos/site-memo.entity';
import { StaffLeave } from '../staff-leaves/staff-leave.entity';
import { SiteImage } from '../site-images/site-image.entity';
import { SiteFile } from '../site-files/site-file.entity';
import { DailyNote } from '../daily-notes/daily-note.entity';

export const authStaffOnTopCase = (authStaffId: string) => {
	return `CASE
                WHEN (staffs.id = '${authStaffId}') THEN 1
                ELSE 0
            END`;
};

@Entity({ name: 'staffs' })
export class Staff extends MainEntity {
	@Column()
	userId: string;

	@Column()
	staffTeamId: string;

	// @Column()
	// username: string;

	@Column()
	name: string;

	@Column()
	email: string;

	@Column()
	password: string;

	@Column()
	nameFurigana: string;

	@Column()
	nameOnTab: string;

	@Column()
	phone: string;

	@Column()
	image: string;

	@Column()
	status: 'in_work' | 'leave' | 'retired';

	@Column()
	isSiteStaff: boolean;

	@Column()
	isSiteAdmin: boolean;

	@Column()
	isSystemAdmin: boolean;

	@Column()
	orderNo: number;

	@Column()
	isOwner: boolean;

	@Column()
	loginType: string;

	@ManyToOne(() => StaffTeam, (staffTeam) => staffTeam.staffs)
	@JoinColumn({ name: 'staffTeamId' })
	staffTeam: StaffTeam;

	@OneToMany(() => SalesIncharge, (saleIncharge) => saleIncharge.staff)
	salesIncharges: SalesIncharge[];

	@OneToMany(() => Site, (site) => site.staff)
	staffs: Staff[];

	@OneToMany(() => SiteMemo, (siteMemo) => siteMemo.staffs)
	siteMemos: SiteMemo[];

	@ManyToMany(() => Customer, (customer) => customer.staffs)
	@JoinTable({
		name: 'sales_incharges',
		joinColumn: {
			name: 'staffId',
			referencedColumnName: 'id',
		},
		inverseJoinColumn: {
			name: 'customerId',
			referencedColumnName: 'id',
		},
	})
	customers: Customer[];

	@OneToMany(() => ScheduleStaff, (scheduleStaff) => scheduleStaff.staff)
	scheduleStaffs: ScheduleStaff[];

	@OneToMany(() => StaffLeave, (staffLeave) => staffLeave.staff)
	staffLeaves: StaffLeave[];

	@OneToMany(() => SiteImage, (siteImage) => siteImage.staff)
	siteImages: SiteImage[];

	@OneToMany(() => SiteFile, (siteFile) => siteFile.staff)
	siteFiles: SiteFile[];

	@OneToMany(() => DailyNote, (dailyNote) => dailyNote.staff)
	dailyNotes: DailyNote[];
}
