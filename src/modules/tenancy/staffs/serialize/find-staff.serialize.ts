import { Expose, Transform, Type } from 'class-transformer';
import moment from 'moment';

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}

export class FindStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	email: string;

	@Expose()
	password: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	image: string;

	@Expose()
	orderNo: number;

	@Expose()
	nameFurigana: string;

	@Expose()
	status: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: boolean;

	@Expose()
	loginType: string;

	@Expose()
	createdAt: Date;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;

	@Expose()
	@Type(() => StaffLeave)
	staffLeaves: StaffLeave[];

	@Expose({ name: 'schCount' })
	@Transform(({ value }) => value > 0)
	hasScheduled: boolean;

	@Expose({ name: 'customerCount' })
	@Transform(({ value }) => value > 0)
	hasCustomers: boolean;
}

class StaffLeave {
	@Expose()
	id: string;

	@Expose()
	@Transform(({ value }) =>
		value ? moment(value).format('YYYY-MM-DD') : value,
	)
	date: string;
}
