import { Expose, Type } from 'class-transformer';

export class GetStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	image: string;

	@Expose()
	@Type(() => staffTeam)
	staffTeam: staffTeam[];
}

class staffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}
