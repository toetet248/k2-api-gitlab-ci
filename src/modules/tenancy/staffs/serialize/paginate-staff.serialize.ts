import { Expose, Transform, Type } from 'class-transformer';

class StaffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;

	@Expose()
	deletedAt: string;
}

class StaffLeave {
	@Expose()
	id: string;

	@Expose()
	date: string;
}

export class PaginateStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	email: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	image: string;

	@Expose()
	orderNo: number;

	@Expose()
	status: string;

	@Expose()
	loginType: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	isSiteAdmin: boolean;

	@Expose()
	isSystemAdmin: boolean;

	@Expose()
	isOwner: boolean;

	@Expose()
	@Type(() => StaffTeam)
	staffTeam: StaffTeam;

	@Expose()
	@Type(() => StaffLeave)
	staffLeaves: StaffLeave[];
}
