import { Expose, Type } from 'class-transformer';
export class CreateStaffSerialize {
	@Expose()
	id: string;

	@Expose()
	phone: string;

	@Expose()
	name: string;

	@Expose()
	nameOnTab: string;

	@Expose()
	email: string;

	@Expose()
	image: string;

	@Expose()
	orderNo: string;

	@Expose()
	status: string;

	@Expose()
	staffTeamId: string;

	@Expose()
	isSiteStaff: boolean;

	@Expose()
	loginType: string;

	@Expose({ name: 'schedule_staffs_dayNightSupport' })
	isDay: string;

	@Expose({ name: 'schedule_staffs_nightSupportCount' })
	isSupport: string;

	// @Expose()
	// @Type(() => ScheduleStaffs)
	// scheduleStaffs: ScheduleStaffs[];

	@Expose()
	@Type(() => staffTeam)
	staffTeam: staffTeam[];
}

class staffTeam {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	colorCode: string;
}
