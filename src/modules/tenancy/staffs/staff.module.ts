import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from 'src/common/common.module';
import { TenantModule } from 'src/modules/central/tenants/tenant.module';
import { UserModule } from 'src/modules/central/users/user.module';
import { StaffController } from './staff.controller';
import { Staff } from './staff.entity';
import { StaffService } from './staff.service';

@Module({
	imports: [
		// CacheModule.register({
		// 	ttl: 10,
		// }),
		TypeOrmModule.forFeature([Staff]),
		// NestjsFormDataModule,
		ConfigModule,
		CommonModule,
		TenantModule,
		UserModule,
		// ParseFormDataJsonPipe,
	],
	controllers: [StaffController],
	providers: [
		StaffService,
		// {
		// 	provide: APP_INTERCEPTOR,
		// 	useClass: HttpCacheInterceptor,
		// },
	],
	exports: [StaffService],
})
export class StaffModule {}
