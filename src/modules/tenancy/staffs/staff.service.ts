import {
	BadRequestException,
	ForbiddenException,
	Inject,
	Injectable,
	NotAcceptableException,
} from '@nestjs/common';
import { Brackets, Code, Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { CreateStaffDTO } from './dto/create-staff.dto';
import { Staff } from './staff.entity';
import { Pagination } from 'nestjs-typeorm-paginate';
import * as bcrypt from 'bcrypt';
import { UpdateStaffDTO } from './dto/update-staff.dto';
import { generateId, generateNumber } from 'src/common/service/helper.service';
import { StaffTeam } from '../staff-teams/staff-team.entity';
import { FileService } from 'src/common/service/file.service';
import { PaginateStaffDTO } from './dto/paginate-staff.dto';
import { UserService } from 'src/modules/central/users/user.service';
import { TenantService } from 'src/modules/central/tenants/tenant.service';
import { MailService } from 'src/common/service/mail.service';
import { ConfigService } from '@nestjs/config';
import { SmsService } from 'src/common/service/sms.service';

@Injectable()
export class StaffService {
	public readonly repo: Repository<Staff>;
	public readonly staffTeamRepo: Repository<StaffTeam>;

	constructor(
		@Inject(CONNECTION) connection: Connection,
		@Inject(FileService) private readonly fileService: FileService,
		@Inject(MailService) private readonly mailService: MailService,
		@Inject(SmsService) private readonly smsService: SmsService,
		@Inject(UserService) private readonly userService: UserService,
		@Inject(TenantService) private readonly tenantService: TenantService,
		private configService: ConfigService,
	) {
		this.repo = connection.getRepository(Staff);
		this.staffTeamRepo = connection.getRepository(StaffTeam);

		this.fileService = fileService;
	}

	async paginateStaff(
		paginateStaffDTO: PaginateStaffDTO,
	): Promise<Pagination<Staff>> {
		let {
			search,
			status,
			isSiteStaff,
			isSiteAdmin,
			isSystemAdmin,
			staffTeamIds,
			noTeam,
			page = 1,
		} = paginateStaffDTO;

		const query = this.repo
			.createQueryBuilder('staffs')
			// .withDeleted()
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeams')
			.leftJoinAndSelect('staffs.staffLeaves', 'staffLeaves')
			.select([
				// staffs
				'staffs.id',
				'staffs.phone',
				'staffs.name',
				'staffs.email',
				'staffs.nameOnTab',
				'staffs.staffTeamId',
				'staffs.image',
				'staffs.orderNo',
				'staffs.status',
				'staffs.loginType',
				'staffs.isSiteStaff',
				'staffs.isSiteAdmin',
				'staffs.isSystemAdmin',
				'staffs.isOwner',
				// staffTeams
				'staffTeams.id',
				'staffTeams.name',
				'staffTeams.colorCode',
				'staffTeams.deletedAt',
				// staffLeaves
				'staffLeaves.id',
				'staffLeaves.date',
			])
			// .loadRelationCountAndMap('staffs.schCount', 'staffs.scheduleStaffs')
			// .andWhere('staffs.deletedAt IS NULL')
			.orderBy('staffs.orderNo', 'ASC');

		if (search)
			query.andWhere(
				new Brackets((q) => {
					return (
						q
							// .where('staffs.username like :username', {
							// 	username: `%${search}%`,
							// })
							.where('staffs.name like :name', {
								name: `%${search}%`,
							})
							.orWhere('staffs.nameOnTab like :nameOnTab', {
								nameOnTab: `%${search}%`,
							})
							.orWhere('staffs.phone like :phone', {
								phone: `%${search}%`,
							})
							.orWhere('staffs.email like :email', {
								email: `%${search}%`,
							})
							.orWhere('staffTeams.name like :name', {
								name: `%${search}%`,
							})
					);
				}),
			);

		if (noTeam) {
			query.andWhere(
				new Brackets((q) => {
					q.where('staffs.staffTeamId IN(:staffTeamIds)', {
						staffTeamIds: staffTeamIds || [''],
					}).orWhere('staffs.staffTeamId IS NULL');
				}),
			);
		} else if (staffTeamIds) {
			query.andWhere('staffs.staffTeamId IN(:staffTeamIds)', {
				staffTeamIds,
			});
		}

		if (status) {
			// normal ==>  exclude retired staffs
			if (status === 'normal') {
				query.andWhere(
					new Brackets((q) => {
						q.where('staffs.status != :status', {
							status: 'retired',
						}).orWhere('staffs.status is NULL');
					}),
				);
			} else {
				query.andWhere(
					new Brackets((q) => {
						q.where('staffs.isOwner = :isOwner', {
							isOwner: false,
						}).andWhere('staffs.status = :status', {
							status: status,
						});
					}),
				);
			}
		}

		if (isSiteStaff || isSiteAdmin || isSystemAdmin) {
			query.andWhere(
				new Brackets((q) => {
					if (isSiteStaff)
						q.orWhere('staffs.isSiteStaff = :isSiteStaff', {
							isSiteStaff: true,
						});
					if (isSiteAdmin)
						q.orWhere('staffs.isSiteAdmin = :isSiteAdmin', {
							isSiteAdmin: true,
						});
					if (isSystemAdmin)
						q.orWhere('staffs.isSystemAdmin = :isSystemAdmin', {
							isSystemAdmin: true,
						});
				}),
			);
		}

		// const data = await paginate<Staff>(query, { page, limit: 30 });

		const data = await query.paginate(50);

		return {
			items: data.data,
			meta: {
				totalItems: data.total,
				itemCount: data.data.length,
				itemsPerPage: data.per_page,
				totalPages: data.last_page,
				currentPage: data.current_page,
			},
		};
		// const data = await query.paginate(30);

		// return {
		// 	data: data.data,
		// 	meta: {
		// 		totalItems: data.total,
		// 		itemCount: data.data.length,
		// 		itemsPerPage: data.per_page,
		// 		totalPages: data.last_page,
		// 		currentPage: data.current_page,
		// 	},
		// };j
		// return data;
	}

	// async createStaff(createStaffDTO: CreateStaffDTO, tenantName: string) {
	// 	let { password, image } = createStaffDTO;

	// 	// if (!(await validateExists(this.staffTeamRepo, ['id', staffTeamId])))
	// 	// 	throw new BadRequestException(['staffTeam doesssss not exists']);

	// 	// set order no to create data
	// 	const orderNo =
	// 		(await (
	// 			await this.repo
	// 				.createQueryBuilder('staffs')
	// 				.select('MAX(staffs.orderNo)', 'max')
	// 				.getRawOne()
	// 		).max) + 1;

	// 	const salt = await bcrypt.genSalt();

	// 	return await this.repo.save({
	// 		...createStaffDTO,
	// 		id: generateId(),
	// 		password: await bcrypt.hash(password, salt),
	// 		image: this.fileService.uploadFile(
	// 			image,
	// 			'/staffs/images',
	// 			'SI',
	// 			tenantName,
	// 		),
	// 		orderNo,
	// 	});
	// }

	async createStaff(
		createStaffDTO: CreateStaffDTO,
		tenantName: string,
		isInvite: boolean,
	) {
		let { image, email, phone, name, loginType } = createStaffDTO;

		const tenant = await this.tenantService.repo
			.createQueryBuilder('tenants')
			.select(['tenants.id', 'tenants.companyName'])
			.leftJoinAndSelect('tenants.subscriptions', 'subscriptions')
			.where('tenants.name = :tenantName', { tenantName })
			.getOne();

		// start - check plan and unauthorized
		const activeSubscription = await this.tenantService.activeSubscription(
			tenantName,
		);
		const currentPlan = await this.tenantService.currentPlan(
			activeSubscription,
		);

		if (currentPlan.name == 'free') {
			const activeStaffsCount = await this.repo.count({
				where: [
					{
						status: 'in_work',
					},
					{
						status: 'leave',
					},
				],
			});

			if (activeStaffsCount >= currentPlan.options['staffLimit'])
				throw new NotAcceptableException([
					'not-accepted-for-free-plan',
				]);
		}
		// end - check plan and unauthorized
		const code = generateNumber();

		const user = await this.userService.repo.save({
			id: generateId(),
			name,
			email,
			phone,
			isActive: createStaffDTO.status !== 'retired',
			tenantId: tenant.id,
			role: 'staff',
			code,
		});

		// create staff
		// set order no to create data
		const orderNo =
			(await (
				await this.repo
					.createQueryBuilder('staffs')
					.select('MAX(staffs.orderNo)', 'max')
					.getRawOne()
			).max) + 1;

		// send code
		if (isInvite)
			this.sendCode(loginType, email, phone, code, tenant.companyName);

		console.log(code, '...code');
		// send sms

		const requestData = {
			staffTeamId: createStaffDTO.staffTeamId,
			name: createStaffDTO.name,
			email: createStaffDTO.email,
			nameFurigana: createStaffDTO.nameFurigana,
			nameOnTab: createStaffDTO.nameOnTab,
			phone: createStaffDTO.phone,
			loginType,
			status: createStaffDTO.status,
			isSiteStaff: createStaffDTO.isSiteStaff,
			isSiteAdmin: createStaffDTO.isSiteAdmin,
			isSystemAdmin: createStaffDTO.isSystemAdmin,
		};

		// also for staff create from scheduler
		const createStaff = await this.repo.save({
			...requestData,
			id: generateId(),
			userId: user.id,
			image: this.fileService.uploadFile(
				image,
				'/staffs/images',
				'SI',
				tenantName,
			),
			orderNo,
		});

		const result = await this.repo
			.createQueryBuilder('staffs')
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeam')
			.leftJoin('staffs.scheduleStaffs', 'schedule_staffs')
			.addSelect([
				'schedule_staffs.dayNightSupport',
				'schedule_staffs.nightSupportCount',
			])
			.orderBy('staffs.orderNo', 'ASC')
			.select([
				'staffs.id',
				'staffs.name',
				'staffs.image',
				'staffs.email',
				'staffs.phone',
				'staffs.nameOnTab',
				'staffs.orderNo',
				'staffs.status',
				'staffs.staffTeamId',
				'staffs.isSiteStaff',
				'staffs.loginType',
				'schedule_staffs',
				'staffTeam',
			])
			.where('staffs.id = :id', { id: createStaff.id })
			.getOne();

		return result;
	}

	async updateStaff(
		id: string,
		updateStaffDTO: UpdateStaffDTO,
		tenantName: string,
		isInvite: boolean,
	) {
		let { image, email, phone, status, loginType } = updateStaffDTO;

		const staff = await this.repo.findOne({
			where: { id },
		});

		if (staff) {
			// start - check plan and unauthorized
			const activeSubscription =
				await this.tenantService.activeSubscription(tenantName);
			const currentPlan = await this.tenantService.currentPlan(
				activeSubscription,
			);

			if (currentPlan.name == 'free') {
				const activeStaffsCount = await this.repo.count({
					where: [
						{
							status: 'in_work',
						},
						{
							status: 'leave',
						},
					],
				});

				if (staff.status == 'retired') {
					if (status !== 'retired') {
						if (
							activeStaffsCount >=
							currentPlan.options['staffLimit']
						)
							throw new NotAcceptableException([
								'not-accepted-for-free-plan',
							]);
					}
				}
			}
			// end - check plan and unauthorized

			if (image) this.fileService.delete(staff.image);

			const requestData = {
				staffTeamId: updateStaffDTO.staffTeamId,
				name: updateStaffDTO.name,
				email: updateStaffDTO.email,
				nameFurigana: updateStaffDTO.nameFurigana,
				nameOnTab: updateStaffDTO.nameOnTab,
				phone: updateStaffDTO.phone,
				status: updateStaffDTO.status,
				loginType,
				isSiteStaff: updateStaffDTO.isSiteStaff,
				isSiteAdmin: updateStaffDTO.isSiteAdmin,
				isSystemAdmin: updateStaffDTO.isSystemAdmin,
			};

			// if (
			// 	isInvite ||
			// 	(staff.loginType === 'none' && loginType !== staff.loginType)
			// ) {
			// 	const user = await this.userService.repo
			// 		.createQueryBuilder('users')
			// 		.select(['users.code', 'tenants.companyName'])
			// 		.leftJoinAndSelect('users.tenant', 'tenants')
			// 		.where('users.id = :userId', { userId: staff.userId })
			// 		.getOne();
			// 	let code = user.code ?? generateNumber();

			// 	if (!user.code) {
			// 		await this.userService.repo.update(staff.userId, {
			// 			code,
			// 		});
			// 	}

			// 	// send code
			// 	this.sendCode(
			// 		loginType,
			// 		email,
			// 		phone,
			// 		code,
			// 		user.tenant.companyName,
			// 	);
			// }

			// update staff
			const updateStaff = await this.repo.save({
				...staff,
				...requestData,
				image:
					image === 'delete'
						? null
						: this.fileService.uploadFile(
								image,
								'/staffs/images',
								'SI',
								tenantName,
						  ) ?? staff.image,
			});

			// update user from central
			await this.userService.repo.update(staff.userId, {
				name: updateStaff.name,
				email,
				phone,
				isActive: updateStaff.status !== 'retired',
			});

			return await this.repo.findOne({
				where: { id },
				relations: ['staffTeam'],
				withDeleted: true,
			});
		}

		throw new BadRequestException(['staff not found']);
	}

	async sendCode(
		loginType: string,
		email: string,
		phone: string,
		code: string,
		companyName: string,
	) {
		const frontendUrl = this.configService.get('app.frontendUrl');
		// send mail
		if (loginType == 'email') {
			if (email)
				try {
					this.mailService.sendStaffCodeMail(
						email,
						code,
						phone,
						companyName,
						frontendUrl,
					);
				} catch (error) {
					throw new BadRequestException([
						'SMTP mail error. Please, try again.',
					]);
				}
		}
		// send sms
		else {
			if (phone)
				try {
					const frontendUrl =
						this.configService.get('app.frontendUrl');

					this.smsService.sendStaffCodeSms(
						'+81' + phone.substring(1),
						email,
						code,
						companyName,
						frontendUrl,
					);
				} catch (error) {
					throw new BadRequestException([
						'SMS sending error. Please, try again.',
					]);
				}
		}
	}

	async resetPassword(id: string) {
		console.log('...reset');
		const staff = await this.repo.findOne({
			where: { id },
			select: ['email', 'name', 'phone', 'userId', 'loginType'],
		});

		if (staff) {
			const code = generateNumber();
			const salt = await bcrypt.genSalt();
			const password = await bcrypt.hash(code, salt);
			const tenant = await this.tenantService.repo
				.createQueryBuilder('tenant')
				.leftJoinAndSelect('tenant.users', 'users')
				.select('tenant.companyName')
				.where('users.id = :userId', { userId: staff.userId })
				.getOne();
			console.log(code, '...reset code');

			if (staff.loginType == 'email') {
				if (staff.email)
					try {
						const frontendUrl =
							this.configService.get('app.frontendUrl');
						await this.mailService.sendStaffCodeMail(
							staff.email,
							code,
							staff.phone,
							tenant.companyName,
							frontendUrl,
						);
					} catch (error) {
						throw new BadRequestException([
							'SMTP mail error. Please, try again.',
						]);
					}
			} else {
				if (staff.phone)
					try {
						const frontendUrl =
							this.configService.get('app.frontendUrl');

						await this.smsService.sendStaffCodeSms(
							'+81' + staff.phone.substring(1),
							staff.email,
							code,
							tenant.companyName,
							frontendUrl,
						);
					} catch (error) {
						throw new BadRequestException([
							'SMS sending error. Please, try again.',
						]);
					}
			}

			// update user from central
			await this.userService.repo.update(staff.userId, {
				code,
				resetPasswordAt: null,
			});

			return;

			// return await this.repo.update(id, {
			// 	password,
			// });
		}

		throw new BadRequestException(['staff not found']);
	}

	async deleteStaff(id: string) {
		const staff = await this.repo
			.createQueryBuilder('staff')
			.select(['staff.userId'])
			.loadRelationCountAndMap(
				'staff.scheduleCount',
				'staff.scheduleStaffs',
			)
			.loadRelationCountAndMap(
				'staff.salesInchargeCount',
				'staff.salesIncharges',
			)
			.where('staff.id = :id', { id })
			.getOne();

		if (staff) {
			if (staff['scheduleCount'])
				throw new BadRequestException([
					'staff cannot be deleted ! That staff is positioned in schedule staff',
				]);

			if (staff['salesInchargeCount'])
				throw new BadRequestException([
					'staff cannot be deleted ! That staff is positioned in sales incharge',
				]);

			return await Promise.all([
				this.repo.softDelete(id),
				this.userService.repo.softDelete(staff.userId),
			]);
		}

		throw new BadRequestException(['staff not found']);
	}
}
