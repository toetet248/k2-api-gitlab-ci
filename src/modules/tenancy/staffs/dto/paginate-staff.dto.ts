import { Transform } from 'class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';

export class PaginateStaffDTO {
	@IsOptional()
	page: string;

	@IsOptional()
	search: string;

	@IsOptional()
	status: string;

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => (value == 'true' || value == '1') ? true : false)
	isSiteStaff: boolean;

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => (value == 'true' || value == '1') ? true : false)
	isSiteAdmin: boolean;

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => (value == 'true' || value == '1') ? true : false)
	isSystemAdmin: boolean;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	staffTeamIds: any[];

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => value === 'true' || value === '1')
	noTeam: boolean;
}
