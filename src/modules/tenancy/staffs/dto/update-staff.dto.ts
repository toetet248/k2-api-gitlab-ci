import { IsIn, IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsOneTrue } from 'src/common/validator/is-one-true.validator';
import { IsPhoneNo } from 'src/common/validator/is-phone-no.validators';
import { IsUnique } from 'src/common/validator/is-unique.validator';
import { IsEmailOrPhoneContains } from 'src/common/validator/is-email-or-phone-contains.validator';

export class UpdateStaffDTO {
	@IsOptional()
	@IsExist('staff_teams', 'id', 'new')
	staffTeamId: string;

	// @IsNotEmpty()
	// // @IsUnique('staffs', 'username', 'id')
	// username: string;

	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	// @IsOptional()
	// @IsEmailValidate()
	@IsUnique('staffs', 'email', 'id')
	@IsEmailOrPhoneContains()
	email: string;
	// @MinLength(6)
	// @MaxLength(200)
	// // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
	// // 	message: 'password too weak',
	// // })
	// password: string;

	@IsOptional()
	nameFurigana: string;

	@IsNotEmpty()
	@MaxLength(8)
	nameOnTab: string;

	@IsOptional()
	// @IsNotEmpty()
	@IsPhoneNo()
	@IsUnique('staffs', 'phone', 'id')
	phone: string;

	@IsNotEmpty()
	@IsIn(['email', 'phone', 'none'])
	loginType: string;

	@IsOptional()
	@IsBase64Image(true)
	image: any;

	@IsNotEmpty()
	status: 'in_work' | 'leave' | 'retired';

	@IsNotEmpty()
	@IsOneTrue(['isSiteAdmin', 'isSystemAdmin'])
	isSiteStaff: boolean;

	@IsNotEmpty()
	isSiteAdmin: boolean;

	@IsNotEmpty()
	isSystemAdmin: boolean;
}
