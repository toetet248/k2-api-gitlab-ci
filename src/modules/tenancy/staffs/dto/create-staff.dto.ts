import {
	IsEmail,
	IsIn,
	IsNotEmpty,
	IsOptional,
	IsString,
	Matches,
	MaxLength,
	MinLength,
} from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';
import { IsOneTrue } from 'src/common/validator/is-one-true.validator';
import { IsPhoneNo } from 'src/common/validator/is-phone-no.validators';
import { IsUnique } from 'src/common/validator/is-unique.validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsEmailValidate } from 'src/common/validator/is-email-validators';
import { IsEmailOrPhoneContains } from 'src/common/validator/is-email-or-phone-contains.validator';

export class CreateStaffDTO {
	@IsOptional()
	@IsExist('staff_teams', 'id', 'new')
	staffTeamId: string;

	// @IsNotEmpty()
	// // @IsUnique('staffs', 'username')
	// username: string;

	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	// @IsEmailValidate()
	// @IsEmail()
	@IsUnique('staffs', 'email', 'new')
	@IsEmailOrPhoneContains()
	email: string;

	// @IsOptional()
	// // @IsNotEmpty()
	// @IsString()
	// // @MinLength(6)
	// @MaxLength(200)
	// // // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
	// // // 	message: 'password too weak',
	// // // })
	// password: string;

	@IsOptional()
	nameFurigana: string;

	@IsNotEmpty()
	@MaxLength(8)
	nameOnTab: string;

	@IsOptional()
	// @IsNotEmpty()
	@IsPhoneNo()
	@IsUnique('staffs', 'phone')
	phone: string;

	@IsNotEmpty()
	@IsIn(['email', 'phone', 'none'])
	loginType: string;

	@IsOptional()
	@IsBase64Image()
	image: string;

	@IsNotEmpty()
	status: 'in_work' | 'leave' | 'retired';

	@IsNotEmpty()
	@IsOneTrue(['isSiteAdmin', 'isSystemAdmin'])
	isSiteStaff: boolean;

	@IsNotEmpty()
	isSiteAdmin: boolean;

	@IsNotEmpty()
	isSystemAdmin: boolean;
}
