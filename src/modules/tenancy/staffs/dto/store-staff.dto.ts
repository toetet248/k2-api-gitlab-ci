import {
	IsEmail,
	IsNotEmpty,
	IsOptional,
	IsString,
	Matches,
	MaxLength,
	MinLength,
} from 'class-validator';
import { IsBase64Image } from 'src/common/validator/is-base64-image.validator';
import { IsOneTrue } from 'src/common/validator/is-one-true.validator';
import { IsPhoneNo } from 'src/common/validator/is-phone-no.validators';
import { IsUnique } from 'src/common/validator/is-unique.validator';
import { IsExist } from 'src/common/validator/is-exist.validator';
import { IsEmailValidate } from 'src/common/validator/is-email-validators';

export class StoreStaffDTO {
	@IsOptional()
	@IsExist('staff_teams', 'id', 'new')
	staffTeamId: string;

	@IsNotEmpty()
	@MaxLength(30)
	name: string;

	// @IsOptional()
	// @IsEmailValidate()
	@IsEmail()
	@IsNotEmpty()
	@IsUnique('staffs', 'email')
	email: string;

	@IsNotEmpty()
	@IsString()
	@MinLength(6)
	@MaxLength(200)
	// // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
	// // 	message: 'password too weak',
	// // })
	password: string;

	@IsOptional()
	nameFurigana: string;

	@IsNotEmpty()
	@MaxLength(8)
	nameOnTab: string;

	// @IsOptional()
	@IsNotEmpty()
	@IsPhoneNo()
	@IsUnique('staffs', 'phone')
	phone: string;

	@IsOptional()
	@IsBase64Image()
	image: string;

	@IsNotEmpty()
	status: string;

	@IsNotEmpty()
	@IsOneTrue(['isSiteAdmin', 'isSystemAdmin'])
	isSiteStaff: boolean;

	@IsNotEmpty()
	isSiteAdmin: boolean;

	@IsNotEmpty()
	isSystemAdmin: boolean;
}
