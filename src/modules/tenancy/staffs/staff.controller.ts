import {
	BadRequestException,
	Delete,
	Get,
	Param,
	Patch,
	Query,
	UseGuards,
	Body,
	Headers,
} from '@nestjs/common';
import { Controller, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Not } from 'typeorm';
import { BaseController } from 'src/common/controller/base.controller';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { validateExists } from 'src/common/service/helper.service';
import { TENANT_HEADER } from '../tenancy.middleware';
import { CreateStaffDTO } from './dto/create-staff.dto';
import { PaginateStaffDTO } from './dto/paginate-staff.dto';
import { UpdateOrderStaffDTO } from './dto/update-order-staff.dto';
import { UpdateStaffDTO } from './dto/update-staff.dto';
import { CreateStaffSerialize } from './serialize/create-staff.serialize';
import { FindStaffSerialize } from './serialize/find-staff.serialize';
import { GetStaffSerialize } from './serialize/get-staff.serialize';
import { PaginateStaffSerialize } from './serialize/paginate-staff.serialize';
import { UpdateStaffSerialize } from './serialize/update-staff.serialize';
import { StaffService } from './staff.service';

@Controller('tenants/staffs')
@UseGuards(AuthGuard('tenant-jwt'))
export class StaffController extends BaseController {
	constructor(private readonly service: StaffService) {
		super();
	}

	@Get('/')
	@Serialize(GetStaffSerialize)
	async getStaff() {
		const staff = await this.service.repo.find({
			relations: ['staffTeam'],
			where: {
				isOwner: false,
			},
			order: {
				orderNo: 'ASC',
			},
		});
		return this.response(staff);
	}

	@Get('/site-admins')
	async getSiteAdmins() {
		const staffs = await this.service.repo.find({
			select: ['id', 'name', 'nameOnTab'],
			where: {
				isOwner: false,
				isSiteAdmin: true,
				status: Not('retired'),
			},
			order: {
				orderNo: 'ASC',
			},
		});
		return this.response(staffs);
	}

	@Get('/paginate')
	@Serialize(PaginateStaffSerialize)
	async paginateStaff(@Query() paginateStaffDTO: PaginateStaffDTO) {
		const paginate = await this.service.paginateStaff(paginateStaffDTO);

		return this.paginateResponse(paginate.items, paginate.meta);
	}

	@Get('/:id')
	@Serialize(FindStaffSerialize)
	async findStaff(@Param('id') id: string, @Query() query: any) {
		const sql = this.service.repo
			.createQueryBuilder('staffs')
			.withDeleted()
			.leftJoinAndSelect('staffs.staffTeam', 'staffTeams');

		if (query.leaves) {
			sql.leftJoinAndSelect('staffs.staffLeaves', 'staffLeaves');
		}
		const staff = await sql
			.loadRelationCountAndMap('staffs.schCount', 'staffs.scheduleStaffs')
			.loadRelationCountAndMap(
				'staffs.customerCount',
				'staffs.salesIncharges',
				'customerCount',
				(q) => q.where('customerCount.deletedAt IS NULL'),
			)
			.where('staffs.id = :id', { id })
			.andWhere('staffs.deletedAt IS NULL')
			.getOne();

		if (staff) return this.response(staff);

		throw new BadRequestException(['staff not found']);
	}

	@Post('/create')
	@InjectRequestToBody()
	@Serialize(CreateStaffSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async createStaff(
		@Body() createStaffDTO: CreateStaffDTO,
		@Headers(TENANT_HEADER) tenantName: string,
		@Query() query: any,
	) {
		const staff = await this.service.createStaff(
			createStaffDTO,
			tenantName,
			query.isInvite == 'true' || query.isInvite == '1',
		);
		return this.response(staff, { title: 'staff create success' });
	}

	@Patch('/:id/update')
	@InjectRequestToBody()
	@Serialize(UpdateStaffSerialize)
	@UseGuards(AuthGuard('site-admin'))
	async updateStaff(
		@Body() updateStaffDTO: UpdateStaffDTO,
		@Param('id') id: string,
		@Headers(TENANT_HEADER) tenantName: string,
		@Query() query: any,
	) {
		const staff = await this.service.updateStaff(
			id,
			updateStaffDTO,
			tenantName,
			query.isInvite == 'true' || query.isInvite == '1',
		);
		return this.response(staff, { title: 'staff update success' });
	}

	@Patch('/:id/reset/password')
	@UseGuards(AuthGuard('site-admin'))
	async resetPassword(@Param('id') id: string) {
		await this.service.resetPassword(id);
		return this.response(null, { title: 'reset password success' });
	}

	@Patch('/update/order')
	@InjectRequestToBody()
	@UseGuards(AuthGuard('site-admin'))
	async updateOrderStaff(@Body() updateOrderStaffDTO: UpdateOrderStaffDTO) {
		const data = updateOrderStaffDTO.ids.map((id, index) => {
			return this.service.repo.create({
				id,
				orderNo: index + 1,
			});
		});

		this.service.repo.save(data);

		return this.response(null, { title: 'staff order update success' });
	}

	@Delete('/:id')
	@UseGuards(AuthGuard('site-admin'))
	async deleteStaff(@Param('id') id: string) {
		await this.service.deleteStaff(id);

		return this.response(null, { title: 'staff delete success' });
	}

	@Patch('/:id/restore')
	@UseGuards(AuthGuard('site-admin'))
	async restoreStaff(@Param('id') id: string) {
		if (!(await validateExists(this.service.repo, ['id', id])))
			throw new BadRequestException(['staff does not exists']);

		this.service.repo.restore(id);

		return this.response(null, { title: 'staff restore success' });
	}
}
