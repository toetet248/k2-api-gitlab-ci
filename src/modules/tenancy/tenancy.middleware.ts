import { NextFunction, Request, Response } from 'express';

export const TENANT_HEADER = 'x-tenant-name';
export const DEVICE_TYPE_HEADER = 'x-device-type'; // pc || mobile
export const REFRESH_TOKEN_HEADER = 'x-refresh-token'; // pc || mobile

declare global {
	namespace Express {
		interface Request {
			tenantName: any;
		}
	}
}

export function tenancyMiddleware(
	req: Request,
	_res: Response,
	next: NextFunction,
): void {
	const header = req.headers[TENANT_HEADER] as string;
	req.tenantName = header?.toString() || null;
	next();
}
