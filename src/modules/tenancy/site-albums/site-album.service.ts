import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { generateId } from 'src/common/service/helper.service';
import { Connection, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { CreateSiteAlbumDTO } from './dto/create-site-album.dto';
import { UpdateSiteAlbumDTO } from './dto/update-site-album.dto';
import { SiteAlbum } from './site-album.entity';

@Injectable()
export class SiteAlbumService {
	public readonly repo: Repository<SiteAlbum>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(SiteAlbum);
	}

	async createAlbum(createAlbum: CreateSiteAlbumDTO) {
		const data = this.repo.create({
			...createAlbum,
			id: generateId(),
		});

		await this.repo.save(data);

		return data;
	}

	async updateAlbum(id: string, updateAlbumDTO: UpdateSiteAlbumDTO) {
		const albumData = await this.repo.findOne({
			where: { id },
		});

		if (albumData) {
			await this.repo.save({
				...albumData,
				...updateAlbumDTO,
			});
			return albumData;
		}

		throw new BadRequestException(['data not found']);
	}

	async deleteAlbum(id: string, siteId: string) {
		const deleteAlbum = await this.repo.find({
			where: {
				id,
				siteId,
			},
		});
		await this.repo.softRemove(deleteAlbum);
	}
}
