import { Expose } from 'class-transformer';

export class DeleteSiteAlbumSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	siteId: string;
}
