import { Expose } from 'class-transformer';

export class GetSiteAlbumSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	siteId: string;
}
