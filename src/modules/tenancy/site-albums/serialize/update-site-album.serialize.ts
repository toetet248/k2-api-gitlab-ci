import { Expose } from 'class-transformer';

export class UpdateSiteAlbumSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	siteId: string;
}
