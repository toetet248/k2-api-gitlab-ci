import { Expose } from 'class-transformer';

export class CreateSiteAlbumSerialize {
	@Expose()
	id: string;

	@Expose()
	name: string;

	@Expose()
	siteId: string;

}
