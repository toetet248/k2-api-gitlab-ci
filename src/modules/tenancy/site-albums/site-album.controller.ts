import {
	Get,
	Param,
	UseGuards,
	UseInterceptors,
	UploadedFile,
	Post,
	Delete,
} from '@nestjs/common';
import { Body, Controller, Patch } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { FormDataRequest, NestjsFormDataModule } from 'nestjs-form-data';

import { UpdateSiteAlbumDTO } from './dto/update-site-album.dto';
import { SiteAlbumService } from './site-album.service';
import { UpdateSiteAlbumSerialize } from './serialize/update-site-album.serialize';
import { GetSiteAlbumSerialize } from './serialize/get-site-album.serialize';

import { BaseController } from 'src/common/controller/base.controller';
import { Serialize } from 'src/common/interceptor/serialize.interceptor';
import { CreateSiteAlbumSerialize } from './serialize/create-site-album.serialize';
import { CreateSiteAlbumDTO } from './dto/create-site-album.dto';
import { InjectRequestToBody } from 'src/common/decorator/inject-request-to.decorator';

@Controller('tenants/site-albums')
@UseGuards(AuthGuard('tenant-jwt'))
export class SiteAlbumController extends BaseController {
	constructor(private readonly service: SiteAlbumService) {
		super();
	}

	@Get('/')
	@Serialize(GetSiteAlbumSerialize)
	async getAlbum(@Param('id') id: string) {
		const data = await this.service.repo.findOne(id);

		return this.response(data);
	}

	@Post('/create')
	@InjectRequestToBody()
	@Serialize(CreateSiteAlbumSerialize)
	async createAlbum(@Body() createAlbumDTO: CreateSiteAlbumDTO) {
		const data = await this.service.createAlbum(createAlbumDTO);

		return this.response(data, { title: 'album create success' });
	}

	@Patch('/:id/update')
	@Serialize(UpdateSiteAlbumSerialize)
	async updateAlbum(
		@Param('id') id: string,
		@Body() updateAlbumDTO: UpdateSiteAlbumDTO,
	) {
		const data = await this.service.updateAlbum(id, updateAlbumDTO);
		return this.response(data, { title: 'albumn update success' });
	}

	@Delete('/:id')
	async deleteAlbum(@Param('id') id: string, siteId: string) {
		const data = this.service.deleteAlbum(id, siteId);
		return this.response(data, { title: 'album deleted success' });
	}
}
