import { IsNotEmpty } from 'class-validator';

export class UpdateSiteAlbumDTO {
	@IsNotEmpty()
	name: string;

	@IsNotEmpty()
	siteId: string;
}
