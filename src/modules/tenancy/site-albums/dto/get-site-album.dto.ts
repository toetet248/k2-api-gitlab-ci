import { IsNotEmpty } from 'class-validator';

export class GetSiteAlbumDTO {
	@IsNotEmpty()
	name: string;

	@IsNotEmpty()
	siteId: string;
}
