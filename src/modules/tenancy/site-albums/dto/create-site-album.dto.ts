import { IsNotEmpty } from 'class-validator';
// import { Unique } from 'src/common/validator/unique.validator';

export class CreateSiteAlbumDTO {
	@IsNotEmpty()
	// @Unique('site_albums', 'name')
	name: string;

	@IsNotEmpty()
	siteId: string;
}
