import { IsNotEmpty } from 'class-validator';

export class DeleteSiteAlbumDTO {
	@IsNotEmpty()
	name: string;

	@IsNotEmpty()
	siteId: string;
}
