import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Site } from '../sites/site.entity';
import { SiteImage } from '../site-images/site-image.entity';

@Entity({ name: 'site_albums' })
export class SiteAlbum extends MainEntity {
	@Column()
	name: string;

	@Column()
	siteId: string;

	@ManyToOne(() => Site, (site) => site.siteAlbums, {
		primary: true,
	})
	@JoinColumn({ name: 'siteId' })
	site: Site;

	@OneToMany(() => SiteImage, (siteImage) => siteImage.site)
	siteImages: SiteImage[];
}
