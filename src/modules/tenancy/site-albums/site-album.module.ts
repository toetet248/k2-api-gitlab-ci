import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { SiteAlbumController } from './site-album.controller';
import { SiteAlbum } from './site-album.entity';
import { SiteAlbumService } from './site-album.service';

@Module({
	imports: [TypeOrmModule.forFeature([SiteAlbum]), NestjsFormDataModule],
	controllers: [SiteAlbumController],
	providers: [SiteAlbumService],
})
export class SiteAlbumModule {}
