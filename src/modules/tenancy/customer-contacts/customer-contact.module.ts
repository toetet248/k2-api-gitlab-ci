import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';

import { CustomerContact } from './customer-contact.entity';
import { CustomerContactService } from './customer-contact.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([CustomerContact]),
		NestjsFormDataModule,
	],
	providers: [CustomerContactService],
	exports: [CustomerContactService], // if directory isn't has
})
export class CustomerContactModule {}
