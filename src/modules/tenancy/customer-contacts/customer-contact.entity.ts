import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Customer } from '../customers/customer.entity';
import { Site } from '../sites/site.entity';

@Entity({ name: 'customer_contacts' })
export class CustomerContact extends MainEntity {
	@Column()
	customerId: string;

	@Column()
	name: string;

	@Column()
	phone: string;

	@Column()
	email: string;

	@ManyToOne(() => Customer, (customer) => customer.customerContacts, {
		primary: true,
	})
	@JoinColumn({ name: 'customerId' })
	customer: Customer;

	@OneToMany(() => Site, (site) => site.customerContact)
	sites: Site[];
}
