import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { CustomerContact } from './customer-contact.entity';

@Injectable()
export class CustomerContactService {
	public readonly repo: Repository<CustomerContact>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(CustomerContact);
	}

	//remove customer contact
	async removeOtherCustomerContactsByIds(ids: string[], customerId: string) {
		const removeCustomerContacts = await this.repo.find({
			where: {
				id: Not(In(ids)),
				customerId,
			},
		});

		await this.repo.softRemove(removeCustomerContacts);
	}
}
