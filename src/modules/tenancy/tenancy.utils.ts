import { BadRequestException } from '@nestjs/common';
import { Connection, createConnection, getConnectionManager } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import * as config from './../../config/config';

export async function getTenantConnection(tenantName: string): Promise<Connection> {
	const connectionName = `${tenantName}`;
	const connectionManager = getConnectionManager();

	if (connectionManager.has(connectionName)) {
		const connection = connectionManager.get(connectionName);
		return await Promise.resolve(
			connection.isConnected ? connection : connection.connect(),
		);
	}

	return await createConnection({
		...(config.default().database.tenant as MysqlConnectionOptions),
		name: connectionName,
		database: connectionName,
	}).catch((error) => {
		throw new BadRequestException(['tenant does not exists']);
	});
}
