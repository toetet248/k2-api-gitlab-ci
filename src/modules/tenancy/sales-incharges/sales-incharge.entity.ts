import { MainEntity } from 'src/common/entity/main.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Customer } from '../customers/customer.entity';
import { Staff } from '../staffs/staff.entity';

@Entity({ name: 'sales_incharges' })
export class SalesIncharge extends MainEntity {
	@Column()
	staffId: string;

	@Column()
	customerId: string;

	@ManyToOne(() => Customer, (customer) => customer.salesIncharges, {
		primary: true,
	})
	@JoinColumn({ name: 'customerId' })
	customer: Customer;

	@ManyToOne(() => Staff, (staff) => staff.salesIncharges, {
		primary: true,
	})
	@JoinColumn({ name: 'staffId' })
	staff: Staff;
}
