import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { CONNECTION } from '../tenancy.symbols';
import { SalesIncharge } from './sales-incharge.entity';

@Injectable()
export class SalesInchargeService {
	public readonly repo: Repository<SalesIncharge>;

	constructor(@Inject(CONNECTION) connection: Connection) {
		this.repo = connection.getRepository(SalesIncharge);
	}

	//remove customer contact
	async removeOtherSalesInchargesByIds(ids: string[], customerId: string) {
		const removeSalesIncharges = await this.repo.find({
			where: {
				id: Not(In(ids)),
				customerId,
			},
		});

		await this.repo.remove(removeSalesIncharges);
	}

	// async getSalesIncharge() {
	// 	const salesIncharges = await this.repo.find({
	// 		relations: ['staffs'],
	// 		where: {
	// 			isOwner: 0,
	// 		},
	// 	});
	// }
}
