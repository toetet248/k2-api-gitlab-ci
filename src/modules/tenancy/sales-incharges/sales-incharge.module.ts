import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { SalesIncharge } from './sales-incharge.entity';
import { SalesInchargeService } from './sales-incharge.service';

@Module({
	imports: [TypeOrmModule.forFeature([SalesIncharge]), NestjsFormDataModule],
	providers: [SalesInchargeService],
	exports: [SalesInchargeService],
})
export class SalesInchargeModule {}
