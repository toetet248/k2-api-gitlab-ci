import {
	MigrationInterface,
	QueryRunner,
	TableColumn,
	TableForeignKey,
} from 'typeorm';

export class ChangeAuthorInDailyNotesTable1665467734590
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE daily_notes DROP COLUMN author`);

		await queryRunner.addColumn(
			'daily_notes',
			new TableColumn({
				name: 'staffId',
				type: 'varchar',
				isNullable: true,
			}),
		);

		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('daily_notes', staffForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
