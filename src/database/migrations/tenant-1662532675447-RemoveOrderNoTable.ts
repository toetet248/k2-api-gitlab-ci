import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveOrderNoTable1662532675447 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
		// await queryRunner.query(
		// 	`ALTER TABLE staff_teams DROP COLUMN orderNo`,
		// );

		// await queryRunner.query(
		// 	`ALTER TABLE vehicle_groups DROP COLUMN orderNo`,
		// );

		await queryRunner.query(
			`ALTER TABLE customer_contacts DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE operations DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE operation_instructions DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE site_albums DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE site_images DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE site_files DROP COLUMN orderNo`,
		);

		await queryRunner.query(
			`ALTER TABLE site_memos DROP COLUMN orderNo`,
		);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
