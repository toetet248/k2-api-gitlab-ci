import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddOrderNoTable1666251392719 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'staff_teams',
			new TableColumn({
				name: 'orderNo',
				type: 'varchar',
				length: '50',
				isNullable: false,
			}),
		);

		await queryRunner.addColumn(
			'vehicle_groups',
			new TableColumn({
				name: 'orderNo',
				type: 'varchar',
				length: '50',
				isNullable: false,
			}),
		);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
