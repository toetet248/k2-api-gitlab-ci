import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCustomerIdInTenantsTable1663136870662
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE tenants ADD COLUMN customerId VARCHAR(255)`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
