import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateUsersTenantsTable1653805948418
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'users_tenants',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'userId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'tenantId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'role',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'password',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'isInvite',
						type: 'bool',
						isNullable: false,
						default: false,
					},
					{
						name: 'resetPasswordAt',
						type: 'datetime',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		queryRunner.clearSqlMemory();

		const usersForeignKey = new TableForeignKey({
			columnNames: ['userId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'users',
			onDelete: 'CASCADE',
		});
		await queryRunner.createForeignKey('users_tenants', usersForeignKey);

		const tenantsForeignKey = new TableForeignKey({
			columnNames: ['tenantId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'tenants',
			onDelete: 'CASCADE',
		});
		await queryRunner.createForeignKey('users_tenants', tenantsForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE users_tenants`);
	}
}
