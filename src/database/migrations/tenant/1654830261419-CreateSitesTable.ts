import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSitesTable1654830261419 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'sites',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'customerId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'customerContactId',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'staffId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'addressPrefecture',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'addressCity',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'addressBuilding',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'lat',
						type: 'varchar',
						isNullable: true,
						length: '20',
					},
					{
						name: 'lng',
						type: 'varchar',
						isNullable: true,
						length: '20',
					},
					{
						name: 'startDate',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'endDate',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'startTime',
						type: 'time',
						isNullable: true,
					},
					{
						name: 'endTime',
						type: 'time',
						isNullable: true,
					},
					{
						name: 'price',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		queryRunner.clearSqlMemory();
		// customer foreign key
		const customerForeignKey = new TableForeignKey({
			columnNames: ['customerId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'customers',
		});
		await queryRunner.createForeignKey('sites', customerForeignKey);
		// customer contact foreign key
		const customerContactForeignKey = new TableForeignKey({
			columnNames: ['customerContactId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'customer_contacts',
		});
		await queryRunner.createForeignKey('sites', customerContactForeignKey);

		// staff foreign key
		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('sites', staffForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE sites`);
	}
}
