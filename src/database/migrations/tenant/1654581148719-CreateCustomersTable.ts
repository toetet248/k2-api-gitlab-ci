import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateCustomersTable1654581148719 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'customers',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'nameFurigana',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'businessTypes',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'postalCode',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'addressPrefecture',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'addressCity',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'addressBuilding',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'phone',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'fax',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE customers`);
	}
}
