import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddOrderNoColumnSiteInstrcutionTable1668168960892
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE operation_instructions ADD COLUMN orderNo  INT DEFAULT NULL AFTER operationId`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
