import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateScheduleSitesTable1656484192099
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'schedule_sites',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'scheduleDate',
						type: 'date',
						isNullable: false,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		queryRunner.clearSqlMemory();

		//site foreign key
		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey('schedule_sites', siteForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE schedule_sites`);
	}
}
