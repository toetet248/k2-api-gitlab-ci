import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSiteAlbumsTable1654830327188 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'site_albums',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		queryRunner.clearSqlMemory();

		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey('site_albums', siteForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE site_albums`);
	}
}
