import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSiteImagesTable1654830337200 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'site_images',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'albumId',
						type: 'varchar',
						isNullable: true,
					},
					// {
					// 	name: 'name',
					// 	type: 'varchar',
					// 	isNullable: false,
					// },
					{
						name: 'image',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'description',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'size',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'staffId',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		// site froeign key
		queryRunner.clearSqlMemory();

		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey('site_images', siteForeignKey);

		// album foreign key
		const albumnForeignKey = new TableForeignKey({
			columnNames: ['albumId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'site_albums',
		});
		await queryRunner.createForeignKey('site_images', albumnForeignKey);

		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('site_images', staffForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE site_images`);
	}
}
