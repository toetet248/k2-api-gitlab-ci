import {
    MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,} from "typeorm";

export class CreateStaffLeavesTable1664448053952 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
			new Table({
				name: 'staff_leaves',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'staffId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'date',
						type: 'date',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		queryRunner.clearSqlMemory();

		//staff foreign key
		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('staff_leaves', staffForeignKey);
	}

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`DROP TABLE staff_leaves`);
    }

}
