import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateScheduleVehiclesTable1656484192089
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'schedule_vehicles',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'vehicleId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'dailyNoteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'dayNightSupport',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'nightSupportCount',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		queryRunner.clearSqlMemory();

		//vehicle foreign key
		const vehicleForeignKey = new TableForeignKey({
			columnNames: ['vehicleId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'vehicles',
		});
		await queryRunner.createForeignKey(
			'schedule_vehicles',
			vehicleForeignKey,
		);

		//staff foreign key
		const dailyNoteForeignKey = new TableForeignKey({
			columnNames: ['dailyNoteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'daily_notes',
		});
		await queryRunner.createForeignKey(
			'schedule_vehicles',
			dailyNoteForeignKey,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE schedule_vehicles`);
	}
}
