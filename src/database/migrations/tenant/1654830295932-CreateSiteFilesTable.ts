import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSiteFilesTable1654830295932 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'site_files',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'file',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'size',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'staffId',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		// site froeign key
		queryRunner.clearSqlMemory();

		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey('site_files', siteForeignKey);

		const staffForeignKey2 = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('site_files', staffForeignKey2);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE site_files`);
	}
}
