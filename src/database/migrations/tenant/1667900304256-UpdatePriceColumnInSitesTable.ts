import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdatePriceColumnInSitesTable1667900304256
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE sites MODIFY COLUMN price bigint DEFAULT NULL`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
