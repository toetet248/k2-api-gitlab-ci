import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateOperationInstructionsTable1654834383524
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'operation_instructions',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'operationId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		queryRunner.clearSqlMemory();

		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey(
			'operation_instructions',
			siteForeignKey,
		);
		// operation foreign key
		const operationForeignKey = new TableForeignKey({
			columnNames: ['operationId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'operations',
		});
		await queryRunner.createForeignKey(
			'operation_instructions',
			operationForeignKey,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE operation_instructions`);
	}
}
