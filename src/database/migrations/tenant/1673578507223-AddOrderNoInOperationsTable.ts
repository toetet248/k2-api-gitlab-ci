import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddOrderNoInOperationsTable1673578507223
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE operations ADD COLUMN orderNo INT DEFAULT 0 AFTER colorCode`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
