import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateVehicleGroupsTable1654572846033
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'vehicle_groups',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'colorCode',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE vehicle_groups`);
	}
}
