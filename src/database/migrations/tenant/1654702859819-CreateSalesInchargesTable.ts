import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSalesInchargesTable1654702859819
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'sales_incharges',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'staffId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'customerId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		queryRunner.clearSqlMemory();

		const staffIdForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey(
			'sales_incharges',
			staffIdForeignKey,
		);

		const customerIdForeignKey = new TableForeignKey({
			columnNames: ['customerId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'customers',
		});
		await queryRunner.createForeignKey(
			'sales_incharges',
			customerIdForeignKey,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE sales_incharges`);
	}
}
