import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateStaffsTable1653385595143 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'staffs',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'userId',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'staffTeamId',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'email',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'password',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'nameFurigana',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'nameOnTab',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'phone',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'image',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'status',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'isSiteStaff',
						type: 'bool',
						isNullable: true,
						default: false,
					},
					{
						name: 'isSiteAdmin',
						type: 'bool',
						isNullable: true,
						default: false,
					},
					{
						name: 'isSystemAdmin',
						type: 'bool',
						isNullable: true,
						default: false,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'isOwner',
						type: 'bool',
						default: false,
					},
					{
						name: 'loginType',
						type: 'varchar',
						length: '10',
						default: "'email'",
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		// clear sqls in memory to avoid removing tables when down queries executed.
		queryRunner.clearSqlMemory();

		const staffTeamsForeignKey = new TableForeignKey({
			columnNames: ['staffTeamId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staff_teams',
		});
		await queryRunner.createForeignKey('staffs', staffTeamsForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE staffs`);
	}
}
