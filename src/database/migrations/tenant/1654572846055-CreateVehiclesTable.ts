import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateVehiclesTable1654572846055 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'vehicles',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'nameOnTab',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'licenseEndDate',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'insuranceEndDate',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'status',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'vehicleGroupId',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'image',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'memo',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		// clear sqls in memory to avoid removing tables when down queries executed.
		queryRunner.clearSqlMemory();

		const vehicleGroupsForeignKey = new TableForeignKey({
			columnNames: ['vehicleGroupId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'vehicle_groups',
		});
		await queryRunner.createForeignKey('vehicles', vehicleGroupsForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE vehicles`);
	}
}
