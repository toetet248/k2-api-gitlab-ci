import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateScheduleStaffsTable1656484126700
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'schedule_staffs',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'staffId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'dailyNoteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'dayNightSupport',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'nightSupportCount',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'orderNo',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		queryRunner.clearSqlMemory();

		//staff foreign key
		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('schedule_staffs', staffForeignKey);

		//staff foreign key
		const dailyNoteForeignKey = new TableForeignKey({
			columnNames: ['dailyNoteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'daily_notes',
		});
		await queryRunner.createForeignKey(
			'schedule_staffs',
			dailyNoteForeignKey,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE schedule_staffs`);
	}
}
