import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateDailyNotesTable1654834383529 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'daily_notes',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'siteId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'operationId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'operationInstructionId',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'scheduleDate',
						type: 'date',
						isNullable: false,
					},
					{
						name: 'reserveMan',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'reserveHour',
						type: 'float',
						isNullable: true,
					},
					{
						name: 'reserveManPower',
						type: 'float',
						isNullable: true,
					},
					{
						name: 'reserveStartTime',
						type: 'varchar',
						isNullable: true,
						length: '10',
					},
					{
						name: 'reserveEndTime',
						type: 'varchar',
						isNullable: true,
						length: '10',
					},
					{
						name: 'actualMan',
						type: 'integer',
						isNullable: true,
					},
					{
						name: 'actualHour',
						type: 'float',
						isNullable: true,
					},
					{
						name: 'actualManPower',
						type: 'float',
						isNullable: true,
					},
					{
						name: 'actualStartTime',
						type: 'varchar',
						isNullable: true,
						length: '10',
					},
					{
						name: 'actualEndTime',
						type: 'varchar',
						isNullable: true,
						length: '10',
					},
					{
						name: 'staffId',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'memo',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
		// clear sqls in memory to avoid removing tables when down queries executed.
		// site froeign key
		queryRunner.clearSqlMemory();

		const siteForeignKey = new TableForeignKey({
			columnNames: ['siteId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'sites',
		});
		await queryRunner.createForeignKey('daily_notes', siteForeignKey);
		// operation foreign key
		const operationForeignKey = new TableForeignKey({
			columnNames: ['operationId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'operations',
		});
		await queryRunner.createForeignKey('daily_notes', operationForeignKey);

		// operationInstruction foreign key
		const operationInstructionForeignKey = new TableForeignKey({
			columnNames: ['operationInstructionId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'operation_instructions',
		});
		await queryRunner.createForeignKey(
			'daily_notes',
			operationInstructionForeignKey,
		);

		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('daily_notes', staffForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE daily_notes`);
	}
}
