import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddLoginTypeInStaffsTable1666440679566
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'staffs',
			new TableColumn({
				name: 'loginType',
				type: 'varchar',
				length: '10',
				isNullable: true,
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
