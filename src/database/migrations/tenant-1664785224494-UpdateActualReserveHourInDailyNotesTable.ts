import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateActualReserveHourInDailyNotesTable1664785224494
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE daily_notes MODIFY COLUMN reserveHour float`,
		);

		await queryRunner.query(
			`ALTER TABLE daily_notes MODIFY COLUMN actualHour float`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
