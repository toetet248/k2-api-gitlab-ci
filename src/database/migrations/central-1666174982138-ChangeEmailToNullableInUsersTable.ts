import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeEmailToNullableInUsersTable1666174982138
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE users MODIFY COLUMN email VARCHAR(255) DEFAULT NULL`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
