import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddCompanyDataInTenantsTable1665725859618
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumns('tenants', [
			new TableColumn({
				name: 'companyName',
				type: 'varchar',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyPhone',
				type: 'varchar',
				length: '50',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyAddressPrefecture',
				type: 'varchar',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyAddressCity',
				type: 'varchar',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyAddressBuilding',
				type: 'varchar',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyImage',
				type: 'varchar',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyPostalCode',
				type: 'varchar',
				length: '20',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyLat',
				type: 'varchar',
				length: '20',
				isNullable: true,
			}),
			new TableColumn({
				name: 'companyLng',
				type: 'varchar',
				length: '20',
				isNullable: true,
			}),
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
