import {
	MigrationInterface,
	QueryRunner,
	TableColumn,
	TableForeignKey,
} from 'typeorm';

export class AddStaffIdInSiteImagesAndFiles1665040732583
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'site_images',
			new TableColumn({
				name: 'staffId',
				type: 'varchar',
				length: '50',
				isNullable: true,
			}),
		);

		await queryRunner.addColumn(
			'site_files',
			new TableColumn({
				name: 'staffId',
				type: 'varchar',
				length: '50',
				isNullable: true,
			}),
		);

		queryRunner.clearSqlMemory();

		//staff foreign key
		const staffForeignKey = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('site_images', staffForeignKey);

		const staffForeignKey2 = new TableForeignKey({
			columnNames: ['staffId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'staffs',
		});
		await queryRunner.createForeignKey('site_files', staffForeignKey2);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
