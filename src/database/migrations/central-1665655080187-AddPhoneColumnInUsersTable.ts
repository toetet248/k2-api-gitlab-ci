import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddPhoneColumnInUsersTable1665655080187
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'users',
			new TableColumn({
				name: 'phone',
				type: 'varchar',
				length: '50',
				isNullable: true,
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
