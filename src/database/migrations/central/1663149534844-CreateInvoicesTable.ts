import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateInvoicesTable1663149534844 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'invoices',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'tenantId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'subscriptionId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'recurringId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'planId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'orderId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'amount',
						type: 'integer',
						isNullable: false,
					},
					{
						name: 'tax',
						type: 'integer',
						default: 0,
					},
					{
						name: 'processDate',
						type: 'datetime',
						isNullable: false,
					},
					{
						name: 'status',
						type: 'varchar',
						length: '30',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		queryRunner.clearSqlMemory();

		// tenants foreign key
		const tenantForeignKey = new TableForeignKey({
			columnNames: ['tenantId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'tenants',
		});
		await queryRunner.createForeignKey('invoices', tenantForeignKey);

		// tenants foreign key
		const subscriptionForeignKey = new TableForeignKey({
			columnNames: ['subscriptionId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'subscriptions',
		});
		await queryRunner.createForeignKey('invoices', subscriptionForeignKey);

		// plans foreign key
		const planForeignKey = new TableForeignKey({
			columnNames: ['planId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'plans',
		});
		await queryRunner.createForeignKey('invoices', planForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE invoices`);
	}
}
