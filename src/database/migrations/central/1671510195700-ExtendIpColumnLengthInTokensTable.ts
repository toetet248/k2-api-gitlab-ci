import { MigrationInterface, QueryRunner } from 'typeorm';

export class ExtendIpColumnLengthInTokensTable1671510195700
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE tokens MODIFY COLUMN ipAddress VARCHAR(50) DEFAULT NULL`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
