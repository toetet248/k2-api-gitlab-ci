import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameCompanyPostalCodeToCompanyFaxInTenantsTable1672981643484
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.renameColumn(
			'tenants',
			'companyPostalCode',
			'companyFax',
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
