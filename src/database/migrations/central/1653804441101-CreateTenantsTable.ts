import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTenantsTable1653804441101 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'tenants',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: false,
					},
					{
						name: 'gmoId',
						type: 'varchar',
						length: '100',
						isNullable: true,
					},
					{
						name: 'customerId',
						type: 'varchar',
						length: '100',
						isNullable: true,
					},
					{
						name: 'isDatabaseExist',
						type: 'boolean',
						default: false,
					},
					{
						name: 'companyName',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyPhone',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'companyEmail',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyAddressPrefecture',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyAddressCity',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyAddressBuilding',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyImage',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'companyPostalCode',
						type: 'varchar',
						length: '20',
						isNullable: true,
					},
					{
						name: 'companyLat',
						type: 'varchar',
						length: '20',
						isNullable: true,
					},
					{
						name: 'companyLng',
						type: 'varchar',
						length: '20',
						isNullable: true,
					},
					{
						name: 'companyBusinessTypes',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE tenants`);
	}
}
