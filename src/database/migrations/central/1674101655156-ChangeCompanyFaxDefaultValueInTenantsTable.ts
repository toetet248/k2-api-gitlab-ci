import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeCompanyFaxDefaultValueInTenantsTable1674101655156
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE tenants MODIFY COLUMN companyFax VARCHAR(20) DEFAULT NULL`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
