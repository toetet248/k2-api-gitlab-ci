import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePlansTable1658812572777 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'plans',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'nameFurigana',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'price',
						type: 'integer',
						isNullable: false,
						default: 0,
					},
					{
						name: 'options',
						type: 'json',
						isNullable: false,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE plans`);
	}
}
