import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateUsersTable1653804441202 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'users',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'name',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'email',
						type: 'varchar',
						isNullable: true,
					},
					{
						name: 'phone',
						type: 'varchar',
						length: '50',
						isNullable: true,
					},
					{
						name: 'isActive',
						type: 'bool',
						isNullable: false,
						default: true,
					},
					{
						name: 'tenantId',
						type: 'varchar',
						length: '100',
						isNullable: false,
					},
					{
						name: 'role',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'code',
						type: 'varchar',
						length: '10',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		queryRunner.clearSqlMemory();

		// tenants foreign key
		const tenantForeignKey = new TableForeignKey({
			columnNames: ['tenantId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'tenants',
		});
		await queryRunner.createForeignKey('users', tenantForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE users`);
	}
}
