import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddResetPasswordAtInUsersTable1668394644536
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'users',
			new TableColumn({
				name: 'resetPasswordAt',
				type: 'datetime',
				isNullable: true,
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
