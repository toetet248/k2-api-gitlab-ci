import {
	MigrationInterface,
	QueryRunner,
	Table,
	TableForeignKey,
} from 'typeorm';

export class CreateSubscriptionsTable1658814095004
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'subscriptions',
				columns: [
					{
						name: 'id',
						type: 'varchar',
						length: '50',
						isPrimary: true,
						generationStrategy: 'uuid',
					},
					{
						name: 'tenantId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'planId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'recurringId',
						type: 'varchar',
						length: '50',
						isNullable: false,
					},
					{
						name: 'status',
						type: 'varchar',
						length: '30',
						isNullable: true,
					},
					{
						name: 'startedAt',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'endedAt',
						type: 'date',
						isNullable: true,
					},
					{
						name: 'createdAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'updatedAt',
						type: 'datetime',
						isNullable: false,
						default: 'CURRENT_TIMESTAMP',
					},
					{
						name: 'deletedAt',
						type: 'datetime',
						isNullable: true,
					},
				],
			}),
			false,
		);

		queryRunner.clearSqlMemory();

		// tenants foreign key
		const tenantForeignKey = new TableForeignKey({
			columnNames: ['tenantId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'tenants',
		});
		await queryRunner.createForeignKey('subscriptions', tenantForeignKey);

		// plans foreign key
		const planForeignKey = new TableForeignKey({
			columnNames: ['planId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'plans',
		});
		await queryRunner.createForeignKey('subscriptions', planForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		queryRunner.query(`DROP TABLE subscriptions`);
	}
}
