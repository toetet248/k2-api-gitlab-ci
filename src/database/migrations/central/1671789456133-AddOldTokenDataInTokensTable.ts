import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddOldTokenDataInTokensTable1671789456133
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumns('tokens', [
			new TableColumn({
				name: 'oldRefreshToken',
				type: 'varchar',
				length: '50',
				isNullable: true,
			}),
			new TableColumn({
				name: 'oldRefreshTokenExpire',
				type: 'datetime',
				isNullable: true,
			}),
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
