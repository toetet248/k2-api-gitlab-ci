import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class AddTenantIdForeignKeyInUsersTable1666237011244
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		// tenants foreign key
		const tenantForeignKey = new TableForeignKey({
			columnNames: ['tenantId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'tenants',
		});
		await queryRunner.createForeignKey('users', tenantForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
