import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangePhoneToNullableInUsersTable1666174982137
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`ALTER TABLE users MODIFY COLUMN phone VARCHAR(50) DEFAULT NULL`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
