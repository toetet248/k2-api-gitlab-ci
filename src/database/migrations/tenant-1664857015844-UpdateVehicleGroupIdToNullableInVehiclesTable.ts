import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class UpdateVehicleGroupIdToNullableInVehiclesTable1664857015844
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		const indexes = await queryRunner.query(
			'SHOW INDEX FROM vehicles;',
			undefined,
		);
		const foreignKeyIndexName = indexes[1].Key_name;

		// //drop column
		await queryRunner.query(
			`ALTER TABLE vehicles DROP FOREIGN KEY ${foreignKeyIndexName}`,
			undefined,
		);
		// // //change data type
		await queryRunner.query(
			`ALTER TABLE vehicles MODIFY COLUMN vehicleGroupId VARCHAR(100) DEFAULT NULL`,
			undefined,
		);
		queryRunner.clearSqlMemory();

		// //vehicle foreign key
		// await queryRunner.query(
		// 	`ALTER TABLE vehicles ADD CONSTRAINT vehicleGroupId_fk1 FOREIGN KEY (vehicleGroupId) REFERENCES vehicle_groups(id) ON DELETE NO ACTION ON UPDATE NO ACTION`,
		// 	undefined,
		// );

		const vehicleGroupForeignKey = new TableForeignKey({
			columnNames: ['vehicleGroupId'],
			referencedColumnNames: ['id'],
			referencedTableName: 'vehicle_groups',
		});
		await queryRunner.createForeignKey('vehicles', vehicleGroupForeignKey);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
