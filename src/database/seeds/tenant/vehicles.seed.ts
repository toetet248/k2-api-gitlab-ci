import { generateId } from 'src/common/service/helper.service';

export async function VehiclesSeed(vehicleGroups: any[]): Promise<any[]> {
	return [
		{
			id: generateId(),
			vehicleGroupId: vehicleGroups[0].id,
			nameOnTab: 'トラック01',
			status: 'in_operation',
			orderNo: 1,
		},
	];
}
