import { generateId } from 'src/common/service/helper.service';

export async function OperationsSeed() {
	return [
		{
			id: generateId(),
			name: 'タスク1',
			colorCode: 'color6',
			orderNo: 1,
		},
		{
			id: generateId(),
			name: '工程1',
			colorCode: 'color9',
			orderNo: 2,
		},
	];
}
