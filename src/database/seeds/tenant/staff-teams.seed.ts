import { generateId } from 'src/common/service/helper.service';

export async function StaffTeamsSeed() {
	return [
		{
			id: generateId(),
			name: 'チーム１',
			colorCode: 'color1',
			orderNo: 1,
		},
	];
}
