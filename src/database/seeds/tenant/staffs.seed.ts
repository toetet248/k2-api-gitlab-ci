import { generateId, generateNumber } from 'src/common/service/helper.service';
import * as bcrypt from 'bcrypt';

export async function StaffsSeed(
	staffTeams: any[],
	users: any[],
): Promise<any[]> {
	const salt = await bcrypt.genSalt();
	const hashPassword = await bcrypt.hash(generateNumber(), salt);

	return [
		{
			id: generateId(),
			userId: users[0].id,
			staffTeamId: staffTeams[0].id,
			name: users[0].name,
			email: users[0].email,
			password: hashPassword,
			nameOnTab: users[0].name,
			status: 'in_work',
			isSiteStaff: true,
			isSiteAdmin: true,
			orderNo: 2,
			loginType: 'email',
		},
	];
}
