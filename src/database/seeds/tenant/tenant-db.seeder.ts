import { Connection, getConnectionManager } from 'typeorm';
import { Staff } from '../../../modules/tenancy/staffs/staff.entity';
import { StaffTeam } from '../../../modules/tenancy/staff-teams/staff-team.entity';
import { VehicleGroup } from '../../../modules/tenancy/vehicle-groups/vehicle-group.entity';
import { StaffTeamsSeed } from 'src/database/seeds/tenant/staff-teams.seed';
import { VehicleGroupsSeed } from 'src/database/seeds/tenant/vehicle-groups.seed';
import { OperationsSeed } from './operations.seed';
import { Operation } from 'src/modules/tenancy/operations/operation.entity';
import { StaffsSeed } from './staffs.seed';
import * as configuration from './../../../config/config';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { UsersSeed } from './users.seed';
import { User } from 'src/modules/central/users/user.entity';
import { CustomersSeed } from './customers.seed';
import { Customer } from 'src/modules/tenancy/customers/customer.entity';
import { SalesIncharge } from 'src/modules/tenancy/sales-incharges/sales-incharge.entity';
import { generateId } from 'src/common/service/helper.service';
import { VehiclesSeed } from './vehicles.seed';
import { Vehicle } from 'src/modules/tenancy/vehicles/vehicle.entity';

export async function tenantDatabaseSeeder(
	tenantId: string,
	connection: Connection,
) {
	const connectionManager = getConnectionManager();

	let centralConnection;
	const config = configuration.default();
	if (connectionManager.has('default')) {
		centralConnection = connectionManager.get('default');
		centralConnection = await Promise.resolve(
			centralConnection.isConnected
				? centralConnection
				: centralConnection.connect(),
		);
	} else {
		centralConnection = await getConnectionManager()
			.create(config.database.central as MysqlConnectionOptions)
			.connect();
	}

	// staff teams, users, vehicle groups, operations seed
	const [staffTeams, users, vehicleGroups, operations] = await Promise.all([
		StaffTeamsSeed(),
		UsersSeed(tenantId),
		VehicleGroupsSeed(),
		OperationsSeed(),
	]);
	await Promise.all([
		connection.getRepository(StaffTeam).save(staffTeams),
		centralConnection.getRepository(User).save(users),
		connection.getRepository(VehicleGroup).save(vehicleGroups),
		connection.getRepository(Operation).save(operations),
	]);

	// staffs, vehicles and customers seed
	const [staffs, customers, vehicles] = await Promise.all([
		StaffsSeed(staffTeams, users),
		CustomersSeed(),
		VehiclesSeed(vehicleGroups),
	]);
	await Promise.all([
		connection.getRepository(Staff).save(staffs),
		connection.getRepository(Customer).save(customers),
		connection.getRepository(Vehicle).save(vehicles),
	]);

	// sales incharges seed
	await connection.getRepository(SalesIncharge).save({
		id: generateId(),
		staffId: staffs[0].id,
		customerId: customers[0].id,
	});
}

export async function customTenantDatabaseSeeder(
	tenantId: string,
	connection: Connection,
) {
	const connectionManager = getConnectionManager();

	let centralConnection;
	const config = configuration.default();
	if (connectionManager.has('default')) {
		centralConnection = connectionManager.get('default');
		centralConnection = await Promise.resolve(
			centralConnection.isConnected
				? centralConnection
				: centralConnection.connect(),
		);
	} else {
		centralConnection = await getConnectionManager()
			.create(config.database.central as MysqlConnectionOptions)
			.connect();
	}

	// staff teams, users, vehicle groups, operations seed
	const [staffTeams, users, vehicleGroups, operations] = await Promise.all([
		StaffTeamsSeed(),
		UsersSeed(tenantId),
		VehicleGroupsSeed(),
		OperationsSeed(),
	]);
	await Promise.all([
		connection.getRepository(StaffTeam).save(staffTeams),
		centralConnection.getRepository(User).save(users),
		connection.getRepository(VehicleGroup).save(vehicleGroups),
		connection.getRepository(Operation).save(operations),
	]);

	// staffs, vehicles and customers seed
	const [staffs, customers, vehicles] = await Promise.all([
		StaffsSeed(staffTeams, users),
		CustomersSeed(),
		VehiclesSeed(vehicleGroups),
	]);
	await Promise.all([
		connection.getRepository(Staff).save(staffs),
		connection.getRepository(Customer).save(customers),
		connection.getRepository(Vehicle).save(vehicles),
	]);

	// sales incharges seed
	await connection.getRepository(SalesIncharge).save({
		id: generateId(),
		staffId: staffs[0].id,
		customerId: customers[0].id,
	});
}
