import { generateId } from 'src/common/service/helper.service';

export async function VehicleGroupsSeed() {
	return [
		{
			id: generateId(),
			name: 'グループ1',
			colorCode: 'color3',
			orderNo: 1,
		},
	];
}
