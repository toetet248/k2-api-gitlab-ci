import { generateEmail, generateId } from 'src/common/service/helper.service';

export async function UsersSeed(tenantId: string) {
	return [
		{
			id: generateId(),
			name: 'スタッフ一01',
			email: generateEmail(),
			role: 'staff',
			orderNo: 1,
			tenantId,
		},
	];
}
