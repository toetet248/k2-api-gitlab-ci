import { generateId } from 'src/common/service/helper.service';

export async function CustomersSeed() {
	return [
		{
			id: generateId(),
			name: '得意先担01',
			orderNo: 1,
		},
	];
}
