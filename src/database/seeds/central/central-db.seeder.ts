import { Plan } from 'src/modules/central/plans/plan.entity';
import { User } from 'src/modules/central/users/user.entity';
import { Connection } from 'typeorm';
import { PlansSeed } from './plans.seed';
import { UsersSeed } from './users.seed';

export async function centralDatabaseSeeder(connection: Connection) {
	// await connection.getRepository(User).save(await UsersSeed());
	await connection.getRepository(Plan).save(await PlansSeed());
}
