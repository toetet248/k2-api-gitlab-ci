import { generateCode, generateId } from 'src/common/service/helper.service';
import * as bcrypt from 'bcrypt';

export async function UsersSeed() {
	const salt = await bcrypt.genSalt();
	return [
		{
			id: generateId(),
			name: 'AAA',
			email: 'aaa@gmail.com',
			password: await bcrypt.hash('password', salt),
			code: generateCode(),
		},
	];
}
