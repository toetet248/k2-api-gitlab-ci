import { generateId } from 'src/common/service/helper.service';

export async function PlansSeed() {
	return [
		{
			id: generateId(),
			name: 'free',
			nameFurigana: 'フリー',
			price: 0,
			options: {
				safeFile: false,
				staffLimit: 5,
				vehicleLimit: 5,
				siteImageslimit: 0,
				siteFileslimit: 0,
			},
		},
		// {
		// 	id: generateId(),
		// 	name: 'light',
		// 	nameFurigana: 'ライト',
		// 	price: 4950,
		// 	options: {
		// 		safeFile: false,
		// 		staffLimit: null,
		// 		vehicleLimit: null,
		// 		siteImageslimit: 30,
		// 		siteFileslimit: 30,
		// 	},
		// },
		{
			id: generateId(),
			name: 'standard',
			nameFurigana: 'スタンダード',
			price: 8980,
			options: {
				safeFile: true,
				staffLimit: null,
				vehicleLimit: null,
				siteImageslimit: null,
				siteFileslimit: null,
			},
		},
	];
}
