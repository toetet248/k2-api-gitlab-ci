import { faker } from '@faker-js/faker';
import { map } from 'modern-async';
import moment from 'moment';
import { generateId } from 'src/common/service/helper.service';
import { Site } from 'src/modules/tenancy/sites/site.entity';
import { Connection } from 'typeorm';

export async function DailyNotesFaker(
	tenantConnection: Connection,
	count: Number,
	betweenDates: string,
	operationInstructions: any[],
) {
	const data = [];

	const [start, end] = betweenDates.split('_');

	const startDate = moment(start).toISOString();
	const endDate = moment(end).toISOString();

	const siteIds = [];

	for (let i = 0; i < operationInstructions.length; i++) {
		const operationInstruction = operationInstructions[i];

		if (!siteIds.includes(operationInstruction.siteId))
			siteIds.push(operationInstruction.siteId);

		const dates = [];

		for (let index = 0; index < count; index++) {
			let date = moment(faker.date.between(startDate, endDate)).format(
				'YYYY-MM-DD',
			);
			while (dates.includes(date)) {
				date = moment(faker.date.between(startDate, endDate)).format(
					'YYYY-MM-DD',
				);
			}
			data.push({
				id: generateId(),
				siteId: operationInstruction.siteId,
				operationId: operationInstruction.operationId,
				operationInstructionId: operationInstruction.id,
				scheduleDate: date,
			});
			dates.push(date);
		}
	}
	console.log('generate daily notes');
	const siteUpdatePromises = [];
	siteIds.map(function (siteId) {
		const dates = [];

		data.map((s) => {
			if (s.siteId == siteId) dates.push(moment(s.scheduleDate));
		});

		const minDate = moment.min(dates).format('YYYY-MM-DD');
		const maxDate = moment.max(dates).format('YYYY-MM-DD');

		siteUpdatePromises.push(
			tenantConnection
				.createQueryBuilder()
				.update(Site)
				.set({ startDate: minDate, endDate: maxDate })
				.where('id = :id', { id: siteId })
				.execute(),
		);
	});
	console.log('before update sites');

	await Promise.all(siteUpdatePromises);
	console.log('after update sites');

	return data;
}
