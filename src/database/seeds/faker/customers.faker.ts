import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';

export async function CustomersFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		const name = faker.name.firstName();
		data.push({
			id: generateId(),
			name: name,
			orderNo: i + 2,
		});
	}

	return data;
}
