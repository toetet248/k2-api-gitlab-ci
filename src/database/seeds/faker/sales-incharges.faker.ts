import { faker } from '@faker-js/faker';
import moment from 'moment';
import { generateId, generateNumber } from 'src/common/service/helper.service';
// import { UserTenant } from 'src/modules/users-tenants/user-tenant.entity';
import { User } from 'src/modules/central/users/user.entity';
import { Connection } from 'typeorm';
import { UsersTenantsFaker } from './users-tenants.faker';
import { UsersFaker } from './users.faker';

export async function SalesInchargesFaker(customers: any[], staffs: any[]) {
	const data = [];
	for (let i = 0; i < customers.length; i++) {
		data.push({
			id: generateId(),
			customerId: customers[i].id,
			staffId: staffs[Math.floor(Math.random() * staffs.length)].id,
			orderNo: i + 2,
		});
	}

	return data;
}
