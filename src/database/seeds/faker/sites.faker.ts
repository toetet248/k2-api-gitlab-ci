import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';

export async function SitesFaker(count: Number, salesIncharges: any[]) {
	const data = [];
	for (let i = 0; i < count; i++) {
		const salesIncharge =
			salesIncharges[Math.floor(Math.random() * salesIncharges.length)];
		data.push({
			id: generateId(),
			name: faker.commerce.department(),
			customerId: salesIncharge.customerId,
			staffId: salesIncharge.staffId,
			orderNo: i + 1,
		});
	}

	return data;
}
