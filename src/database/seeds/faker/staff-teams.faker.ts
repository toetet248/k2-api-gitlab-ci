import { faker } from '@faker-js/faker';
import moment from 'moment';
import { generateId, generateNumber } from 'src/common/service/helper.service';
import { colorCodes } from './color-code';

export async function StaffTeamsFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			name: faker.company.bsNoun() + i,
			colorCode:
				colorCodes[Math.floor(Math.random() * colorCodes.length)],
			orderNo: i + 2,
		});
	}

	return data;
}
