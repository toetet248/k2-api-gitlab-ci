import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';
// import { UserTenant } from 'src/modules/users-tenants/user-tenant.entity';
import { User } from 'src/modules/central/users/user.entity';
import { Connection } from 'typeorm';
import { UsersFaker } from './users.faker';

export async function StaffsFaker(
	centralConnection: Connection,
	tenant: any,
	count: Number,
	staffTeams: any[],
	users: any[],
) {
	const data = [];
	const emails = [];

	for (let i = 0; i < count; i++) {
		let usersFaker = await UsersFaker(1);
		while (emails.includes(usersFaker[0].email))
			usersFaker = await UsersFaker(1);

		emails.push(usersFaker[0].email);

		await centralConnection.getRepository(User).save(usersFaker);

		// const usersTenantsFaker = await UsersTenantsFaker(
		// 	usersFaker,
		// 	[tenant],
		// 	false,
		// );
		// await centralConnection
		// 	.getRepository(UserTenant)
		// 	.save(usersTenantsFaker);

		const userName = faker.name.firstName();
		data.push({
			id: generateId(),
			staffTeamId:
				staffTeams[Math.floor(Math.random() * staffTeams.length)].id,
			userId: usersFaker[0].id,
			name: userName,
			email: usersFaker[0].email,
			nameOnTab: userName,
			phone: faker.phone.number('501-###-###'),
			status: 'in_work',
			isSiteStaff: i % 2 == 0 ? true : false,
			isSiteAdmin: i % 2 == 0 ? false : true,
			isSystemAdmin: Math.random() % 2 == 0 ? true : false,
			orderNo: i + 2,
		});
	}

	return data;
}
