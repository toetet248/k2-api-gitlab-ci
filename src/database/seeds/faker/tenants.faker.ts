import { faker } from '@faker-js/faker';
import moment from 'moment';
import { generateId, generateNumber } from 'src/common/service/helper.service';

export async function TenantsFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			name:
				faker.name.firstName().toLowerCase() +
				moment().format('YmdHis') +
				generateNumber(3),
		});
	}

	return data;
}
