import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';

export async function UsersFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			email: faker.internet.email().toLowerCase(),
		});
	}

	return data;
}
