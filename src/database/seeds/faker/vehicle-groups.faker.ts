import { faker } from '@faker-js/faker';

import { generateId } from 'src/common/service/helper.service';
import { colorCodes } from './color-code';

export async function VehicleGroupsFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			name: faker.company.bsNoun() + i,
			colorCode:
				colorCodes[Math.floor(Math.random() * colorCodes.length)],
			orderNo: i + 2,
		});
	}

	return data;
}
