import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';

export async function VehiclesFaker(count: Number, vehicleGroups: any[]) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			vehicleGroupId:
				vehicleGroups[Math.floor(Math.random() * vehicleGroups.length)]
					.id,
			nameOnTab: faker.vehicle.type(),
			licenseEndDate: null,
			insuranceEndDate: null,
			status: 'in_work',
			orderNo: i + 1,
		});
	}

	return data;
}
