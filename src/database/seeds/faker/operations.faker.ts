import { faker } from '@faker-js/faker';
import { generateId } from 'src/common/service/helper.service';
import { colorCodes } from './color-code';

export async function OperationsFaker(count: Number) {
	const data = [];
	for (let i = 0; i < count; i++) {
		data.push({
			id: generateId(),
			name: faker.commerce.productAdjective() + i,
			colorCode:
				colorCodes[Math.floor(Math.random() * colorCodes.length)],
			orderNo: i + 2,
		});
	}

	return data;
}
