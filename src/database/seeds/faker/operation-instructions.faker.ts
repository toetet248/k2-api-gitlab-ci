import { generateId } from 'src/common/service/helper.service';

export async function OperationInstructionsFaker(
	sites: any[],
	operations: any[],
) {
	const data = [];
	for (let i = 0; i < sites.length; i++) {
		const randomOperationsNo = Math.floor(
			Math.random() * operations.length,
		);
		const operationIds = [];
		for (let index = 0; index < randomOperationsNo; index++) {
			let operationId =
				operations[Math.floor(Math.random() * operations.length)].id;

			while (operationIds.includes(operationId))
				operationId =
					operations[Math.floor(Math.random() * operations.length)]
						.id;

			operationIds.push(operationId);

			data.push({
				id: generateId(),
				siteId: sites[i].id,
				operationId: operationId,
				orderNo: index + 1,
			});
		}
	}

	return data;
}
