import { User } from 'src/modules/central/users/user.entity';
import { getConnectionManager, getManager } from 'typeorm';
import { faker } from '@faker-js/faker';
import * as configuration from '../../../config/config';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { UsersFaker } from './users.faker';
import { TenantsFaker } from './tenants.faker';
import { Tenant } from 'src/modules/central/tenants/tenant.entity';
// import { UsersTenantsFaker } from './users-tenants.faker';
// import { UserTenant } from 'src/modules/users-tenants/user-tenant.entity';
import { map } from 'modern-async';
import { getTenantConnection } from 'src/modules/tenancy/tenancy.utils';
import { tenantDatabaseSeeder } from '../tenant/tenant-db.seeder';
import { Staff } from 'src/modules/tenancy/staffs/staff.entity';
import { generateId } from 'src/common/service/helper.service';
import { Company } from 'src/modules/tenancy/my-company/company.entity';
import { StaffTeam } from 'src/modules/tenancy/staff-teams/staff-team.entity';
import { StaffTeamsFaker } from './staff-teams.faker';
import { StaffsFaker } from './staffs.faker';
import { VehicleGroupsFaker } from './vehicle-groups.faker';
import { VehicleGroup } from 'src/modules/tenancy/vehicle-groups/vehicle-group.entity';
import { VehiclesFaker } from './vehicles.faker';
import { Vehicle } from 'src/modules/tenancy/vehicles/vehicle.entity';
import { CustomersFaker } from './customers.faker';
import { Customer } from 'src/modules/tenancy/customers/customer.entity';
import { SalesInchargesFaker } from './sales-incharges.faker';
import { SalesIncharge } from 'src/modules/tenancy/sales-incharges/sales-incharge.entity';
import { SitesFaker } from './sites.faker';
import { Site } from 'src/modules/tenancy/sites/site.entity';
import { OperationInstructionsFaker } from './operation-instructions.faker';
import { OperationInstruction } from 'src/modules/tenancy/operation-instructions/operation-instruction.entity';
import { OperationsFaker } from './operations.faker';
import { Operation } from 'src/modules/tenancy/operations/operation.entity';
import { DailyNotesFaker } from './daily-notes.faker';
import { DailyNote } from 'src/modules/tenancy/daily-notes/daily-note.entity';
import { ScheduleStaffsFaker } from './schedule-staffs.faker';
import { ScheduleStaff } from 'src/modules/tenancy/schedule-staffs/schedule-staff.entity';
import { ScheduleVehicle } from 'src/modules/tenancy/schedule-vehicles/schedule-vehicle.entity';
import { ScheduleVehilcesFaker } from './schedule-vehicles.faker';

export async function runTestFakerCli() {
	const testConfig = {
		tenants: {
			dataPerTenant: {
				staffTeams: 80,
				staffs: 500,
				vehicleGroups: 80,
				vehicles: 500,
				customers: 500,
				operations: 30,
				sites: 30,
				// daily notes per operations instructions of site
				// must be maximum of "dailyNotesBetween" total dates counts
				dailyNotesPerSite: 10,
				dailyNotesBetween: '2022-08-01_2022-09-30',
				scheduleStaffsPerDailyNote: 30,
				scheduleVehiclesPerDailyNote: 30,
			},
			count: 1,
		},
	};

	const config = configuration.default();

	const centralDBConfig = config.database.central;

	const connectionManager = getConnectionManager();

	const connection = await connectionManager
		.create(centralDBConfig as MysqlConnectionOptions)
		.connect();

	const usersFaker = await UsersFaker(testConfig.tenants.count);
	await connection.getRepository(User).save(usersFaker);

	const tenantsFaker = await TenantsFaker(testConfig.tenants.count);
	await connection.getRepository(Tenant).save(tenantsFaker);

	// const usersTenantsFaker = await UsersTenantsFaker(
	// 	usersFaker,
	// 	tenantsFaker,
	// 	true,
	// );
	// await connection.getRepository(UserTenant).save(usersTenantsFaker);

	const tenantDbPrefix = config.database.tenantDbPrefix;

	await map(tenantsFaker, async function (tenant, index) {
		await getManager().query(
			`CREATE SCHEMA ${
				tenantDbPrefix + tenant.name
			} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`,
		);

		const tenantConnection = await getTenantConnection(
			`${tenantDbPrefix + tenant.name}`,
		);

		await tenantConnection.runMigrations();

		// insert login in created tenant
		const tenantQuery = tenantConnection.createQueryBuilder();

		await tenantDatabaseSeeder(tenant.id, tenantConnection);

		const user = usersFaker[index];
		const userName = faker.name.firstName();

		await tenantQuery
			.insert()
			.into(Staff)
			.values([
				{
					id: generateId(),
					userId: user.id,
					name: userName,
					nameOnTab: userName,
					phone: faker.phone.number(),
					email: user.email,
					isOwner: true,
				},
			])
			.execute();

		// insert company in created tenant
		await tenantConnection
			.createQueryBuilder()
			.insert()
			.into(Company)
			.values([
				{
					id: generateId(),
					name: faker.name.lastName(),
					phone: faker.phone.number(),
				},
			])
			.execute();

		const dataPerTenant = testConfig.tenants.dataPerTenant;

		const staffTeamsFaker = await StaffTeamsFaker(dataPerTenant.staffTeams);
		await tenantConnection.getRepository(StaffTeam).save(staffTeamsFaker);
		console.log('staffTeamsFaker');

		const staffsFaker = await StaffsFaker(
			connection,
			tenant,
			dataPerTenant.staffs,
			staffTeamsFaker,
			usersFaker,
		);
		await tenantConnection.getRepository(Staff).save(staffsFaker);
		console.log('staffsFaker');

		const vehicleGroupsFaker = await VehicleGroupsFaker(
			dataPerTenant.vehicleGroups,
		);
		await tenantConnection
			.getRepository(VehicleGroup)
			.save(vehicleGroupsFaker);
		console.log('vehicleGroupsFaker');

		const vehiclesFaker = await VehiclesFaker(
			dataPerTenant.vehicles,
			vehicleGroupsFaker,
		);
		await tenantConnection.getRepository(Vehicle).save(vehiclesFaker);
		console.log('vehiclesFaker');

		const customersFaker = await CustomersFaker(dataPerTenant.customers);
		await tenantConnection.getRepository(Customer).save(customersFaker);
		console.log('customersFaker');

		const operationsFaker = await OperationsFaker(dataPerTenant.operations);
		await tenantConnection.getRepository(Operation).save(operationsFaker);
		console.log('operationsFaker');

		const salesInchargesFaker = await SalesInchargesFaker(
			customersFaker,
			staffsFaker,
		);
		await tenantConnection
			.getRepository(SalesIncharge)
			.save(salesInchargesFaker);
		console.log('salesInchargesFaker');

		const sitesFaker = await SitesFaker(
			dataPerTenant.sites,
			salesInchargesFaker,
		);
		await tenantConnection.getRepository(Site).save(sitesFaker);
		console.log('sitesFaker');

		const operationInstructionsFaker = await OperationInstructionsFaker(
			sitesFaker,
			operationsFaker,
		);
		await tenantConnection
			.getRepository(OperationInstruction)
			.save(operationInstructionsFaker);
		console.log('operationInstructionsFaker');

		const dailyNotesFaker = await DailyNotesFaker(
			tenantConnection,
			dataPerTenant.dailyNotesPerSite,
			dataPerTenant.dailyNotesBetween,
			operationInstructionsFaker,
		);
		await tenantConnection.getRepository(DailyNote).save(dailyNotesFaker);
		console.log('dailyNotesFaker');

		console.log(
			`===> tenantName: ${tenant.name}, email: ${user.email} <===`,
		);

		//Schedule Staffs
		const scheduleStaffsFaker = await ScheduleStaffsFaker(
			dataPerTenant.scheduleStaffsPerDailyNote,
			staffsFaker,
			dailyNotesFaker,
		);
		await tenantConnection
			.getRepository(ScheduleStaff)
			.save(scheduleStaffsFaker);
		console.log('scheduleStaffsFaker');

		//Schedule Vehicles
		const scheduleVehilcesFaker = await ScheduleVehilcesFaker(
			dataPerTenant.scheduleVehiclesPerDailyNote,
			vehiclesFaker,
			dailyNotesFaker,
		);
		await tenantConnection
			.getRepository(ScheduleVehicle)
			.save(scheduleVehilcesFaker);
		console.log('scheduleVehilcesFaker');

		await tenantConnection.close();
	});

	// const connection = await getTenantConnection(
	// 	`${dbPrefix + tenant.name}`,
	// );

	// await connection.getRepository(Plan).save(await PlansSeed());

	await connection.close();

	process.exit(1);
}

runTestFakerCli();
