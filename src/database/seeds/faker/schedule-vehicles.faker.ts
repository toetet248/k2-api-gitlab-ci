import { generateId } from 'src/common/service/helper.service';

export async function ScheduleVehilcesFaker(
	count: number,
	vehicles: any[],
	dailyNotes: any[]) {

		const data = [];
		const dayNightSupport = ['day','support'];
		
		for (let i = 0; i < dailyNotes.length; i++) {
			const scheduleVehicleIds = [];
			const randomVehicleCount = Math.floor(
				Math.random() * count,
			);
	
			for (let index = 0; index < randomVehicleCount; index++) {
				let scheduleVehicleId =
					vehicles[Math.floor(Math.random() * vehicles.length)].id;
	
					scheduleVehicleIds.push(scheduleVehicleId);
	
				data.push({
					id: generateId(),
					vehicleId: scheduleVehicleId,
					dailyNoteId: dailyNotes[i].id,
					dayNightSupport: dayNightSupport[Math.floor(Math.random() * dayNightSupport.length)],
					nightSupportCount: index,
					orderNo: index + 1,
				});
			}
		}

	return data;
}
