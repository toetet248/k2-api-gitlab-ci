import { generateId } from 'src/common/service/helper.service';

export async function ScheduleStaffsFaker(
	count: number,
	staffs: any[],
	dailyNotes: any[]) {

	const data = [];
	const dayNightSupport = ['day','support'];
	
	for (let i = 0; i < dailyNotes.length; i++) {
		const scheduleStaffIds = [];
		const randomStaffCount = Math.floor(
			Math.random() * count,
		);
		// console.log('randomStaffCount',randomStaffCount);
		for (let index = 0; index < randomStaffCount; index++) {
			let scheduleStaffId =
				staffs[Math.floor(Math.random() * staffs.length)].id;

			scheduleStaffIds.push(scheduleStaffId);

			data.push({
				id: generateId(),
				staffId: scheduleStaffId,
				dailyNoteId: dailyNotes[i].id,
				dayNightSupport: dayNightSupport[Math.floor(Math.random() * dayNightSupport.length)],
				nightSupportCount: index,
				orderNo: index + 1,
			});
		}
	}

	return data;
}
