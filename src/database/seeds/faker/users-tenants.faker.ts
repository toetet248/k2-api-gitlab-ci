import { faker } from '@faker-js/faker';
import { generateId, todayDatetime } from 'src/common/service/helper.service';
import * as bcrypt from 'bcrypt';

export async function UsersTenantsFaker(
	users: any[],
	tenants: any[],
	isOwner: boolean,
) {
	const data = [];
	const salt = await bcrypt.genSalt();
	const hashPassword = await bcrypt.hash('password', salt);

	for (let i = 0; i < users.length; i++) {
		data.push({
			id: generateId(),
			userId: users[i].id,
			tenantId: tenants[i].id,
			role: isOwner ? 'owner' : 'staff',
			password: hashPassword,
			resetPasswordAt: todayDatetime(),
		});
	}

	return data;
}
