import { UnauthorizedException } from '@nestjs/common';
import { messages } from './constants';

export class UnauthorizedResetPasswordException extends UnauthorizedException {
	constructor() {
		super(messages.unauthorizedResetPasswordMessage);
	}
}
