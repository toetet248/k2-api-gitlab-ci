import { UnauthorizedException } from '@nestjs/common';
import { messages } from './constants';

export class UnauthorizedNoneLoginException extends UnauthorizedException {
	constructor() {
		super(messages.unauthorizedNoneLoginMessage);
	}
}
