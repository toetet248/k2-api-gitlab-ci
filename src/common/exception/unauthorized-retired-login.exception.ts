import { UnauthorizedException } from '@nestjs/common';
import { messages } from './constants';

export class UnauthorizedRetiredLoginException extends UnauthorizedException {
	constructor() {
		super(messages.unauthorizedRetiredLoginMessage);
	}
}
