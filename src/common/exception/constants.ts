export const messages = {
	unauthorizedRetiredLoginMessage: `unauthorized-retired`,
	unauthorizedResetPasswordMessage: `unauthorized-reset-password-required`,
	unauthorizedNoneLoginMessage: `unauthorized-none-login`,
};
