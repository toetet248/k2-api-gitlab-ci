export interface ResponseInterface {

	data?:Object | Object[];

	message?:{
		title:string,
		body:string
	};

	token?:{
		access_token:string,
		// refresh_token:string
	};

	// response();
}