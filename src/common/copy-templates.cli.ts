import { copySync, existSync } from 'fs-extra';
import { join } from 'path';

async function runCopyTemplates() {
	await copySync(
		join(__dirname, '../../src/mail-templates'),
		join(__dirname, '../mail-templates'),
	);
	process.exit(1);
}

runCopyTemplates();
