import {
	Injectable,
	NestInterceptor,
	ExecutionContext,
	CallHandler,
	Inject,
} from '@nestjs/common';
import { map } from 'rxjs/operators';
import { createCipheriv, createDecipheriv, randomBytes } from 'crypto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class ApiResponseInterceptor implements NestInterceptor {
	// constructor(
	// 	private jwtService: JwtService,
	// 	@Inject(UserService) private userService: UserService,
	// ) {}
	async intercept(context: ExecutionContext, next: CallHandler) {
		// const request = context.switchToHttp().getRequest();
		// console.log('......REQUEST....');
		// console.log(request.body);
		// if (request.headers.authorization) {
		// 	const token = request.headers.authorization.replace('Bearer ', '');
		// 	const payload = this.jwtService.decode(token);
		// 	request.user = payload;
		// }
		// decrypt data
		// const algorithm = 'aes-256-cbc';
		// const initVector = "c3bcd8e87d3e5bed";
		// const securityKey = "07814961d28a4471a6c844fccfefaedb";
		// const request = context.switchToHttp().getRequest();

		// const decipher = createDecipheriv(algorithm, securityKey, initVector);

		// let decryptedData = decipher.update(JSON.stringify(request.body.body), "hex", "utf-8");

		// decryptedData += decipher.final("utf8");

		return next.handle().pipe(
			map(function (response) {
				// encrypt data
				// const d = JSON.stringify(data);

				// const cipher = createCipheriv(algorithm, securityKey, initVector);

				// var encryptedData = cipher.update(d, "utf-8", "hex");

				// encryptedData += cipher.final("hex");

				// return {
				// 	data: encryptedData,
				// };
				// console.log(response)

				// if test response
				if (response?.test) return { test: response.test };

				// if actual response
				// add header
				if (response?.token) {
					const res = context.switchToHttp().getResponse();
					// const { access_token, refresh_token } = response.token;
					// res.header('access_token', access_token);
					res.header('access_token', response.token.access_token);
					res.header('refresh_token', response.token.refresh_token);
					res.header(
						'access_token_expire',
						response.token.access_token_expire,
					);
				}
				console.log('......api response interceptor');
				return {
					data: response?.data ?? {},
					message: response?.message ?? {
						title: 'success',
						body: '',
					},
					token: response?.token ?? {},
					meta: response?.meta ?? {},
				};
			}),
		);
	}
}
