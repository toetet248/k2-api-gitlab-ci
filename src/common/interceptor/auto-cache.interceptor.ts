import {
	CACHE_KEY_METADATA,
	CacheInterceptor,
	ExecutionContext,
	Injectable,
	CallHandler,
	CACHE_TTL_METADATA,
} from '@nestjs/common';
import { isFunction, isNil } from 'lodash';
import { Observable, of, tap } from 'rxjs';

@Injectable()
export class AutoCacheInterceptor extends CacheInterceptor {
	protected cachedRoutes = new Map();
	async intercept(
		context: ExecutionContext,
		next: CallHandler,
	): Promise<Observable<any>> {
		const key = this.trackBy(context);
		const ttlValueOrFactory =
			this.reflector.get(CACHE_TTL_METADATA, context.getHandler()) ??
			null;

		if (!key) {
			return next.handle();
		}
		try {
			const value = await this.cacheManager.get(key);
			console.log(ttlValueOrFactory, '...value');
			if (!isNil(value)) {
				return of(value);
			}

			const ttl = isFunction(ttlValueOrFactory)
				? await ttlValueOrFactory(context)
				: ttlValueOrFactory;

			return next.handle().pipe(
				tap((response) => {
					const args = isNil(ttl)
						? [key, response]
						: [key, response, { ttl }];
					console.log('...response');
					this.cacheManager.set(...args);
				}),
			);
		} catch {
			console.log('....ERROR...');
			return next.handle();
		}
	}

	trackBy(context: ExecutionContext): string | undefined {
		const request = context.switchToHttp().getRequest();
		// later we can get the type of request (query/mutation) and if query get its field name, and attributes and cache accordingly. Otherwise, clear the cache in case of the request type is mutation.
		if (!request) {
			return undefined;
		}
		const { httpAdapter } = this.httpAdapterHost;
		const isHttpApp = httpAdapter && !!httpAdapter.getRequestMethod;
		const cacheMetadata = this.reflector.get(
			CACHE_KEY_METADATA,
			context.getHandler(),
		);
		console.log(cacheMetadata, '...cacheMetadata');

		if (!isHttpApp || cacheMetadata) {
			return cacheMetadata;
		}
		const isGetRequest = httpAdapter.getRequestMethod(request) === 'GET';
		if (!isGetRequest) {
			setTimeout(async () => {
				for (const values of this.cachedRoutes.values()) {
					for (const value of values) {
						console.log(value, '...cache value');
						// you don't need to worry about the cache manager as you are extending their interceptor which is using caching manager as you've seen earlier.
						await this.cacheManager.del(value);
					}
				}
			}, 0);
			return undefined;
		}
		// to always get the base url of the incoming get request url.
		// set cached routes values
		const key = httpAdapter.getRequestUrl(request).split('?')[0];
		if (
			this.cachedRoutes.has(key) &&
			!this.cachedRoutes
				.get(key)
				.includes(httpAdapter.getRequestUrl(request))
		) {
			this.cachedRoutes.set(key, [
				...this.cachedRoutes.get(key),
				httpAdapter.getRequestUrl(request),
			]);
			return httpAdapter.getRequestUrl(request);
		}
		this.cachedRoutes.set(key, [httpAdapter.getRequestUrl(request)]);

		return httpAdapter.getRequestUrl(request);
	}
}
