import { Injectable, NestInterceptor, ExecutionContext, CallHandler, BadRequestException } from '@nestjs/common';
import { catchError } from 'rxjs/operators';
import { QueryFailedError } from 'typeorm';
import { createCipheriv, createDecipheriv, randomBytes } from 'crypto';

@Injectable()
export class ErrorHandlingInterceptor implements NestInterceptor {
	
	async intercept(context: ExecutionContext, next: CallHandler) {

	    return next.handle().pipe(
	    	catchError(error => {
		        if (error instanceof QueryFailedError) {
		        	if (error.message.includes("email") && error.message.includes("Duplicate entry")) {
			          	throw new BadRequestException(['Account with this email already exists.']);
		          	}
		        }
		        throw error;
	      	})
		);
		
	}	
}