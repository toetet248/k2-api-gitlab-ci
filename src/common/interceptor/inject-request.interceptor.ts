import {
	CallHandler,
	ExecutionContext,
	Injectable,
	NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';

export const REQUEST_CONTEXT = '_requestContext';

@Injectable()
export class InjectRequestInterceptor implements NestInterceptor {
	constructor(private type?: NonNullable<'query' | 'body' | 'params'>) {}

	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		const request = context.switchToHttp().getRequest();

		const { query, params, tenantName, headers } = request;
		if (this.type && request[this.type]) {
			const passContextValues = {
				query,
				params,
				tenantName,
				headers,
			};
			const requestType = request[this.type];

			Object.keys(requestType).map(function (key, index) {
				if (Array.isArray(requestType[key])) {
					requestType[key] = requestType[key].map((property) => {
						if (typeof property == 'object') {
							return {
								...property,
								[REQUEST_CONTEXT]: passContextValues,
							};
						} else {
							return property;
						}
					});
				}
			});
			request[this.type][REQUEST_CONTEXT] = passContextValues;
		}

		return next.handle();
	}
}
