import {
	NestInterceptor,
	ExecutionContext,
	CallHandler,
	UseInterceptors,
	ConsoleLogger,
} from '@nestjs/common';
import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';

interface ClassConstructor {
	new (...args: any[]): any;
}

export function Serialize(dto: ClassConstructor | ClassConstructor[]) {
	return UseInterceptors(new SerializeInterceptor(dto));
}

export class SerializeInterceptor implements NestInterceptor {
	constructor(private dto: any) {}

	async intercept(context: ExecutionContext, next: CallHandler) {
		const self = this;

		return next.handle().pipe(
			map(function (response) {
				const data = plainToClass(self.dto, response.data, {
					excludeExtraneousValues: true,
				});
				return {
					...response,
					data,
				};
			}),
		);
	}
}
