import { applyDecorators, UseInterceptors, UsePipes } from '@nestjs/common';
import { InjectRequestInterceptor } from '../interceptor/inject-request.interceptor';
import { StripRequestContextPipe } from '../pipe/strip-request-context.pipe';

// export function InjectRequestToQuery() {
// 	return applyDecorators(InjectRequestTo('query'));
// }

export function InjectRequestToBody() {
	return applyDecorators(InjectRequestTo('body'));
}

// export function InjectRequestToParam() {
// 	return applyDecorators(InjectRequestTo('params'));
// }

export function InjectRequestTo(context: 'query' | 'body' | 'params') {
	return applyDecorators(
		UseInterceptors(new InjectRequestInterceptor(context)),
		UsePipes(StripRequestContextPipe),
	);
}
