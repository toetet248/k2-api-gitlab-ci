import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FileService } from './service/file.service';
import { GmoService } from './service/gmo.service';
import { MailService } from './service/mail.service';
import { SmsService } from './service/sms.service';

@Module({
	imports: [
		HttpModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (config: ConfigService) => ({
				// timeout: 60.0,
				baseURL: config.get('gmo.baseUrl'),
			}),
			inject: [ConfigService],
		}),
	],
	providers: [FileService, MailService, GmoService, SmsService],
	exports: [FileService, MailService, GmoService, SmsService],
})
export class CommonModule {}
