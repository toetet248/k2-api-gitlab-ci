export class BaseController {
	public response(
		data?: Object | Object[],
		message?: {
			title?: string;
			body?: string;
		},
		token?: {
			accessToken?: string;
			refreshToken?: string;
			accessTokenExpire?: string;
		},
	): Object {
		return { data, message, token };
	}

	public testResponse(test?: any): Object {
		return { test };
	}

	public fileResponse(file?: any): Object {
		return { file };
	}

	public paginateResponse(data?: Object | Object[], meta?: Object) {
		return { data, meta };
	}
}
