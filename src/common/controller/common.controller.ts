import { Controller } from '@nestjs/common';
import { FileService } from '../service/file.service';

@Controller('')
export class CommonController {
	constructor(private readonly service: FileService) {}
}
