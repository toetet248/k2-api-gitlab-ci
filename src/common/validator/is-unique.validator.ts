import { Injectable } from '@nestjs/common';
import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
	ValidatorConstraint,
	ValidatorConstraintInterface,
} from 'class-validator';
import {
	EntitySchema,
	getConnectionManager,
	getRepository,
	In,
	Repository,
} from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { REQUEST_CONTEXT } from '../interceptor/inject-request.interceptor';

@ValidatorConstraint({ name: 'Unique', async: false })
@Injectable()
export class UniqueConstraint implements ValidatorConstraintInterface {
	constructor(private configService: ConfigService) {}

	async validate(value: any, args?: ValidationArguments) {
		const [table, uniqueField = 'id', exceptField = null] =
			args.constraints;

		if (!value && exceptField && table) {
			return true;
		} else {
			if (!value || !table) return false;

			// start -- get central connection or tenant connection repo
			const validateArguments = args?.object[REQUEST_CONTEXT] ?? {};

			let {
				tenantName = null,
				params = null,
				query = null,
			} = validateArguments;

			let repo: Repository<EntitySchema>;

			if (tenantName) {
				const connectionManager = getConnectionManager();

				const tenantDbPrefix = this.configService.get(
					'database.tenantDbPrefix',
				);

				const tenantConnectionName = `${tenantDbPrefix + tenantName}`;

				repo = connectionManager
					.get(tenantConnectionName)
					.getRepository(table);
			} else {
				repo = getRepository(table);
			}
			// end --

			const record = await repo.findOne({
				where: {
					[uniqueField]: value,
				},
			});

			if (!record) return true;

			if (!exceptField) return false;

			const exceptFieldValue = (args.object as any)[exceptField];

			// check in params and query if exceptFieldValue does not exists
			if (!exceptFieldValue) {
				if (params[exceptField])
					return record[exceptField] === params[exceptField];

				if (query[exceptField])
					return record[exceptField] === query[exceptField];
			}

			if (!record[exceptField]) return false;

			// return true if exceptField value is equal to given exceptField value
			return record[exceptField] === exceptFieldValue;
		}
	}

	public defaultMessage(args: ValidationArguments) {
		return `${args.property} already exists`;
	}
}

export function IsUnique(
	table: string,
	uniqueField: string,
	exceptField: string = null,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [table, uniqueField, exceptField],
			validator: UniqueConstraint,
		});
	};
}
