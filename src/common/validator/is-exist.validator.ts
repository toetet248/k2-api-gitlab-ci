import { Injectable } from '@nestjs/common';
import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
	ValidatorConstraint,
	ValidatorConstraintInterface,
} from 'class-validator';
import {
	EntitySchema,
	getConnectionManager,
	getRepository,
	In,
	Repository,
} from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { REQUEST_CONTEXT } from '../interceptor/inject-request.interceptor';
import { isArray } from 'lodash';

@ValidatorConstraint({ name: 'Exist', async: false })
@Injectable()
export class ExistConstraint implements ValidatorConstraintInterface {
	constructor(private configService: ConfigService) {}

	async validate(value: string, args?: ValidationArguments) {
		const [table, property = 'id', exceptField = null] = args.constraints;

		if (!value && exceptField && table) {
			return true;
		} else {
			if (!value || !table) return false;

			// start -- get central connection or tenant connection repo
			const validateArguments = args?.object[REQUEST_CONTEXT] ?? {};

			let {
				tenantName = null,
				params = null,
				query = null,
			} = validateArguments;

			let repo: Repository<EntitySchema>;

			if (tenantName) {
				const connectionManager = getConnectionManager();
				const tenantDbPrefix = this.configService.get(
					'database.tenantDbPrefix',
				);

				const tenantConnectionName = `${tenantDbPrefix + tenantName}`;

				repo = connectionManager
					.get(tenantConnectionName)
					.getRepository(table);
			} else {
				repo = getRepository(table);
			}

			let record;
			if (Array.isArray(value)) {
				record = await repo.count({
					where: {
						[property]: In(value),
					},
				});

				return value.length == record;
			} else {
				record = await repo.count({
					where: {
						[property]: value,
					},
				});
				return record;
			}
		}
	}

	public defaultMessage(args: ValidationArguments) {
		return `${args.property} doesn't exists`;
	}
}

export function IsExist(
	table: string,
	uniqueField: string,
	exceptField: string = null,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [table, uniqueField, exceptField],
			validator: ExistConstraint,
		});
	};
}
