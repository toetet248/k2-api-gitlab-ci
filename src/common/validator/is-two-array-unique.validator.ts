import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsTwoArrayUnique', async: false })
export class TwoArrayUniqueConstraint implements ValidatorConstraintInterface {
	validate(value: any[], args?: ValidationArguments) {
		if (!value.length) {
			return true;
		}

		const difference = value.filter((x) =>
			(args.object as any)[args.constraints[0]].includes(x),
		);

		return difference.length > 0 ? false : true;
	}

	defaultMessage(args: ValidationArguments) {
		return `${args.property} must be unique.`;
	}
}

export function IsTwoArrayUnique(
	otherProperty: string,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [otherProperty],
			validator: TwoArrayUniqueConstraint,
		});
	};
}
