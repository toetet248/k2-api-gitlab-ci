import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'EmailConstraint', async: false })
export class EmailConstraint implements ValidatorConstraintInterface {
	validate(email: string, args: ValidationArguments) {
		const regEx =
			/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/;

		if (email.match(regEx) || email.length === 0) {
			return true;
		}
	}

	defaultMessage(args: ValidationArguments) {
		return `${args.property} does not have valid format.`;
	}
}

export function IsEmailValidate(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: EmailConstraint,
		});
	};
}
