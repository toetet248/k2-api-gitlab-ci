import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	registerDecorator,
	ValidationOptions,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsMatch', async: false })
export class IsMatchConstraint implements ValidatorConstraintInterface {
	validate(value: string, args: ValidationArguments) {
		return value === (args.object as any)[args.constraints[0]];
	}

	defaultMessage(args: ValidationArguments) {
		return `${args.property} do not match with ${args.constraints[0]}`;
	}
}

export function IsMatch(
	otherProperty: string,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [otherProperty],
			validator: IsMatchConstraint,
		});
	};
}
