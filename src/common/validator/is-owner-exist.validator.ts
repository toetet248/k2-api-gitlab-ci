import { Injectable } from '@nestjs/common';
import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
	ValidatorConstraint,
	ValidatorConstraintInterface,
} from 'class-validator';
import {
	Brackets,
	EntitySchema,
	getConnectionManager,
	getRepository,
	In,
	Repository,
} from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { REQUEST_CONTEXT } from '../interceptor/inject-request.interceptor';
import { isArray } from 'lodash';

@ValidatorConstraint({ name: 'IsOwnerExist', async: false })
@Injectable()
export class IsOwnerExistConstraint implements ValidatorConstraintInterface {
	constructor(private configService: ConfigService) {}

	async validate(value: string, args?: ValidationArguments) {
		if (!value) {
			return true;
		} else {
			// start -- get central connection
			const validateArguments = args?.object[REQUEST_CONTEXT] ?? {};

			let { tenantName } = validateArguments;

			const repo = getRepository('users');

			const recordCount = await repo
				.createQueryBuilder('users')
				.leftJoin('users.tenant', 'tenant')
				.where('tenant.name != :tenantName', { tenantName })
				.andWhere('tenant.isDatabaseExist = :isDatabaseExist', {
					isDatabaseExist: true,
				})
				.andWhere('users.role = :role', {
					role: 'owner',
				})
				.andWhere(
					new Brackets((q) => {
						q.where('users.email = :email', {
							email: value,
						}).orWhere('users.phone = :phone', {
							phone: value,
						});
					}),
				)
				.getCount();

			return recordCount === 0;
		}
	}

	public defaultMessage(args: ValidationArguments) {
		return `${args.property} has been already taken as an owner.`;
	}
}

export function IsOwnerExist(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [],
			validator: IsOwnerExistConstraint,
		});
	};
}
