import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsImage', async: false })
export class IsImage implements ValidatorConstraintInterface {
	private mime = ['image/jpg', 'image/png', 'image/jpeg'];

	validate(value: any, args: ValidationArguments) {
		if (value?.mimetype && (this.mime ?? []).includes(value?.mimetype))
			return true;

		return false;
	}

	defaultMessage(args: ValidationArguments) {
		return 'invalid image';
	}
}
