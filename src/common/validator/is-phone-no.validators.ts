import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'PhoneNoConstraint', async: false })
export class PhoneNoConstraint implements ValidatorConstraintInterface {
	validate(phone: string, args: ValidationArguments) {
		var regEx =
			/^[0][(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3,4}[-.]?[\s\.]?[0-9]{4,6}$/im;

		if (phone.match(regEx) || phone.length === 0) {
			return true;
		}
	}

	defaultMessage(args: ValidationArguments) {
		return `${args.property} does not have valid format.`;
	}
}

export function IsPhoneNo(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: PhoneNoConstraint,
		});
	};
}
