import { Injectable } from '@nestjs/common';
import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
	ValidatorConstraint,
	ValidatorConstraintInterface,
} from 'class-validator';
import { ConfigService } from '@nestjs/config';
import { atob } from 'buffer';

@ValidatorConstraint({ name: 'Base64Image', async: false })
@Injectable()
export class Base64PdfConstraint implements ValidatorConstraintInterface {
	constructor(private configService: ConfigService) {}

	async validate(value: string, args?: ValidationArguments) {
		let validTypes = ['data:application/pdf;base64'];

		const [type, base64] = value.split(',');

		// check extension type
		if (!validTypes.includes(type)) return false;

		try {
			atob(base64);
		} catch {
			return false;
		}

		return true;
	}

	public defaultMessage(args: ValidationArguments) {
		return `${args.property} does not have valid format.`;
	}
}

export function IsBase64Pdf(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: Base64PdfConstraint,
		});
	};
}
