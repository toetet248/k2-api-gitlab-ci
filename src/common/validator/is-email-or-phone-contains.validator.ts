import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsEmailOrPhoneContains', async: false })
export class ValidateIsEmailOrPhoneContainsConstraint
	implements ValidatorConstraintInterface
{
	validate(value: any, args: ValidationArguments): boolean {
		if (args.object['loginType'] === 'none') return true;

		if (value) {
			const regEx =
				/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/;

			if (value.match(regEx)) return true;
		} else {
			return args.object['phone'] == null ? false : true;
		}
	}

	defaultMessage(args: ValidationArguments) {
		return `email or phone value is invalid.`;
	}
}

export function IsEmailOrPhoneContains(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: ValidateIsEmailOrPhoneContainsConstraint,
		});
	};
}
