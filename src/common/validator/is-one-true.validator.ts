import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsOneTrue', async: false })
export class ValidateOneTrueConstraint implements ValidatorConstraintInterface {
	validate(value: any, args: ValidationArguments): boolean {
		for (let index = 0; index < args.constraints[0].length; index++) {
			const otherProperties = (args.object as any)[
				args.constraints[0][index]
			];
			if (value || otherProperties) return true;
		}
	}
	defaultMessage(args: ValidationArguments) {
		return `${args.property} needs at least one role.`;
	}
}

export function IsOneTrue(
	otherProperty: any,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [otherProperty],
			validator: ValidateOneTrueConstraint,
		});
	};
}
