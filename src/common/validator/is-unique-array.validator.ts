import { Injectable } from '@nestjs/common';
import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
	ValidatorConstraint,
	ValidatorConstraintInterface,
} from 'class-validator';
import {
	EntitySchema,
	getConnectionManager,
	getRepository,
	In,
	Repository,
} from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { REQUEST_CONTEXT } from '../interceptor/inject-request.interceptor';

@ValidatorConstraint({ name: 'IsUniqueArray', async: false })
@Injectable()
export class IsUniqueArrayConstraint implements ValidatorConstraintInterface {
	constructor(private configService: ConfigService) {}

	async validate(value: any, args?: ValidationArguments) {
		const [property] = args.constraints;

		const indexes = this.getIndexes(value, property);

		return indexes.length ? false : true;
	}

	public defaultMessage(args: ValidationArguments) {
		const [property] = args.constraints;
		const value = args.value;
		const indexes = this.getIndexes(value, property);

		return `${indexes.toString()}.${property} already exists`;
	}

	public getIndexes(values: [], property: string): any[] {
		const arr = [];
		const indexes = [];
		// [Operation]
		values.map((val, index) => {
			if (!arr.some((a) => a === val[property])) arr.push(val[property]);
			else indexes.push(index);
		});

		return indexes;
	}
}

export function IsUniqueArray(
	property: string,
	validationOptions?: ValidationOptions,
) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			constraints: [property],
			validator: IsUniqueArrayConstraint,
		});
	};
}
