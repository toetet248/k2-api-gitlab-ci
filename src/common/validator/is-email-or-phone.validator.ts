import { applyDecorators } from '@nestjs/common';
import {
	ValidatorConstraint,
	ValidatorConstraintInterface,
	ValidationArguments,
	ValidationOptions,
	registerDecorator,
	IsEmail,
} from 'class-validator';
import { IsPhoneNo } from './is-phone-no.validators';

@ValidatorConstraint({ name: 'EmailOrPhoneConstraint', async: false })
export class EmailOrPhoneConstraint implements ValidatorConstraintInterface {
	validate(value: string, args: ValidationArguments) {
		let regEx;
		if (value.includes('@'))
			regEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		else
			regEx =
				/^[0][(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3,4}[-.]?[\s\.]?[0-9]{4,6}$/im;

		if (value.match(regEx) || value.length === 0) {
			return true;
		}
		return false;
	}

	defaultMessage(args: ValidationArguments) {
		return `${args.property} does not have valid format.`;
	}
}

export function IsEmailOrPhone(validationOptions?: ValidationOptions) {
	return function (object: any, propertyName: string) {
		registerDecorator({
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: EmailOrPhoneConstraint,
		});
	};
}
