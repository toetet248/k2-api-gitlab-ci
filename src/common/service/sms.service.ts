import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Vonage from '@vonage/server-sdk';

@Injectable()
export class SmsService {
	private vonage;
	constructor(configService: ConfigService) {
		const { key, secret } = configService.get('sms');
		this.vonage = new Vonage({
			apiKey: key,
			apiSecret: secret,
		});
	}

	async sendStaffCodeSms(
		to: string,
		email: string,
		code: string,
		companyName: string,
		url: string,
	) {
		const userId = '0' + to.substring(3) + (email ? ' | ' + email : '');

		const text = `ログイン : ${url}\n${companyName}\n\nID : ${userId}\nコード : ${code}`;

		await this.send(to, text);
	}

	async send(to: string, text: string) {
		this.vonage.message.sendSms(
			'KenkouKanri',
			to,
			text,
			{
				type: 'unicode',
			},
			(err: any, responseData: any) => {
				if (err) {
					console.log(err);
				} else {
					if (responseData.messages[0]['status'] === '0') {
						console.log('Message sent successfully.');
					} else {
						console.log(
							`Message failed with error: ${responseData.messages[0]['error-text']}`,
						);
					}
				}
			},
		);
	}
}
