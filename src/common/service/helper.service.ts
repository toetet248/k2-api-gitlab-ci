import moment from 'moment';
import { BaseEntity, In, Repository } from 'typeorm';
import { BadRequestException } from '@nestjs/common';
import { v1 as uuidv1 } from 'uuid';

const allCapsAlpha = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZ'];
const allLowerAlpha = [...'abcdefghijklmnopqrstuvwxyz'];
const allUniqueChars = [...'~!@#$%^&*()_+-=[]{}|;:\'",./<>?'];
const allNumbers = [...'0123456789'];

export const filterImageFile = (req, file, callback) => {
	const mime = ['image/jpg', 'image/png', 'image/jpeg'];

	if (file?.mimetype && mime.includes(file?.mimetype)) {
		return callback(null, true);
	}

	return callback(
		new BadRequestException(['Only image files are allowed!']),
		false,
	);
};

export const filterPdfFile = (req, file, callback) => {
	const mime = ['application/pdf'];
	if (file?.mimetype && mime.includes(file?.mimetype)) {
		return callback(null, true);
	}

	return callback(
		new BadRequestException(['only pdf files are allowed!']),
		false,
	);
};

export const filterFile = (req, file, callback) => {
	const splitValues = file?.originalname.split('.');
	const ext = splitValues[splitValues.length - 1];

	if (ext.includes('xls') || ext === 'pdf') {
		return callback(null, true);
	}

	return callback(
		new BadRequestException(['only pdf or excel files are allowed!']),
		false,
	);
};

export const todayDatetime = () => {
	return moment().format('YYYY-MM-DD HH:mm:ss');
};

export const formatDate = (date) => {
	return moment(date).format('YYYY-MM-DD');
};

export const todayDate = () => {
	return moment().format('YYYY-MM-DD');
};

export const afterOneMonthDate = (days) => {
	return moment().add(days, 'days');
};

export const generateId = () => {
	return uuidv1().replace(/^(.{4})(.{4})-(.{4})-(.{4})/, '$4$3-$1-$2');

	// return nanoid(50);
};

export const generateNumber = (length = 6) => {
	const base = [...allNumbers];
	return [...Array(length)]
		.map((i) => base[(Math.random() * base.length) | 0])
		.join('');
};

export const generateEmail = () => {
	const base = [...allLowerAlpha];
	const firstString = [...Array(3)]
		.map((i) => base[(Math.random() * base.length) | 0])
		.join('');
	const secondString = moment().format('YYYYMMDD');
	return firstString + secondString + '@kenkou-kanri.jp';
};

export const generateCode = (length = 6) => {
	const base = [...allCapsAlpha, ...allNumbers];
	return [...Array(length)]
		.map((i) => base[(Math.random() * base.length) | 0])
		.join('');
};

export const generateToken = (length = 6) => {
	const base = [...allCapsAlpha, ...allLowerAlpha, ...allNumbers];
	return [...Array(length)]
		.map((i) => base[(Math.random() * base.length) | 0])
		.join('');
};

export const generateHashCode = (length = 16) => {
	const base = [
		...allCapsAlpha,
		...allLowerAlpha,
		...allUniqueChars,
		...allNumbers,
	];
	return [...Array(length)]
		.map((i) => base[(Math.random() * base.length) | 0])
		.join('');
};

export const validateExists = async (
	repo: Repository<BaseEntity>,
	exists: [column: string, value: any],
): Promise<boolean> => {
	const [existsColumn, existsValue] = exists;

	if (typeof existsValue === 'object') {
		const count = await repo.count({
			where: { [existsColumn]: In(existsValue) },
		});

		return count === existsValue.length;
	}

	const data = await repo.count({
		where: { [existsColumn]: existsValue },
	});

	return !!data;
};

export const validateDateString = (date: string): boolean => {
	return moment(date, 'YYYY-MM-DD', true).isValid();
};

export const validateTwoArrayUnique = (
	firstArray: any[],
	secondArray: any[],
): boolean => {
	if (!firstArray.length || !secondArray.length) {
		return true;
	}

	const difference = firstArray.filter((x) => secondArray.includes(x));

	return difference.length > 0 ? false : true;
};

// order site status
export const siteStatusOrder = () => {
	let startDate: any;
	let endDate: any;
	const today = new Date().toLocaleString();
	if (startDate == null && endDate == null) {
		return 1;
	} else if (today < startDate) {
		return 2;
	} else if (today >= startDate && today <= endDate) {
		return 3;
	} else if (today > endDate) {
		return 4;
	}
};
