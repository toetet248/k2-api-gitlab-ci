import { BadRequestException, HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom, map } from 'rxjs';
import FormData from 'form-data';
import moment from 'moment';
import { CreatePaymentMethodDTO } from 'src/modules/tenancy/payments/dto/create-payment-method.dto';
import { Boolify } from 'node-boolify';
import { MailService } from './mail.service';
import { Plan } from 'src/modules/central/plans/plan.entity';
import { generateToken } from './helper.service';
import { isEmpty } from 'lodash';
import { Subscription } from 'src/modules/central/subscriptions/subscription.entity';
import { Staff } from 'src/modules/tenancy/staffs/staff.entity';
import { Tenant } from 'src/modules/central/tenants/tenant.entity';
import { UpdateDefaultMethodDTO } from 'src/modules/tenancy/payments/dto/update-default-method.dto';

@Injectable()
export class GmoService {
	constructor(
		private configService: ConfigService,
		private readonly httpService: HttpService,
		private readonly mailService: MailService,
	) {}

	parseObject(data: string) {
		const splitData = data.split('&');

		const obj = {};

		const arrObj = [];

		splitData.map((d) => {
			const [key, value] = d.split('=');
			const splitValue = value.split('|');

			if (splitValue.length > 1) {
				splitValue.map((v, k) => {
					if (!arrObj[k]) arrObj[k] = {};

					arrObj[k][key] = v;

					// obj[key] = splitValue[k];
				});
			} else {
				obj[key] = value;
			}
		});

		// no credit card exists
		if(obj['ErrInfo']  && obj['ErrInfo'] === 'E01240002'){
			return [];
		}


		if (obj['ErrCode']) {
			throw new BadRequestException([
				'invalid payment data format',
				{ error: obj },
			]);
		}

		if (!isEmpty(obj) && arrObj.length) {
			arrObj.map((ao) => {
				Object.entries(ao).forEach(([key, value]) => {
					if (!obj[key]) obj[key] = [];
					obj[key].push(value);
				});
			});

			return obj;
		}

		return arrObj.length > 0 ? arrObj : obj;
	}

	parseArray(data: string) {
		const splitLine = data.replace(/['"]+/g, '').split(/\r\n|\r|\n/);

		return splitLine.map((sl) => {
			const splitArray = sl.split(',');
			return splitArray.map((sa) => {
				if (sa.includes('ErrCode'))
					throw new BadRequestException([
						'invalid payment data format',
						,
						{ error: sa },
					]);
				return sa;
			});
		});
	}

	async gmoApi(uri: string, payload: Object) {
		const { siteId, sitePass, shopId, shopPass } =
			this.configService.get('gmo');

		payload = {
			SiteID: siteId,
			SitePass: sitePass,
			ShopID: shopId,
			ShopPass: shopPass,
			...payload,
		};

		const formData = new FormData();

		Object.entries(payload).forEach(([key, value]) => {
			formData.append(key, value);
		});

		const response = await lastValueFrom(
			this.httpService.post(uri, formData).pipe(
				map((response) => {
					return response.data;
				}),
			),
		);

		return response;
	}

	async createGmoMember(tenantName: string, companyName: string) {
		const response = await this.gmoApi('SaveMember.idPass', {
			MemberID: `${tenantName}${moment().format('YYYYMMDDHHmmss')}`,
			MemberName: companyName,
		});

		return this.parseObject(response);
	}

	async addPaymentMethod(
		createPaymentMethodDTO: CreatePaymentMethodDTO,
		gmoId: string,
		defaultFlag?: boolean,
	) {
		const { cardName, token } = createPaymentMethodDTO;
		// console.log({
		// 	MemberID: gmoId,
		// 	DefaultFlag: 1,
		// 	CardName: cardName,
		// 	Token: token,
		// });

		const response = await this.gmoApi('SaveCard.idPass', {
			MemberID: gmoId,
			DefaultFlag: Boolify(defaultFlag) ? '1' : '0',
			CardName: cardName,
			Token: token,
		});

		return this.parseObject(response);
	}

	async updateDefaultMethod(
		updateDefaultMethodDTO: UpdateDefaultMethodDTO,
		gmoId: string,
	) {
		const { CardSeq, CardName, Expire, HolderName } =
			updateDefaultMethodDTO;
		const response = await this.gmoApi('SaveCard.idPass', {
			MemberID: gmoId,
			DefaultFlag: 1,
			CardSeq,
			CardName,
			Expire,
			HolderName,
			UpdateType: 2, //1：全て更新（デフォルト）2：カード番号以外を更新。
		});

		return this.parseObject(response);
	}

	async getPaymentMethods(
		gmoId: string,
		isGmoRegister: boolean,
	): Promise<any> {
		if (!isGmoRegister) return [];

		const response = await this.gmoApi('SearchCard.idPass', {
			MemberID: gmoId,
		});
		return this.parseObject(response);
	}

	async deletePaymentMethod(gmoId: string, cardSequence: string) {
		const response = await this.gmoApi('DeleteCard.idPass', {
			MemberID: gmoId,
			CardSeq: cardSequence,
		});
		return this.parseObject(response);
	}

	async sendPaymentMethodMail(
		staff: Staff,
		customerId: string,
		activeSubscription: Subscription,
	) {
		const { frontendUrl } = this.configService.get('app');
		const subject = '[建工管理] お支払い方法変更の確認';
		const template = 'payment-method';

		const upcomingInvoice = await this.upcomingInvoice(activeSubscription);
		// date('Y 年 m 月 d 日', strtotime($upcomingInvoice->NextChargeDate)),
		const mailData = {
			updatedTime: moment().format('YYYY 年 MM 月 DD 日'),
			nextChargeDate: upcomingInvoice['NextChargeDate']
				? moment(upcomingInvoice['NextChargeDate'], 'YYYYMMDD').format(
					'YYYY 年 MM 月 DD 日',
				)
				: '',
			url: frontendUrl,
			customerId,
			fullName: staff.name,
		};

		this.mailService.send(staff.email, subject, template, mailData);
	}

	async getDefaultPayment(gmoId: string, isGmoRegister: boolean) {
		if (!isGmoRegister) return null;

		let paymentMethods = await this.getPaymentMethods(gmoId, isGmoRegister);
		if (!Array.isArray(paymentMethods)) paymentMethods = [paymentMethods];

		return paymentMethods.find((pm: any) => pm['DefaultFlag'] == '1');
	}

	generateRecurringId() {
		return `${moment().format('YMMDDHHmmss')}${generateToken(1)}`;
	}

	async createNewSubscription(
		tenant: Tenant,
		plan: Plan,
		recurringId: string,
	) {
		const payload = {
			MemberID: tenant.gmoId,
			RecurringID: recurringId,
			RegistType: '1',
			ClientField1: 'Tenant:' + tenant.name,
			Amount: plan.price.toString(),
			ChargeDay: moment().add(1, 'days').format('DD').toString(),
			ClientField2: 'Plan:' + plan.name,
			plan_id: plan.name,
		};

		await this.gmoApi('RegisterRecurringCredit.idPass', payload);

		return payload;
	}

	async sendPlanChangeMail(staff: Staff, customerId: string, plan: Plan) {
		const { frontendUrl } = this.configService.get('app');
		const subject = '[建工管理] プラン変更のお知らせ';
		const template = 'plan-change';

		const mailData = {
			planName: plan.name,
			updatedTime: moment().format('YYYY 年 MM 月 DD 日'),
			amount: plan.price,
			url: frontendUrl,
			customerId,
			fullName: staff.name,
		};

		this.mailService.send(staff.email, subject, template, mailData);
		// this.mailService.sendPlanChangeMail(staff.email, staff.name, )
	}

	async upcomingInvoice(activeSubscription: Subscription) {
		if (!activeSubscription) return { NextChargeDate: null };

		const response = await this.gmoApi('SearchRecurring.idPass', {
			RecurringID: activeSubscription.recurringId,
		});
		return this.parseObject(response);
	}

	async deleteSubscription(recurringId: string) {
		const response = await this.gmoApi('UnregisterRecurring.idPass', {
			RecurringID: recurringId,
		});
		return this.parseObject(response);
	}

	async searchRecurringResultFile(date: string) {
		const response = await this.gmoApi('SearchRecurringResultFile.idPass', {
			Method: 'RECURRING_CREDIT',
			ChargeDate: date,
		});


		if (response.includes('ErrInfo=M01301002'))
			return [];

		return this.parseArray(response);
	}
}
