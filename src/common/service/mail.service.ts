import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
	constructor(private mailerService: MailerService) {}

	async sendTenantRegisteredSuccessMail(
		to: string,
		companyName: string,
		url: string,
	) {
		// companyName, url, email, companyName
		await this.send(
			to,
			'[建工管理] 貴社専用県境情報のお知らせ',
			'tenant-registered-success',
			{
				email: to,
				companyName: companyName || '',
				url,
			},
		);
	}

	async sendTenantVerificationCodeMail(
		to: string,
		code: string,
		companyName: string,
	) {
		await this.send(
			to,
			'【建工管理】パスコード',
			'tenant-verification-code',
			{
				code,
				companyName: companyName || '',
			},
		);
	}

	async sendStaffCodeMail(
		to: string,
		code: string,
		phone: string,
		companyName: string,
		url: string,
	) {
		const userId = to + (phone ? ' | ' + phone : '');
		await this.send(
			to,
			'【建工管理】' + companyName + ' パスコード',
			'staff-code',
			{
				userId,
				companyName: companyName || '',
				code,
				url,
			},
		);
	}

	async sendPlanChangeMail(to: string, name: string, code: string) {
		await this.send(to, '[建工管理] プラン変更のお知らせ', 'plan-change', {
			name,
			code,
		});
	}

	async sendPaymentNoticeMail(to: string, data: any) {
		await this.send(
			to,
			'[建工管理] 請求のお知らせ',
			'payment-notice',
			data,
		);
	}

	async send(
		to: string,
		subject: string,
		template: string,
		context?: Object,
	) {
		await this.mailerService.sendMail({
			from: '建工管理<no-reply@o-technique.jp>',
			to,
			subject,
			template,
			context,
		});
	}
}
