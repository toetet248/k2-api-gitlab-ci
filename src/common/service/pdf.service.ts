import { Injectable } from '@nestjs/common';
import moment from 'moment';

var pdfMake = require('pdfmake/build/pdfmake');
var pdfFonts = require('pdfmake/build/vfs_fonts');

pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.fonts = {
	ipa: {
		normal: 'ipagp.ttf',
		bold: 'ipagp.ttf',
	},
};

const k_logo =
	'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKsAAAAoCAMAAAC2EvpdAAAAV1BMVEVHcEwgIDAiIjIhITMhITQgIDAgIDUgIDQiIjQiIjQgIDAhITMgIDIiIjMgIDMiIjMiIjMhITMhITMhITMiIjMhITQhITMhITQgIDMiIjQgIDQgIDMiIjR+WYsCAAAAHHRSTlMAMHCwwBA/QIB/IKBgkG+f8NDg36+/789Qj09fbxUYCgAAA+1JREFUWMPVmdmCoyAQRQsUAfdd033//zvnAVSiSLR7eiZdTyEleiK1XAiRMTaNg/4Q9AusHBEXMdD15Zvzir7msSIilhdt9Nao89gwipUZSLzzS31AEf0KVjG1kkKsrOt6nbxFFEeNwTiyWr4s4lkDdH2Z/G9WSPKz9kDX60QYh+QRgKn6e8AiPRvcZ4VkeRED2BySZw3wCNwtHcf0auxhdgbj/D1WE67NzjEvM3wmEfI+xd76EvaDO6w2lY4OGwBPNBEAAIyIY7NBLL/raIyIiIplGO8HRLUdjYwoW+dNwoOUmwZ2dEQtMFXaPs16zY0iIuUC2bnKwyqJiHI4eE8DInIulc5EdRIDLC98DiFVFD+t1qyUMivIlFIqAzKVraxMWQPi5WNqXmutVAwoJXcDIlonpCSBzLjOWd3gsEFx3iRi1MKNV0/QHuKRMyIFEJHS6TbYT5CANK5rrFmeNUDXRef5pIiIaa11BVS6AqryBasJhGbLxhzNddY+OWG19VWJ84xOj7H5glUk1j6BcvnMfKxlUgFJsrGKHo0p9SesgZIKZM/pAgBtmFVM8FnuYd3nluwaRrbU32XNzGu9F6/Si2rrQJD1gWJdYL6yTkaxvGAVQERfYeVyb42XlcsMkNKwsqmW7l2Wq6MY6HrdhVnVAsY99TXEKj31JA7nVjuMkSCiT/2RHnKL5UUcBeWgGJcHuLlVpz/CyoqciMSAtsY46I98H69h40A3DENKJNZeYOt9iJWhTt0GioqIIhTmnlprYNBa7OJ12cU0ilKjq26xqrXHBrWLt77Kk6hxvkvr7YqZiEhUUGsK3XyvFMVxAxRuL9BapxdZM9N3syfW2pE5TNn2q2azSWmZr29heHyyK7imaqk7ubVbhONyuBO29quRCW+P5SpqgUG/VM0jop3+q9mPsBb1HNADQqqav06vmb4Yr/ZiofjJBJEkpscmKdHynI3V7lqv9i1qTUMVTrweDm/OWCu92InYUW5PO+osI7X16hjCx1wzwA+6Wt6uA7Ze7SfwMKuR2vHqaIHQCUFk1Sv/JmvsnyCl6bFS0AX9qgz6ye4tBTLiI/tWvD73rNPcusJqVmP0shZAKoBR/EvWwazyKat/D2P0QA5MgoiI1XX606xGa3e9TlR9izU3AjkDer3ZUgdEab8Ahp1rEf3WJoc1txNKYW4wAKYXGq09blqb32K1BWvZey/GzyW1fJlbwT33wykY689GeYVVtbZmN77NSJjV1VnP+5dT1met7UbJVJXJ5YNZ5ih85vty59q5nTBPnUudK4jaASdqWkgVtQAGczT0BofIrAie0FngqSp1+xv+pLHA+f/G+ANbJJT/3eE9YAAAAABJRU5ErkJggg==';

const k_stamp =
	'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABECAIAAAC6b3SiAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAABexSURBVGhD1Vpne+M4kkYiJSpblizb7dBpZnbm7v7/L7nde3Y62O22bCvnQCLcWyAly7bc07v36arVMgWAQFWhwlsguXOO/f+n18VAh7Wcc4bPpiX7EpxZi2vq9X/oO50nHUNXRDQALX4A5sNP+vYdaTe1YxX6QXdtl0MjfTsnlMJaKZPUKwQNfUFPxPAsOJ4kptd1iWZhSOzGMU0tFAsk+rmzbh3zfA7cMSGZThwGQLJ8mHKT0qMk/nv3J62oFMm3WpGIKvCcYwz4tBaTc8HDgEYVCma59BxCPNICbpS1OpPCNz3SjhjgGLIaw3jMbh+YqrCTOhv22WjsNSTYyRsWSreY8MGUVRquHLrpRMwXkNOJgJ+dYBhmS1WO+Thmw1V6nXbt6nK9ZN0uy0Ws2cyWxkiXmLtbmYTs7A1T2UC7morxjJUPWSFknTZjATtq0rQkVkbZvFjKWWO+fmWzlTVJPJ0liyXak8k4ni90PkhWc9PpgkfLrZ7P9WgCnky/Fw+GrHXCmofEB22aIDmEyDj2LXTtL2wS2/ncTEeu29FX18l4brGfoO1gx2W+uF7MaKOSJP0wG9vFJL7+qu86NgiTQdddX7nZjG7Z0GY34AiDPh8MTbUu6jn955UqNsVFK7mCYFp+OLe9WzvT4e+/MbNa//2TrLXUeTP5/BlGJQ5rbr6GAbAgYFqb+dx6p8LMASy7WCT+lqQUlgvZOmazBYtyrHnM8gE17tJymny7hcXIcpk2BwQJ4ziez5h1sGF1duastnd3QevYHR5ud2MjhknI1rp9PV+IQqgHIybzohS58SQoN9h5U3fbrDNi+cgJ7hYLpgIZ5WScsGLBhErKkG5frZj02s3l2HyOeQ28E3qdzUSlAs7InZQU1arThis/0hPZG0QFtb+xlTG1KjjG1vhO6pVhoPs9O55CU6J64OJFkMu7cpkXS+kYEiObBRuyWMBf7XhEDPGAFyJRKvJikRxOx265dknM46Xuj1Wp6soRk0qgFyr3ZpnSVkMp2fFYX12pjx8xEj/TYbtj0EI/nWHTEbYi+PAb7dVLWs3iP7/S5lgHloLjIxvHolqjrd76hp1OndZYSR4ciEZD5XIqKqmTE35QgwwWUiUGmoD0AmuQ3IKXSiKfZzBfqNyzkn5S2kolvGMImNx+crBDF6/tcMAe+kHjiOUyS0v1m14T5SOOX/kKLxWCMMcKsFXvcp4yo9L39+r4mMG779sOYYHCAEw7EsWSajXZes66PdyWUPiDk8MV4biYwkI3wS+/wtKwLAV4zOjtI73A5HYyMTc34e+/w97gM8IvnHHnnOl31GLpEssLBR0n6u0lJaV9ZEZ91n4wxcPwsuVWiPihGQ6h9LQ3MyrXH4h62V1/M5QBIoxgIpDlkAJXVAnOW+RhFJMNW47c/YhHNX584COM4wgsvV6yjEPkGZ+trNbp7JjYIIJ7kRRyBTzH5zXvQhzhgTJPteag52qVS4XUuF8IEqMnHvqsfsqa5XSMGQxkve4vvVGR+qRgyziZLXixyo9gcI6HBfnm0uZCsY7tymBh1++I+wfWmZgk0dOB/vyZa4a1zTp2QT5sNE2xqMNQ53IWblAui2ZT1GoUc8B6FNlSiVUq1FWrOegeUQtdSBqNOkfMgfyvy0AElQhJVpL9fkKZUbnRCDEk/nqljs54XepP33juIHj/BgFXxJqfv3HrmRwuNBdOGDdd8DCSxTyyR3h8wurVdK69hEjF7u7Y0RH8MmvaIbK97PIvyPQ6YjRx9Rav7+wGjMrL5V0cHluruvWKrp+TM7EW2PrZNF7F6u1FcNqA5DxfEaenPEDcSLKBrxDCAw+CFCO9pJ+UASQDxbRlxSeQhxzSU5Y+3XzBYVd7iSubWKeToFYh54iTMB9l9yNWgBOTzfWM0n3mTiMW/dhe/pqSdfy9bcOSyOXSaZ9RthtmCTsJfQsIS2arIlRiDsYN4hMib9ruEwXNJSA5JkVC9INT2l77iMUokhaKHKH53yJnfLQY9kWQZ2Wf7DaxjiLhxlOy3SAsORmQzkLlNZ2lVUqM+Fogq1imiWNqS9bcYyHieDZDGqJWRCdgxPWavG29xoctFvauLRPHkJVhEvvIy2wpb6xWgFt2OjGTCVuv8Y1oafp93nlI/vH3ZLJUH9/LZgWjM8aJj03uRyN+kGTQ+T/+G5k0+Ntvjq/0P6+CUoufN02/7TojhFAG5H/xXlTybjVIPrVV/VSc1mnJdltWKqwQsenUB2VI7eEq0Ds2TQWs0WD5fVkZfBjNgTkeOoSywAVEBTNaE3oCegcax3UQ8tqBKO8JD7sBN4tUdjBw/T5CpzhAtDXIgEJFBDeSBVIHSgzKxrUDwjB8rb/d80ItaFTcfJ70+0BOIghdFCG/0ox+/2Sh4JD14dxeR3vJLpei14EsvBghBgjkbyrVgPoVD3KUA8AbYiON9cVWEFhsMgzdm+g+MXo9aDDpDxzCqxAqDJHDpJBJkkikLcwFUZDJAOmAyZJVapu0DKJYuSYPmyz/CPVS+oEAWJQs22iEFpELde/OTdcBFjUG7RxYIzU3z5tZrqBDDxpIEvq0Wshje7K47fVls0F5yv+kjpQPAhS4Qrj0l1MA6e9MhfK4JaqVzDJTVPuvEJbAdNmP2SS5vhYoWmgBwJpNwMDCqab8UOKCGi2fjs08Dv/jDzsZAyynY7PdcL0uIqNJtEMaoHt9mH9cakPwEkB/DmBb2FU1cHFQrrDWUfb7NYLz9QesdkBu4wmBXIzHbrxib04wa9r4Y7KzMf/e5pfvbLzmwOoEiHykIjmE1N2BmSxQlHBrUO4E2Edj4GQwKQXLgblD4gR1Oe0sLsF6Wp0JFEmFXNLvuvuOnS8c4OPTD/C/WU7dfBj/43/YGgBmh10IRpEqIa3/FaUa54EEK3DIXSV7o8Kf2Sz+8lXUmrKVWdsOOR6EHgUyt54kV98YLwQf3lGQ2RDchg8HyXAYqCBGUQXCGukyZCdwNriv1SpHBUzjsWqzdi0Q7gYre3IsohfF4F5KZvb6RjTODTcE2DxjmVGhyNTXXxEkKD48I+5EGPF8ATuIwjD+8hmxC+CFsj5qsVwe0VbA7UBJDJfFhFSLr9e236cy4/iYqjFnZVRkUZ5U5mdNKRNjYlzzkP+sGHP99as6/cXEC1nfcXH8saMREDzZyWqdYvJHgs/FazMYyuaROj0B1DO9Hh3q0I0WNhMettxxy498ZBFFn725kc0mR1R5nTIxxtq1mjy/P0VuiVgFM6uJ6HRc5cQKI4Gat75BIxKNSMoLRXFQF4eHzz4QgAfKLVYkpJSy1UJpH7z/EHx4L8rKTBcMgRgSpBvrCaUFRe1t2PkB4cZXcvwzgjNgrDdVrucLpN3MlPA7241eF6YZf2v7WZ+S14G/gLYpDcFmkEWCd+9Zniff/pSLvD05UdXIjyadYTlsr+h2KYVvQvte2hiVds3GzxqVie3VZ35wzgB2i4XU0b0YsFysWq3YdWoqj6RRKiyXEpgcVp524QYkPSl5LnQs1jefg7hsGoey9qTaptwEMWo1VBqpYFnHU7JuLcYT11uy81Oe+ymj4vHM3bZ583LXNzKjIgA8n0M5HIEMSRCRh661RALHnShEE42PsIi5mgF16oSYw51IMr5M3RIt5ncMS9IBD659XtvtfSTqppM+AgpZ06uU6gLDCPYqsVtZ+Ku0ezUynTv9cJ883JvOAy50pwuAbvM5PRmb7gM+yX3b9jqEvjpdzyhgD/DomuCQJ+LKzyaiKAZi9eAXlDbuXqSU/hJ5wlRPOl4hjAFWMTEdGWRNnjZ6QqkZSCq4fSbK9LezJFcBnUP6VElTAYAgJcJMv10Zlgs/vKXzQoBc30mLxXF8fy+KpQCpHa6CLIl6BjVDkJ0iY35ahVk+mdiHATt7Q0dHrxMtnBrnbLr+9DX3X/8JVI9aP+3NxID6ZbVqlys+n8EUgAjRDqzmWYKtOYN9Z8AQCu1Ga3yDyAi15aUyj5SYzjFALxaqWERQ1sYEkXf6BJeawDyMlkt21HIExmjVdHK4rPv0hR82GKInHSxkTx1QyYBvAoLpGRca5wiJmi3mCfD7+7duseBRlLrdRoz2rVyv4lUiKlW0IagCY2tsEQi6nE7IMSA67qGdwFYgUUrsj1UB0ggMSxwdSZRHqXib/QSlF8QcFIposVyyIHLInrnArRYcVp3Pu+XcTma+0Esl87qHuyBkA5ln++Z3A4zlC4hG2FA7ndJJ5G4WJzGQyGUoLy+ymTYEpuz1tV0sg19/e57jzVrf3tvRmAU5dfYGuYWhynmJA3bJJGwVo6y1s5Fpt4PqIUtT50vyUv0APqM2lIeH6XUmhm63FZL1eIaKXDYOsZVZB6LCqMtGC4liqlqlAJc2g7BL/a4dTnghClDfpbkfmy4VOCOUlVoIyl3vjtnO0CCUXnM2GAIm6qgokb994y6Rx89n7qHLzi+wb1mrJ6iVNgurvyybTLcL4JB8/gRQxHJ5qaSMIuONCmvYMIIUdB4GXpW3qDhOViuHPJPLh798wDBgYSqpx2OVyyejUVAqEcBBzM3l1cV5KgMtSPIYc/udTebqjz/SlpdEYozHbjDgZxfbU91ntKf6QwCVR83k61dkj+DDRx79xEEGAsHnLzKqibN9VoGKhQt9ewvh7cGBQLG+IYfC+Pq7k5G4OGM7MHmXLArj8YgBUwIH5bZHNk/IDodit/rDH9PpiHpd39wANSFkUQ3me7Eo/qRjUnX6a48BsdR4HLbOWb1MY18jquOf8AoxzKcv6vCUHWW6fEnpblhw9cuvz27f0vMz3C2BUdRMfDoBxoYDsdFQTqe4gBXig3n5eAQIhG8MsMMRahey/h/TCyYQ6kgR0AcpZz+Ryujjn0X+BD3uhmw09PW1tM42GrABksjHb5TzexekU9FvN2HzjB1kj3x+kpxe20+f5MGJa9Z3C8Fdot0YDgGcqeL9gW9sznCfGxVbLSnUbHWMCwrMjlvyUfg91APnRg/V60kSnlz+hVE9JSzm1lP+7ZbnD+xJU7xSf5MYCGWIewf1n3Hxx1nSZ19Wm6Ba42EOkTeoVoMCPWYMuFT1ulMqiWMK5vk80idHkIWQacb9lwh2C9UgJf7YqJAxNicPrxGkTS8exUiBaqCUrVXV5YW6vATGtgE3y2UsQlavoyV4+1Yc1OjZWrVKB3M/znT7yO8ypyMYY4CQXxOEbAS7rf/iuJ7U4WlnN3y+BGInTI6JsKGzqe1P5LsPqkXJkqDhco4CUqDWG/TN1Q3KjR9k2deIjhi1tYBhANtZ2z7ChixXPtX9Nb1QJ7YpdYxk5R46TuVZIS/o1QrmpiPWG+p8mR012cWFOKii7ZlRpZ7245XN9zYAGR1vbsa/SsCFvqh5jfYYVUoJXNzvNQ9QPMXk7p7gBNzGjgfi0HsVF+oMMAGJ6ekyXgWvrOzcbOxub8xkKT684zUKDOQDrxEk9NEl+7mXNrdnYnitkDkKmYUFM1mgcHXLmekN7GSs2zesP+WVotgpNSELs+v0mo7eBl3dvkXdC4SGD3IwQ/IZDOjNhOWcdTv2pm2ni+DyLYvyf4EgPdnZjODm67TVQhZw9cODkiLudkWxos5OSQ3oiNe80yWJhbCLuWieuFLhyfKdTtzr0ZNo7oTG/jo3mSKaoSxBsR6vVgIXwNtCxBA5yotKhd7XQYijVT0P/m8224bQCWuhWHb/wFrHhOL2EQVc1A7eyzd54+5Orlb0fPfiMn0bZUs06Xxueu2g1kKdQL9hkXEMr7ADyuVpdRVbJ4+PeS4HdsEZjfEzg8AmkiqqMcB4JHUgN4kYTudrincGrHzAivSuRjo6/c5UhVt23jmiLj9nugl7xWjL5ToxRraOUDBRXYx8R+fxHIULHXOslvbgUMG5QfGa3bZR1EkkkHIFeQAT2/HQLNbwJV4ti1xelMtPwrHVrN/Ti0S0Wrb/oGYLdnjEKuXkBmB0FZy+caE3Hs/N1lTSmotY9y3YzLQ9pb3QsCtRUvW6DLU4UohzEnYfBsl87lCylUqydezAtOfMXl/p0Vx9fEfn6l5JWMSZhMeJxtSwBwgmAo3FyyWei3io6OlEsmTf72F6/KDi9NI+jFhQlM2qi1duPA5y9N5Glu/wnQZMfEPZCP34po/SMPBKVZRQ9PHdLL4Vo4MqlPZghmISZXeKzNBrfd0IATamtlom3wGEbXB+Bo+nMV6MXaKHg6hbUBWiJypQkV0koG5R6Pc79CDv3aVdDEx7olon8viQJWsbJ2LzOs6W6MzJbwXsAlZqPGYNqw12dozePWKAe3oStZP8/dmRx5ib33bQM51BAOPGPda4RAclehWBkW4e7WdXKqozpUS9TvpLCRX3n5+gZHVUs92uqh2xZiPrekovtQNKrr7waSLevRWlnHl4kEd07IL2TAw2mdDzwngtwzCZTMErpR7jz0A4skWgojC+vRO1uqiW9V2bayFPju2kZ0fTEOgtT4mZV6oOUcU/sHzJQUbGuttvyWjCgkL49pRBd6+h3BfkkJG/3wpZ8Bg8oN3YINzNFHDo8VhOpuAVbCdxEvdG9FReoiaew3bju47gIS9URbkiAsAIxYtlflRlSqynSzZf4h573+ajIbu+MfddMnSvoExNG3IucYAwYQQpKKjSU8UnA/YSjVkv+cO9Tow7bUIGavVm7/s3YljgqIuTRMCxcursTDSKzjo6Tm8dGGNEsyUbdcKOlPsRaOm9ROwjUDH2PjxssHdvCZ58/GhykjdqajaC5bD7DpvNt2EnJasX3HJ1eMwvL0TB79vTAbu0ldDRm1AdZlX47n2KxH3ro/yb3YAVITTBxB0jS0oVRUoPaTQgSXpzGt3xP1AG4cjEMlCU7KmLfF0eNFmxaI+P1K+/sGqBjnM2sCclsAyQL8pRdosnXG45Tin96SU0ZjQU4wnjIaPqescCX4pBb0BxhdtgqLiXOM/nEBusdipE0qV7qM8jNYySQUBrQIydeTdaFaIEY1MsilwY4LasOSVa27rwMcV6XrPvLWU/4xWd0I0neh6z0xNWLacRPxUdMSktLkAZE/7lY4/PCVKQJOmrXZiO8jFqP6exS1LS43qKU8u0AHQaeWa1esrphqCXqPiMP6YCg1i8jNNfz3t3yI5HyZcv8T8/x3A8cuWsPSX8opcgs19bMRCA9cLqGGlPRDmlAp0klMRVSK+nIXPFS1mpWIUbaX/ovQVMlANKhTV6S9vQM/PYJacTgcpPCl7Mvco+CHq6v9dX1yzMCzrJtub2BlEk690RfrvSxiTQDqi6tulDI53EEmUgfFkbbJTpD+xgrk5aiNbohWGpMKCcIguIxclk+MQBvJOAtms8Xgx77LZjC6+/Roatn07ttys2HIe//gEsrI5Pwt//CI+Pzc118vXKzubZwCU9790m5UfLtsMxp7TkY5ljASI64Ax3ElDCCcQWhsACTYNjAwENoY8gEMctJByeZDZKD0GBz+dzizCdfi/mxFnvnnXuBEJzqcQrr52kGNbruO/fhcqRJ0QBTCPraTTkcYti7s13Nhi7dUzF6Q5t0h+IzqOG7vBI1Cum9110l+zNOS+K5M9PQubF8RH4doAqEG2xsGsdon4qlSwFN5rBAUH2eiFqYNgxWpYovzi9VwyIOhpjszHSFUri8OCJOWF5AE1E8NkMgCgEYi0UGap83/PMc4C+bK/H5guZL7LzN1mrpyzkZTfQe3pYO9D9W3MzDj78Iso5d3eV9GcYAzwrozylDONfmkduRDKbz4VUaAPTstHA/nDoWyk4D6ZEsPD/9hOti1g0GIIzoAV1cUFiPyVoKL09VTZNuZqzyYIBhuyI+lQMEBiCwcHcl5rehlLcjEcIUJT0mPOP8R0hiDCkF2zjJFAymc3o/V50QZcAuXEMkwV02syJQI2ayb/Xh5mTxPhHFrQoLBOGpy3CtywVDVRDUZEDUOvp1DPucaGfg8YrylGCzsLpwZifPKMdo9oQcUCvZtH5i4GLo4TAAoJvzRG3IIghqZC+fXFHMmIu/8ASohKPSUJ3pfaGLghGqcnzBIH98yqay5c0uIJvgxX0p4MtsEzKPz4e3tJPfFCi4adv36U9Yvxf6OUC/wb9y5Mw9r8r4jCbrO0ZzQAAAABJRU5ErkJggg==';

@Injectable()
export class PdfService {
	// PDF Make
	async generatePDF(invoice: any): Promise<Buffer> {
		const {
			orderId,
			amount,
			processDate,
			tenant: {
				companyName,
				companyAddressPrefecture,
				companyAddressCity,
				companyAddressBuilding,
			},
		} = invoice;

		const pdfBuffer: Buffer = await new Promise((resolve) => {
			// customer company info
			const company = [
				{
					text: companyName,
					fontSize: 13,
				},
				{
					text: [
						companyAddressPrefecture,
						companyAddressCity,
						companyAddressBuilding,
					].join(''),
					margin: [0, 15],
					fontSize: 10,
				},
			];

			const summaryTable = {
				table: {
					// headers are automatically repeated if the table spans over multiple pages
					// you can declare how many rows should be treated as headers
					headerRows: 1,
					widths: ['auto', 'auto'],
					body: [
						[
							{
								text: '合計金額（税込）',
								fontSize: 10,
								alignment: 'center',
								fillColor: '#e0e0e0',
								border: [false, false, false, true],
								margin: [0, 4, 0, 0],
							},
							{
								text: `¥${Number(amount).toLocaleString()} -`,
								fontSize: 16,
								alignment: 'center',
								border: [false, false, false, true],
							},
						],
					],
				},
				layout: {
					paddingLeft: function (i, node) {
						return 10;
					},
					paddingRight: function (i, node) {
						return 10;
					},
					paddingTop: function (i, node) {
						return 10;
					},
					paddingBottom: function (i, node) {
						return 10;
					},
				},
			};
			//  invoice table
			const table = {
				// layout: 'lightHorizontalLines', / optional
				table: {
					// headers are automatically repeated if the table spans over multiple pages
					// you can declare how many rows should be treated as headers
					headerRows: 1,
					widths: ['*', 'auto', 100],

					body: [
						[
							{
								text: 'サービス名',
								alignment: 'center',
								colSpan: 2,
								fillColor: '#222234',
								color: 'white',
								fontSize: 10,
							},
							{},
							{
								text: '金額',
								alignment: 'center',
								color: 'white',
								fillColor: '#222234',
								fontSize: 10,
							},
						],
						[
							{
								text: '建工管理（PROプラン）',
								alignment: 'center',
								colSpan: 2,
							},
							{},
							{
								text: `¥${Number(amount).toLocaleString()}`,
								alignment: 'center',
								fontSize: 10,
							},
						],
						[
							{ text: '', border: [false] },
							{
								text: '税込',
								alignment: 'center',
								fillColor: '#e0e0e0',
							},
							{
								text: `¥${Number(amount).toLocaleString()}`,
								alignment: 'center',
							},
						],
					],
				},
				layout: {
					paddingLeft: function (i, node) {
						return 10;
					},
					paddingRight: function (i, node) {
						return 10;
					},
					paddingTop: function (i, node) {
						return 10;
					},
					paddingBottom: function (i, node) {
						return 10;
					},
				},
			};

			const customFooter = [
				{
					stack: [
						{
							columns: [
								{ text: '', width: '*' },
								{
									stack: [
										{
											text: 'グロースチェンジ株式会社',
											fontSize: 13,
											alignment: 'left',
										},
										{
											stack: [
												'〒490-1202',
												'愛知県あま市富塚二反地13-1　美和ビル201号',
											],
											fontSize: 10,
											alignment: 'left',
											margin: [0, 10, 0, 0],
										},
									],
									width: 'auto',
								},
								// stamp
								{
									image: k_stamp,
									width: 50,
									background: 'yellow',
								},
							],
							columnGap: 30,
						},
					],

					margin: [50, 80, 0, 0],
				},
			];

			const docDefinition = {
				content: [
					{
						columns: [
							{
								// kenkokanri logo
								image: k_logo,
								width: 120,
							},
							{
								stack: [
									`No：	${orderId}`,
									`発行日：	${moment(processDate).format(
										'YYYY/MM/DD',
									)}`,
								],

								width: '*',
								fontSize: 10,
								alignment: 'right',
							},
						],
						// optional space between columns
						columnGap: 10,
					},
					{
						text: '領収書',
						fontSize: 16,
						alignment: 'center',
						margin: [0, 20, 0, 10],
					},
					// company name
					[...company],
					{
						text: '下記の通り、領収申し上げます。',
						fontSize: 10,
						margin: [0, 0, 0, 15],
					},
					summaryTable,
					{ text: '', margin: [0, 0, 0, 30] },
					// table
					table,

					// footer
					[...customFooter],
				],
				defaultStyle: {
					font: 'ipa',
				},
			};

			var doc = pdfMake.createPdf(docDefinition);
			doc.getBuffer(function (buffer: Buffer) {
				resolve(buffer);
			});
		});

		return pdfBuffer;
	}
}
