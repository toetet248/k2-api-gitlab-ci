import { Injectable, NestInterceptor, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { S3 } from 'aws-sdk';
import {
	existsSync,
	mkdirSync,
	readFileSync,
	unlink,
	writeFile,
	writeFileSync,
} from 'fs';
import moment from 'moment';
import * as AWS from 'aws-sdk';
import sharp from 'sharp';
import { ceil } from 'lodash';
import JSZip from 'jszip';

@Injectable()
export class FileService {
	private driver: string;
	private s3Bucket: string;
	private s3Region: string;
	private s3: S3;
	private path: string;
	private resizeWidthHeight: any;
	fileInterceptor: NestInterceptor;

	constructor(configService: ConfigService) {
		const storage = configService.get('storage');
		this.driver = storage.driver;
		this.s3Region = storage.s3Region;
		this.resizeWidthHeight = storage.resizeWidthHeight;

		if (this.driver === 's3') {
			this.s3Bucket = storage.s3Bucket;
			// this.s3Region = storage.s3Bucket;
			this.s3 = new S3();
		} else {
			this.path = storage.path;
		}
	}

	async getFile(res: any, file: string) {
		if (this.s3) {
			const client = {
				Bucket: this.s3Bucket,
				Key: file,
			};

			const checkObject = await this.s3
				.headObject(client)
				.promise()
				.then(
					() => true,
					(err) => false,
				);

			if (!checkObject) throw new NotFoundException();

			const stream = this.s3.getObject(client);
			return stream.createReadStream().pipe(res);
		} else {
			const root = `.${file.substring(0, file.lastIndexOf('/') + 1)}`;
			const fileName = file.substring(
				file.lastIndexOf('/') + 1,
				file.length,
			);

			if (existsSync(`.${file}`)) return res.sendFile(fileName, { root });
			else throw new NotFoundException();
		}
	}

	private upload(
		centralOrTenant: string,
		extension: string,
		base64: string,
		path: string,
		sku: string,
	): string {
		if (!base64 || !base64.length) return null;

		const location = `${this.path ?? ''}${centralOrTenant}${path}`;
		const file = `${location}/${this.generateFileName(sku, extension)}`;

		if (this.s3) {
			const s3 = new AWS.S3({
				params: {
					Bucket: this.s3Bucket,
				},
			});
			const buf = Buffer.from(base64, 'base64');
			const data = {
				Key: file,
				Body: buf,
				ContentType: extension,
				Bucket: this.s3Bucket,
			};

			s3.putObject(data, function (err, data) {});
		} else {
			mkdirSync(`.${location}`, { recursive: true });

			writeFile(`.${file}`, base64, 'base64', function (err) {});
		}

		return file;
	}

	uploadFile(
		base64: string,
		path: string,
		sku: string,
		tenantName?: string,
	): string {
		// console.log(base64.length);
		if (!base64 || !base64.length) return null;

		const centralOrTenant = tenantName
			? `/tenant/${tenantName}`
			: '/central';

		const [type, b64] = base64.split(',');

		const extension = type.substring(
			type.indexOf(':') + 1,
			type.lastIndexOf(';'),
		);

		return this.upload(centralOrTenant, extension, b64, path, sku);
	}

	uploadFiles(
		base64: string[],
		path: string,
		sku: string,
		tenantName?: string,
	): string[] {
		if (!base64 || !base64.length) return null;

		const centralOrTenant = tenantName
			? `/tenant/${tenantName}`
			: '/central';

		return base64.map((b64String) => {
			const [type, b64] = b64String.split(',');

			const extension = type.substring(
				type.indexOf(':') + 1,
				type.lastIndexOf(';'),
			);
			return this.upload(centralOrTenant, extension, b64, path, sku);
		});
	}

	generateFileName(
		sku: string,
		extension: string, // application/pdf \\ image/png
		type?: string,
		custom?: string,
	) {
		const date = moment().format('Y-MM-DD_H-mm-ss');
		const name = `${sku ?? 'F'}_${type ?? 'file'}_${custom ?? ''}_${date}`;
		const randomName = Array(8)
			.fill(null)
			.map(() => Math.round(Math.random() * 16).toString(16))
			.join('');

		const [file, ext] = extension.split('/');
		return `${name}_${randomName}.${ext}`;
	}

	uploadedFile(newFile: any, oldFile?: string): string {
		if (newFile) {
			return this.s3 ? newFile.key : '/' + newFile.path;
		}

		return oldFile;
	}

	delete(file: string) {
		if (this.s3)
			this.s3.deleteObject(
				{ Bucket: this.s3Bucket, Key: file },
				(err: any, data: any) => {
					console.error(err, '...s3 file delete error');
				},
			);

		if (existsSync(`.${file}`)) unlink(`.${file}`, () => {});
	}

	async resizeImage(imageFile: any) {
		const image = sharp(imageFile.path);
		const metaData = await image.metadata();
		let width = metaData.width;
		let height = metaData.height;

		let returnImagePath;

		if (this.driver === 's3') {
			let buffer;
			let S3CompressedName;
			if (
				width > this.resizeWidthHeight ||
				height > this.resizeWidthHeight
			) {
				const { resizeWidth, resizeHeight } =
					this.calculateResizeWidthHeight(width, height);

				S3CompressedName =
					imageFile.destination.replace('./storage', '') +
					'/CP_' +
					imageFile.filename;
				buffer = await image
					.resize(resizeWidth, resizeHeight)
					.toBuffer();
			} else {
				S3CompressedName =
					imageFile.destination.replace('./storage', '') +
					'/' +
					imageFile.filename;
				buffer = await image.toBuffer();
			}
			await this.s3
				.upload({
					Bucket: this.s3Bucket,
					Key: S3CompressedName,
					Body: buffer,
				})
				.promise();
			returnImagePath = S3CompressedName;
		} else {
			if (
				width > this.resizeWidthHeight ||
				height > this.resizeWidthHeight
			) {
				let localCompressedName =
					imageFile.destination.replace('./', '') +
					'/CP_' +
					imageFile.filename;

				const { resizeWidth, resizeHeight } =
					this.calculateResizeWidthHeight(width, height);

				try {
					await image
						.resize({
							width: resizeWidth,
							height: resizeHeight,
						})
						.toFile(localCompressedName);
				} catch (err) {
					console.log(err);
				}
				returnImagePath = '/' + localCompressedName;
			} else {
				return '/' + imageFile.path;
			}
		}

		if (existsSync(imageFile.path)) unlink('./' + imageFile.path, () => {});

		return returnImagePath;
	}

	calculateResizeWidthHeight = (width: any, height: any) => {
		let resizeWidth = width;
		let resizeHeight = height;
		if (width > this.resizeWidthHeight && width > height) {
			resizeWidth = this.resizeWidthHeight;
			resizeHeight = ceil((this.resizeWidthHeight / width) * height);
		} else if (height > this.resizeWidthHeight && height > width) {
			resizeWidth = ceil((this.resizeWidthHeight / height) * width);
			resizeHeight = this.resizeWidthHeight;
		} else if (
			width > this.resizeWidthHeight &&
			height > this.resizeWidthHeight
		) {
			resizeWidth = this.resizeWidthHeight;
			resizeHeight = this.resizeWidthHeight;
		}
		return { resizeWidth, resizeHeight };
	};

	async uploadZip(jszip: JSZip, path: string, name: string) {
		const s3 = this.s3;
		const s3Bucket = this.s3Bucket;
		return await jszip
			.generateAsync({ type: 'nodebuffer' })
			.then(async function (content) {
				if (s3) {
					const client = new AWS.S3({
						params: {
							Bucket: s3Bucket,
						},
					});

					const data = {
						Key: `${path}${name}`,
						Body: content,
						ContentType: 'application/zip',
						Bucket: s3Bucket,
					};

					client.putObject(data, function (err, data) {});

					return `${path}${name}`;
				} else {
					mkdirSync(`storage${path}`, { recursive: true });
					writeFileSync(`storage${path}${name}`, content);

					return `/storage${path}${name}`;
				}
			});
	}

	async getBuffer(filePath: string): Promise<any> {
		if (this.s3) {
			const client = {
				Bucket: this.s3Bucket,
				Key: filePath,
			};

			const checkObject = await this.s3
				.headObject(client)
				.promise()
				.then(
					() => true,
					(err) => false,
				);

			if (checkObject) {
				const object = await this.s3.getObject(client).promise();
				return object.Body.toString('utf-8');
			}
		} else {
			if (existsSync('.' + filePath)) {
				const buffer = readFileSync('.' + filePath);
				return buffer;
			}
		}

		return null;
	}
}
