import { Injectable, PipeTransform } from '@nestjs/common';
import { omit } from 'lodash';
import { REQUEST_CONTEXT } from '../interceptor/inject-request.interceptor';

@Injectable()
export class StripRequestContextPipe implements PipeTransform {
	transform(value: any) {
		// if (value)
		if (value[REQUEST_CONTEXT]) value = omit(value, REQUEST_CONTEXT);

		return value;
	}
}
