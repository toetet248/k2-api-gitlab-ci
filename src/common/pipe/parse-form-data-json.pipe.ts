import { PipeTransform, ArgumentMetadata } from '@nestjs/common';

type TParseFormDataJsonOptions = {
	except?: string[];
};

export class AddTenantPipe implements PipeTransform {
	constructor(private options?: TParseFormDataJsonOptions) {}

	transform(request: any, _metadata: ArgumentMetadata) {
		return {
			...request.body,
			tenantName: request.headers['x-tenant-name'],
		};
	}
}
