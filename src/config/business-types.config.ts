export default () => {
	return {
		businessTypes: [
			{
				id: 1,
				name: '土木一式',
			},
			{
				id: 2,
				name: '建築一式',
			},
			{
				id: 3,
				name: '大工工事',
			},
			{
				id: 4,
				name: '左官工事',
			},
			{
				id: 5,
				name: 'とび・土工',
			},
			{
				id: 6,
				name: '石工事',
			},
			{
				id: 7,
				name: '根工事',
			},
			{
				id: 8,
				name: '電気工事',
			},
			{
				id: 9,
				name: '土管工事',
			},
			{
				id: 10,
				name: 'タイル・れんが・ブロック工事',
			},
			{
				id: 11,
				name: '鋼構造物工事',
			},
			{
				id: 12,
				name: '鉄筋工事',
			},
			{
				id: 13,
				name: '舗装工事',
			},
			{
				id: 14,
				name: 'しゅんせつ工事',
			},
			{
				id: 15,
				name: '板金工事',
			},
			{
				id: 16,
				name: '土ガラス工事',
			},
			{
				id: 17,
				name: '塗装工事',
			},
			{
				id: 18,
				name: '防水工事',
			},
			{
				id: 19,
				name: '内装仕上工事',
			},
			{
				id: 20,
				name: '機械器具設置工事',
			},
			{
				id: 21,
				name: '熱絶縁工事',
			},
			{
				id: 22,
				name: '電気通信工事',
			},
			{
				id: 23,
				name: '土造園工事',
			},
			{
				id: 24,
				name: 'さく井工事',
			},
			{
				id: 25,
				name: '建具工事',
			},
			{
				id: 26,
				name: '水道施設工事',
			},
			{
				id: 27,
				name: '消防施設工事',
			},
			{
				id: 28,
				name: '清掃施設工事',
			},
			{
				id: 29,
				name: '解体工事',
			},
			{
				id: 30,
				name: 'コンクリート工事',
			},
			{
				id: 31,
				name: 'その他',
			},
		],
	};
};
