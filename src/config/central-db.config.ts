import * as configuration from './config';

export default configuration.default().database.central;
