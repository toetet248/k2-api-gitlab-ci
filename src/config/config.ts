// import keypair from 'keypair';
import { join } from 'path';
import fs from 'fs';
import { ExtractJwt } from 'passport-jwt';
var env = require('dotenv').config().parsed;

export default () => {
	const jwtPrivateKey = fs.readFileSync('./jwt_private_key.pem', 'utf8');

	const jwtAlgorithm = 'RS256';

	return {
		app: {
			env: env.APP_ENV,
			port: env.APP_PORT,
			url: env.APP_URL,
			frontendUrl: env.FRONTEND_URL,
			timezone: env.APP_TIMEZONE,
		},
		database: {
			tenantDbPrefix: env.TENANT_DB_PREFIX,
			central: {
				type: 'mysql',
				host: env.DB_HOST,
				port: env.DB_PORT,
				username: env.DB_USERNAME,
				password: env.DB_PASSWORD,
				database: env.CENTRAL_DB_NAME,
				extra: {
					charset: 'utf8mb4_unicode_ci',
				},
				entities: ['dist/modules/**/**/*.entity{ .ts,.js}'],
				synchronize: false,
				migrations: [
					join(
						__dirname,
						'./../../dist/database/migrations/central/*{.ts,.js}',
					),
				],
				cli: {
					migrationsDir: 'migration',
				},
				migrationsRun: false,
			},
			tenant: {
				type: 'mysql',
				host: env.DB_HOST,
				port: env.DB_PORT,
				username: env.DB_USERNAME,
				password: env.DB_PASSWORD,
				extra: {
					charset: 'utf8mb4_unicode_ci',
				},
				entities: ['dist/modules/tenancy/**/*.entity{.ts,.js}'],
				synchronize: false,
				migrations: [
					join(
						__dirname,
						'./../../dist/database/migrations/tenant/*{.ts,.js}',
					),
				],
				cli: {
					migrationsDir: 'migration',
				},
				migrationsRun: false,
			},
		},
		jwt: {
			pcRefreshTokenExpire: env.PC_REFRESH_TOKEN_EXPIRE,
			mobileRefreshTokenExpire: env.MOBILE_REFRESH_TOKEN_EXPIRE,
			accessTokenExpire: env.ACCESS_TOKEN_EXPIRE,
			encode: {
				privateKey: jwtPrivateKey,
				publicKey: fs.readFileSync('./jwt_public_key.pem', 'utf8'),
				signOptions: {
					expiresIn: env.ACCESS_TOKEN_EXPIRE.replace('-', ''),
					algorithm: jwtAlgorithm,
				},
			},
			decode: {
				jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
				ignoreExpiration: false,
				secretOrKey: jwtPrivateKey,
				algorithms: [jwtAlgorithm],
				passReqToCallback: true,
			},
		},
		storage: {
			driver: env.STORAGE_DRIVER,
			path: env.STORAGE_PATH,
			resizeWidthHeight: parseInt(env.STORAGE_RESIZE_WIDTH_HEIGHT),
			s3Region: env.S3_REGION,
			s3Bucket: env.S3_BUCKET,
		},
		cache: {
			driver: '',
			ttl: '10s',
		},
		mail: {
			host: env.MAIL_HOST,
			port: env.MAIL_PORT,
			user: env.MAIL_USER,
			password: env.MAIL_PASSWORD,
		},
		sms: {
			key: env.VONAGE_API_KEY,
			secret: env.VONAGE_API_SECRET,
		},
		gmo: {
			siteId: env.GMO_SITEID,
			sitePass: env.GMO_SITEPASS,
			baseUrl: env.GMO_BASEURL,
			shopId: env.GMO_SHOPID,
			shopPass: env.GMO_SHOPPASS,
		},
	};
};
