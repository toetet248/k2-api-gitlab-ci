
# Kenkoukanri 2 API

## Description

Using [Nest](https://github.com/nestjs/nest) framework TypeScript.

## Installation

```bash
$ npm install
```

## Setup

```bash
# copy .env.example to .env
$ cp .env.example .env
```

#### Setup Database

change .env value for database

```bash
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=password

# central database name
CENTRAL_DB_NAME=k2_central

# tenant database prefix
TENANT_DB_PREFIX=k2_tenant_
```

#### Setup Storage

change .env value for storage

```bash
# storage driver - local or s3
STORAGE_DRIVER=local

# storage path
STORAGE_PATH=/storage

# s3 region and bucket
S3_REGION=us-east-1
S3_BUCKET=k2-bucket-test
```

#### Setup SMTP

change .env value for smtp

```bash
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=465
MAIL_USER=toetet@o-technique-myanmar.com
MAIL_PASSWORD=sdiusnoxcvwoqosb
```

#### Setup GMO

change .env value for GMO

```bash
GMO_SITEID=tsite00040907
GMO_SITEPASS=7rdaur1q
GMO_BASEURL=https://pt01.mul-pay.jp/payment/
GMO_SHOPID=tshop00047046
GMO_SHOPPASS=s3frt4bq
```

#### Setup JWT

change .env value for JWT

```bash
ACCESS_TOKEN_EXPIRE=2-h
PC_REFRESH_TOKEN_EXPIRE=6-h
MOBILE_REFRESH_TOKEN_EXPIRE=1-m
```

## Build the app

<b>NOTES:</b> Always build the app, after changing migration files

```bash
# development
$ npm run build
```

## Migration, Fresh and Seed

### Central CMD

For initial setup, need to run
<b>npm run central:migration:run</b> cmd and
<b>npm run central:db:seed</b> cmd.

```bash
# crete migration file for central database
npm run central:migration:create

# run migration file for central database
npm run central:migration:run

# revert migration for central database
npm run central:migration:revert

# seed data for central database
npm run central:db:seed

# fresh central database
npm run central:db:fresh
```

### Tenant CMD

no need to run these cmd for initial setup

```bash
# crete migration file for tenant database
npm run tenant:migration:create

# run migration file for tenant database
npm run tenant:migration:run

# revert migration for tenant database
npm run tenant:migration:revert

# seed data for tenant database
npm run tenant:db:seed

# fresh tenant database
npm run tenant:db:fresh
```

#### Seeder

```bash
# seeder files path for central database
./src/database/seeds/central/

# seeder files path for tenant database
./src/database/seeds/tenant/
```

#### PDF Service

```bash
# copy fonts folder into node_modules/pdfmake/
$ cp fonts/ -r node_modules/pdfmake/
 
# move into above directory
$ cd node_modules/pdfmake/

# build fonts
$ node build-vfs.js  "./fonts/ipag/"
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## API Documentation

You can find <b>"K2 API"</b> Collection in <b>"O-Technique"</b> Workspace from POSTMAN.

#### Initial Setup

In auth folder,

1. Register User Account
2. Login User and Set Token

NOTES: set token again if you get 401 Authorization error while using tenants requests

In tenants folder,

3. Create Tenant
4. Change <b>"x-tenant-name"</b> header value to request from specific teneant.
